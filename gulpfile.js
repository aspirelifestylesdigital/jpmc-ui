'use strict';

var gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    babel = require('gulp-babel'),
    cleanCss = require('gulp-clean-css'),
    concat = require('gulp-concat'),
    connect = require('gulp-connect'),
    del = require('del'),
    pug = require('gulp-pug'),
    imageop = require('gulp-imagemin'),
    insert = require('gulp-insert'),
    log = require('gulp-util').log,
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    watchLogger = event => log(`File ${event.path.split('/').pop()} was ${event.type}`);

//--GULP
gulp.task('default', ['pug', 'sass', 'fonts', 'images', 'js', 'connect', 'watch']);

gulp.task('prod', ['build', 'connectProd']);
gulp.task('build', ['clean'], () => {
  gulp.start('pugProd');
  gulp.start('sassProd');
  gulp.start('fontsProd');
  gulp.start('imagesProd');
  gulp.start('jsProd');
});

//--SERVER
gulp.task('connect', () => {
  connect.server ({
    root: 'dev_dist',
    livereload: true,
    port: 2019
  })
});

gulp.task('connectProd', () => {
  connect.server ({
    root: 'dist',
    port: 3019
  })
});

//--CLEAN
gulp.task('clean', () => del('dist/**'));

//--PUG
gulp.task('pug', () =>
  gulp.src('src/pug/*.pug')
    .pipe(pug({
      basedir: 'src/pug',
      pretty: true
    }))
    .pipe(gulp.dest('dev_dist'))
    .pipe(connect.reload())
);

gulp.task('pugProd', () =>
  gulp.src('src/pug/*.pug')
    .pipe(pug({
      basedir: 'src/pug',
      pretty: true
    }))
    .pipe(gulp.dest('dist'))
);

//--SASS
gulp.task('sass', () =>
  gulp.src('src/sass/*.scss')
    .pipe(sourcemaps.init())
    .pipe(concat('style.min.css'))
    .pipe(sass({
      errLogToConsole: true,
      outputStyle: 'expanded'
    }).on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 5 versions'],
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dev_dist/css'))
    .pipe(connect.reload())
);

gulp.task('sassProd', () =>
  gulp.src('src/sass/*.scss')
    .pipe(concat('style.min.css'))
    .pipe(sass({
      errLogToConsole: true,
      outputStyle: 'expanded'
    }).on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 5 versions'],
    }))
    .pipe(cleanCss())
    .pipe(gulp.dest('dist/css'))
);

//--FONTS
gulp.task('fonts', () => 
  gulp.src('src/fonts/**')
    .pipe(gulp.dest('dev_dist/fonts'))
    .pipe(connect.reload())
);

gulp.task('fontsProd', () => 
  gulp.src('src/fonts/**')
    .pipe(gulp.dest('dist/fonts'))
);

//--IMAGES
gulp.task('images', () =>
  gulp.src('src/img/**')
    .pipe(gulp.dest('dev_dist/images'))
    .pipe(connect.reload())
);

gulp.task('imagesProd', () =>
  gulp.src('src/img/**')
    .pipe(imageop({
      optimizationLevel: 5,
      progressive: true,
      interlaced: true
    }))
    .pipe(gulp.dest('dist/images'))
);

//--JS
gulp.task('js', () =>
  gulp.src('src/js/**/*.js')
    .pipe(concat('app.min.js'))
    .pipe(babel({ presets: ['es2015'] }))
    .pipe(gulp.dest('dev_dist/js'))
    .pipe(connect.reload())
);

gulp.task('jsProd', () =>
  gulp.src('src/js/**/*.js')
    .pipe(concat('app.min.js'))
    .pipe(babel({ presets: ['es2015'] }))
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'))
);

//--WATCH
gulp.task('watch', () => {
  gulp.watch('src/pug/**/*.pug', ['pug']).on('change', watchLogger);
  gulp.watch('src/sass/**/*.scss', ['sass']).on('change', watchLogger);
  gulp.watch('src/fonts/**/*', ['fonts']).on('change', watchLogger);
  gulp.watch('src/img/**/*', ['images']).on('change', watchLogger);
  gulp.watch('src/js/**/*.js', ['js']).on('change', watchLogger);
});
