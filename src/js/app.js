//--Choose city dd
$('body').on('click', '[data-dd-toggle]', (e) => {
  e.preventDefault();
  
  let $target = $(e.currentTarget),
      ddId = $target.attr('data-dd-toggle'),
      $dd = $('[data-dd=' + ddId + ']');
  
  if(!$dd.length) return false;

  $target.toggleClass('_active');
  $dd.toggleClass('_active');
});

//--Open modal
$('body').on('click', '[data-modal-open]', (e) => {
  e.preventDefault();

  let $target = $(e.currentTarget),
      mId = $target.attr('data-modal-open'),
      $md = $('[data-modal=' + mId + ']');

  if(!$md.length) return false;

  $($md[0]).addClass('_open');
  $('body').addClass('_mdopen');

  var resizeIt = () => {
    if(window.innerWidth < 768) {
      $($md[0]).find('.modal-wrap').css({height: window.innerHeight});
    } else {
      $($md[0]).find('.modal-wrap').css({height: ''});
    }
  }

  resizeIt();

  $(window).on('resize.modalWindowResize', resizeIt);
});

//--Close modal
$('body').on('click', '[data-close-modal]', (e) => {
  e.preventDefault();

  $(window).off('resize.modalWindowResize');
  $('[data-modal]._open').removeClass('_open');
  $('body').removeClass('_mdopen');
});

function selectCity(cityId, cityName) {
  if(!(cityId && cityName)) return false;
  
  //Place selected city name into header
  $('[data-head-dest]').html(cityName);

  //Close header and hero destination dropdowns
  $('[data-dd-toggle="header-choose-city"]').removeClass('_active');
  $('[data-dd="header-choose-city"]').removeClass('_active');
  $('[data-dd-toggle="hero-choose-city"]').removeClass('_active');
  $('[data-dd="hero-choose-city"]').removeClass('_active');

  //Update selected city in header and hero destination dropdowns
  $('[data-dest-city]').removeClass('_selected');
  $('[data-dest-city="' + cityId + '"]').addClass('_selected');

  //Update hero button text when city is selected
  $('[data-dd-toggle="hero-choose-city"] .__default_text').html('CHANGE');
  $('[data-dd-toggle="hero-choose-city"] .__active_text').html('CHANGE LATER');

  //Add city name into hero section text
  $('.hero-body-choose').addClass('_selected');
  $('[data-hero-city]').html(cityName);

  //Add city name into benefits section text and categories buttons
  $('[data-benefits-city]').html(' IN ' + cityName.toUpperCase());
  $('[data-categorybtn-city]').html(' IN ' + cityName.toUpperCase());

  //Hide Choose destination section
  $('.destination').addClass('_hidden');
}

//--Choose a city in header or hero dropdown
$('body').on('click', '[data-dest-city]', (e) => {
  e.preventDefault();
  
  let $target = $(e.currentTarget),
      cityName = $target.text(),
      cityId = $target.attr('data-dest-city');

  selectCity(cityId, cityName);
});

//--Hero dd search submit button
$('body').on('click', '[data-hero-search-submit]', (e) => {
  e.preventDefault();

  $('[data-dd-toggle="choose-city"]').removeClass('_active');
  $('[data-dd="choose-city"]').removeClass('_active');
});

//--Choose destination click
$('body').on('click', '[data-moveto-hero]', (e) => {
  e.preventDefault();

  $('[data-dd-toggle="hero-choose-city"]').trigger('click');
  $('html, body').animate({
		scrollTop: $('[data-dd-toggle="hero-choose-city"]').offset().top - 50,
	}, 250);
});

//--Reservation dropdowns
$('body').on('click', '.reservation-ddhead', (e) => {
  e.preventDefault();

  $(e.currentTarget).toggleClass('_active');

  if(window.innerWidth < 768) {
    $('html, body').animate({
      scrollTop: $(e.currentTarget).offset().top - 16,
    }, 100);
  }
});

$('body').on('click', '.reservation-ddbody .__item', (e) => {
  e.preventDefault();

  let $curItem = $(e.currentTarget),
      $parUl = $curItem.parent(),
      $initEl = $curItem.closest('.reservation-dd'),
      $head = $initEl.find('.reservation-ddhead'),
      $headSpan = $head.find('span');

  $parUl.find('.__item').removeClass('_active');
  $curItem.addClass('_active');
  $headSpan.html($curItem.text());
  $head.removeClass('_active');
});

//--Reservation date picker
if($('[data-datepicker]').length) $('[data-datepicker]').datepicker({
  dateFormat: 'D, M d, yy',
  minDate: new Date(),
  onSelect: (date, instance) => {
    $(instance.input).find('span').html(date);
    $(instance.input).find('.reservation-ddhead').removeClass('_active');
  }
});
