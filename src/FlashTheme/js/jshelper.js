﻿var jshelper = function () {
    function isNil(obj) {
        return (
            obj == undefined || obj == null || obj.toString().length == 0 ||
            obj === undefined || obj === null || obj.toString().length === 0
        );
    }

    function isTrue(objVal) {
        if (!isNil(objVal)) {
            var strVal = objVal.toString().toLowerCase();
            return strVal == 'true' || strVal === 'true' || strVal == '1' || strVal === '1'
        }
        return false;
    }

    function isNilArr(objArray) {
        return (
            objArray == undefined || objArray == null || objArray.length == 0 ||
            objArray === undefined || objArray === null || objArray.length === 0
        );
    }

    function isFunc(objFunction) {
        return
        (objFunction != undefined && objFunction != null && $.isFunction(objFunction)) ||
        (objFunction !== undefined && objFunction !== null && $.isFunction(objFunction));
    }

    function isEquals(obj1, obj2) {
        if (isNil(obj1) && isNil(obj2)) return true;
        if (!isNil(obj1) && !isNil(obj2))
            return obj1.toString() == obj2.toString() || obj1.toString() === obj2.toString();
        return false;
    }

    function getFullYear(last2DigitOfYear) {
        var first2DigitOfYear = new Date().getFullYear().toString().substr(0, 2);
        return first2DigitOfYear + last2DigitOfYear;
    }

    function bindChange(element, onchangeCallback) {
        if (element == undefined || element == null || element.length == 0) return;
        if (onchangeCallback == undefined || onchangeCallback == null || !$.isFunction(onchangeCallback)) return;

        element.removeAttr('event-element-isunbindchange');

        var oldVal = element.val();
        var intv = setInterval(function () {

            if (element.val().toString() != oldVal) {
                onchangeCallback(element.val());
                oldVal = element.val();
            }
            if (element.attr('event-element-isunbindchange') == '1') {
                clearInterval(intv);
                element.removeAttr('event-element-isunbindchange');
                return;
            }

        }, 500);
    }

    function unbindChange(element) {
        if (element == undefined || element == null || element.length == 0) return;
        element.attr('event-element-isunbindchange', '1');
    }

    function numPad(strNum, max) {
        strNum = strNum.toString();
        return strNum.length < max ? numPad('0' + strNum, max) : strNum;
    }

    function getDaysInMonth(month, year) {
        var nowDate = new Date();
        if (month == undefined || month == null || month.toString().length == 0 || month.toString() == '-1')
            month = nowDate.getMonth();
        if (year == undefined || year == null || year.toString().length == 0 || year.toString() == '-1')
            year = nowDate.getFullYear();

        var date = new Date(year, month, 1);
        var days = [];
        while (date.getMonth() === month) {
            days.push(date.getDate());
            date.setDate(date.getDate() + 1);
        }
        return days;
    }

    function parseArrayToString(arr, separate) {
        if (isNilArr(arr)) return '';
        var ret = '';
        $.each(arr, function (i, val) {
            ret += val + separate;
        });
        ret = ret.substring(0, ret.length - separate.length);
        return ret;
    }

    function readAvararUrl(input, imgSelector) {
        //(/^[A-Za-z0-9-_ ]+(.jpg|.jpeg|.png|.gif)$/).test('0-adaa1341313zcfa3wjukewfwjow234289374234_s.gif')
        if (input.files && input.files[0]) {
            var regx = /^[A-Za-z0-9-_ ]+(.jpg|.jpeg|.png|.gif)$/;
            if (regx.test(input.files[0].name)) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $("#" + imgSelector).attr('src', e.target.result).width(140).height(140);
                };
                reader.readAsDataURL(input.files[0]);
                $('#' + imgSelector).parent().show();
            }
            else {
                alert('Image file name must be in range "a" to "z" include uppercase, "0" to "9", underscore, minus and space, and only accept extensions as (.jpg|.jpeg|.png|.gif)');
                $(input).val('');
                $("#" + imgSelector).attr('src', '');
            }
        }
    }

    function loadImage(fileUploadObj, imgSelectorID, validMaxSizeInMB, width, height) {
        // (/^[A-Za-z0-9-_ ]+(.jpg|.jpeg|.png|.gif)$/).test('0-adaa1341313zcfa3wjukewfwjow234289374234_s.gif')
        if (fileUploadObj.files && fileUploadObj.files[0]) {
            
            var fileObj = fileUploadObj.files[0];

            // Validate file name and file type...
            var regx = /^[A-Za-z0-9-_ ]+(.jpg|.jpeg|.png|.gif|.Jpg|.Jpeg|.Png|.Gif|.JPG|.JPEG|.PNG|.GIF)$/;
            if (!regx.test(fileObj.name)) {
                alert('The image file name must be in range "a" to "z" include uppercase, "0" to "9", underscore, minus and space, and only accept extensions as (.jpg|.jpeg|.png|.gif)');
                $(fileUploadObj).val('');
                return;
            }

            // Validate file size...            
            if (validMaxSizeInMB != undefined && validMaxSizeInMB != null) {
                // 1 MB = 1024 KB = 1048576 B
                if (fileObj.size > parseInt(validMaxSizeInMB) * 1048576) {
                    alert('The image file size must be less than or equal to "' + validMaxSizeInMB + ' MB"');
                    $(fileUploadObj).val('');
                    return;
                }
            }

            // All were passed...
            var reader = new FileReader();
            reader.onload = function (e) {
                $("#" + imgSelectorID).attr('src', e.target.result).width(width).height(height);
            };
            reader.readAsDataURL(fileUploadObj.files[0]);
            $('#' + imgSelectorID).parent().show();
        }
    }

    // Public to use...
    return {
        isNil: isNil,
        isTrue: isTrue,
        isNilArr: isNilArr,
        isFunc: isFunc,
        isEquals: isEquals,
        getFullYear: getFullYear,
        bindChange: bindChange,
        unbindChange: unbindChange,
        numPad: numPad,
        getDaysInMonth: getDaysInMonth,
        parseArrayToString: parseArrayToString,
        readAvararUrl: readAvararUrl,
        loadImage: loadImage
    };
}();