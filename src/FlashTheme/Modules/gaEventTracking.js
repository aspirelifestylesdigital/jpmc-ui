
(function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

ga('create', gaID, 'auto');
ga('send', 'pageview');
ga('create', gaID, { 'siteSpeedSampleRate': siteSpeedSampleRate });
function handleOutboundLinkClicks(event, eventCategory, eventAction, eventLabel) {
    ga('send', event, eventCategory, eventAction, eventLabel);
}
//Header - Logo
$(".header .logo-header").on('click', function () {
    handleOutboundLinkClicks("event", "Header", "Click on logo", "");
});
// Header - Menu have Href
$(".header li a").on('click', function () {
    var href = $(this).attr('href');
    if (href != undefined && href !== "#" && href !== "javscript:void(0)") {
        var eventLabel = $(this).text();
        var parentUl = $(this).parents(".sub-list");
        if (parentUl.length > 0) {
            var label = parentUl.prev().children("span .menu-text").text();
            eventLabel = label + " > " + eventLabel;
        }
        handleOutboundLinkClicks("event", "Navigation", "Click on navigation item", eventLabel);
    }
});
//Form
$(".fr-booking").on("submit", function () {
    if ($(this).children("error").length < 0) {
        var eventLabel = "";
        if ($(this).find("input[name=MobileNumber]").val().length > -1) {
            eventLabel += "Phone";
        }
        if ($(this).find("input[name=Email]").val().length > -1) {
            if (eventLabel !== "")
            { eventLabel += ", "; }
            eventLabel += "Email";
        }
        var eventCategory = "Request: " + $(document).find("title").text();
        handleOutboundLinkClicks("event", eventCategory, "Submit request", eventLabel);
    }
});
//Title Module
$(".slide").on("click", function () {
    var eventLabel = $(this).children("a").attr('href');
	if(eventLabel) {
		handleOutboundLinkClicks("event", "Title", "Click on tile", eventLabel);	
	}
});
//Hero Module
$(".slides-intro a").on("click", function () {
    var eventLabel = $(this).attr('href');
    handleOutboundLinkClicks("event", "Hero", "Click on call to action", eventLabel);
});
//Left-Right-Navigation
$(".slides-intro>button").on("click", function () {
    var eventLabel = $(this).attr('aria-label');
    if (eventLabel === "" || eventLabel == undefined) {
        eventLabel = "Navigation";
    }
    handleOutboundLinkClicks("event", "Hero", "Navigate hero", eventLabel);
});
//Sticky concierge module popup (Open-Close)
$(".module-help .clicked").on("click", function () {
    var eventAction = "Close concierge module";

    if ($(this).hasClass("expand")) {
        eventAction = "Open concierge module";
    }
    handleOutboundLinkClicks("event", "Sticky concierge module", eventAction, "");
});
//Sticky concierge module popup (Send Request)
$(".module-help .clicked a").on("click", function () {
    var href = $(this).attr("href");
    var eventAction = "Start request";

    if (href.indexOf('tel') > -1) {
        eventAction = "Start call";
    }
    handleOutboundLinkClicks("event", "Sticky concierge module", eventAction, "");
});
//Footer
$("footer a").on("click", function () {
    var eventLabel = $(this).text();
    handleOutboundLinkClicks("event", "Footer", "Click on footer link", eventLabel);
});