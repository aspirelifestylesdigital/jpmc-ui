
var HotelSubCat = function () {
    
    function moduleFilter() {
        $('.list-checkbox li').on("click", function() {
            if($(this).hasClass('on')) {
                $(this).removeClass('on');
            } else {
                $(this).addClass('on');
            };
        });
        $(document).on("click",".icon-arrow", function() {
            if($(this).hasClass('toggle')) {
                $(this).removeClass('toggle');
                $(this).parent().children('.list-checkbox').slideDown('fast');
                $(this).parent().children(".btn-select-all").slideDown('fast');
            } else {
                $(this).addClass('toggle');
                $(this).parent().children('.list-checkbox').slideUp('fast');
                $(this).parent().children(".btn-select-all").slideUp('fast');
            };
        });
    }
    
    function travelCommonAnimation(){
        var btnChangeThumnnail = $(".block-article-desc .box-btn button");
        btnChangeThumnnail.on("click", function(){
            btnChangeThumnnail.removeClass("active");
            $(this).addClass("active");
        });
        
    }
	function createSlick(){
    	$('.slides-recommends').slick({
            dots: true,
            arrows: false,
            infinite: false,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            slidesPerRow : 1,
            rows:2,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        rows: 2,
                        slidesPerRow : 1
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        rows: 2,
                        slidesPerRow : 1
                    }
                }
            ]
        });
	}
	function showBoxFilter() {
        var boxFilter = $(".form-filter-top"), filterBtn = $(".wrap-btn-filter .btn-filter-top"), btnCloseFilter = $(".form-filter-title .icon-close");
        $(window).on("load resize", function () {
            var winHeight = $(window).height();
            boxFilter.css("height", winHeight);
        });
        filterBtn.on("click", function () {
            boxFilter.addClass("active");
        });
        btnCloseFilter.on("click", function () {
            boxFilter.removeClass("active");
        });
    }
    return {
        init: function () {
            App.init();
            moduleFilter();
        },
		appInit: App.init,
        uiInit: UI.init,
        initBoxFilter: showBoxFilter,
		createSlick: createSlick
    }
}();