
var Travel = function () {
    function initSlick() {
        $(document).ready(function(){
            $(window).on("resize load", function(){
                var winWidth = $(window).width();
                if(winWidth < 768){
                    $('.slides-recommends').slick("unslick");
                    $('.slides-recommends').slick({
                        dots: true,
                        arrows: false,
                        infinite: false,
                        speed: 300,
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        rows: 2
                    });
                } else if((winWidth >= 768) && (winWidth < 1024)){
                    $('.slides-recommends').slick("unslick");
                    $('.slides-recommends').slick({
                        dots: true,
                        arrows: false,
                        infinite: false,
                        speed: 300,
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        rows: 1,
                        slidesPerRow : 1
                    });
                } else{
                    $('.slides-recommends').slick("unslick");
                    $('.slides-recommends').slick({
                        dots: true,
                        arrows: false,
                        infinite: false,
                        speed: 300,
                        slidesToShow: 4,
                        slidesToScroll: 4,
                        rows: 1,
                        slidesPerRow : 1
                    });
                }
            });
			//Filter by Travel Package Type
			function filterByPackageType(value) {
				$(".travel_package").each(function () {
					var $this = $(this);
					if ($this.find(".packageType").attr("package-type")) {
						var packageType = $this.find(".packageType").attr('package-type');
						if (packageType === value) {
							if (!$this.hasClass('hidden'))
								$this.addClass('hidden');
						} else {
							$this.removeClass('hidden');
						}
					}
				})
			}
			//Generate PackageType button
			function addButtonClick() {
				$(".box-btn button").each(function () {
					$(this).click(function (e) {
						var $this = $(this);
						e.preventDefault();
						$(".box-btn button").removeClass('active');
						$this.addClass('active');
						filterByPackageType($this.text());
					});
				});
			}
        });
    }
    function moduleFilter() {
        $('.list-checkbox li').on("click", function() {
            if($(this).hasClass('on')) {
                $(this).removeClass('on');
            } else {
                $(this).addClass('on');
            }
        });
        $(document).on("click",".icon-arrow", function() {
            if($(this).hasClass('toggle')) {
                $(this).removeClass('toggle');
                $(this).parent().children('.list-checkbox').slideDown('fast');
            } else {
                $(this).addClass('toggle');
                $(this).parent().children('.list-checkbox').slideUp('fast');
            };
        });
    }

    return {
        init: function () {
            App.init();
            // initSlick();
            moduleFilter();
        }
    }
}();
