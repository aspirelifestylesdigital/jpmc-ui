﻿"use strict"; 
window.onload = function () {
    App.init();
    $('#flight-fr-booking').validation({
        selectCreditName: {
            name: 'ReservationName'
        },
        txtContactNumber: {
            name: 'MobileNumber'
        },
        txtEmail: {
            name: 'Email'
        }
    });
    $('#numberOfKids').focus();
    $('#roundTrip').focus();
    removeAllFields();
}

var maxAdultNumber = UI.FormSetting.FlightFormSetting.maxAdultNumber;
var limitKidsNumber = UI.FormSetting.FlightFormSetting.maxKidNumber;
var minAdultNumber = UI.FormSetting.FlightFormSetting.minAdultNumber;

function regenerateSummaryFlightTitle() {
    var flightList = $('.info-book').find('.number-flight').find('.block-infor').find('.flight-title');
    var summaryFlight = $('.info-book').find('.item.destination-infor');
    if (summaryFlight && flightList) {
        var placeholderFlightNo = summaryFlight.attr('label-flight-no');
        for (let i = 0; i < flightList.length; i++) {
            var item = flightList[i];
            var order = i + 1;
            $(item).text(placeholderFlightNo + order);
        }
    }
}

function addFlight() {
    var flight = $('.flights');
    var countOfFlight = $('#multi-city .flight-infor').length;
    //var order = countOfFlight + 1;
    //get the last one added, get the order, then 
    var maxOrderNumber = $('.row.flight-infor:last-child').attr('data-order');
    var order = parseInt(maxOrderNumber) + 1;
    //place holder    
    var placeholderRequiredStar = $('#multi-city').attr('placeholder-required-star');
    var placeholderDepart = $('#multi-city').attr('placeholder-depart');
    var placeholderDepartP = $('#multi-city').attr('placeholder-depart-p');
    var placeholderDestination = $('#multi-city').attr('placeholder-destination');
    var placeholderDestinationP = $('#multi-city').attr('placeholder-destination-p');
    var placeholderDepartDate = $('#multi-city').attr('placeholder-depart-date');
    var placeholderDepartDateP = $('#multi-city').attr('placeholder-depart-date-p');
    if (countOfFlight > 5) return;
    flight.append('<div class="row flight-infor" data-order="' + order + '">'
        + '<div class="col-md-5 col-sm-12 col-xs-12 depart-width">'
        + '<div class="form-group">'
        + '<label for="depart">' + placeholderDepart + ' <span class="txt-red">' + placeholderRequiredStar + '</span></label>'
        + '<div class="box-group">'
        + '<label class="input-group-before"> <i class="fas fa-plane"></i></label>'
        + '<input type="text" class="form-control" name="MultiCityViewModel.Depart' + order + '" id="txtDepartMulti' + order + '" value="" '
        + ' placeholder="' + placeholderDepartP + '" required>'
        + '<!-- <label id="-error" class="error lt-error" for="Location" style="display: none;">This field is required.</label> -->'
        + '</div>'
        + '</div>'
        + '</div>'
        + '<div class="col-md-5 col-sm-12 col-xs-12 destination-width">'
        + '<div class="form-group">'
        + '<label for="destination">' + placeholderDestination + ' <span class="txt-red">' + placeholderRequiredStar + '</span></label>'
        + '<div class="box-group">'
        + '<label class="input-group-before"><i class="fas fa-plane"></i></label>'
        + '<input type="text" class="form-control" name="MultiCityViewModel.Destination' + order + '" id="txtDestinationMulti' + order + '" value="" '
        + ' placeholder="' + placeholderDestinationP + '" required>'
        + '</div>'
        + '</div>'
        + '</div>'
        + '<div class="col-md-2 col-sm-12 col-xs-12 date-width">'
        + '<div class="departure-date">'
        + '<div class="form-group">'
        + '<label>' + placeholderDepartDate + ' <span class="txt-red">' + placeholderRequiredStar + '</span></label>'
        + '<div class="box-group">'
        + '<div class="input-group-before">'
        + '<span class="sprites icon-calendar"></span>'
        + '</div><input class="form-control departureDateMultiCity" name = "MultiCityViewModel.DepartureDate' + order + '" type="text" id="txtDepartDateMulti' + order + '" readonly="true" '
        + 'placeholder="' + placeholderDepartDateP + '" aria-label="Departure Date" '
        + 'aria-labelledby="" required>'
        + '</div>'
        + '</div>'
        + '</div>'
        + '</div>'
        + '<div class="remove-flight">'
        + '<label><span><i class="fas fa-times-circle"></i></span></label>'
        + '</div>'
        + '</div>');

    $('#flight-fr-booking').validation({});

    //implement summary
    $('input[id^="txtDepartMulti"]').on('change', function () {
        updateFlightData(this, 'from', 'txtDepartMulti');
    });

    $('input[id^="txtDestinationMulti"]').on('change', function () {
        updateFlightData(this, 'to', 'txtDestinationMulti');
    });
    //add summary data
    var summaryFlight = $('.info-book').find('.item.destination-infor');    
    //placeholder
    var placeholderFlightNo = summaryFlight.attr('label-flight-no');
    var placeholderFrom = summaryFlight.attr('label-from');
    var placeholderTo = summaryFlight.attr('label-to');
    
    summaryFlight.append('<div class="number-flight extra" style="position: relative" data-order="' + order + '">' +
        '<!-- <i class="sprites icon-maps"></i>-->' +
        '<div class="block-infor flight-no-' + order + '">' +
        '<label class="flight-title">' + placeholderFlightNo + order + '</label>' +
        '<div class="depart-from" style="display: none">' +
        '<label class="depart-label">' + placeholderFrom + ':</label>' +
        '<p></p>' +
        '</div>' +
        '<div class="depart-to" style="display: none">' +
        '<label class="depart-label">' + placeholderTo + ':</label>' +
        '<p></p>' +
        '</div>' +
        '</div>' +
        '</div>');
    regenerateSummaryFlightTitle();

}

function removeAllFields() {    
    //$("body").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");        
    
    $.ajax({
        url: "",
        async: true,
        beforeSend: function () {
            $('form :input[id^=txtDepart]').val('');
            $('form :input[id^=txtDestination]').val('');
            $('form :input[id^=txtChildName]').val('');
            $('.numberOfAdults').val(1);
            $('.numberOfKids').val(0);
            $("select").each(function (index) {
                var emptyOption = $(this).find('option[value=""]');
                if (typeof (emptyOption) !== 'undefined' && emptyOption.length > 0) {
                    $(this).val("");
                } else {
                    var placeHolder = $(this).attr("placeHolder");
                    if (typeof (placeHolder) === 'undefined' || placeHolder == null) {
                        placeHolder = "Please Select";
                    }
                }
                if ($(this).attr('name') == 'TypeOfClass') {
                    var options = $(this).children().clone();
                    $(this).empty().html(options);
                }
                if ($(this).attr('name') == 'NumberOfStop') {
                    var options = $(this).children().clone();
                    $(this).empty().html(options);
                }
                $(this).multiselect('refresh');
            });
            removeAllFlights();
            onNumberOfChildrenChange();
            updatePassengersData([false, false, false, true]);
            clearSummary();
            $('#flight-fr-booking').validation({});
        },
        complete: function () {            
             
             $("#ajaxLoading").remove();
        }
    });
    
    

}
function removeAllFlights() {
    $('.row.flight-infor:not(:first-child)').remove();
    $('.info-book').find('.number-flight.extra').remove();
}
function removeFlight(self) {
    $(self.parentNode).remove();        
}
function navTab(id) {
    removeAllFields();
    var arrayOpts = $('.flight-option');
    arrayOpts.each(function (index, value) {
        $(value).css('display', 'none');
    });
    $('#' + id).css('display', 'block');
}

function addChild() {
    var countOfChild = $('.child-infor-row').length;
    var order = countOfChild + 1;
    if (order > limitKidsNumber) {
        return false;
    }
    //place holder    
    var placeholderChildNoName = $('.child-infor').attr('placeholder-child-no-name').split("{0}")[0] + order + $('.child-infor').attr('placeholder-child-no-name').split("{0}")[1];
    var placeholderChildNoNameP = $('.child-infor').attr('placeholder-child-no-name-p');
    var placeholderChildNoAge = $('.child-infor').attr('placeholder-child-no-age').split("{0}")[0] + order + $('.child-infor').attr('placeholder-child-no-age').split("{0}")[1];
    var placeholderChildNoAgeP = $('.child-infor').attr('placeholder-child-no-age-p');


    $('.child-infor').append('<div class="col-md-12 col-sm-12 col-xs-12 child-infor-row" data-order="' + order + '">'
        + '<div class="col-md-6 col-sm-12 col-xs-12 name-child">'
        + '<div class="form-group">'
        + '<label for="nameOfChild">' + placeholderChildNoName + '</label>'
        + '<div class="box-group">'
        + '<label class="input-group-before"><i class="fas fa-user"></i></label>'
        + '<input type="text" class="form-control" name="childName" id="txtChildName' + order + '" value="" '
        + 'placeholder="' + placeholderChildNoNameP + '">'
        + '</div>'
        + '</div>'
        + '</div>'
        + '<div class="col-md-6 col-sm-12 col-xs-12 child-age">'
        + '<div class="form-group">'
        + '<label for="ageOfChild">' + placeholderChildNoAge + '</label>'
        + '<div class="box-group">'
        + '<label class="input-group-before"><i class="fas fa-user"></i></label>'
        + '<select class="form-control flight-child-select-box" id="ageOfChild' + order + '" data-spy="singleSelectBox" name="childAge" '
        + 'placeholder="' + placeholderChildNoAgeP + '" aria-label="childAge" aria-labelledby="flight-fr-booking" aria-invalid="false">'
        // + '<option value="" disabled selected>' + placeholderChildNoAgeP + '</option>'
        + '<option value="" hidden disabled selected>' + placeholderChildNoAgeP + '</option>'
        + '<option value="0">Infant</option>'
        + '<option value="1">1</option>'
        + '<option value="2">2</option>'
        + '<option value="3">3</option>'
        + '<option value="4">4</option>'
        + '<option value="5">5</option>'
        + '<option value="6">6</option>'
        + '<option value="7">7</option>'
        + '<option value="8">8</option>'
        + '<option value="9">9</option>'
        + '<option value="10">10</option>'
        + '<option value="11">11</option>'
        + '<option value="12">12</option>'
        + '<option value="13">13</option>'
        + '<option value="14">14</option>'
        + '<option value="15">15</option>'
        + '<option value="16">16</option>'
        + '<option value="17">17</option>'
        + '</select>'
        + '</div>'
        + '</div>'
        + '</div>'
        + '</div>');
    //$('#flight-fr-booking').validation({});
    $.ajax(
        {
            url: "",
            beforeSend: function () {
                UI.renderSelectBox();
                $(".flight-child-select-box").next().find('button').next().find('ul').find('li:first-child').addClass('hidden');
            },
            complete: function () {
                //var placeholder = $(".flight-child-select-box").attr('placeholder');
                //$(".flight-child-select-box").next().find('button').attr('title', placeholder);
                //$(".flight-child-select-box").next().find('button').find('span').text(placeholder);
                //$(".flight-child-select-box").next().find('button').next().find('ul').find('li:first-child').addClass('hidden');

            }

        });
    
    //set title of button to placeholder

};
function removeChildRow() {
    var childRows = $('.child-infor-row').last();
    childRows.remove();
}

//summary
//helper function
function updateFlightInfoToSummary(flightNo, updateField, updateText) {
    if (flightNo && updateField && updateText && flightNo > 0) {
        if (updateField == 'from') {            
            var updateElement = $('.block-infor.flight-no-' + flightNo).find('.depart-from').find('p');            
            updateElement.text(updateText);   
            updateElement.parent().show();
        }
        if (updateField == 'to') {
            var updateElement = $('.block-infor.flight-no-' + flightNo).find('.depart-to').find('p');
            updateElement.text(updateText);
            updateElement.parent().show();
        }        
    }    
}
function clearSummary() {
    //$('.info-book').find('p').text('');
    var allFieldInSummary = $('.info-book').find('p');
    for (let i = 0; i < allFieldInSummary.length; i++) {
        var item = $(allFieldInSummary[i]);        
        if (item.text() == "") {            
            item.parent().hide();
        }
    }    
    $('.info-book').find('.item.destination-infor').find('.number-flight').find('.depart-from').find('p').text('');
    $('.info-book').find('.item.destination-infor').find('.number-flight').find('.depart-from').hide();
    $('.info-book').find('.item.destination-infor').find('.number-flight').find('.depart-to').find('p').text('');
    $('.info-book').find('.item.destination-infor').find('.number-flight').find('.depart-to').hide();
}
function updateFlightData(element, updateType, baseId) {
    var data = $(element).attr('id').split(baseId);
    if (data[1] == "") {
        updateFlightInfoToSummary(1, updateType, $(element).val());
    } else {
        var order = parseInt(data[1], 10);        
        updateFlightInfoToSummary(order, updateType, $(element).val());
    }
}
function checkExistedIndex(list, index) {
    if (list && list.length) {
        if (list.length > index) {
            return list[index];
        }
    } else {
        return undefined;
    }
}
function updatePassengersData(params) {    
    var isAdult = checkExistedIndex(params, 0);
    var isAdd = checkExistedIndex(params, 1);
    var isEdit = checkExistedIndex(params, 2);
    var isInit = checkExistedIndex(params, 3);
    
    if (isInit) {
        var adult = parseInt($('#numberOfAdults').val(), 10);
        var kid = parseInt($('#numberOfKids').val(), 10);
        putDataToPassengersFieldInSummary(adult, kid);
    } else {
        var adult = parseInt($('#numberOfAdults').val(), 10);
        var kid = parseInt($('#numberOfKids').val(), 10);

        var isAdultPressSubtractWhenAtZero = adult == 0 && isAdult && !isAdd;
        var isAdultPressAdd = !isEdit && isAdult && isAdd;
        var isAdultPressSubtract = !isEdit && isAdult && !isAdd;

        var isKidPressSubtractWhenAtZero = kid == 0 && !isAdult && !isAdd;
        var isKidPressAdd = !isEdit && !isAdult && isAdd;
        var isKidPressSubtract = !isEdit && !isAdult && !isAdd;

        //get current adults
        
        var currentAdults = isAdultPressSubtractWhenAtZero ? adult : (isAdultPressAdd ? adult + 1 : (isAdultPressSubtract ? adult - 1 : adult));        
        //get current children
        
        var currentKids = isKidPressSubtractWhenAtZero ? kid : (isKidPressAdd ? kid + 1 : (isKidPressSubtract ? kid - 1 : kid));        
        putDataToPassengersFieldInSummary(currentAdults, currentKids);
    }
    
    function putDataToPassengersFieldInSummary(numberOfAdults, numberOfKids) {
        //placeholder
        var passengers = $('.passenger-info').find('.infoPassengers');        
        if (passengers.length > 0) {
            var singularAdultLabel = passengers.attr('singular-adult-label');            
            var pluralAdultLabel = passengers.attr('plural-adult-label');            
            var singularChildLabel = passengers.attr('singular-child-label');            
            var pluralChildLabel = passengers.attr('plural-child-label');            
            var andLabel = passengers.attr('and-label');
            var passengerInfo = numberOfAdults + ' ' + (numberOfAdults == 1 ? singularAdultLabel : pluralAdultLabel) + ' ' + andLabel + ' ' + numberOfKids + ' ' + (numberOfKids == 1 || numberOfKids == 0 ? singularChildLabel : pluralChildLabel); 
            //update passenger info on IE11
            if (App.isIE11Check()) {                
                $('.passenger-info').find('.infoPassengers').attr('adult-number', numberOfAdults);
                $('.passenger-info').find('.infoPassengers').attr('child-number', numberOfKids);     
                //add value back to input 
                $('input#numberOfAdults').val(numberOfAdults);
                $('input#numberOfKids').val(numberOfKids);
            }
            passengers.text(passengerInfo);
            passengers.parent().show();            
        }
    }
}

//Event Handle - Start ************************************************

$(document).delegate(".flight-infor .remove-flight", "click", function () {
    //***remove left side data***
    $(this.parentNode).remove();
    //***remove right side data***
    var dataOrder = $(this).parents('.row.flight-infor').attr('data-order');
    $('.info-book').find('.number-flight').find('.flight-no-' + dataOrder).parents('.number-flight').remove();
    //***re-order-flight-no***    
    regenerateSummaryFlightTitle();


});
//round trip
$('#txtDepartRoundTrip').on('change', function () {
    updateFlightInfoToSummary(1, 'from', $(this).val());
});
$('#txtDestinationRoundTrip').on('change', function () {
    updateFlightInfoToSummary(1, 'to', $(this).val());
});
//one-way
$('#txtDepartOneWay').on('change', function () {
    updateFlightInfoToSummary(1, 'from', $(this).val());
});
$('#txtDestinationOneWay').on('change', function () {
    updateFlightInfoToSummary(1, 'to', $(this).val());
});
//multi city
$('input[id^="txtDepartMulti"]').on('change', function () {
    updateFlightData(this, 'from', 'txtDepartMulti');
});

$('input[id^="txtDestinationMulti"]').on('change', function () {
    updateFlightData(this, 'to', 'txtDestinationMulti');
});

//passengers
//adults

$('.input-group-before.bg-light.lt-spinner-prev.adults-number').on('touchstart click', function () {    
    var valueOfAdult = parseInt($('#numberOfAdults').val(), 10) - 1;
    if (valueOfAdult <= 0) {
        $('#numberOfAdults').val(minAdultNumber);
        return;
    }
    updatePassengersData([true, false, false]);
});
$('#numberOfAdults').on('change keyup keydown input', function () {    
    var valueOfAdult = parseInt($('#numberOfAdults').val(), 10);
    if (valueOfAdult > maxAdultNumber) {
        $('#numberOfAdults').val(maxAdultNumber);
    } else if (valueOfAdult <= 0) {
        $('#numberOfAdults').val(minAdultNumber);
    }
    updatePassengersData([true, false, true]);    
    
});
$('#numberOfAdults').on('focusout', function () {
    if ($(this).val() == undefined || $(this).val() == null || $(this).val() == "") {
        $(this).val(minAdultNumber);
        updatePassengersData([false, false, true]);
    }
});
$('.input-group-after.bg-light.lt-spinner-next.adults-number').on('touchstart click', function () {    
    var valueOfAdult = parseInt($('#numberOfAdults').val(), 10) + 1;
    if (valueOfAdult <= maxAdultNumber) {
        updatePassengersData([true, true, false]);
    } else {
        $('#numberOfAdults').val(maxAdultNumber);
    }
});
//kids
$('.input-group-before.bg-light.lt-spinner-prev.kids-number').on('touchstart click', function (e) {    
    if (checkChildNumber()) {
        removeChildRow();
        updatePassengersData([false, false, false]);
    }
    
});
$('#numberOfKids').on('change keyup keydown input', function () {
    var valueOfKids = $('#numberOfKids').val();
    if (valueOfKids > limitKidsNumber) {
        $('#numberOfKids').val(limitKidsNumber);
    }    
    onNumberOfChildrenChange();
    updatePassengersData([false, false, true]);
});
$('#numberOfKids').on('focusout', function () {    
    if ($(this).val() == undefined || $(this).val() == null || $(this).val() == "") {
        $(this).val(0);    
        onNumberOfChildrenChange();
        updatePassengersData([false, false, true]);
    }
});
$('.input-group-after.bg-light.lt-spinner-next.kids-number').on('touchstart click', function (e) {
    if (checkChildNumber()) {
        addChild();
        var valueOfKids = parseInt($('#numberOfKids').val(), 10) + 1;
        if (valueOfKids <= limitKidsNumber) {
            updatePassengersData([false, true, false]);
        } else {
            $('#numberOfKids').val(limitKidsNumber);
        }
        
    }
    
});


//Event Handle - End ************************************************

function checkChildNumber() {
    return $('input.numberOfKids').val() != undefined && $('input.numberOfKids').val() != null && $('input.numberOfKids').val() != '';
}

function onNumberOfChildrenChange() {
    if (checkChildNumber()) {
        var count = $('input.numberOfKids').val(); // get the current value of the input field.
        var numberOfEle = $('.child-infor-row').length;
        while (numberOfEle > count) {
            removeChildRow();
            numberOfEle--;
        };
        while (numberOfEle < count) {
            addChild();
            numberOfEle++;
        }
    }
}

//Ajax-Call Helper Function

function buildListChildInformation() {
    var childInformations = new Array();
    var $childName = $('input[name="childName"]');
    if ($childName.length > 0) {
        var totalEl = $childName.length;
        for (var i = 1; i <= totalEl; i++) {
            var objectValue = new Object();
            var childName = $($childName[i - 1]).val();
            var childAge = $('select#ageOfChild' + i).val();

            objectValue.ChildName = childName;
            objectValue.ChildAge = childAge;
            childInformations.push(objectValue);
        }
    }
    var postData = { name: 'ChildInformationString', value: JSON.stringify(childInformations) };
    return postData;
}

function buildListCity() {
    var multiCityInfos = new Array();
    var $multiDepart = $('input[id^="txtDepartMulti"]');
    if ($multiDepart.length > 0) {
        var totalEl = $multiDepart.length;
        for (var i = 1; i <= totalEl; i++) {
            var objectValue = new Object();
            var departValue = $($multiDepart[i - 1]).val();
            var desValue = $('input#txtDestinationMulti' + i).val();
            var departDate = $('input#txtDepartDateMulti' + i).val();

            objectValue.Depart = departValue;
            objectValue.Destination = desValue;
            objectValue.DepartureDate = new Date(departDate);
            multiCityInfos.push(objectValue);
        }
    }
    var multiCityData = { name: 'MultiCityViewModel.MultiDepartAndDestinationString', value: JSON.stringify(multiCityInfos) };
    return multiCityData;
}

function onSubmitClick(item) {
    //force validate form
    var $flightForm = $(item);
    if (!$flightForm.valid()) {
        return false;
    }

    var attrClass = $('.btn-flight-submit').attr('class');
    var showAjaxLoading = false;
    if (isNotNullAndEmpty(attrClass)) {
        showAjaxLoading = attrClass.indexOf('showAjaxLoading') >= 0;
    }
    var dataModels = $(item).serializeArray();
    //bind list child
    var postData = buildListChildInformation();
    dataModels.push(postData);
    
    if ($('#roundTrip').is(':checked')) {
        dataModels.push({ name: 'RoundTrip', value: true });
    } else if ($('#oneWay').is(':checked')) {
        dataModels.push({ name: 'OneWay', value: true });
    } else {
        dataModels.push({ name: 'MultiCity', value: true });
        //bind list city
        var multiCityData = buildListCity();
        dataModels.push(multiCityData);
    }

    $.ajax({
        type: "POST",
        url: $flightForm.attr('action'),
        dataType: "json",
        data: dataModels,
        beforeSend: function () {
            if (showAjaxLoading) {
                $("body").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
            }
        },
        complete: function () {
            if (showAjaxLoading) {
                $("#ajaxLoading").remove();
            }
        },
        success: function (response) {
            var result = response.Result;
            if (typeof (result) !== 'undefined' && result != null) {
                if (result.success) {
                    redirectToPath(result.returnUrl);
                } else {
                    //$('#submit_message').show();
                    //var currentText = $('#submit_message').find('p').text();
                    //$('#submit_message').find('p').text(currentText + ' / ' + response.Result.message);
                    showSubmissionErrorMessage();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#ajaxLoading").remove();
            showSubmissionErrorMessage();
        }
    });
    return false;
}



//JRCW-488
$(document).ready(function () {
    NewFlightFormBeforeSubmit();
});
function NewFlightFormBeforeSubmit() {
    //JRCW-488
    $('.btn.btn-red.btn-flight-submit.showAjaxLoading').clone().appendTo('.info-book');
    $('.btn.btn-red.btn-flight-submit.showAjaxLoading').first().hide();
    $('.btn.btn-red.btn-flight-submit.showAjaxLoading').next().removeAttr("type").attr("onClick", "submitNewFlightForm(event)");
    
}
function submitNewFlightForm(e) {
    e.preventDefault();
    var formId = "#flight-fr-booking";
    //check valid first
    if ($(formId).valid()) {
        $(".btn-submit-CreditCardNumber-footer").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
        App.verifyCC(function () { onSubmitClick(formId); });
    }

}