﻿"use strict";

if (isPrefilledRestaurantName == 'true' || isPrefilledRestaurantName == 'True') {
    $("#RestaurantName").attr('readonly', true);
}   
$('.MainCourse').each(function (index, dish) {
$('.fr-booking').validation({
    selectCreditName: {
        name: 'ReservationName'
    },
    txtContactNumber: {
        name: 'MobileNumber'
    },
    txtEmail: {
        name: 'Email'
    },
    txtCreditCard: {
        name: 'CreditCardDigits'
    },
    dishWarning: {
        name: dish.name
    }
    })
})
var textplaceholderDish = typeof placeHolderDish == 'undefined' ? 'You are entitled for 1 complimentary dishes' : placeHolderDish;
var textErrorMsgMaxDish = typeof placeholderErrorMsgMaxDish == 'undefined' ? 'You can only select up to 0 dish(es)' : placeholderErrorMsgMaxDish;


$("#creditCardDigits").keyup(function () {
    var binNumber = $('#creditCardDigits').val();
    if (binNumber.length >= 6) {
        $.ajax({
            type: 'POST',
            url: "/sm/FormModule/MaxDishes",
            data: {
                binNumber
            },
            dataType: 'json',
            success: function (result) {
                var maxDishNumber = "";
                maxDishNumber = result.Result.maxDishesCanOrder;
                $('#PlaceholderNumberOfDish').text(textplaceholderDish.replace('1', maxDishNumber));
                $('#MaxDishNumber').attr('value', maxDishNumber);
                for (var i = 0; i < dishCount; i++) {
                    $("#Dishes_" + i + "__Quantity").rules('add', {
                        messages: {
                            max: textErrorMsgMaxDish.replace('0', maxDishNumber)
                        }
                    });
                    $("#Dishes_" + i + "__Quantity").attr({ "max": maxDishNumber }).val('0');
                    $("#Dishes_" + i + "__Quantity-error").hide();
                    if ($("input").hasClass("error lt-error"))
                        $("input").removeClass("error lt-error");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }
})

$(document).click(function () {
    console.log(Number($('.MainCourse').val()), Number($('#MaxDishNumber').val()));
    var a = 0;        
    $('#dishesform').find('.MainCourse').each(function (index, value) {
            a += Number($(value).val());
    })
    if (a != 0) {
       $('#dishWarning').find('label.error.lt-error').remove();
    }
    else {
        var error = $('#dishesform').find('label.error.lt-error');
        if ($('#dishesform input').hasClass('error')) {
            $('#dishesform input').removeClass('error')
        }
        $('#dishWarning').html(error);
        $('#dishesform').find('label.error.lt-error:not(:first)').remove();
    }
})
$('.MainCourse').on('change', function () {
    $('.MainCourse').blur();
})
$('.selected-number').on('click', function () {
    $('.MainCourse').blur();
})

