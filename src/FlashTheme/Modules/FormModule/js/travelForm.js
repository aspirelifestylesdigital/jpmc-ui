"use strict";
jQuery(document).ready(function () {
    if (isPrefilledLocation == 'true' || isPrefilledLocation == 'True') {
        $("#Location").attr('readonly', true);
    }

    if (isPrefilledHotelName == 'true' || isPrefilledHotelName == 'True') {
        $("#HotelName").attr('readonly', true);
    }
});
