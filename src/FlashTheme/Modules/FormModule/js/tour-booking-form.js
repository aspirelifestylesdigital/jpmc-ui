﻿jQuery(document).ready(function () {
    var TourDetails = null;
    var currentCurrency = 'USD';
    var priceFroms = {};

    $('.fr-booking').validation({
        txtContactNumber: {
            name: 'mobileNumber'
        },
        txtEmail: {
            name: 'emailAddress'
        }
    });

    function loadTourByDestination(suggestion) {
        var lastTourLocation = $('#infoLocation').text();
        var tourLocation = suggestion.value;

        if (tourLocation && tourLocation.length > 0) {
            if (tourLocation == lastTourLocation && isFormeset == false) {
                return;
            }
            isFormeset = false;
            clearTours();

            $('#infoLocation').text(tourLocation);
            var prefillTourId = $('#PrefillTourCode').val();
            var locationInfo = suggestion.data.split('|');
            if (isNotNull(locationInfo) && locationInfo.length > 0) {
                var locationCity = isNotNullAndEmpty(locationInfo[0]) ? locationInfo[0].trim() : "";
                var locationState = "";
                var locationCountry = "";
                $('#TourCity').val(locationCity);
                if (locationInfo.length > 1) {
                    locationState = isNotNullAndEmpty(locationInfo[1]) ? locationInfo[1].trim() : "";
                    $('#TourState').val(locationState);
                }
                if (locationInfo.length > 2) {
                    locationCountry = isNotNullAndEmpty(locationInfo[2]) ? locationInfo[2].trim() : "";
                    $('#TourCountry').val(locationCountry);
                }
                var searchData = {
                    city: locationCity,
                    state: locationState,
                    country: locationCountry,
                    tourId: prefillTourId
                };
                $.ajax({
                    url: "/sm/DCRApi/SearchDCRTours",
                    type: "POST",
                    data: searchData,
                    beforeSend: function () {
                        $("body").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
                    },
                    complete: function () {
                        $("#ajaxLoading").remove();
                    },
                    success: function (response) {
                        if (response == "") {
                            return;
                        }
                        var tours = response.tours;
                        var selectedTourCode = null;
                        $.each(tours, function (i, item) {
                            $('#TourCode').append($('<option>', {
                                value: item.ProductCode,
                                text: item.TourName,
                                price: item.Price,
                                id: item.Id
                            }));
                            if (prefillTourId && item.Id == prefillTourId) {
                                selectedTourCode = item.ProductCode;
                            }
                        });
                        rebuildSelection($('#TourCode'));

                        if (prefillTourId && prefillTourId.length > 0) {
                            $('#TourCode').val(selectedTourCode);
                            $('#TourCode').multiselect('refresh');
                            $('#TourCode').change();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            }
        }
    }

    function loadTourByDestinationFromAK(tourLocationAK) {
        var lastTourLocation = $('#infoLocation').text();
        //var tourLocation = suggestion.value;

        if (tourLocationAK && tourLocationAK.length > 0) {
            if (tourLocationAK == lastTourLocation && isFormeset == false) {
                return;
            }
            isFormeset = false;
            clearTours();

            $('#infoLocation').text(tourLocationAK);
            var prefillTourId = $('#PrefillTourCode').val();
            //var locationInfo = tourLocationAK;//.data.split('|');
            if (isNotNull(tourLocationAK)) {
                var locationState = "";
                var locationCountry = "";
                var searchData = {
                    city: tourLocationAK,
                    state: locationState,
                    country: locationCountry,
                    tourId: prefillTourId
                };
                $.ajax({
                    url: "/sm/DCRApi/SearchAKTours",
                    type: "POST",
                    data: searchData,
                    beforeSend: function () {
                        $("body").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
                    },
                    complete: function () {
                        $("#ajaxLoading").remove();
                    },
                    success: function (response) {
                        if (response == "") {
                            return;
                        }
                        var tours = response.tours;
                        var selectedTourCode = null;
                        $.each(tours, function (i, item) {
                            $('#TourCode').append($('<option>', {
                                value: item.ProductCode,
                                text: item.TourName,
                                price: item.Price,
                                id: item.Id
                            }));
                            if (prefillTourId && item.Id == prefillTourId) {
                                selectedTourCode = item.ProductCode;
                            }
                        });
                        rebuildSelection($('#TourCode'));

                        if (prefillTourId && prefillTourId.length > 0) {
                            $('#TourCode').val(selectedTourCode);
                            $('#TourCode').multiselect('refresh');
                            $('#TourCode').change();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            }
        }
    }

    //disable autocomplete if datasource is AK
    var dataSourceEl = $('#mastersource');
    if (dataSourceEl.length > 0 && dataSourceEl.val() !== 'ak') {
        $("#Location").autocomplete({
            serviceUrl: "/sm/DCRApi/LocationAutoComplete",
            params: {
                limit: 10,
                apiItemType: 'sightseeing_activity'
            },
            minChars: 3,
            appendTo: $("#Location").parent(),
            autoSelectFirst: true,
            showNoSuggestionNotice: true,
            onSelect: loadTourByDestination,
            onSearchComplete: function (query, suggestions) {
                $(this).siblings(".autocomplete-suggestions").addClass('scrollbar-outer');
                $(this).siblings(".autocomplete-suggestions").scrollbar();
            }
        });
    } else {
        $("#Location").blur(function () {
            //alert($(this).val());
            loadTourByDestinationFromAK($(this).val());
        });
    }

    $('#TourCode').change(function () {
        $('.bookingQuestion').remove();
        var selectedTour = $(this).find('option:selected');
        var prodCode = selectedTour.val();
        priceFroms = {};
        $('#infoTourName').text(selectedTour.text());
        $('#TourName').val(selectedTour.text());
        if (prodCode && prodCode.length > 0) {
            $.ajax({
                url: "/sm/DCRApi/GetTourDetails",
                type: "POST",
                data: { productCode: prodCode, currency: currentCurrency },
                beforeSend: function () {
                    $("body").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
                },
                complete: function () {
                    $("#ajaxLoading").remove();
                },
                success: function (detail) {
                    if (!isNotNull(detail)) {
                        return;
                    }
                    if (!isNotNull(detail.tourDetails)) {
                        return;
                    }
                    var ddlHotel = $('#HotelId');
                    var divHotelList = $('#divHotelList');
                    var placeholderDropdown = $("#divHotelList").data("placeholder");
                    clearDropdownList(ddlHotel, placeholderDropdown);
                    $('input[name=hotelPickup]').prop("checked", false);
                    if (detail.tourDetails.HotelPickup == true) {
                        var hotelPickups = detail.tourDetails.HotelPickupInfo;
                        $('#grpHotelPickups').show('slide');
                        if (typeof (hotelPickups) !== 'undefined' && hotelPickups != null
                            && hotelPickups.length > 0) {
                            divHotelList.show();
                            $.each(hotelPickups, function (i, item) {
                                if (item.Address && item.Address != null && item.Address.length > 0) {
                                    ddlHotel.append($('<option>', {
                                        value: item.Id,
                                        text: item.Name,
                                        address: item.Address,
                                        city: item.City
                                    }));
                                }
                            });
                            rebuildSelection($('#HotelId'));
                            $('#inList').prop("checked", true);
                            onHotelPickupChange($('#inList'));
                        } else {
                            divHotelList.hide();
                            $('#notListed').prop("checked", true);
                            onHotelPickupChange($('#notListed'));
                        }
                    } else {
                        $('#grpHotelPickups').hide('slide');
                    }

                    var bookingQuestions = detail.tourDetails.BookingQuestions;
                    if (bookingQuestions != null && bookingQuestions.length > 0) {
                        $.each(bookingQuestions, function (i, question) {
                            var questionDiv1 = $('<div class="row bookingQuestion"></div>');
                            var questionDiv2 = $('<div class="col-md-6 col-sm-12 col-xs-12 infoRestaurant"></div>');
                            var questionDiv3 = $('<div class="form-group"></div>');
                            var questionLabel = '<label for="select-name">' + question.Title +
                                (question.Required == true ? '<span class="txt-red">*</span>' : '') + '</label>';
                            questionDiv3.append(questionLabel);

                            var questionDiv4 = $('<div class="box-group"></div>');
                            var hiddenQuestionid = '<input type="hidden" id="txt_ques_id_' + question.QuestionId + '" ' +
                                'value="' + question.QuestionId + '"' +
                                'name="BookingQuestions[' + i + '].Id" />';

                            var hiddenQuestionLabel = '<input type="hidden" id="txt_ques_label_' + question.QuestionId + '" ' +
                                'value="' + question.Title + '"' +
                                'name="BookingQuestions[' + i + '].Question" />';

                            var questionTextArea = '<textarea id="txt_answer_' + question.QuestionId + '" ' +
                                ' name="BookingQuestions[' + i + '].Answer" ' +
                                ' class="form-control small-textarea" placeholder="' +
                                question.Message + '" ' +
                                (question.Required == true ? 'required' : '') + '></textarea>';
                            questionDiv4.append(hiddenQuestionid);
                            questionDiv4.append(hiddenQuestionLabel);
                            questionDiv4.append(questionTextArea);
                            questionDiv3.append(questionDiv4);

                            var questionSubTitle = '<label><i>' + question.SubTitle + '</i></label>';
                            questionDiv3.append(questionSubTitle);

                            questionDiv2.append(questionDiv3);
                            questionDiv1.append(questionDiv2);

                            $("#secSpecialRequirement").before(questionDiv1);
                        });
                    }

                    clearTourDate();
                    var ddlMonthYear = $('#AvailableMonthYear');
                    var availableDates = detail.tourDetails.AvailableDates
                    if (availableDates != null && availableDates.length > 0) {
                        $.each(availableDates, function (i, item) {
                            ddlMonthYear.append($('<option>', {
                                value: item.MonthYear,
                                text: buildFormattedMonthYear(item.MonthYear),
                                ListOfDays: item.ListOfDays
                            }));
                        });
                        rebuildSelection(ddlMonthYear);

                        ddlMonthYear.val(availableDates[0].MonthYear);
                        ddlMonthYear.change();
                        ddlMonthYear.multiselect('refresh');
                    }
                    if (!isNotNull(detail.tourDetails.AllDetails)) {
                        return;
                    }

                    var productResponse = JSON.parse(detail.tourDetails.AllDetails);
                    if (productResponse.success == false) {

                        App.getPlaceholderSelfFormByValue(productResponse.ErrorMessage, 'tour', function (errorMsg) { alert(errorMsg); });

                        //alert(productResponse.ErrorMessage);
                        return;
                    }
                    TourDetails = productResponse.data;
                    showPassengers(TourDetails.ageBands, TourDetails.maxTravellerCount);
                    showTourGrades(TourDetails.tourGrades, true);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
        }
    });

    $('#AvailableMonthYear').change(function () {
        clearAvailableDate();
        var ddlAvailableDate = $('#AvailableDay');
        var selectedMonthYear = $(this).find('option:selected');
        var strListOfDays = selectedMonthYear.attr("ListOfDays");
        if (strListOfDays && strListOfDays.length > 0) {
            var listOfDays = strListOfDays.split(',');
            if (listOfDays != null && listOfDays.length > 0) {
                $.each(listOfDays, function (i, dt) {
                    ddlAvailableDate.append($('<option>', {
                        value: dt,
                        text: dt
                    }));
                });
                rebuildSelection(ddlAvailableDate);
                ddlAvailableDate.val(listOfDays[0]);
                ddlAvailableDate.multiselect('refresh');
            }
            updateInfoTravelDate();
        }
    });

    $('#AvailableDay').change(function () {
        if (TourDetails != null && TourDetails.code != null) {
            var travelDate = $('#AvailableMonthYear').val() + '-' + $('#AvailableDay').val();
            updateInfoTravelDate();
            GetTourGrades(TourDetails.code, travelDate, currentCurrency);
        }
    });

    // Change Age Band Number event
    $(document).on("change", ".ageBands", function () {
        if (TourDetails == null) {
            return;
        }
        var passengerCount = 0;
        if ($(this).val() != "") {
            passengerCount = parseInt($(this).val());
        }

        showTravelers($(this).attr("data-bandId"), $(this).attr("data-ageBand"), $(this).attr("data-label-ageBand"), passengerCount);
        var travelDate = $('#AvailableMonthYear').val() + '-' + $('#AvailableDay').val();
        GetTourGrades(TourDetails.code, travelDate, currentCurrency);

        var txtInfoAgeBand = $('#infoAgeBand' + $(this).attr('data-bandId'));
        txtInfoAgeBand.text(passengerCount.toString());
        updatePrice();
    });

    $(document).on("click", ".traveler-islead", function () {
        var self = $(this);

        $(".traveler-islead").each(function () {
            if ($(this).attr("name") != self.attr("name")) {
                $(this).attr('checked', false);
            }
        });
    });

    function rebuildSelection($select) {
        $select.multiselect('rebuild');
        $select.next().find(".multiselect-selected-text").css({ "color": "rgb(85, 85, 85)" });
    };

    function clearAvailableDate() {
        clearDropdownList($("#AvailableDay"), $("#AvailableDay").data("placeholder"));
    }

    function clearAvailableMonthYear() {
        clearDropdownList($("#AvailableMonthYear"), $("#AvailableMonthYear").data("placeholder"));
    }

    function clearTours() {
        clearTourDate();
        clearDropdownList($("#TourCode"), $("#TourCode").data("placeholder"));
    }

    function clearDropdownList(dropdownList, placeHolderText) {
        dropdownList.empty();
        dropdownList.append($('<option>', {
            value: "",
            text: placeHolderText
        }));
        rebuildSelection(dropdownList);
    }

    function clearTourDate() {
        clearAvailableMonthYear();
        clearAvailableDate();
        updateInfoTravelDate();
    }

    function loadCountries() {
        var ddlCountry = $("#PaymentCountry");
        var placeholderSelect = ddlCountry.data("placeholder");
        $.ajax({
            type: 'GET',
            url: "/sm/DCRApi/GetISOCountries",
            success: function (result) {
                ddlCountry.empty();
                var op = $("<option value=''> " + placeholderSelect + " </option>");
                ddlCountry.append(op);
                if (result.Countries && result.Countries.length > 0) {
                    result.Countries.forEach(function (cnt) {
                        op = new Option(cnt.Name, cnt.Code);
                        ddlCountry.append(op);
                    });
                    rebuildSelection(ddlCountry);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }

    function initialize() {
        Booking.getValueCardExpiryDate("fr-booking", "CardExpiredDate");
        clearTours();
        clearDropdownList($('#HotelId'), 'Please select your hotel');
        $('#SpecialRequirement').removeAttr('cols');
        $('#SpecialRequirement').removeAttr('rows');
        loadCountries();
        $('#grpHotelPickups').hide();
        $('#divPassengers').hide();
        $('.tour-grades').hide();
    }

    function buildFormattedMonthYear(orgMonthYear) {
        var firstDayOfMonth = orgMonthYear + '-01';
        var dt = moment(firstDayOfMonth).format('DD-MMM-YYYY');
        return dt.substring(3, 11);
    }

    function showTourGrades(tourGrades, isLoadingTour) {
        $('.tour-grades').show();
        var hasAvailable = false;

        $("#tourGrades").empty();

        if (tourGrades == null || tourGrades.length <= 0) {
            $("#tourGrades").append('<span class="txt-red">' + PlaceholderNoTour + '</span>');
            return;
        }

        for (var index = 0; index < tourGrades.length; index++) {
            var tourGrade = tourGrades[index];
            var available = tourGrade.available == undefined ? true : tourGrade.available;
            var priceFormatted = tourGrade.priceFromFormatted == undefined ?
                tourGrade.retailPriceFormatted : tourGrade.priceFromFormatted;
            var gradeTitle = (tourGrade.gradeTitle != null && tourGrade.gradeTitle != '') ?
                tourGrade.gradeTitle : tourGrade.gradeCode;

            if (typeof (tourGrade.priceFromFormatted) !== 'undefined' && tourGrade.priceFromFormatted != null) {
                priceFroms[tourGrade.gradeCode] = tourGrade.priceFromFormatted;
            }

            if (available == true) {
                var gradeRow = '<div class="box-group">' +
                    '<input type="radio" id="radioTourGrade' + index + '" name="TourGradeCode"' +
                    ' value="' + tourGrade.gradeCode + '" gradeTitle="' + gradeTitle + '"' +
                    ' retailPrice="' + priceFormatted + '" class="rdoTourGrade" />' +
                    '<label for="radioTourGrade' + index + '">' +
                    tourGrade.gradeTitle + " (" + priceFormatted + ')</label> </div>';

                $("#tourGrades").append(gradeRow);
                hasAvailable = true;
            }
        }

        if (hasAvailable == false) {
            $("#tourGrades").append('<span class="txt-red">' + PlaceholderNoTour + '</span>');
        } else {
            var firstTourGrade = $("#tourGrades").find(".rdoTourGrade").first();
            if (typeof (firstTourGrade) !== 'undefined' && firstTourGrade != null) {
                firstTourGrade.prop("checked", true);
                updatePrice();
            }
        }
    }

    // Will be called when selecting a tour / booking data
    function GetTourGrades(productCode, bookingDate, currencyCode) {
        currencyCode = currencyCode == undefined ? "USD" : currencyCode;
        var ageBands = [];

        $(".ageBands").each(function () {
            if ($(this).val() > 0) {
                ageBands.push({ bandId: $(this).attr("data-bandId"), count: $(this).val() });
            }
        });

        if (ageBands.length <= 0) {
            ageBands = [{ bandId: 1, count: 1 }];
        }

        $.ajax({
            url: "/sm/DCRApi/GetTourGrades",
            type: "POST",
            data: { productCode: productCode, currencyCode: currencyCode, bookingDate: bookingDate, ageBands: ageBands },
            beforeSend: function () {
                $("body").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
            },
            complete: function () {
                $("#ajaxLoading").remove();
            },
            success: function (response) {
                if (response == "") {
                    return;
                }

                var productResponse = JSON.parse(response);

                if (productResponse.success == false) {
                    showTourGrades(null, false);
                    return;
                }

                showTourGrades(productResponse.data, false);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }

    function showPassengers(ageBands, maxTravellerCount) {
        $('#divPassengers').show();
        $("#ageBandsId").empty();
        $("#tblPassengers tbody").empty();
        updatePrice();

        // Show MaximumTraveller Number
        $("#txtMaximumTraveller").text(maxTravellerCount);
        var placeholderSelect = $("#divPassengers").data("placeholder-select");
        // Render list of Age Band Select
        var options = '<option value="">' + placeholderSelect + '</option>';
        for (var ageOptionIndex = 1; ageOptionIndex <= maxTravellerCount; ageOptionIndex++) {
            options += '<option value="' + ageOptionIndex + '">' + ageOptionIndex + '</option>';
        }

        $('.passengerCountSummary').remove();
        var labelAge = $("#divPassengers").data("label-age");
        for (var ageBandIndex = 0; ageBandIndex < ageBands.length; ageBandIndex++) {
            var ageBand = ageBands[ageBandIndex];
            var title = $("#divPassengers").data("label-" + ageBand.description.toLowerCase());
            if (title == undefined)
                title = ageBand.description;
            var label = title + " (" + labelAge + " " + ageBand.ageFrom + " - " + ageBand.ageTo + ")";
            var required = "";
            var requiredMask = "";
            if (ageBand.bandId == 1) {
                required = "required";
                var requiredMask = '<span class="txt-red">*</span>';
            }

            var ageBandRow = '<div class="row"> \
                                <div class="col-md-6 col-sm-5 col-xs-12"> \
                                    <div class="form-group"> \
                                        <label for="">' + label + ' ' + requiredMask + '</label> \
                                        <div class="box-group"> \
                                            <div class="input-group-before"> \
                                                <span class="sprites icon-user"></span> \
                                            </div> \
                                            <select id="ageBands' + ageBand.description + '" name="Passengers[' + ageBandIndex + '].Number" data-label-ageBand="' + title + '" data-ageBand="' + ageBand.description + '" data-bandId="' + ageBand.bandId +
                '" class="form-control ageBands showDefaultVal" data-spy="singleSelectBox" ' + required + '>'
                + options +
                '</select> \
                                            <input type="hidden" name="Passengers[' + ageBandIndex + '].AgeBand" value="' + ageBand.description + '" class="form-control"> \
                                        </div> \
                                    </div> \
                                </div> \
                                </div>';

            $("#ageBandsId").append(ageBandRow);
            rebuildSelection($("#ageBands" + ageBand.description));
            $("#tblPassengers tbody").append('<tr id="beginPassengerOf' + ageBand.description + '"><td colspan="4" style="padding: 0; height: 0"></tr>');

            var dlPassengerCount = '<dl class="passengerCountSummary"><dt class="infoTitle">' + title +
                ': <span class="summary-info-item-clear infoAgeBandCount" id="infoAgeBand' + ageBand.bandId + '">0</span></dt></dl>';

            $('#dlTotalPricePlaceHolder').before(dlPassengerCount);
        }
    }

    function showTravelers(bandId, ageBandName, labelAgeBandName, count) {
        var passengers = $("#divPassengers");
        // Remove all input rows out of range
        if (count <= 0) {
            $(".row" + ageBandName).remove();
        }

        var rowCount = 1;
        $(".row" + ageBandName).each(function () {
            // Remove
            if (rowCount > count) {
                $(this).remove();
            }

            rowCount++;
        });

        var currentNum = $(".row" + ageBandName).length;
        var $element = $(".row" + ageBandName + ":last");

        if ($element.length <= 0) {
            $element = $("#beginPassengerOf" + ageBandName);
        }
        var placeholderFirstName = passengers.data("placeholder-firstname");
        var placeholderLastName = passengers.data("placeholder-lastname");
        var labelTraveler = passengers.data("label-traveler");
        for (var index = count; index > currentNum; index--) {
            var isLeadHtml = "";

            if (bandId == 1) {
                var rdoLeadId = ageBandName + index;
                isLeadHtml = '<input type="radio" class="rdoLeadTraveller traveler-islead" value="true" id="' + rdoLeadId + '"> \
                              <label for="' + rdoLeadId + '"></label>';
            }

            var inputRow = '<tr class="row' + ageBandName + '"> \
                                <td>'+ labelTraveler + ' <span class="traveler-no"/> (' + labelAgeBandName + ') <span class="txt-red">*</span></td> \
                                <td class="text-center">' +
                isLeadHtml +
                '</td> \
                                <td> \
                                    <div class="box-group"> \
                                        <label class="input-group-before" for=""> \
                                            <span class="sprites icon-user"></span> \
                                        </label> \
                                        <input type="text" class="form-control traveler-first-name" placeholder="'+ placeholderFirstName + '" required> \
                                    </div> \
                                </td> \
                                <td> \
                                    <div class="box-group"> \
                                        <label class="input-group-before" for=""> \
                                            <span class="sprites icon-user"></span> \
                                        </label> \
                                        <input type="text" class="form-control traveler-last-name" placeholder="'+ placeholderLastName + '" required> \
                                        <input type="hidden" value="' + bandId + '" class="traveler-age-bandid"> \
                                        <input type="hidden" value="' + ageBandName + '" class="traveler-age-band"> \
                                    </div> \
                                </td> \
                            </tr>';

            $element.after(inputRow);
        }

        // Set traveler No
        var travelerNo = 1;
        $(".traveler-no").each(function () {
            $(this).text(travelerNo);
            travelerNo++;
        });

        // Rename islead of traveler
        var travelerNo = 0;
        $(".traveler-islead").each(function () {
            $(this).attr('name', 'Travelers[' + travelerNo + '].IsLead');
            travelerNo++;
        });
        // Rename first name of traveler
        var travelerNo = 0;
        $(".traveler-first-name").each(function () {
            $(this).attr('name', 'Travelers[' + travelerNo + '].FirstName');
            travelerNo++;
        });
        // Rename last name of traveler
        travelerNo = 0;
        $(".traveler-last-name").each(function () {
            $(this).attr('name', 'Travelers[' + travelerNo + '].LastName');
            travelerNo++;
        });
        // Rename age band of traveler
        travelerNo = 0;
        $(".traveler-age-bandid").each(function () {
            $(this).attr('name', 'Travelers[' + travelerNo + '].AgeBandId');
            travelerNo++;
        });
        // Rename age band of traveler
        travelerNo = 0;
        $(".traveler-age-band").each(function () {
            $(this).attr('name', 'Travelers[' + travelerNo + '].AgeBand');
            travelerNo++;
        });

        if ($(".rdoLeadTraveller").is(":checked") == false) {
            var firstTraveller = $(".rdoLeadTraveller").first();
            firstTraveller.prop("checked", true);
        }

        var maxTravellerCountRemaining = TourDetails.maxTravellerCount - $(".traveler-first-name").length;

        if (maxTravellerCountRemaining >= 0) {
            var select = passengers.data("placeholder-select");
            var options = '<option value="">' + select + '</option>';
            for (var ageOptionIndex = 1; ageOptionIndex <= maxTravellerCountRemaining; ageOptionIndex++) {
                options += '<option value="' + ageOptionIndex + '">' + ageOptionIndex + '</option>';
            }

            $(".ageBands").each(function () {
                if ($(this).val() <= 0 || $(this).val() == "") {
                    $(this).empty();
                    $(this).append(options);
                } else {
                    var maxCount = parseInt($(this).val()) + maxTravellerCountRemaining;
                    var maxOptionValue = parseInt($(this).find("option:last").val());

                    if (maxOptionValue < maxCount) {
                        for (var addIndex = maxOptionValue + 1; addIndex <= maxCount; addIndex++) {
                            $(this).append('<option value="' + addIndex + '">' + addIndex + '</option>');
                        }
                    } else {
                        for (var removeValue = maxOptionValue; removeValue > maxCount; removeValue--) {
                            $(this).find('option[value="' + removeValue + '"]').remove();
                        }
                    }
                }

                rebuildSelection($(this));
            });
        }
    }

    $(document).on("change", ".rdoTourGrade", function () {
        updatePrice();
    });

    function updatePrice() {
        var selectedTourGrade = $(".rdoTourGrade:checked").first();
        var hasPrice = false;
        if (typeof (selectedTourGrade) !== 'undefined' && selectedTourGrade != null) {
            $('#infoTourGrade').text(selectedTourGrade.attr("gradeTitle"));
            var gradeCode = selectedTourGrade.val();
            var prcFrom = priceFroms[gradeCode];
            if (typeof (prcFrom) !== 'undefined' && prcFrom != null) {
                $('#infoPrice').text(prcFrom);
            }
            var bookingCount = 0;
            $('.infoAgeBandCount').each(function () {
                var ageCount = parseInt($(this).text());
                if (typeof (ageCount) !== 'undefined' && ageCount != null && ageCount > 0) {
                    bookingCount += ageCount;
                }
            });
            var retailPrice = selectedTourGrade.attr('retailPrice');
            if (typeof (retailPrice) !== 'undefined' && retailPrice != null) {
                hasPrice = true;
                if (bookingCount > 0) {
                    $('#infoTotalPrice').text(retailPrice);
                } else {
                    $('#infoTotalPrice').text('$0');
                }
            }
        }
        if (hasPrice == false) {
            $('#infoTourGrade').text('');
            $('#infoPrice').text('');
            $('#infoTotalPrice').text('$0');
        }
    }

    $('input[name=hotelPickup]').change(function () {
        onHotelPickupChange($(this));
    });

    function onHotelPickupChange(ctrlPickupOption) {
        if (ctrlPickupOption.prop('checked') == true) {
            var pickupType = ctrlPickupOption.attr('id');
            $('#HotelPickupType').val(pickupType);
            if (pickupType == 'notListed') {
                $('#HotelName').attr('required', 'required');
            } else {
                $('#HotelName').removeAttr('required');
            }
        }
    }

    $('#HotelId').change(function () {
        var pickupType = $("input[name='hotelPickup']:checked").attr('id');
        if (pickupType == 'inList') {
            var selectedHotel = $(this).find('option:selected');
            var hotelId = selectedHotel.val();
            if (hotelId && hotelId.length > 0) {
                $('#HotelName').val(selectedHotel.text());
            }
        }
    });

    $('#btnPlaceRequest').click(function () {
        var bookingForm = $('#fr-booking');
        bookingForm.validate();
        if (bookingForm.valid()) {
            var selectedTourGrade = $(".rdoTourGrade:checked").val();
            if (typeof (selectedTourGrade) === 'undefined' || selectedTourGrade == null ||
                selectedTourGrade.length == 0) {
                alert(PlaceholderSelectTour);
                return;
            }
            var bookerName = $('#ReservationName').val();
            var nameSeparationIndex = bookerName.trim().indexOf(' ');
            if (nameSeparationIndex <= 0) {
                alert(PlaceholderReservationNameEnterSpace);
                return;
            }

            $.ajax({
                url: bookingForm.attr('action'),
                type: "POST",
                dataType: "json",
                data: JSON.stringify(bookingForm.serializeObject()),
                contentType: "application/json;charset=utf-8",
                beforeSend: function () {
                    $("body").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
                },
                complete: function () {
                    $("#ajaxLoading").remove();
                },
                success: function (response) {
                    if (typeof (response.Result) !== 'undefined' && response.Result != null) {
                        if (response.Result.IsSuccess == true) {
                            redirectToPath(response.Result.Path);
                        } else {

                            var errMessage = '';
                            $.each(response.Result.ErrorMessages, function () {
                                errMessage = errMessage + '\n' + this;
                            });
                            showSubmissionErrorMessage();
                            alert(errMessage);
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                    showSubmissionErrorMessage();
                }
            });
        }
    });

    function updateInfoTravelDate() {
        var travelDate = $('#AvailableMonthYear').val() + '-' + $('#AvailableDay').val();
        $('#infoTravelDate').text(travelDate);
    }

    function prefillInfo() {
        var prefillLocation = $('#PrefillDestinationId').val();
        if (prefillLocation && prefillLocation.length > 0) {
            var locationParts = prefillLocation.split('|');
            if (isNotNull(locationParts) && locationParts.length >= 3) {
                var city = locationParts[0].trim();
                var state = locationParts[1].trim();
                var country = locationParts[2].trim();
                var dataSourceEl = $('#mastersource');
                if (dataSourceEl.length > 0 && dataSourceEl.val() == 'ak') {
                    //use for AK
                    var locationTour = "";
                    if (isNotNullAndEmpty(city)) {
                        locationTour = city;
                    }
                    $('#Location').val(locationParts[0].trim());
                    loadTourByDestinationFromAK(city);
                } else {
                    if (isNotNullAndEmpty(city)
                        || isNotNullAndEmpty(state)
                        || isNotNullAndEmpty(country)) {
                        // if prefill info contains city, use DCR auto complete API to get location
                        if (isNotNullAndEmpty(city)) {
                            var params = {
                                query: city,
                                limit: 20,
                                apiItemType: 'sightseeing_activity'
                            };
                            $.ajax({
                                url: "/sm/DCRApi/LocationAutoComplete",
                                type: "GET",
                                data: params,
                                beforeSend: function () {
                                    $("body").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
                                },
                                complete: function () {
                                    $("#ajaxLoading").remove();
                                },
                                success: function (response) {
                                    if (response == "") {
                                        return;
                                    }
                                    var suggestions = response.suggestions;
                                    if (isNotNull(suggestions) && suggestions.length > 0) {
                                        $.each(suggestions, function (i, item) {
                                            if (isNotNullAndEmpty(item.data)) {
                                                if (item.data == prefillLocation) {
                                                    $('#Location').val(item.value);
                                                    loadTourByDestination(item);
                                                }
                                            }
                                        });
                                    }
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    console.log(textStatus, errorThrown);
                                }
                            });
                        } else {
                            var searchLocation = null;
                            if (isNotNullAndEmpty(state) && isNotNullAndEmpty(country)) {
                                searchLocation = state + ", " + country;
                            } else {
                                if (isNotNullAndEmpty(state)) {
                                    searchLocation = state;
                                } else {
                                    searchLocation = country;
                                }
                            }
                            $('#Location').val(searchLocation);
                            var suggestion = {
                                value: searchLocation,
                                data: city + "| " + state + "| " + country
                            };
                            loadTourByDestination(suggestion);
                        }
                    }
                }
            }
        }
    }

    initialize();
    prefillInfo();
});