﻿var windowProxy;
var proxyUrl = 'https://www.razorgator.com/proxy.html';

jQuery(document).ready(function () {
    var mapContainer = $("#MapContainer");
    var dataSeatingCharts = {
        MinPrice: 0,
        MaxPrice: 0,
        TicketCounts: 0,
        Tickets: []
    };
    var selectedTicket = null; // {TicketId, Quantity, Price}
    var ticketInfo = {};
    var selectedShippingMethod = null; // {CategoryName, ShippingMethodId, ShippingMethodName, Cost}
    var selectedPerformace = null; // {Id, Name, DateTime, VenueId, Vennue: {Name, City, Country}}
    var tblTicketList = null;

    var NO_TIME_TEXT = 'TBD';

    var listOfEvents = null;
    var loadMoreSize = 20;
    var maxItemDisplayed = 0;
    var SIZE_TO_CALCULATE_AVERAGE = 40;
    var lastScrollTop = 0;
    var currentPageNo = 0;
    var averageHeight = 0;
    var cssMaxHeight = 0;

    var isoCountries = null;
    var caStates = null;
    var usStates = null;

    $("#NameOfEvent").on("onSelectScroll", function (event) {
        OnSelectScroll(event);
    });

    $('.fr-booking').validation({
        txtContactNumber: {
            name: 'mobileNumber'
        },
        txtEmail: {
            name: 'emailAddress'
        },
        txtExpiryDate: {
            name: 'CardExpiredDate'
        }
    });
    Booking.getValueCardExpiryDate("fr-booking", "CardExpiredDate");

    initialize();

    $(".nameOfLocation").autocomplete({
        serviceUrl: "/sm/DCRApi/LocationAutoComplete",
        params: { limit: 10, apiItemType: 'event' },
        minChars: 3,
        appendTo: $(".nameOfLocation").parent(),
        autoSelectFirst: true,
        showNoSuggestionNotice: true,
        onSelect: function (suggestion) {
            clearEvent();
            var locationInfo = suggestion.data.split("|");
            if (locationInfo.length == 3) {
                $("#EventCity").val(locationInfo[0].trim());
                var state = null;
                var country = null;
                var stateName = locationInfo[1].trim();
                var countryName = locationInfo[2].trim();
                if (isoCountries != null) {
                    $.each(isoCountries, function (index, value) {
                        if (value.Name == countryName) {
                            country = value.Code;
                        }
                    });
                }
                if (usStates != null && caStates != null) {
                    var allStates = usStates.concat(caStates);
                    $.each(allStates, function (index, value) {
                        if (value.Name == stateName) {
                            state = value.Code;
                        }
                    });
                }
                $("#EventState").val(state);
                $("#EventCountry").val(country);
            }
            $.ajax({
                url: "/sm/DCRApi/SearchEvents",
                type: "POST",
                data: { location: suggestion.data },
                beforeSend: function () {
                    $("body").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
                },
                complete: function () {
                    $("#ajaxLoading").remove();
                },
                success: function (response) {
                    listOfEvents = response.items;
                    buildListOfEvents();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
        },
        onSearchComplete: function (query, suggestions) {
            $(this).siblings(".autocomplete-suggestions").addClass('scrollbar-outer');
            $(this).siblings(".autocomplete-suggestions").scrollbar();
        }
    });

    $("#NameOfEvent").change(function () {
        var eventId = $(this).val();

        var selectedEvent = $(this).find('option:selected');
        if (typeof (selectedEvent) !== 'undefined' && selectedEvent != null) {
            $(".infoEventName").text(selectedEvent.text());
        } else {
            $(".infoEventName").text('');
        }
        $("#DisplayEventName").val($(".infoEventName").text());

        clearPerformance();
        clearPerformanceSummary();

        if (eventId == "" || eventId == null) {
            return;
        }

        $.ajax({
            url: "/sm/DCRApi/SearchPerformances",
            type: "POST",
            data: { eventId: eventId },
            beforeSend: function () {
                $("body").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
            },
            complete: function () {
                $("#ajaxLoading").remove();
            },
            success: function (response) {
                if (typeof (response) === 'undefined' || response == null || response == '') {
                    return;
                }
                $.each(response.items, function (i, item) {
                    var eventDateTime = getJsonDateAndTime(item.DateTime, 'MMM D, YYYY', 'h:mm A');
                    var strEventDateTime = eventDateTime[0] + ' ' + eventDateTime[1];
                    $('#PerformaceId').append($('<option>', {
                        value: item.Id,
                        text: strEventDateTime + " - " + item.Name,
                        datetime: item.DateTime,
                        venueId: item.VenueId,
                    }));

                    rebuildSelection($('#PerformaceId'));

                    var prefillPerformanceId = $('#PrefillPerformanceId').val();
                    if (prefillPerformanceId && prefillPerformanceId.length > 0) {
                        $('#PerformaceId').val(prefillPerformanceId);
                        $('#PerformaceId').change();
                        $('#PerformaceId').multiselect('refresh');
                    }
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    });

    function getJsonDateAndTime(strJsonTime, dateFormat, timeFormat) {
        var performanceTime = moment(strJsonTime);
        var timeZoneDirection = strJsonTime.substring(strJsonTime.length - 7, strJsonTime.length - 6);
        if (timeZoneDirection === '-' || timeZoneDirection === '+') {
            var timeZone = strJsonTime.substring(strJsonTime.length - 7, strJsonTime.length - 4)
                + ':' + strJsonTime.substring(strJsonTime.length - 4, strJsonTime.length - 2);
            performanceTime = performanceTime.utcOffset(timeZone);
        }
        var strPerformanceTime = performanceTime.format(timeFormat);
        if (strPerformanceTime === '12:00 AM') {
            strPerformanceTime = NO_TIME_TEXT;
        }
        return [performanceTime.format(dateFormat), strPerformanceTime]
    }

    $("#PerformaceId").change(function (e) {
        clearDateTime();

        var peformanceId = $(this).val();
        if (peformanceId == null || peformanceId == "") {
            return;
        }

        var element = $(this).find('option:selected');
        var strJsonTime = $(element).attr("datetime");

        // date and time used for displaying on client
        var jsonDateAndTime = getJsonDateAndTime(strJsonTime, 'dddd MMM D YYYY', 'h:mm A');
        var eventDateInfo = jsonDateAndTime[0];
        var eventTimeInfo = jsonDateAndTime[1];

        selectedPerformace = {
            Id: peformanceId,
            Name: $(this).text(),
            DateTime: strJsonTime,
            VenueId: $(element).attr("venueId"),
        };

        $("#Date").val(eventDateInfo);
        $("#Time").val(eventTimeInfo);

        // Set summary
        $(".infoDate").text(eventDateInfo);
        $(".infoTime").text(eventTimeInfo);

        // date and time used for submitting to server
        var eventDateAndTime = getJsonDateAndTime(strJsonTime, 'YYYY-MM-DD', 'HH:mm');
        $("#EventDate").val(eventDateAndTime[0]);
        $("#EventTime").val(eventDateAndTime[1]);

        $.ajax({
            url: "/sm/DCRApi/GetVenue",
            type: "POST",
            data: { venueId: selectedPerformace.VenueId },
            asyn: false,
            beforeSend: function () {
                $("body").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
            },
            complete: function () {
                $("#ajaxLoading").remove();
            },
            success: function (response) {
                selectedPerformace.Vennue = {
                    Name: response.VenueName,
                    City: response.City,
                    Country: response.Country
                };

                // Set summary
                $(".infoVenue").text(selectedPerformace.Vennue.Name);
                $("#btnModalTicket").removeClass("disabled");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    });

    $(document).on("click", "#btnModalTicket", function (e) {
        if (selectedPerformace == null) {
            e.preventDefault();
            return false;
        }

        var performanceInfo = {
            Name: selectedPerformace.Name,
            Venue: selectedPerformace.Vennue.Name,
            DateTime: eval("new " + selectedPerformace.DateTime.slice(1, -1)),
            PerformanceId: selectedPerformace.Id,
        };
        var tixWrapperRequest = $.Deferred();

        var jsonDateAndTime = getJsonDateAndTime(selectedPerformace.DateTime, 'dddd MMM D YYYY', 'h:mm A');
        $("#modalDate").text(jsonDateAndTime[0]);
        $("#modalFullTime").text(jsonDateAndTime[1]);

        $("#modalCountryName").text(selectedPerformace.Vennue.Country);
        $("#modalLocation").text(selectedPerformace.Vennue.Name);
        $("#modalCityName").text(selectedPerformace.Vennue.City);

        $("#ajax_loader_table").show();
        $("#ttikketTable tbody").empty();
        mapContainer.empty();

        $.ajax({
            url: "/sm/DCRApi/GetTicketByPerformanceId",
            type: "POST",
            data: { performanceId: performanceInfo.PerformanceId },
            success: function (rs) {
                if (rs != "") {
                    var jsonParseTickets = JSON.parse(rs);
                    var tickets = ConvertToRzgMapsTickets(jsonParseTickets);
                    dataSeatingCharts.Tickets = tickets;
                    dataSeatingCharts.TicketCounts = tickets.length;

                    //Convert data
                    dataSeatingCharts.Tickets.forEach(function (item, index) {
                        //Get Max Price
                        if (item.Price > dataSeatingCharts.MaxPrice) {
                            dataSeatingCharts.MaxPrice = item.Price;
                        }
                        //Get min Price
                        if (index == 0) {
                            dataSeatingCharts.MinPrice = item.Price;
                        }
                        if (dataSeatingCharts.MinPrice > item.Price) {
                            dataSeatingCharts.MinPrice = item.Price;
                        }
                    });

                    tixWrapperRequest.resolve(dataSeatingCharts.Tickets);
                    RzgMaps.CreateMap(performanceInfo, tixWrapperRequest, mapContainer, onUpdateCallback);
                }

                setTimeout(function () { $("#ajax_loader_table").hide(); }, 1000);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    });

    $("#Quantity").change(function (e) {
        if ($(this).val() == "") {
            return;
        }

        selectedTicket.Quantity = $(this).find('option:selected').val();
        updateTotalSummary();
    });

    $("#ShippingMethod").change(function () {
        var selectedShippingMethodId = $(this).val();
        if (selectedShippingMethodId) {
            if (ticketInfo.AvailableShippingMethods) {
                ticketInfo.AvailableShippingMethods.forEach(function (item) {
                    if (item.ShippingMethodId == selectedShippingMethodId) {
                        selectedShippingMethod = item;
                    }
                });
            }
            updateTotalSummary();
        }
    });

    $("#ShippingAddress_Country").change(function () {
        var shippingCountry = $(this).val();
        var shippingCountryName = $('#ShippingAddress_Country :selected').text();
        getStateForCountry(shippingCountry, setShippingState);

        if (selectedTicket != null) {
            getListingInfo(selectedTicket.TicketId, selectedTicket.Quantity, shippingCountry);
        }
    });

    $("#BillingAddress_Country").change(function () {
        var billingCountry = $(this).val();
        var billingCountryName = $('#BillingAddress_Country :selected').text();
        getStateForCountry(billingCountry, setBillingState);
    });

    $('#btnPlaceRequest').click(function () {
        checkout();
    });

    function initialize() {
        windowProxy = new Porthole.WindowProxy(proxyUrl, 'ifRazogator');
        windowProxy.addEventListener(onPlaceOrderComplete);

        loadISOCountries();
        loadStatesToList('US');
        loadStatesToList('CA');

        var shippingCountry = $("#ShippingAddress_Country").val();
        var shippingCountryName = $('#ShippingAddress_Country :selected').text();
        if (shippingCountryName) {
            getStateForCountry(shippingCountry, setShippingState);
        }

        var billingCountry = $("#BillingAddress_Country").val();
        var billingCountryName = $('#BillingAddress_Country :selected').text();
        if (billingCountryName) {
            getStateForCountry(billingCountry, setBillingState);
        }

        clearSummary();

        $("#btnModalTicket").addClass("disabled");

        //Init datatable
        tblTicketList = $('#ttikketTable').DataTable({
            "aaData": dataSeatingCharts.Tickets,
            paging: false,
            searching: false,
            "scrollY": "377px",
            "info": false,
            "scrollCollapse": true,
            "destroy": true,
            "processing": true,
            "aoColumns": [
                { "mData": "Section" },
                { "mData": "Row" },
                { "mData": "Quantities" },
                { "mData": "Price" }
            ],
            "columnDefs": [
                {
                    // The `data` parameter refers to the data for the cell (defined by the
                    // `data` option, which defaults to the column being worked with, in
                    // this case `data: 0`.
                    "render": function (data, type, row) {
                        var id = row.RzgTicketId;
                        return '<a class="BuyLink" lang=' + id + '>Buy</a>';
                    },
                    "targets": 4,
                    //"width": "20%"
                },
                {
                    "render": function (data, type, row) {
                        var strQuantity = '<select lang="Quantity" id="' + row.Id + '">';
                        var quanlitySort;
                        if (data != null) {
                            quanlitySort = data.sort(function (a, b) { return a - b });
                            quanlitySort.forEach(function (item, index) {
                                var option = '<option value = "' + item + '">' + item + '</option>';
                                strQuantity += option;
                            });
                        }

                        strQuantity = strQuantity + "</select>";
                        var id = row.RzgTicketId;
                        return strQuantity;
                    },
                    "targets": 2
                },
                {
                    "render": function (data, type, row) {
                        return '$' + data;
                    },
                    "targets": 3
                }
            ]
        });
        reTitleMultipleSelect('ShippingAddress_Country');
        reTitleMultipleSelect('BillingAddress_Country');
    };

    function clearSummary() {
        $(".infoEventName").text('');
        clearPerformanceSummary();
    };

    function clearPerformanceSummary() {
        $(".infoDate").text("");
        $(".infoTime").text("");
        $(".infoVenue").text("");
        $(".infoSection").text("");
        $(".infoRow").text("");
        $(".infoPrice").text("");
        $(".infoFee").text("");

        $("#Quantity").empty();
        rebuildSelection($("#Quantity"));

        $(".infoTickets").text("");
        $(".infoConnectFee").text("");
        $(".infoShipping").text("");
        $(".infoTotal").text("");
    }

    function clearEvent() {
        clearPerformance();
        clearSummary();

        listOfEvents = null;
        lastScrollTop = 0;
        currentPageNo = 0;
        averageHeight = 0;

        $("#NameOfEvent").empty();
        $('#NameOfEvent').append($('<option>', {
            value: "",
            text: ""
        }));
        rebuildSelection($("#NameOfEvent"));
    }

    function clearPerformance() {
        clearDateTime();

        selectedPerformace = null;
        $("#PerformaceId").empty();
        $('#PerformaceId').append($('<option>', {
            value: "",
            text: ""
        }));
        rebuildSelection($("#PerformaceId"));
    }

    function clearDateTime() {
        $("#Date").val("");
        $("#Time").val("");
        $(".infoDate").text('');
        $(".infoTime").text('');

        $("#btnModalTicket").addClass("disabled");
    }

    function onUpdateCallback(tickets) {
        var showingTickets = dataSeatingCharts.Tickets.filter(function (item) {
            return tickets.some(function (x) {
                return x.Id == item.Id;
            });
        });

        tblTicketList.clear().rows.add(showingTickets).draw();
        // Add scroll bar table
        $("#ttikketTable").parent().addClass('scrollbar-outer');
        $("#ttikketTable").parent().scrollbar();
        //Hover item table
        $("#ttikketTable").find("tr").on("mouseenter", function () {
            var id = $(this).find("a").attr("lang");
            if (id != undefined) {
                RzgMaps.HighlightTicket(id);
            }
        });

        $("#ttikketTable").find("tr").on("mouseleave", function () {
            RzgMaps.RemoveHighlight();
        });

        $(".BuyLink").on("click", function () {
            var ticketId = $(this).attr("lang");
            var section = $(this).parent().parent().find("td:nth-child(1)").html();
            var row = $(this).parent().parent().find("td:nth-child(2)").html();
            var price = $(this).parent().parent().find("td:nth-child(4)").html().replace("$", "");
            var quantity = $(this).parent().parent().find("td:nth-child(3)").find("select").val();
            buyTicket(ticketId, section, row, price, quantity);
        });
    }

    function updateTotalSummary() {
        var grandTotalPrice = (ticketInfo.Price + ticketInfo.Fee) * selectedTicket.Quantity + selectedShippingMethod.Cost;

        $(".infoFee").text(ticketInfo.Fee);

        $(".infoTickets").text(selectedTicket.Quantity + " x " + ticketInfo.Price.toString() + "$");
        $(".infoConnectFee").text(selectedTicket.Quantity + " x " + ticketInfo.Fee.toString() + "$");
        $(".infoShipping").text(selectedShippingMethod.Cost.toString() + "$");
        $(".infoTotal").text(grandTotalPrice.toString());
    }

    function buyTicket(ticketId, section, row, price, quantity) {
        $("#modalTickets").modal("hide");

        selectedTicket = {
            TicketId: ticketId,
            Quantity: quantity,
            Price: price,
        }

        $(".infoSection").text(section);
        $(".infoRow").text(row);
        $(".infoPrice").text(price);
        $(".infoFee").text("");
        $("#Quantity").empty();

        $("#PrefSeating").val(section + " - " + row);

        var countryCode = $("#ShippingAddress_Country").val();
        if (countryCode == "") {
            countryCode = "US";
        }
        if (ticketId && quantity) {
            getListingInfo(ticketId, quantity, countryCode);
        }
    }

    function getListingInfo(ticketId, quantity, countryCode) {
        var shippingMethodSelect = $("#ShippingMethod"),
            infoQuantitySelect = $("#Quantity"),
            grandTotalPrice = 0,
            op1 = "",
            op = "";
        if (ticketId && ticketId != "") {
            $.ajax({
                url: "/sm/DCRApi/GetListingInfo",
                type: "POST",
                data: { ticketId: ticketId, quantity: quantity, countryCode: countryCode },
                beforeSend: function () {
                    $("body").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
                },
                complete: function () {
                    $("#ajaxLoading").remove();
                },
                success: function (data) {
                    if (data.Success == false) {
                        App.getPlaceholderSelfFormByValue(data.ErrorMessage, 'event', function (errorMsg) { alert(errorMsg); });
                        //alert(data.ErrorMessage);
                        return;
                    }

                    ticketInfo = data;
                    shippingMethodSelect.empty();
                    op1 = $("<option value='0'> Please select shipping method </option>");
                    shippingMethodSelect.append(op1);

                    ticketInfo.AvailableShippingMethods.forEach(function (item, i) {
                        op1 = new Option(item.ShippingMethodName + " - $" + item.Cost, item.ShippingMethodId);
                        if (i == 0) {
                            op1.selected = true;
                            selectedShippingMethod = item;
                        }

                        shippingMethodSelect.append(op1);
                    });

                    shippingMethodSelect.multiselect('rebuild');
                    updateTotalSummary();

                    //set Available Quantities
                    infoQuantitySelect.empty();
                    op = $("<option value='" + 0 + "'>" + 0 + "</option>")
                    infoQuantitySelect.append(op);
                    if (ticketInfo.AvailableQuantities) {
                        //sort AvailableQuantities
                        var quanlitySort = ticketInfo.AvailableQuantities.sort(function (a, b) { return a - b });
                        quanlitySort.forEach(function (num) {
                            op = new Option(num, num);
                            if (num == quantity) {
                                op.selected = true;
                            }

                            infoQuantitySelect.append(op);
                        });

                        $('#Quantity').multiselect('rebuild');
                        $('#Quantity').next().find(".multiselect-selected-text").css({ "color": "rgb(85, 85, 85)" });
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
        }
    }

    function rebuildSelection($select) {
        $select.multiselect('rebuild');
        $select.next().find(".multiselect-selected-text").css({ "color": "rgb(85, 85, 85)" });
    };

    function buildListOfEvents() {
        var container = $('#NameOfEvent').next().find(".container-multiselect");
        cssMaxHeight = container.css("max-height").replace(/[^0-9]/g, '');
        currentPageNo = 1;
        LoadNextSetOfOptions(container, currentPageNo);
    }

    function OnSelectScroll(event) {
        var container = $('#NameOfEvent').next().find(".container-multiselect");
        var scrTop = container.scrollTop();
        var totalheight = 0;
        var allOptions = container.find("li");
        if (typeof (allOptions) !== 'undefined' && allOptions != null && allOptions.length > 0) {
            if (averageHeight <= 0 || allOptions.length <= SIZE_TO_CALCULATE_AVERAGE) {
                $.each(allOptions, function (i) {
                    totalheight += $(this).height()
                });
                averageHeight = Math.ceil(totalheight / allOptions.length);
            } else {
                totalheight = averageHeight * allOptions.length;
            }
        }

        var maxItemDisplayed = loadMoreSize;
        if (typeof (cssMaxHeight) !== 'undefined' && cssMaxHeight != null && cssMaxHeight.length > 0) {
            var fMaxHeight = parseFloat(cssMaxHeight);
            if (typeof (fMaxHeight) !== 'undefined' && fMaxHeight != null && fMaxHeight > 0) {
                maxItemDisplayed = Math.round(fMaxHeight / averageHeight);
            }
        }

        if (scrTop > lastScrollTop) {
            if ((scrTop + (maxItemDisplayed * averageHeight)) >= totalheight) {
                if (LoadNextSetOfOptions(container, currentPageNo + 1)) {
                    currentPageNo++;
                }
            }
        }
        lastScrollTop = scrTop;
    }

    function LoadNextSetOfOptions(container, pageNo) {
        if (typeof (listOfEvents) === 'undefined' || listOfEvents == null || listOfEvents.length == 0) {
            return false;
        }
        var startOption = ((pageNo - 1) * loadMoreSize);
        var endOption = startOption + loadMoreSize;
        if (startOption > listOfEvents.length) {
            return false;
        }
        var ddlEvents = $("#NameOfEvent");
        endOption = endOption > listOfEvents.length ? listOfEvents.length : endOption;

        for (i = startOption; i < endOption; i++) {
            ddlEvents.append($('<option>', {
                value: listOfEvents[i].SourceId,
                text: listOfEvents[i].Name
            }));
        }
        rebuildSelection(ddlEvents);
        return true;
    }

    // Function to convert checkout's tickets to RzgMap.Ticket
    function ConvertToRzgMapsTickets(tix) {
        return tix.map(function (ticket) {
            return {
                Section: ticket.Section,
                Row: ticket.Row,
                Id: ticket.Id,
                Quantities: ticket.AvailableBlocks,
                Price: ticket.DisplayPrice,
                MaxQty: ticket.AvailableBlocks[0],
                IsDigital: ticket.IsDigital,
                RzgTicketId: ticket.Id,
                Notes: ticket.DisplayNotes
            };
        });
    }

    function setShippingState(states) {
        buildStates($("#ShippingAddress_State"), states);
    }

    function setBillingState(states) {
        buildStates($("#BillingAddress_State"), states);
    }

    function buildStates(ddState, states) {
        ddState.empty();
        var op = $("<option value=''>- " + PlaceHolderShippingAddressState + " -</option>")
        ddState.append(op);
        if (states) {
            states.forEach(function (state) {
                op = new Option(state.Name, state.Code);
                ddState.append(op);
            });
        } else {
            op = new Option(PlaceHolderShippingAddressStateNonUS, "XX");
            op.selected = true;
            ddState.append(op);
            ddState.val("XX");
        }

        ddState.multiselect('rebuild');
    }

    function getStateForCountry(vCountryCode, callback) {
        if (vCountryCode) {
            if (vCountryCode == 'US' || vCountryCode == 'CA') {
                if (vCountryCode == 'US') {
                    callback(usStates);
                } else if (vCountryCode == 'CA') {
                    callback(caStates);
                }
            } else {
                callback(null);
            }
        }
    }

    function loadStatesToList(vCountryCode) {
        $.ajax({
            type: 'GET',
            url: $('#txtGetStateUrl').val(),
            data: { countryCode: vCountryCode },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                var jsonData = JSON.stringify(result);
                var jsonParse = JSON.parse(jsonData);
                if (vCountryCode == 'US') {
                    usStates = jsonParse;
                } else if (vCountryCode == 'CA') {
                    caStates = jsonParse;
                }
            },
            error: function (result) {
                console.log('Fail to get states for country' + vCountryCode);
            }
        });
    }

    function loadISOCountries() {
        $.ajax({
            type: 'GET',
            url: "/sm/DCRApi/GetISOCountries",
            success: function (result) {
                isoCountries = result.Countries;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }

    function checkout() {
        var bookingForm = $("#fr-booking"),
            email,
            cardType,
            cardNumber,
            cardExpMonth,
            cardExpYear,
            billingFirstName,
            billingLastName,
            billingCompany,
            billingStreet,
            billingCity,
            billingCountry,
            billingState,
            billingPostalCode,
            billingPhone,
            shippingMethodId,
            shippingFirstName,
            shippingLastName,
            shippingCompany,
            shippingStreet,
            shippingCity,
            shippingState,
            shippingCountry,
            shippingPostalCode,
            shippingPhone,
            expireDate,
            useShippingAddress = false,
            GrandTotalPrice = 0,
            OrderId,
            TotalOrder,
            securityCode,

            reservationName,
            reservationPhoneNumber,
            reservationEmail;

        if (selectedTicket == null) {
            alert(PlaceholderSelectTicket);
            return;
        }
        bookingForm.validate();
        if (bookingForm.valid()) {
            cardNumber = $('#CardNumber').val().trim();
            expireDate = $('#CardExpiredDate').val();
            if (expireDate) {
                cardExpMonth = expireDate.split('/')[0];
                cardExpYear = expireDate.split('/')[1];
                if (cardExpYear && cardExpYear.length == 2) {
                    cardExpYear = "20" + cardExpYear;
                }
            }
            securityCode = $('#SecurityCode').val().trim();
            shippingFirstName = $('#ShippingAddress_FirstName').val();
            shippingLastName = $('#ShippingAddress_LastName').val();
            shippingCompany = $('#ShippingAddress_CompanyName').val();
            shippingPhone = $('#ShippingAddress_PhoneNumber').val();
            shippingStreet = $('#ShippingAddress_Address').val();
            shippingCity = $('#ShippingAddress_City').val();
            shippingCountry = $('#ShippingAddress_Country').val();
            shippingState = $('#ShippingAddress_State').val();
            shippingPostalCode = $('#ShippingAddress_ZipCode').val();
            shippingMethodId = parseInt($("#ShippingMethod").val());

            useShippingAddress = $('#IsUserShippingAddress').is(':checked');

            if (useShippingAddress) {
                billingFirstName = shippingFirstName;
                billingLastName = shippingLastName;
                billingCompany = shippingCompany;
                billingStreet = shippingStreet;
                billingCity = shippingCity;
                billingCountry = shippingCountry;
                billingState = shippingState;
                billingPostalCode = shippingPostalCode;
                billingPhone = shippingPhone;

                $('#BillingAddress_FirstName').val(shippingFirstName);
                $('#BillingAddress_LastName').val(shippingLastName);
                $('#BillingAddress_CompanyName').val(shippingCompany);
                $('#BillingAddress_Address').val(shippingStreet);
                $('#BillingAddress_City').val(shippingCity);
                $('#BillingAddress_ZipCode').val(shippingPostalCode);
                $('#BillingAddress_PhoneNumber').val(shippingPhone);

                $('#BillingAddress_Country').val(shippingCountry);
                $('#BillingAddress_Country').change();
                $('#BillingAddress_Country').multiselect('refresh');

                $('#BillingAddress_State').val(shippingState);
                $('#BillingAddress_State').change();
                $('#BillingAddress_State').multiselect('refresh');
            } else {
                billingFirstName = $('#BillingAddress_FirstName').val();
                billingLastName = $('#BillingAddress_LastName').val();
                billingCompany = $('#BillingAddress_CompanyName').val();
                billingStreet = $('#BillingAddress_Address').val();
                billingCity = $('#BillingAddress_City').val();
                billingCountry = $('#BillingAddress_Country').val();
                billingState = $('#BillingAddress_State').val();
                billingPostalCode = $('#BillingAddress_ZipCode').val();
                billingPhone = $('#BillingAddress_PhoneNumber').val();
            }

            reservationName = $('#ReservationName').val();
            var phoneNumberCountryCode = $('#phoneCountryCode').val();
            var reservationPhoneNumber = phoneNumberCountryCode + $('#MobileNumber').val();
            reservationEmail = $('#Email').val();
            numQuantity = parseInt($("#Quantity").val());

            cardType = 'visa';

            $.ajax({
                url: "/sm/DCRApi/GetPartnerGuid",
                type: "POST",
                success: function (data) {
                    if (data && data.length > 0) {
                        partnerGuid = data;

                        var orderInfo = {
                            TicketId: selectedTicket.TicketId,
                            Quantity: numQuantity,
                            ShippingMethodId: shippingMethodId,
                            PartnerGuid: partnerGuid,
                            Email: reservationEmail,
                            CardType: cardType,
                            CardNumber: cardNumber,
                            CardExpMonth: parseInt(cardExpMonth),
                            CardExpYear: parseInt(cardExpYear),
                            CardCvNumber: securityCode,
                            BillingFirstName: billingFirstName,
                            BillingLastName: billingLastName,
                            BillingCompany: billingCompany,
                            BillingStreet: billingStreet,
                            BillingCity: billingCity,
                            BillingState: billingState,
                            BillingPostalCode: billingPostalCode,
                            BillingCountry: billingCountry,
                            BillingPhone: billingPhone,
                            BillingCellPhone: billingPhone,
                            ShippingFirstName: shippingFirstName,
                            ShippingLastName: shippingLastName,
                            ShippingCompany: shippingCompany,
                            ShippingStreet: shippingStreet,
                            ShippingCity: shippingCity,
                            ShippingState: shippingState,
                            ShippingPostalCode: shippingPostalCode,
                            ShippingCountry: shippingCountry,
                            ShippingPhone: shippingPhone,
                            ShippingCellPhone: shippingPhone
                        };
                        windowProxy.post({ "Type": "OrderSubmission", "Parameters": orderInfo });
                    } else {
                        alert(PlaceholderPartnerIssue);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                    alert(PlaceholderPartnerIssue);
                }
            });
        }
        return false;
    }

    function onPlaceOrderComplete(data) {
        if (data && data.data.Parameters && data.data.Parameters.Errors && data.data.Parameters.Errors.length > 0) {
            showSubmissionErrorMessage();
            var message = data.data.Parameters.Errors[0];
            App.getPlaceholderSelfFormByValue(message, 'event', function (errorMsg) { alert(errorMsg); });
            //alert(message);
            document.getElementById('ifRazogator').src = "https://checkout.razorgator.com/Checkout?tid=CxhsajG7LtWSo4bcBerCpdiDJKcstyLA&quantity=4&partnerGuid=C3B12670-1BCF-4E0A-9924-106CC6546C2E&parentDomain=www.seatgeek.com";
        } else {
            if (data.data.Type == "OrderPlacement") {
                if (data.data.Parameters.OrderId != -1) {
                    $("#ticketId").val(selectedTicket.TicketId);
                    var bookingForm = $("#fr-booking");
                    var bookingInfo = bookingForm.serializeObject();
                    bookingInfo["OrderId"] = data.data.Parameters.OrderId;
                    $.ajax({
                        url: bookingForm.attr('action'),
                        type: "POST",
                        dataType: "json",
                        data: JSON.stringify(bookingInfo),
                        contentType: "application/json;charset=utf-8",
                        beforeSend: function () {
                            $("body").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
                        },
                        complete: function () {
                            $("#ajaxLoading").remove();
                        },
                        success: function (response) {
                            if (typeof (response.Result) !== 'undefined' && response.Result != null) {
                                if (response.Result.IsSuccess == true) {
                                    redirectToPath(response.Result.Path);
                                } else {
                                    console.log("Problem in server side");
                                }
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(textStatus, errorThrown);
                        }
                    });
                } else {
                    showSubmissionErrorMessage();
                    document.getElementById('ifRazogator').src = "https://checkout.razorgator.com/Checkout?tid=CxhsajG7LtWSo4bcBerCpdiDJKcstyLA&quantity=4&partnerGuid=C3B12670-1BCF-4E0A-9924-106CC6546C2E&parentDomain=www.seatgeek.com";
                }
            }
        }
    }

    prefillInfo();

    function prefillInfo() {
        var prefillLocation = $('#PrefillLocation').val();
        if (prefillLocation && prefillLocation.length > 0) {
            $('#Location').val(prefillLocation);
        }
        var prefillEventId = $('#PrefillEventId').val();
        var prefillEventName = $('#PrefillEventName').val();
        if (prefillEventId && prefillEventId.length > 0
            && prefillEventName && prefillEventName.length > 0) {
            $('#NameOfEvent').append($('<option>', {
                value: prefillEventId,
                text: prefillEventName
            }));
            rebuildSelection($('#NameOfEvent'));
            $('#NameOfEvent').val(prefillEventId);
            $('#NameOfEvent').change();
        }
    }

    function reTitleMultipleSelect(id) {
        $('#' + id).siblings('.btn-group.btn-group-selectbox').find('button.multiselect ').attr('title', id);
    }
});