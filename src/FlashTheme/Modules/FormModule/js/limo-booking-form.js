﻿jQuery(document).ready(function () {
    $('#divSearchResult').hide();
    var SERVICE_TYPE_TO_AIRPORT = 100;
    var SERVICE_TYPE_FROM_AIRPORT = 99;
    var SERVICE_TYPE_ONE_WAY = 131072;
    var SERVICE_TYPE_HOURLY = 4096;
    var DEFAULT_SERVICE_TYPE = SERVICE_TYPE_TO_AIRPORT.toString();
    var LIMO_PAGE_SIZE = 2;
    var AIRPORT_FILTER_SZIE = 10;
    var DEFAULT_COUNTRY = 'US';
    var ATTR_AIRPORT_FULLNAME = 'airportFullName';

    var listOfLimoSearchResult = null;
    var currentPage = 1;

    var allStates = null;
    var allAirports = null;
    var filterAirports = null;

    var isValidVehicleType = false;

    function getCountryCodeForSearching(ddlCountry) {
        var countryCode = ddlCountry.val();
        if (isNorthAmerican(countryCode)) {
            countryCode = '';
        } else {
            if (isNonEmptyString(countryCode) == true) {
                countryCode = "," + countryCode;
            } else {
                countryCode = '';
            }
        }
        return countryCode;
    }

    function validateSearchFields() {
        var serviceType = $("#ServiceType").val();
        var rideDate = $("#ReservationDate").datepicker("getDate");
        var pickupTime = $("#PickupTime").val();
        var dropOffTime = $("#DropOffTime").val();
        var pickupPlace = null;
        var dropOffPlace = null;
        var numberOfPassengers = $("#NumberOfPassengers").val()
        var arrVehicleTypes = $("#VehicleType").val();
        var errorMessage = '';

        var vehicleTypes = (arrVehicleTypes != null && arrVehicleTypes.length > 0) ?
            arrVehicleTypes.join(',') : null;

        if (isNonEmptyString(serviceType) == false) {
            serviceType = DEFAULT_SERVICE_TYPE;
            $("#ServiceType").val(serviceType);
            refreshSelectionChange($("#ServiceType"), true);
        }

        if (rideDate != null && rideDate != '' && rideDate != undefined) {
            rideDate = moment(rideDate).format('M/D/YYYY');
            $("#PickupDate").val(moment(rideDate).format('YYYY-MMM-DD'));
        } else {
            rideDate = '';
        }

        var pickupCountry = getCountryCodeForSearching($('#PickUpCountry'));
        var dropOffCountry = getCountryCodeForSearching($('#DropOffCountry'));

        if (serviceType == SERVICE_TYPE_FROM_AIRPORT.toString()) {
            pickupPlace = $('#PickupAirport').val();
            dropOffPlace = $('#DropOffLocation').val() + dropOffCountry;
        } else if (serviceType == SERVICE_TYPE_TO_AIRPORT.toString()) {
            pickupPlace = $('#PickupLocation').val() + pickupCountry;
            dropOffPlace = $("#DropOffAirport").val();
        } else if (serviceType == SERVICE_TYPE_ONE_WAY.toString()) {
            pickupPlace = $('#PickupLocation').val() + pickupCountry;
            dropOffPlace = $('#DropOffLocation').val() + dropOffCountry;
        } else if (serviceType == SERVICE_TYPE_HOURLY.toString()) {
            pickupPlace = $('#PickupLocation').val() + pickupCountry;
            dropOffPlace = null;
        }

        if (isNonEmptyString(vehicleTypes) == false) {
            isValidVehicleType = false;
            $('#VehicleType-Validator').show();
            $('#VehicleType').siblings('div').find('button:first').css('border', '1px solid #cb4f53');
            return { isSuccess: false, message: 'Please select at least one vehicle type' };
        }
        else {
            isValidVehicleType = true;
            $('#VehicleType').siblings('div').find('button:first').css('border', '1px solid #adaeb0');
            $('#VehicleType-Validator').hide();
        }

        if (isNonEmptyString(pickupPlace) == false) {
            if (serviceType == SERVICE_TYPE_FROM_AIRPORT.toString()) {
                errorMessage = PlaceholderSelectPickupAirport;
            } else {
                var strPickupLocation = $('#PickupLocation').val();
                if (strPickupLocation == null || strPickupLocation == '') {
                    errorMessage = PlaceholderEnterPickupPlace;
                }
            }

            return { isSuccess: false, message: errorMessage };
        }

        if (serviceType != SERVICE_TYPE_HOURLY.toString()) {
            if (isNonEmptyString(dropOffPlace) == false) {
                if (serviceType == SERVICE_TYPE_TO_AIRPORT.toString()) {
                    errorMessage = PlaceholderSelectDropoffAirport;
                } else {
                    var strDropOffLocation = $('#DropOffLocation').val();
                    if (strDropOffLocation == null || strDropOffLocation == '') {
                        errorMessage = PlaceholderEnterDropoffPlace;
                    }
                }
                return { isSuccess: false, message: errorMessage };
            }
        }

        if (isNonEmptyString(rideDate) == false) {
            return { isSuccess: false, message: PlaceholderSelectPickupDate };
        }

        if (isNonEmptyString(pickupTime) == false) {
            return { isSuccess: false, message: PlaceholderSelectPickupTime };
        }

        if (serviceType == SERVICE_TYPE_HOURLY.toString()) {
            if (isNonEmptyString(dropOffTime) == false) {
                return { isSuccess: false, message: PlaceholderSelectDropoffTime };
            }
        }

        if (isNonEmptyString(numberOfPassengers) == false || numberOfPassengers == '0' || parseInt(numberOfPassengers) > 51) {
            return { isSuccess: false, message: PlaceholderPsgBiggerThan0 };
        }

        if (parseInt(numberOfPassengers) > 51) {
            return { isSuccess: false, message: PlaceholderMaxPsg50 };
        }
        return {
            isSuccess: true, message: '', searchInfo: {
                serviceType: serviceType,
                rideDate: rideDate,
                pickupTime: pickupTime,
                dropOffTime: dropOffTime,
                pickupPlace: pickupPlace,
                dropOffPlace: dropOffPlace,
                numberOfPassengers: numberOfPassengers,
                vehicleTypes: vehicleTypes
            }
        }
    }

    function isNonEmptyString(str) {
        return (typeof (str) !== 'undefined' && str != null && str != '');
    }

    function getSelectedComboBoxText(comboBox) {
        var selectedValue = comboBox.val();
        var selectedText = '';
        if (isNonEmptyString(selectedValue) == true) {
            selectedText = comboBox.find("option:selected").text();
        }
        return selectedText;
    }

    function displaySummaryInfo(selector, isShow) {
        if (isShow == true) {
            selector.closest("p").show();
        } else {
            selector.closest("p").hide();
        }
    }

    function setDefaultAirport() {
        var serviceType = $('#ServiceType').val();
        if (serviceType == SERVICE_TYPE_FROM_AIRPORT.toString()) {
            var cityName = $("#countryNameTop").text();
            var ctrlAutoCompPickupAirport = $('#AutoCompPickupAirport');
            if (isNonEmptyString(cityName) == true) {
                if (typeof (allAirports) !== undefined && allAirports != null && allAirports.length > 0) {
                    var airportsInCity = new Array();
                    $.each(allAirports, function (index, airp) {
                        if (isNonEmptyString(airp.data) == true) {
                            var airportInfo = airp.data.split('|');
                            if (airportInfo != null && airportInfo.length == 4 && airportInfo[2].toLowerCase() == cityName.toLowerCase()) {
                                airportsInCity.push(airp);
                            }
                        }
                    });
                    if (airportsInCity.length > 0) {
                        var defaultAirp = airportsInCity[0];
                        var airportInfo = defaultAirp.data.split('|');
                        setAutoCompleteValue(ctrlAutoCompPickupAirport, defaultAirp.value);
                        setAirportInfo('PickupAirport', ctrlAutoCompPickupAirport, airportInfo[0], airportInfo[1]);
                        return;
                    }
                }
            }
            setAutoCompleteValue(ctrlAutoCompPickupAirport, '');
            setAirportInfo('PickupAirport', ctrlAutoCompPickupAirport, '', '');
        }
    }

    function setAutoCompleteValue(ctrlAutoCompSelector, strValue) {
        ctrlAutoCompSelector.val(strValue);
    }

    function initialize() {
        $("#top_message").hide();
        Booking.getValueCardExpiryDate("fr-booking", "CardExpiredDate");
        displaySummaryInfo($(".infoPickupLocation"), false);
        displaySummaryInfo($(".infoDropoffLocation"), false);
        displaySummaryInfo($(".infoVehicle"), false);

        $('#ServiceType').val(DEFAULT_SERVICE_TYPE);
        refreshSelectionChange($('#ServiceType'), true);

        $('#VehicleType').multiselect({
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            includeSelectAllOption: true,
            selectAllText: PlaceholderSelectAllText,
            filterPlaceholder: PlaceholderFilterPlaceholder,
            allSelectedText: PlaceholderAllSelectedText,
            nonSelectedText: PlaceholderNonSelectedText,
        });
        $('#VehicleType').val([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12]);
        $('#VehicleType').multiselect("refresh");
        changeSelectionTextColor($('#VehicleType'));

        var currentForm = 'fr-booking';
        var vehicleTypeFilterID = 'Filter_VehicleType'
        $('#VehicleType').siblings('div').find('ul li').each(function (e) {
            if ($(this).hasClass('multiselect-filter')) {
                var $currentFilterInput = $(this).find('.form-control.multiselect-search');
                $currentFilterInput.attr('id', vehicleTypeFilterID).attr('aria-label', vehicleTypeFilterID).attr('aria-labelledby', currentForm);
            }
            else {
                var $currentInput = $(this).find('input[type="checkbox"]')
                $currentInput.attr('id', 'cbVehicleType' + $currentInput.attr('value')).attr('aria-label', $currentInput.attr('value')).attr('aria-labelledby', currentForm);
            }
        });

        loadCountries();
        loadStates();
        loadAirports();
    }

    function loadCountries() {
        var ddlCountries = [$("#PaymentCountry"), $("#PickUpCountry"), $("#DropOffCountry")];

        $.ajax({
            type: 'GET',
            url: "/sm/DCRApi/GetISOCountries",
            success: function (result) {
                $.each(ddlCountries, function (index, ddlCount) {
                    var op = $("<option value=''>- " + PlaceholderSelectCountry + " -</option>")
                    ddlCount.append(op);
                    if (result.Countries && result.Countries.length > 0) {
                        result.Countries.forEach(function (cnt) {
                            op = new Option(cnt.Name, cnt.Code);
                            ddlCount.append(op);
                        });
                        rebuildSelection(ddlCount, false);
                        initCountryControls();
                    }
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }

    function initCountryControls() {
        var ddlCountries = [$("#PaymentCountry"), $("#PickUpCountry"), $("#DropOffCountry")];
        $.each(ddlCountries, function (index, ddlCount) {
            ddlCount.val(DEFAULT_COUNTRY);
            refreshSelectionChange(ddlCount, true);
        });
    }

    function loadStates() {
        $.ajax({
            type: 'POST',
            url: "/sm/DCRApi/GetAllStateProvinces",
            success: function (result) {
                if (result == null || result == '') {
                    return;
                }
                if (typeof (result.States) !== 'defined' && result.States != null && result.States.length > 0) {
                    allStates = result.States;
                    initCountryControls();
                } else {
                    if (typeof (result.message) !== 'defined' && result.message != null && result.message.length > 0) {
                        alert(result.message);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }

    function loadAirports() {
        $.ajax({
            type: 'GET',
            url: "/sm/DCRApi/GetLimoAirports",
            success: function (result) {
                if (result == null || result == '') {
                    return;
                }
                if (typeof (result.Airports) !== 'defined' && result.Airports != null && result.Airports.length > 0) {
                    allAirports = result.Airports;
                    initAutoCompleteAirport();
                    setDefaultAirport();
                } else {
                    if (typeof (result.message) !== 'defined' && result.message != null && result.message.length > 0) {
                        alert(result.message);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }

    function buildStates(ddState, states, selectedCountry) {
        ddState.empty();
        var op = $("<option value=''>- " + PlaceHolderPaymentState + " -</option>")
        ddState.append(op);
        states.forEach(function (state) {
            op = new Option(state.Name, state.Code);
            ddState.append(op);
        });
        rebuildSelection(ddState, false);
    }

    function isNorthAmerican(countryCode) {
        return (countryCode == 'US' || countryCode == 'CA');
    }

    function rebuildSelection($select, changeSelectedColor) {
        $select.multiselect('rebuild');
        if (changeSelectedColor == true) {
            changeSelectionTextColor($select);
        }
    };

    function refreshSelectionChange($select, changeSelectedColor) {
        $select.change();
        $select.multiselect('refresh');
        if (changeSelectedColor == true) {
            changeSelectionTextColor($select);
        }
    }

    function changeSelectionTextColor($select) {
        $select.next().find(".multiselect-selected-text").css({ "color": "rgb(85, 85, 85)" });
    }

    function displayAutoComplete(autoCompleteControl, isShow) {
        if (typeof (autoCompleteControl) !== 'undefined' && autoCompleteControl != null) {
            if (isShow) {
                autoCompleteControl.show();
            } else {
                autoCompleteControl.hide();
            }
        }
    }

    function displaySingleSelectBox(singleSelectBox, isShow) {
        var container = singleSelectBox.parent();
        if (typeof (container) !== 'undefined' && container != null) {
            if (isShow) {
                container.show();
            } else {
                container.hide();
            }
        }
    }

    function buildLimoSearchResult() {
        if (listOfLimoSearchResult != null && listOfLimoSearchResult.length > 0) {
            var numberOfPages = (listOfLimoSearchResult.length % LIMO_PAGE_SIZE == 0) ?
                (listOfLimoSearchResult.length / LIMO_PAGE_SIZE) :
                (Math.floor(listOfLimoSearchResult.length / LIMO_PAGE_SIZE) + 1);
            currentPage = 1;
            loadPageData(getLimoSearchPageData(currentPage), null);
            $(".pagination").pagination({
                prevText: PlaceholderPrev,
                nextText: PlaceholderNext,
                items: numberOfPages,
                itemsOnPage: 1,
                onPageClick: function (pageNumber, event) {
                    if (event != null) {
                        event.preventDefault();
                    }
                    var pageData = getLimoSearchPageData(pageNumber);
                    var middleContainer = $(".data-container:nth-of-type(2)");
                    if (pageNumber > currentPage) {
                        middleContainer.animate({ 'margin-left': "-100%" }, 500, function () {
                            $(this).css("margin-left", "100%");
                            $(this).appendTo("[data-spy='blockSlide']");
                        });
                        loadPageData(pageData, true);
                    }
                    if (pageNumber < currentPage) {
                        middleContainer.animate({ 'margin-left': "100%" }, 500, function () {
                            $(this).css("margin-left", "-100%");
                            $(this).prependTo("[data-spy='blockSlide']");
                        });
                        loadPageData(pageData, false);
                    }
                    currentPage = pageNumber;
                },
                cssStyle: 'light-theme'
            });
        }
    }

    function loadPageData(pageData, isNextPage) {
        var middleContainer = $(".data-container:nth-of-type(2)");
        var loadContainer = null;
        if (isNextPage == null) {
            loadContainer = middleContainer;
        } else {
            if (isNextPage == true) {
                loadContainer = middleContainer.next();
            } else {
                loadContainer = middleContainer.prev();
            }
        }
        loadContainer.html(pageData);
        loadContainer.animate({ 'margin-left': "0" }, 500);

        var limoItems = loadContainer.find('.limoItem');
        if (typeof (limoItems) !== 'undefined' && limoItems != null && limoItems.length > 0) {
            var selectedResultHandle = $("#ResultHandle").val();
            limoItems.each(function (index, item) {
                var resultHandle = $(this).attr('resultHandle');
                if (typeof (resultHandle) !== 'undefined' && resultHandle != null) {
                    if (selectedResultHandle === resultHandle) {
                        $(".booking-slides-recommends .slide").removeClass("selected");
                        $(this).closest(".slide").addClass("selected");
                    }
                }
            });
        }

        $('.vehicle-image').on('error', function () {
            $(this).attr('src', $('#noImageUrl').val());
            $(this).off('error');
        });
    }

    function getLimoSearchPageData(pageNumber) {
        if (listOfLimoSearchResult != null && listOfLimoSearchResult.length > 0) {
            var numberOfPages = (listOfLimoSearchResult.length % LIMO_PAGE_SIZE == 0) ?
                (listOfLimoSearchResult.length / LIMO_PAGE_SIZE) :
                (Math.floor(listOfLimoSearchResult.length / LIMO_PAGE_SIZE) + 1);
            if (1 <= pageNumber && pageNumber <= numberOfPages) {
                var startIndex = (pageNumber - 1) * LIMO_PAGE_SIZE;
                var endIndex = pageNumber * LIMO_PAGE_SIZE;
                endIndex = endIndex <= listOfLimoSearchResult.length ? endIndex : listOfLimoSearchResult.length;
                var pageData = '';
                for (var ind = startIndex; ind < endIndex; ind++) {
                    pageData += listOfLimoSearchResult[ind];
                }
                return pageData;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    function clearSearchResults() {
        currentPage = 0;
        listOfLimoSearchResult = new Array();
        $("#SearchHandle").val('');
        $("#ResultHandle").val('');
        $("#VehicleHandle").val('');

        $("#PickupPlace_Address").val('');
        $("#PickupPlace_City").val('');
        $("#PickupPlace_State").val('');
        $("#PickupPlace_PostalCode").val('');

        $("#DropOffPlace_Address").val('');
        $("#DropOffPlace_City").val('');
        $("#DropOffPlace_State").val('');
        $("#DropOffPlace_PostalCode").val('');

        $('#divSearchResult').hide();
        displaySummaryInfo($(".infoVehicle"), false);
    }

    function clearInputFields() {
        $("#PickupAirport").val('');
        $("#DropOffAirport").val('');
    }

    function clearAllFields() {
        clearInputFields();
        clearSearchResults();
    }

    function getPlaceAddress(place) {
        if (typeof (place) !== 'undefined' && place != null) {
            return [getStringInfo(place.formatted_address), getStringInfo(place.city),
            getStringInfo(place.state), getStringInfo(place.postal_code)];
        } else {
            return ['', '', '', ''];
        }
    }

    function getStringInfo(info) {
        if (typeof (info) !== 'undefined' && info != null) {
            return info.toString();
        } else {
            return '';
        }
    }

    function isAirportInfoMatchQuery(airportInfo, queryLowerCase, isStartWith) {
        if (isStartWith) {
            return ((airportInfo[0].toLowerCase().indexOf(queryLowerCase) == 0)
                || (airportInfo[1].toLowerCase().indexOf(queryLowerCase) == 0)
                || (airportInfo[2].toLowerCase().indexOf(queryLowerCase) == 0));
        } else {
            return ((airportInfo[0].toLowerCase().indexOf(queryLowerCase) >= 0)
                || (airportInfo[1].toLowerCase().indexOf(queryLowerCase) >= 0)
                || (airportInfo[2].toLowerCase().indexOf(queryLowerCase) >= 0));
        }
    }

    function initAutoCompleteAirport() {
        $('.airportSelector').each(function () {
            var self = $(this);
            var ctrlId = self.attr('useFor');
            self.autocomplete({
                lookup: allAirports,
                params: { limit: AIRPORT_FILTER_SZIE },
                minChars: 3,
                appendTo: self.parent(),
                autoSelectFirst: true,
                showNoSuggestionNotice: true,
                onSearchStart: function (searchParams) {
                    setAirportInfo(ctrlId, self, '', '');
                    var queryLowerCase = '';
                    var indexOfHyphen = searchParams.query.indexOf('-');
                    if (indexOfHyphen > 0 && indexOfHyphen < searchParams.query.length) {
                        queryLowerCase = searchParams.query.substring(indexOfHyphen + 1, searchParams.query.length).trim().toLowerCase();
                    } else {
                        queryLowerCase = searchParams.query.toLowerCase();
                    }
                    filterAirports = new Array();
                    var onlyContainFilterAirports = new Array();

                    allAirports.forEach(function (ap, index) {
                        var airportInfo = ap.data.split('|');
                        if (typeof (airportInfo) !== 'undefined'
                            && airportInfo != null && airportInfo.length == 4) {
                            if (isAirportInfoMatchQuery(airportInfo, queryLowerCase, false)) {
                                if (isAirportInfoMatchQuery(airportInfo, queryLowerCase, true)) {
                                    filterAirports.push(ap);
                                } else {
                                    onlyContainFilterAirports.push(ap);
                                }
                            }
                        }
                    });
                    if (filterAirports.length < AIRPORT_FILTER_SZIE) {
                        $.each(onlyContainFilterAirports, function (index, ap) {
                            if (filterAirports.length < AIRPORT_FILTER_SZIE) {
                                filterAirports.push(ap);
                            }
                        });
                    }
                },
                lookupFilter: function (suggestion, originalQuery, queryLowerCase) {
                    if (typeof (suggestion) !== 'undefined' && suggestion != null
                        && isNonEmptyString(suggestion.value) == true && isNonEmptyString(suggestion.data) == true) {
                        var newQuery = '';
                        var indexOfHyphen = queryLowerCase.indexOf('-');
                        if (indexOfHyphen > 0 && indexOfHyphen < queryLowerCase.length) {
                            newQuery = queryLowerCase.substring(indexOfHyphen + 1, queryLowerCase.length).trim();
                        } else {
                            newQuery = queryLowerCase;
                        }

                        if (suggestion.data.toLowerCase().indexOf(newQuery) < 0) {
                            return false;
                        }

                        if (filterAirports != null && filterAirports.length > 0) {
                            var isSelected = false;
                            filterAirports.forEach(function (ap, index) {
                                if (isNonEmptyString(suggestion.value) == true && (ap.value == suggestion.value)) {
                                    isSelected = true;
                                }
                            });
                            return isSelected;
                        }
                        return false;
                    }
                },
                onSelect: function (suggestion) {
                    var airportInfo = suggestion.data.split('|');
                    var airportValue = '';
                    if (isNonEmptyString(airportInfo[0]) == true) {
                        airportValue = airportInfo[0];
                    }
                    setAirportInfo(ctrlId, self, airportValue, airportInfo[1]);
                },
                onSearchComplete: function (query, suggestions) {
                    $(this).siblings(".autocomplete-suggestions").addClass('scrollbar-outer');
                    $(this).siblings(".autocomplete-suggestions").scrollbar();
                }
            });
        });
    }

    function setAirportInfo(ctrlId, ctrlAutoCompAirport, airportValue, airportFullName) {
        var ctrlAirport = $("#" + ctrlId);
        ctrlAirport.val(airportValue);
        ctrlAutoCompAirport.attr(ATTR_AIRPORT_FULLNAME, airportFullName);
        if (ctrlId == 'PickupAirport') {
            onPickupLocationCtrlChanged();
        } else if (ctrlId == 'DropOffAirport') {
            onDropOffLocationCtrlChanged();
        }
        validateSearchAndDisplayButton();
    }

    function onPickupLocationCtrlChanged() {
        var strServiceType = $("#ServiceType").val();
        if (isNonEmptyString(strServiceType) == true) {
            var serviceType = parseInt(strServiceType);
            var pickUpInfo = '';
            if (serviceType == SERVICE_TYPE_FROM_AIRPORT) {
                var airportFullName = $('#AutoCompPickupAirport').attr(ATTR_AIRPORT_FULLNAME);
                pickUpInfo = isNonEmptyString(airportFullName) == true ? airportFullName : '';
            } else {
                var pickupAddress = $('#PickupLocation').val();
                var selectedCountry = getSelectedComboBoxText($('#PickUpCountry'));
                if (isNonEmptyString(pickupAddress) == true && isNonEmptyString(selectedCountry) == true) {
                    pickUpInfo = pickupAddress + ', ' + selectedCountry;
                } else {
                    pickUpInfo = pickupAddress + selectedCountry;
                }
            }
            if (isNonEmptyString(pickUpInfo) == true) {
                displaySummaryInfo($(".infoPickupLocation"), true);
                $(".infoPickupLocation").text(pickUpInfo);
            } else {
                displaySummaryInfo($(".infoPickupLocation"), false);
            }
        }
    }

    function onDropOffLocationCtrlChanged() {
        var strServiceType = $("#ServiceType").val();
        if (isNonEmptyString(strServiceType) == true) {
            var serviceType = parseInt(strServiceType);
            var dropOffInfo = '';
            if (serviceType == SERVICE_TYPE_TO_AIRPORT) {
                var airportFullName = $('#AutoCompDropOffAirport').attr(ATTR_AIRPORT_FULLNAME);
                dropOffInfo = isNonEmptyString(airportFullName) ? airportFullName : '';
            } else if (serviceType == SERVICE_TYPE_FROM_AIRPORT
                || serviceType == SERVICE_TYPE_ONE_WAY) {
                var dropOffAddress = $('#DropOffLocation').val();
                var selectedCountry = getSelectedComboBoxText($('#DropOffCountry'));
                if (isNonEmptyString(dropOffAddress) && isNonEmptyString(selectedCountry)) {
                    dropOffInfo = dropOffAddress + ', ' + selectedCountry;
                } else {
                    dropOffInfo = dropOffAddress + selectedCountry;
                }
            }
            if (isNonEmptyString(dropOffInfo) == true) {
                displaySummaryInfo($(".infoDropoffLocation"), true);
                $(".infoDropoffLocation").text(dropOffInfo);
            } else {
                displaySummaryInfo($(".infoDropoffLocation"), false);
            }
        }
    }

    function showFlightInfoControls() {
        var lblAirlineTerminal = $('#lblAirlineTerminal');
        var lblFlightHandle = $('#lblFlightHandle');
        var txtAirlineHandle = $('#AirlineHandle');
        var txtFlightTerminal = $('#FlightTerminal');

        var isPrivateFlight = $("#IsPrivateFlight").is(':checked');
        if (isPrivateFlight == false) {
            txtAirlineHandle.show();
            txtFlightTerminal.hide();
            lblAirlineTerminal.text(PlaceholderAirline + ' ');
            lblFlightHandle.text(PlaceholderFlightNumber + ' ');
        } else {
            txtAirlineHandle.hide();
            txtFlightTerminal.show();
            lblAirlineTerminal.text(PlaceholderTerminalName + ' ');
            lblFlightHandle.text(PlaceholderTailNumber + ' ');
        }
    }

    function validateSearchAndDisplayButton() {
        var valSearchFields = validateSearchFields();
        if (valSearchFields.isSuccess) {
            $("#btnSearchRide").removeClass("disabled");
        } else {
            $("#btnSearchRide").addClass("disabled");
        }
    }

    $('#IsPrivateFlight').change(function () {
        showFlightInfoControls();
    });

    $("#DdlPaymentState").change(function () {
        $("#PaymentState").val($("#DdlPaymentState").val());
    });

    $("#TxtPaymentState").change(function () {
        $("#PaymentState").val($("#TxtPaymentState").val());
    });

    $("#PaymentCountry").change(function () {
        var ddlState = $("#DdlPaymentState");
        var txtState = $("#TxtPaymentState");
        var selectedCountry = $("#PaymentCountry").val();
        var lblStateReq = $("#lblStateReq");
        ddlState.empty();
        rebuildSelection(ddlState, false);
        txtState.val('');
        $("#PaymentState").val('');
        if (isNorthAmerican(selectedCountry)) {
            displaySingleSelectBox(ddlState, true);
            lblStateReq.show();
            txtState.hide();
            if (allStates != null && allStates.length > 0) {
                if (isNonEmptyString(selectedCountry) == true) {
                    var countryStates = new Array();
                    allStates.forEach(function (item, index) {
                        if (item.Country == selectedCountry) {
                            countryStates.push(item);
                        }
                    });
                    buildStates(ddlState, countryStates, selectedCountry);
                }
            }
        } else {
            displaySingleSelectBox(ddlState, false);
            lblStateReq.hide();
            txtState.show();
        }
    });

    $(document).on("click", ".limoItem", function () {
        $("#SearchHandle").val($(this).attr("searchHandle"));
        $("#VehicleHandle").val($(this).attr("vehicleHandle"));
        $("#ResultHandle").val($(this).attr("resultHandle"));

        $("#PickupPlace_Address").val($(this).attr("pickupPlaceAddress"));
        $("#PickupPlace_City").val($(this).attr("pickupPlaceCity"));
        $("#PickupPlace_State").val($(this).attr("pickupPlaceState"));
        $("#PickupPlace_PostalCode").val($(this).attr("pickupPlacePostalCode"));

        $("#DropOffPlace_Address").val($(this).attr("dropOffPlaceAddress"));
        $("#DropOffPlace_City").val($(this).attr("dropOffPlaceCity"));
        $("#DropOffPlace_State").val($(this).attr("dropOffPlaceState"));
        $("#DropOffPlace_PostalCode").val($(this).attr("dropOffPlacePostalCode"));

        $("#SelectedVehicleType").val($(this).attr("vehicleType"));

        displaySummaryInfo($(".infoVehicle"), true);
        $(".infoVehicle").text($(this).attr("vehicleMake") + ' ' + $(this).attr("vehicleModel"));
    });

    $(document).on("click", ".booking-slides-recommends .img-inner", function () {
        if ($(this).closest(".slide").hasClass("selected")) {
            $(this).closest(".slide").removeClass("selected");
        } else {
            $(".booking-slides-recommends .slide").removeClass("selected");
            $(this).closest(".slide").addClass("selected");
        }
    });

    $("#countryNameTop").on('DOMSubtreeModified', function () {
        setDefaultAirport();
    });

    $('.search-field').change(function () {
        validateSearchAndDisplayButton();
    });

    $('.pickupLocationCtrl').change(function () {
        onPickupLocationCtrlChanged();
    });

    $('.dropOffLocationCtrl').change(function () {
        onDropOffLocationCtrlChanged();
    });

    //$('.fr-booking').validation({
    //    txtContactNumber: {
    //        name: 'PaxPhoneNumber'
    //    },
    //    contactUsEmail: {
    //        name: 'PaxEmail'
    //    },
    //    numberOfAdults: {
    //        name: 'NumberOfPassengers',
    //        rule: {
    //            max: 50
    //        }
    //    }
    //});

    $('#ServiceType').change(function () {
        var strServiceType = $('#ServiceType').val();

        var lblPickupLocation = $('#lblPickupLocation');
        var txtPickupLocation = $('#PickupLocation');
        var divPickUpCountry = $('#divPickUpCountry');
        var autoCompPickupAirport = $('#AutoCompPickupAirport');

        var lblDropOffLocation = $('#lblDropOffLocation');
        var divDropOffLocation = $('#divDropOffLocation');

        var txtDropOffLocation = $('#DropOffLocation');
        var autoCompDropOffAirport = $('#AutoCompDropOffAirport');
        var divDropOffInfo = $('#divDropOffInfo');
        var dllDropOffCountry = $('#DropOffCountry');
        var dllDropOffTime = $('#DropOffTime');
        var lblDropOffInfo = $('#lblDropOffInfo');

        var divIsPrivateFlight = $('#divIsPrivateFlight');
        var divAirlineInfo = $('#divAirlineInfo');
        var iconDropOffInfo = $('#iconDropOffInfo');

        if (isNonEmptyString(strServiceType) == false) {
            strServiceType = DEFAULT_SERVICE_TYPE;
        }
        $(".infoLimo").text($('#ServiceType').find('option:selected').text());

        var serviceType = parseInt(strServiceType);
        if (serviceType == SERVICE_TYPE_FROM_AIRPORT) {
            divIsPrivateFlight.show();
            divAirlineInfo.show();
            showFlightInfoControls();
        } else {
            divIsPrivateFlight.hide();
            divAirlineInfo.hide();
        }

        if (serviceType == SERVICE_TYPE_FROM_AIRPORT) {
            lblPickupLocation.text(PlaceholderPickupAirport); //LimoSelfServeFormModule.PickupAirport
            lblDropOffLocation.text(PlaceholderDropOffLocation); // LimoSelfServeFormModule.DropOffLocation
            txtPickupLocation.hide();
            displayAutoComplete(autoCompPickupAirport, true);
            divPickUpCountry.hide();
            divDropOffLocation.show();
            txtDropOffLocation.show();
            displayAutoComplete(autoCompDropOffAirport, false);

            divDropOffInfo.show();
            displaySingleSelectBox(dllDropOffCountry, true);
            displaySingleSelectBox(dllDropOffTime, false);
            lblDropOffInfo.text(PlaceholderDropOffCountry); //LimoSelfServeFormModule.DropOffCountry
            iconDropOffInfo.removeClass('icon-time');
            iconDropOffInfo.addClass('icon-global-xs');

            setDefaultAirport();
        } else if (serviceType == SERVICE_TYPE_TO_AIRPORT) {
            lblPickupLocation.text(PlaceholderPickupLocation); //LimoSelfServeFormModule.PickupLocation
            lblDropOffLocation.text(PlaceholderDropOffAirport); //LimoSelfServeFormModule.DropOffAirport
            txtPickupLocation.show();
            displayAutoComplete(autoCompPickupAirport, false);
            divPickUpCountry.show();
            divDropOffLocation.show();
            txtDropOffLocation.hide();
            displayAutoComplete(autoCompDropOffAirport, true);
            divDropOffInfo.hide();
        } else if (serviceType == SERVICE_TYPE_ONE_WAY) {
            lblPickupLocation.text(PlaceholderPickupLocation); //LimoSelfServeFormModule.PickupLocation
            lblDropOffLocation.text(PlaceholderDropOffLocation); //LimoSelfServeFormModule.DropOffLocation
            txtPickupLocation.show();
            displayAutoComplete(autoCompPickupAirport, false);
            divPickUpCountry.show();
            divDropOffLocation.show();
            txtDropOffLocation.show();
            displayAutoComplete(autoCompDropOffAirport, false);

            divDropOffInfo.show();
            displaySingleSelectBox(dllDropOffCountry, true);
            displaySingleSelectBox(dllDropOffTime, false);
            lblDropOffInfo.text(PlaceholderDropOffCountry); //LimoSelfServeFormModule.DropOffCountry
            iconDropOffInfo.removeClass('icon-time');
            iconDropOffInfo.addClass('icon-global-xs');
        } else if (serviceType == SERVICE_TYPE_HOURLY) {
            lblPickupLocation.text(PlaceholderPickupLocation); //LimoSelfServeFormModule.PickupLocation
            txtPickupLocation.show();
            displayAutoComplete(autoCompPickupAirport, false);
            divPickUpCountry.show();
            divDropOffLocation.hide();
            txtDropOffLocation.hide();
            displayAutoComplete(autoCompDropOffAirport, false);

            divDropOffInfo.show();
            displaySingleSelectBox(dllDropOffCountry, false);
            displaySingleSelectBox(dllDropOffTime, true);
            lblDropOffInfo.text(PlaceholderDropOffTime); // LimoSelfServeFormModule.DropOffTime
            iconDropOffInfo.removeClass('icon-global-xs');
            iconDropOffInfo.addClass('icon-time');
        }
        onPickupLocationCtrlChanged();
        onDropOffLocationCtrlChanged();
        var bookingForm = $('#fr-booking');
        var errorLabels = bookingForm.find('label.has-error-icon');
        if (typeof (errorLabels) !== 'undefined' && errorLabels != null && errorLabels.length > 0) {
            var isErrorShow = false;
            $.each(errorLabels, function () {
                if ($(this).is(":visible") == true) {
                    isErrorShow = true;
                }
            });
            if (isErrorShow) {
                bookingForm.validate();
                bookingForm.valid();
            }
        }
    });

    $('#btnSearchRide').click(function () {
        var validateSearch = validateSearchFields();
        if (validateSearch.isSuccess == false) {
            if (isNonEmptyString(validateSearch.message) == true) {
                alert(validateSearch.message);
            }
            return;
        }
        clearSearchResults();
        var searchInfo = validateSearch.searchInfo;
        $.ajax({
            url: "/sm/DCRApi/SearchLimoRides",
            type: "POST",
            data: {
                serviceType: searchInfo.serviceType,
                rideDate: searchInfo.rideDate,
                pickupTime: searchInfo.pickupTime,
                dropOffTime: searchInfo.dropOffTime,
                pickupPlace: searchInfo.pickupPlace,
                dropOffPlace: searchInfo.dropOffPlace,
                numberOfPassengers: searchInfo.numberOfPassengers,
                vehicleTypes: searchInfo.vehicleTypes,
            },
            beforeSend: function () {
                $("body").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
            },
            complete: function () {
                $("#ajaxLoading").remove();
            },
            success: function (response) {
                if (response == null || response == "" || typeof (response.searchResult) === 'undefined' ||
                    response.searchResult == null || response.searchResult == '') {
                    alert(PlaceholderNoResult);
                    return;
                }

                var respSearchResult = JSON.parse(response.searchResult);
                var searchErrors = respSearchResult.search.errors;
                if (typeof (searchErrors) !== 'undefined' && searchErrors != null &&
                    searchErrors.length > 0) {

                    var apiMsgs = [];
                    $.each(searchErrors, function (i, err) {
                        apiMsgs.push(err.error.message);
                    });

                    App.getPlaceholderSelfFormByValues(apiMsgs, 'limo',
                        function (responseMsgs) {
                            var errMessage = '';
                            $.each(responseMsgs, function (i, err) {
                                errMessage += err + '\n';
                            });

                            alert(errMessage);
                        });

                    return;
                }
                $('#divSearchResult').show();
                var resultCount = respSearchResult.search.result_count;
                if (resultCount > 0) {
                    $('#lblOptionsCount').text(resultCount + ' ' + //LimoSelfServeFormModule.Option
                        (resultCount > 1 ? PlaceholderOptionPlural : PlaceholderOption) + ' ' + PlaceholderFound); //LimoSelfServeFormModule.OptionPlural , LimoSelfServeFormModule.Found
                    $('#lblOptionsCount').css({ 'color': 'black' });
                } else {
                    $('#lblOptionsCount').text(PlaceholderNoResultFound); //LimoSelfServeFormModule.NoResultFound
                    $('#lblOptionsCount').css({ 'color': 'red' });
                }

                var searchResults = respSearchResult.search.results;
                if (typeof (searchResults) !== 'undefined' && searchResults != null &&
                    searchResults.length > 0) {
                    var pickupPlace = getPlaceAddress(respSearchResult.search.resolved_pickup_place);
                    var dropOffPlace = getPlaceAddress(respSearchResult.search.resolved_dropoff_place);
                    $.each(searchResults, function (i, itemResult) {
                        var item = itemResult.result;
                        var searchItem =
                            '<div class="slide limoItem" searchHandle="' + respSearchResult.search.search_handle
                            + '" resultHandle="' + item.result_handle
                            + '" vehicleHandle="' + item.vehicle_handle
                            + '" pickupPlaceAddress="' + pickupPlace[0]
                            + '" pickupPlaceCity="' + pickupPlace[1]
                            + '" pickupPlaceState="' + pickupPlace[2]
                            + '" pickupPlacePostalCode="' + pickupPlace[3]
                            + '" dropOffPlaceAddress="' + dropOffPlace[0]
                            + '" dropOffPlaceCity="' + dropOffPlace[1]
                            + '" dropOffPlaceState="' + dropOffPlace[2]
                            + '" dropOffPlacePostalCode="' + dropOffPlace[3]
                            + '" vehicleMake="' + item.vehicle_make
                            + '" vehicleModel="' + item.vehicle_model
                            + '" vehicleType="' + item.vehicle_type
                            + '" vehicleTypeDescription="' + item.vehicle_type_string
                            + '"> \
                            <div class="wrap-content"> \
                              <div class="img-responsive"> \
                                <a href=""> \
                                  <img src="' + item.vehicle_images.thumb_urls + '" alt="" class="img-responsive vehicle-image"> \
                                </a> \
                                <div class="img-inner" > \
                                  <div class="inner"> \
                                    <span>SELECTED</span> \
                                  </div> \
                                </div> \
                              </div> \
                              <div class="content"> \
                                <div class="inner"> \
                                   <div class="inner-item"> \
                                    <p class="title-above">' + item.vehicle_make + ' ' + item.vehicle_model + '</p> \
                                    <h3>' + item.operator_name + '</h3> \
                                     <div class="desc">' + item.vehicle_color + ' # ' + item.vehicle_capacity +
                            '<br>' + item.vehicle_type_string + '</div> \
                                    <p class="price">' + item.grand_total + '</p> \
                                    <p>Tip included</p> \
                                   </div> \
                                 </div> \
                              </div> \
                             </div> \
                           </div>';
                        listOfLimoSearchResult.push(searchItem);
                    });
                    buildLimoSearchResult();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    });

    $('#btnBooking').click(function () {
        var bookingForm = $('#fr-booking');
        var selectedSearchHandle = $("#SearchHandle").val();
        bookingForm.validate();
        if (bookingForm.valid() && isValidVehicleType) {
            if (isNonEmptyString(selectedSearchHandle) == false) {
                alert(PlaceholderSelectALimoItem);
                return;
            }
            $.ajax({
                url: bookingForm.attr('action'),
                type: "POST",
                dataType: "json",
                data: JSON.stringify(bookingForm.serializeObject()),
                contentType: "application/json;charset=utf-8",
                beforeSend: function () {
                    $("body").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
                },
                complete: function () {
                    $("#ajaxLoading").remove();
                },
                success: function (response) {
                    if (typeof (response.Result) !== 'undefined' && response.Result != null) {
                        if (response.Result.IsSuccess == true) {
                            redirectToPath(response.Result.Path);
                        } else {
                            if (isNonEmptyString(response.Result.ErrorMessage) == true) {
                                showSubmissionErrorMessage();
                                alert(response.Result.ErrorMessage);
                            }
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                    showSubmissionErrorMessage();
                }
            });
        }
        if (!isValidVehicleType) {
            $('#VehicleType-Validator').show();
        }
    });

    initialize();
});