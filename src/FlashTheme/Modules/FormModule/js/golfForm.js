"use strict";

if (isPrefilledGolfClub == 'true' || isPrefilledGolfClub == 'True') {
    $("#GolfClub").attr('readonly', true);
}
$('.fr-booking').validation({
    selectCreditName: {
        name: 'ReservationName'
    },
    txtContactNumber: {
        name: 'MobileNumber'
    },
    txtEmail: {
        name: 'Email'
    },
    txtCreditCard: {
        name: 'CreditCardDigits'
    },
    txtOnlyNumber: {
        name: 'Handicap'
    },
    duplicatedAlternativeTime: {
        name: 'AlternativeTeeTime'
    }
})
$('.reservationTime').change(function () {
    $('.alternativeTime').blur();
})
