
var Entertainment = function () {
    function initSlick() {
        $(document).ready(function(){

            createSlick();

        });
    }

    function moduleFilter() {
        $(document).on("click",'.icon-arrow', function() {
            if($(this).hasClass('toggle')) {
                $(this).removeClass('toggle');
                $(this).closest(".item").find('.list-checkbox').slideDown('fast');
                $(this).closest(".item").find('.multi-list').slideDown('fast');
                $(this).closest(".item").find('.box-date-filter').slideDown('fast');
            } else {
                $(this).addClass('toggle');
                $(this).closest(".item").find('.list-checkbox').slideUp('fast');
                $(this).closest(".item").find('.multi-list').slideUp('fast');
                $(this).closest(".item").find('.box-date-filter').slideUp('fast');
            };
        });

        // Handle multi-list checkbox when clicking
        $(document).on("click",'.multi-list .check-status', function() {
            var subList = $(this).siblings('.sub-list-checkbox');
            var inputCheck = $(this).siblings('input');
            var subFormFilter = $(".sub-form-filter-top");
            if($(window).width() >= 992){
                if(!inputCheck.prop("checked")){
                    subList.find('input').prop("checked", true);
                } else{
                    subList.find('input').prop("checked", false);
                }
            } else{
                var dataShow = $(this).attr("data-show");
                // Keeping status when clicking checkbox
                if(inputCheck.prop("checked")){
                    inputCheck.prop("checked", false);
                } else{
                    inputCheck.prop("checked", true);
                }
                subFormFilter.addClass("active");
                subFormFilter.find(".box-filter .item").removeClass("active");
                subFormFilter.find("[data-show='"+ dataShow +"']").addClass("active");
            }
        });

        // Check status of sub-list-checkbox
        $(document).on('click','.multi-list .sub-list-checkbox', function(){
            var inputCheck = $(this).closest('.item-list').children('input');
            if($(this).find("input:checked").length == $(this).find("input").length){
                inputCheck.prop("checked", true);
            } else{
                inputCheck.prop("checked", false);
            }
        });

        // .sub-form-filter-top: Handle btn-back, btn-done, btn-close
        var boxSubFilter = $(".sub-form-filter-top");
        var btnDoneSubFilter = $(".sub-form-filter-top .sub-btn-filter");
        var btnCloseSubFilter = $(".sub-form-filter-top .form-filter-title .icon-close");
        var btnBackSubFilter = $(".sub-form-filter-top .form-filter-title label");
        var item = $(".sub-form-filter-top .box-filter .item");
        function btnInSubFormFilter(btnItem){
            btnItem.on("click", function(){
                boxSubFilter.removeClass("active");
                item.each(function(){
                    var dataShow = $(this).attr("data-show");
                    var dataShowItem = $(".form-filter-top").find("[data-show*='"+ dataShow +"']");
                    var input = dataShowItem.siblings('input');
                    if( $(this).find("input:checked").length > 0){
                        input.prop('checked', true);
                    } else{
                        input.prop('checked', false);
                    }
                });
            });
        }
        // btn-done
        btnInSubFormFilter(btnDoneSubFilter);
        // btn-close
        btnInSubFormFilter(btnCloseSubFilter);
        // btn-back
        btnInSubFormFilter(btnBackSubFilter);
        

        // Handle btn-check-all in .sub-form-filter-top
        $(document).on('click','.sub-form-filter-top .btn-check-all', function(){
            var btnCheckAll = $(this).siblings('input');
            var listCheckbox = $(this).siblings('.list-checkbox');
            if(!btnCheckAll.prop("checked")){
                listCheckbox.find('input').prop('checked', true);
            } else{
                listCheckbox.find('input').prop('checked', false);
            }
        });

        // Check status of btn-check-all in .sub-form-filter-top
        $(document).on('click','.sub-form-filter-top .list-checkbox', function(){
            var btnCheckAll = $(this).closest(".item").children('input');
            if($(this).find("input:checked").length == $(this).find("input").length){
                btnCheckAll.prop("checked", true);
            } else{
                btnCheckAll.prop("checked", false);
            }
        });
    }
    function travelCommonAnimation(){
        var btnChangeThumnnail = $(".block-article-desc .box-btn button");
        btnChangeThumnnail.on("click", function(){
            btnChangeThumnnail.removeClass("active");
            $(this).addClass("active");
        });
        
    }
	function createSlick(){
		$('.slides-recommends').slick({
            dots: true,
            arrows: false,
            infinite: false,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            slidesPerRow : 1,
            rows:2,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        rows: 2,
                        slidesPerRow : 1
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        rows: 2,
                        slidesPerRow : 1
                    }
                }
            ]
        });
	}
	function showBoxFilter() {
        var boxFilter = $(".form-filter-top"), filterBtn = $(".wrap-btn-filter .btn-filter-top"), btnCloseFilter = $(".form-filter-title .icon-close");
        $(window).on("load resize", function () {
            var winHeight = $(window).height();
            boxFilter.css("height", winHeight);
        });
        filterBtn.on("click", function () {
            boxFilter.addClass("active");
        });
        btnCloseFilter.on("click", function () {
            boxFilter.removeClass("active");
        });
    }
    return {
        init: function () {
            App.init();
            // initSlick();
            moduleFilter();
        }
    }
}();