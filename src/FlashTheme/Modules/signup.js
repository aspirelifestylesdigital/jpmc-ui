"use strict";
var SignUp = function () {
    $(window).bind("pageshow", function (event) {
        if (event.originalEvent.persisted) {
            window.location.reload();
        }
    });
    function printExpiryValue() {
        var month = $('.selectExpiryMonth select').find("option:selected").val();
        var year = $('.selectExpiryYear select').find("option:selected").val();
        $('.txtExpiryDate').val(month + "/" + year);
        $('.box-expiry-date input.txtExpiryDate').removeClass('error lt-error');
        $('.box-expiry-date label.lt-error').css('display', 'none');
        $('.selectExpiryMonth .multiselect-selected-text').css("color", "#555");
        $('.selectExpiryYear .multiselect-selected-text').css("color", "#555");
    }
    function getValueCardExpiryDate(formClass, idOfExpiredDate) {
        $('.selectExpiryMonth select').on('change', function () {
            monthVal = $(this).val();
            printExpiryValue();
            $("." + formClass).validate().element("#" + idOfExpiredDate);
        });
        $('.selectExpiryYear select').on('change', function () {
            yearVal = $(this).val();
            printExpiryValue();
            $("." + formClass).validate().element("#" + idOfExpiredDate);
        });

        //show month and year picker
        $(".txtExpiryDate").on("click", function (event) {
            event.stopPropagation();
            $(".cardExpiryPicker").addClass('active');
        });
        $(window).click(function () {
            $(".cardExpiryPicker").removeClass('active');
        });
    }

    function checkAvailability(callback) {
        $(".btn-check-availability").on("click", function (event) {
            event.preventDefault();
            $('#txtEmail').prop('readonly', true);
            $('#txtEmail').css('pointer-events','none');

            $('#txtCreditCardNumber').prop('readonly', true);
            $('html,body').addClass('signup-modal-open');
            var email = $('#txtEmail').val().toLowerCase();
            $.ajax({
                type: "GET",
                url: '/ServiceProvider_VerifyEmail',
                dataType: "json",
                data: { emailAddress: email },
                beforeSend: function () {
                    $("body").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
                },
                complete: function () {
                    $("#ajaxLoading").remove();
                },
                cache: false,
                success: function (response) {
                    if (response.ExistedAccountInOKTA) {
                        //show popup existed acc in OKTA
                        localStorage.setItem('emailAccount', email); //store email for auto fill when login
                        showDialogExitedAccountInOKTANotification();
                    } else {
                        if (response.ExistedAccountInPMA && response.ReturnValue != null && response.ReturnValue.length > 0) {
                            //show popup existed acc in PMA
                            showDialogExitedAccountInPMANotification();
                            processWhenClickButtonOnPopupNofitication($('#btn-exist-account-pma'), response);
                        } else {
                            //show popup has no acc existed
                            showDialogHasNoAccountExistedNotification();
                            processWhenClickButtonOnPopupNofitication($('#btn-no-account-existed'));
                        }
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(ajaxOptions, thrownError);
                }
            });
        });
    }

    function initControlWhenCheckAccountInPMA() {
        //
        //$('.exist-account-pma-modal').modal('hide');
        //$('.no-account-existed-modal').modal('hide');
        //
        $('#txtEmail').prop('readonly', true);
        $('#txtCreditCardNumber').prop('readonly', true);                

        $('.mail-validated').css('display', 'block');
        $('.btn-check-availability').css('display', 'none');
        //$('.noticeEnterInfo').css('display', 'none');

        if (!$('.check-availability').hasClass('validated')) {
            $('.check-availability').addClass('validated');
        }
    }

    function processWhenClickButtonOnPopupNofitication(buttonElement, response) {
        buttonElement.on("click", function (event) {
            event.preventDefault();
            if (buttonElement.attr('id') === 'btn-exist-account-pma') {
                $('.exist-account-pma-modal').modal('hide');
                if (response.PlaceHolderReplace != null && response.PlaceHolderReplace != '') {
                    $('.noticeEnterInfo').html(response.PlaceHolderReplace);
                    $('.noticeEnterInfo').css('display', 'block');
                }
                fillDataFromPMA(response.ReturnValue[0]);
            } else {
                $('.noticeEnterInfo').css('display', 'none');
                $('.no-account-existed-modal').modal('hide');
            }
            setTimeout(initControlWhenCheckAccountInPMA, 400);
        });

    }

    function fillDataFromPMA(data) {
        var salutation = data.Salutation.replace(/\s/g, '').toLowerCase();
        $('#Salutation option').each(function () {
            var value = $(this).text().replace(/\s/g, '').toLowerCase();
            if (value === salutation) {
                $(this).prop('selected', true);
                $('#Salutation').val($(this).text());
                $('#Salutation').next('div').find('button.multiselect').attr('title', data.Salutation);
                $('#Salutation').next('div').find('span.multiselect-selected-text').text(data.Salutation);
                return false;
            }
        });
        if (data.HaveSalutation) {
            $('#Salutation').next('div').find('button.multiselect').attr('disabled', 'disabled');
        }

        $('#txtContactNumber').val(data.MobileNumber);
        $("#txtContactNumber").prop("readonly", data.HaveMobileNumber);

        $('#txtFirstName').val(data.FirstName);
        $("#txtFirstName").prop("readonly", data.HaveFirstName);

        $('#txtLastName').val(data.LastName);
        $("#txtLastName").prop("readonly", data.HaveLastName);
    }
    function styleInvalidFullCreditCardNumber() {
        //console.log("styleInvalidFullCreditCardNumber");
        $(".btn-submit-creditCardNumber").prop('disabled', true);
        $('#txtFullCreditCardNumber').attr("style", "border: 1px solid #9d0329;");
        var mgsFullCreditCardNumber = typeof placeHolderFullCreditCardNumber == 'undefined' ? 'Have you entered a valid credit card number?' : placeHolderFullCreditCardNumber;
        AddErrorMessage('txtFullCreditCardNumber',
            'txtFullCreditCardNumber-error',
            'txtFullCreditCardNumber',
            mgsFullCreditCardNumber); 
    }
    function styleValidFullCreditCardNumber() {
        //console.log("styleValidFullCreditCardNumber");
        $(".btn-submit-creditCardNumber").prop('disabled', false);
        $('#txtFullCreditCardNumber').attr("style", "border: 1px solid #011627;");
        AddErrorMessage('txtFullCreditCardNumber',
            'txtFullCreditCardNumber-error',
            'txtFullCreditCardNumber',
            "");
    }

    function initButtonCheckAvailability() {
        $('.check-availability').removeClass('validated');
        $(".btn-check-availability").prop('disabled', true);
        $(".btn-submit-creditCardNumber").prop('disabled', true);
        $("#txtFullCreditCardNumber").on("change paste keyup", function () {    
            
            if ($("#txtFullCreditCardNumber").valid()) {
                var ccn = $("#txtFullCreditCardNumber").val();                
                if (ccn != null && ccn != undefined && ccn.length == 16) {
                    styleValidFullCreditCardNumber();
                } else {                                       
                    styleInvalidFullCreditCardNumber();
                }                
                
            } else {
                styleInvalidFullCreditCardNumber();                
            }            
            
            
        });
        $("#txtEmail").on("change paste keyup", function () {
            if ($("#txtEmail").valid()) {
                $(".btn-check-availability").prop('disabled', false);
            } else {
                $(".btn-check-availability").prop('disabled', true);
            }
        });
    }

    function showDialogExitedAccountInOKTANotification() {
        $('.signup-modal').modal('show');
    }

    function showDialogExitedAccountInPMANotification() {
        $('.exist-account-pma-modal').modal('show');
    }

    function showDialogHasNoAccountExistedNotification() {
        $('.no-account-existed-modal').modal('show');
    }

    //handle event close modal poup
    $('.no-account-existed-modal .apr-modal-title .btn-close').on('click', function () {
        //set readonly == false if close modal by 'x' button
        //console.log("Capture close modal event by click 'x' button!");
        $('#txtEmail').prop('readonly', false);
    });

    $('.exist-account-pma-modal .apr-modal-title .btn-close').on('click', function () {
        //set readonly == false if close modal by 'x' button
        //console.log("Capture close modal event by click 'x' button!");
        $('#txtEmail').prop('readonly', false);
    });

    $('.signup-modal .apr-modal-title .btn-close').on('click', function () {
        //set readonly == false if close modal by 'x' button
        //console.log("Capture close modal event by click 'x' button!");
        $('#txtEmail').prop('readonly', false);
    });

    function validateBinNumber() {
        $(".btn-submit-binNumber").on("click", function (event) {
            event.preventDefault();
            var creditNumber = $('#txtBinNumber').val();
            $.ajax({
                type: "POST",
                url: '/ServiceProvider_ValidateBinNumber',
                dataType: "json",
                data: { binNumber: creditNumber },
                beforeSend: function () {
                    $("body").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
                },
                complete: function () {
                    $("#ajaxLoading").remove();
                },
                //cache: true,
                success: function (response) {
                    if (response.BinIsEmpty != null && response.BinIsEmpty) {
                        $('#resultMessageWithBin').css('display', 'none');
                        $('#resultMessageWithBin').next('br').remove();
                        $('#txtBinNumber').addClass("error").addClass("lt-error");
                        $('#txtBinNumber').attr('aria-invalid', 'true');
                        if ($('#txtBinNumber-error').length === 0) {
                            $('#txtBinNumber').after('<label id="txtBinNumber-error" class="error lt-error" for="txtBinNumber">' + response.MessageResult + '</label>');
                        } else {
                            if ($('#txtBinNumber-error').css('display') === 'none') {
                                $('#txtBinNumber-error').css('display', 'inline-block');
                            }
                        }
                    }
                    else {
                        if (response.IsValidBinNumber != null && response.IsValidBinNumber) {
                            showContentSignUpForm();
                        } else {
                            showMessageVerifyBin(response.MessageResult);
                        }
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(ajaxOptions, thrownError);
                }
            });
        });
    }
    function AddErrorMessage(modalId, errorId, errorMessageId, errorMessage) {        
        
        if ($('#' + errorId).length === 0) {
            $('#' + errorMessageId).after('<label id="' + errorId + '" class="error lt-error-cc resultMessage error" for="' + modalId + '">' + errorMessage + '</label>');
        } else {
            if ($('#' + errorId).css('display') === 'none') {
                $('#' + errorId).css('display', 'inline-block');
            }
            $('#' + errorId).text(errorMessage);


        }        
    }

    function validateCreditCardNumber() {
        $('.btn-submit-creditCardNumber').on('click', function (event) {
            event.preventDefault();
            var creditNumber = $('#txtFullCreditCardNumber').val();
            var testVal = $('#txtFullCreditCardNumber').valid();
            var url = '/ServiceProvider_ValidateCCNumber';
            var data = { creditCardNumber: creditNumber };
            $('#hiddenFullCreditCardNumber').val(creditNumber);
            ajaxPostMethod(url, data, function (error, responseData) {
                if (error) {
                    return;
                }
                if (responseData.BinIsEmpty != null && responseData.BinIsEmpty) {                    
                    AddErrorMessage('txtFullCreditCardNumber',
                        'txtFullCreditCardNumber-error',
                        'txtFullCreditCardNumber',
                        responseData.MessageResult);
                }
                else {
                    if (responseData.IsValidBinNumber != null && responseData.IsValidBinNumber) {
                        showContentSignUpForm();
                        $('#txtFullCreditCardNumber-error').remove()
                        //AddErrorMessage('txtFullCreditCardNumber',
                        //    'txtFullCreditCardNumber-error',
                        //    'resultMessageWithBin',
                        //    responseData.MessageResult);
                    } else {
                        //showMessageVerifyBin(responseData.MessageResult);
                        AddErrorMessage('txtFullCreditCardNumber',
                            'txtFullCreditCardNumber-error',
                            'txtFullCreditCardNumber',
                            responseData.MessageResult);
                    }
                }
            });
        });
    }


    $('#txtBinNumber').keypress(function (e) {
        if (e.which == 13) {
            $(".btn-submit-binNumber").click();
            return false;
        }
    });

    function showContentSignUpForm() {
        $('#resultMessageWithBin').css('display', 'none');
        $('#resultMessageWithBin').next('br').remove();
        $('.check-availability-bin-first').css('display', 'none');
        $('.check-availability-ccn-first').css('display', 'none');
        $('.check-availability-email-first').css('display', 'block');
        $('.noticeEnterInfo').css('display', 'block');
        $('.noticeEnterInfoWithBin').css('display', 'none');
    }

    function showMessageVerifyBin(message) {
        $('#resultMessageWithBin').html(message);
        $('#resultMessageWithBin').next('br').remove();
        $('#resultMessageWithBin').after('<br />');
        $('#resultMessageWithBin').css('display', 'block');
    }

    function ajaxPostMethod(url, data, callback) {
        $.ajax({
            type: "POST",
            url: url,
            headers: { },
            dataType: "json",
            data: data,
            async: true,
            beforeSend: function () {
                $("body").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
            },
            complete: function () {
                $("#ajaxLoading").remove();
            },
            success: function (response) {
                callback(null, response);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                callback(new Error(thrownError), null);
            }
        });
    }

    function hiddenFullCreditCardNumber() {
        for (var i = 0; i <= 30; i++) {
            //console.log("Start ", i);
            setTimeout(function () {
                $('#hiddenFullCreditCardNumber').attr("style", "display:none");
                //$('#txtFullCreditCardNumber').val("");
                //$('.btn-submit-creditCardNumber').prop("disabled", true);
                //styleInvalidFullCreditCardNumber();
                //console.log("Set ",i);
            }, i*90);
        }
    }
    //implement press enter on check credit card number on sign up
    $('#txtFullCreditCardNumber').on('change keyup keydown input', function (e) {
        var pressKey = e.key;
        if (pressKey == "Enter") {
            e.preventDefault();
            e.stopImmediatePropagation();
            if ($('.btn-submit-creditCardNumber').attr('disabled') == undefined) {
                //call validate cc#
                //console.log('call validate cc#');
                $('.btn-submit-creditCardNumber').click();
            }

        }
    });

    function handleHideAndShowElement(tempDataMessage, loginType, isChaseDomain) {
        //button check avai
        if (typeof tempDataMessage !== typeof undefined && tempDataMessage !== "") {
            $('#txtEmail').prop('readonly', true);
            $('.mail-validated').css('display', 'block');
            $('.btn-check-availability').css('display', 'none');
            if (!$('.check-availability').hasClass('validated')) {
                $('.check-availability').addClass('validated');
            }
        } else {
            $('.mail-validated').css('display', 'none');
        }

        //Bin & email input
        if ((typeof loginType !== typeof undefined && loginType === 'FullMemberShipWithBin') || isChaseDomain) {
            $('.check-availability-email-first').css('display', 'none');
            $('.noticeEnterInfo').css('display', 'none');
            $('.noticeEnterInfoWithBin').css('display', 'block');
            if (!isChaseDomain) {
                $('.check-availability-bin-first').css('display', 'block');
            } 

            if (typeof tempDataMessage !== typeof undefined && tempDataMessage !== "") {
                $('.check-availability-email-first').css('display', 'block');
                $('.check-availability-bin-first').css('display', 'none');
                $('.noticeEnterInfoWithBin').css('display', 'none');
            }
        } else {
            $('.check-availability-email-first').css('display', 'block');
            $('.noticeEnterInfo').css('display', 'block');
            $('.noticeEnterInfoWithBin').css('display', 'none');
        }
    }

    function initDropdownData() {
        var salutationSelected = $('.multiselect-selected-text')[0];
        var salutationSelectedText = $(salutationSelected).text();
        if (salutationSelectedText != undefined && salutationSelectedText != "" && $('#Salutation option:selected').text() != undefined && $('#Salutation option:selected').text() != "") {
            $(salutationSelected).text($("#Salutation option:selected").text());
        }

        var challengeSelected = $('.multiselect-selected-text')[1];
        var challengeSelectedText = $(challengeSelected).text();
        if (challengeSelectedText != undefined && challengeSelectedText != "" && $('#ChallengeQuestion option:selected').text() != undefined && $('#ChallengeQuestion option:selected').text() != "") {
            $(challengeSelected).text($('#ChallengeQuestion option:selected').text());
        }
    }

    function formValidation() {
        $('.signUpForm').validation({
            txtFirstName: {
                name: 'FirstName'
            },
            txtLastName: {
                name: 'LastName'
            },
            selectSalutation: {
                name: 'Salutation'
            },
            selectCreditName: {
                name: 'NameOnCard'
            },
            txtEmail: {
                name: 'EmailAddress'
            },
            datepickerOfBirth: {
                name: 'DateOfBirth'
            },
            txtContactNumber: {
                name: 'MobileNumber'
            },
            txtCreditCard: {
                name: 'CreditCardNumber'
            },
            txtFullCreditCardNumber: {
                name: 'FullCreditCardNumber'
            },
            txtExpiryDate: {
                name: 'DateOfCard'
            },
            txtPassword: {
                name: 'Password'
            },
            txtConfirmPassword: {
                name: 'RepeatPassword'
            },
            txtBinNumber: {
                name: 'BinNumber'
            },
            cbxTerm: {
                name: 'Term'
            },
            cbxTerm1: {
                name: 'Term1'
            },
            cbxTerm2: {
                name: 'Term2'
            },
            selectChallengeQuestion: {
                name: 'ChallengeQuestion'
            },
            txtChallengeAnswer: {
                name: 'ChallengeAnswer'
            }
        });
    }

    return {
        init: function (formClass, idOfExpiredDate, tempDataMessage, loginType, isChaseDomainString) {
            //App.init();
            formValidation();
            var isChaseDomain = (isChaseDomainString === 'true' || isChaseDomainString === 'True');
            handleHideAndShowElement(tempDataMessage, loginType, isChaseDomain);
            initDropdownData();

            getValueCardExpiryDate(formClass, idOfExpiredDate);
            initButtonCheckAvailability();
            checkAvailability();
            validateBinNumber();
            validateCreditCardNumber();

            $("#btn-modal-signin").on("click", function (event) {
                event.preventDefault();
                window.location.replace("/");
            });

            $("#btn-modal-forgot-password").on("click", function (event) {
                event.preventDefault();
                window.location.replace("/?showfpw=true");
            });

            hiddenFullCreditCardNumber();
        }
    }
}();

