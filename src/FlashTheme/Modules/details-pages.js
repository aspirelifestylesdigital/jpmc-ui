"use strict";
var TravelPackagesIndiv = function () {
    function boxBookSticky() {
        $('body, html').css('overflow', 'visible');
        var wrapperOffset = $(".wrapper").offset();
        var wrapperOffsetTop = typeof (wrapperOffset) === 'undefined' ? 0 : $(".wrapper").offset().top;
        var positionContent = $(".content-indiv").offset().top - wrapperOffsetTop;
        $(window).scroll(function () {
            if ($(window).scrollTop() > positionContent || $(window).scrollTop() + $(window).height() == $(document).height()) {
                $(".box-btn-book").addClass("sticky");
            } else {
                $(".box-btn-book").removeClass("sticky");
            }
        });
    }

    function autoScrollItinerary() {
        $(document).ready(function () {
            // Handle when first-time loading
            var activeTab = $(".tab-pane.active");
            var leftHeight = activeTab.find(".tab-inner-left").outerHeight();
            var articleHeight = leftHeight - activeTab.find(".tab-inner-right .itinerary-title").height() - 30;
            activeTab.find(".tab-inner-right .itinerary-article").addClass('scrollbar-outer');
            if ($(window).width() > 991) {
                activeTab.find(".tab-inner-right .itinerary-article").scrollbar({
                    onUpdate: function () {
                        activeTab.find(".tab-inner-right .itinerary-article").css("max-height", articleHeight);
                    }
                });
            } else {
                activeTab.find(".tab-inner-right .itinerary-article").scrollbar();
            }

            // Handle when switch tab
            $('.nav-tabs a').on('shown.bs.tab', function (event) {
                // var currentTab = $(event.target);
                var $this = $($(event.target).attr("href"));
                var leftHeight = $this.find(".tab-inner-left").outerHeight();
                var articleHeight = leftHeight - $this.find(".tab-inner-right .itinerary-title").height() - 30;
                $this.find(".tab-inner-right .itinerary-article").addClass('scrollbar-outer');
                if ($(window).width() > 991) {
                    $this.find(".tab-inner-right .itinerary-article").scrollbar({
                        onUpdate: function () {
                            $this.find(".tab-inner-right .itinerary-article").css("max-height", articleHeight);
                        }
                    });
                } else {
                    $this.find(".tab-inner-right .itinerary-article").scrollbar();
                }
            });
        });
    }


    // check if all images is completely loaded?
    function checkImageCompleteLoading(element) {
        var imageNumber = element.find('.slide').length;
        var count = 0;
        element.find('.slide').each(function () {
            var self = $(this);
            var img = new Image;
            var imgSrc = self.css('background-image').replace('url("', '');
            imgSrc = imgSrc.substring(0, imgSrc.length - 2)
            img.src = imgSrc
            if (img.complete) { count++; } else { return; }
        });
        if (count == imageNumber) { return true; }
    }

    // Set max height to images
    function consistencyHeroBanner(element) {
        if (!element.hasClass('module-hero-switcher')) {
            var maxHeight = 446;
            // Count maximum height of the biggest image
            element.find('.slide').each(function () {
                var self = $(this);
                var img = new Image;
                var imgSrc = self.css('background-image').replace('url("', '');
                imgSrc = imgSrc.substring(0, imgSrc.length - 2)
                img.src = imgSrc
                var widthCoefficience = img.width / element.width();
                var selfHeight = img.height / (widthCoefficience > 1 ? widthCoefficience : 1);
                maxHeight = selfHeight >= maxHeight ? selfHeight : maxHeight;
            });

            // Set background-size: cover to the biggest image and background-size: 100% auto to others;
            //element.find('.slide').css('height', maxHeight);
            element.find('.slide').each(function () {
                var self = $(this);
                var img = new Image;
                var imgSrc = self.css('background-image').replace('url("', '');
                imgSrc = imgSrc.substring(0, imgSrc.length - 2)
                img.src = imgSrc
                var widthCoefficience = img.width / element.width();
                //if (img.height > maxHeight || widthCoefficience > 1) {
                if (img.height > maxHeight) {
                    $(this).css({ 'background-size': 'contain', 'background-position': '50% 60%' });
                } else {
                    $(this).css({ 'background-size': 'auto', 'background-position': '50% 60%' });
                }
            });
        }
    }
    /* ----------------- YELP Detail - start ----------------- */
    function yelpInitControl() {
        $('.infor-msg-error').hide();
    }
    function yelpHandleButtonEdit() {
        //$(document).on('click', 'a.edit-spa-search', function (e) {
        //    e.preventDefault();

        //    $('.yelp-filter-search').show();
        //    $('.yelp-form-infomation').hide();
        //});
    }

    var isEnableBtnReserveNow = false;
    function EnableBtnReserveNow(iValid) {
        if (iValid) {
            $('button.btn-book-spa').prop('disabled', false).addClass('btn-active');
        } else {
            $('button.btn-book-spa').prop('disabled', true).removeClass('btn-active');
        };
    }
   

    function handleYelpCheckAvailable() {
        $(document).on('click', 'a.yelp-detail-search', function (e) {
            e.preventDefault();
            //build data
            var data = new Object();
            //data.Location = 'Chicago'; //hard for test
            data.Reservation_Date = $('.reservationDateYelp').val();
            data.Reservation_Time = $('#reservationTime :selected').text();
            data.Reservation_Covers = $('select[name="peopleSelect"] :selected').text();
            updateBookThroughConciergeLink(data.Reservation_Time, data.Reservation_Date, data.Reservation_Covers);
            //call ajax
            var url = '/view-request/site/yelp/checkavailable';
            ajaxPost(url, data, function (error, responseData) {
                if (error) {
                    return;
                }
                if (responseData.DataReturn != null) {
                    if (responseData.DataReturn.length > 0) {
                        turnOnErrorMessage(false);
                        var elementAppend = '';
                        for (var i = 1; i <= responseData.DataReturn.length; i++) {
                            var radioId = 'rad' + i;
                            var checkStyle = $('#reservationTime :selected').text().toUpperCase() === responseData.DataReturn[i - 1].toUpperCase() ? " checked" : "";
                            elementAppend += '<li><input type="radio" id="' + radioId + '" name="rad" value="choice' + i + '"' + checkStyle + '>'
                                + '<label for="' + radioId + '">'
                                + '<span>' + responseData.DataReturn[i - 1] + '</span>'
                                + '</label></li>';
                        }

                        $('ul.list-time-available').empty();
                        $('ul.list-time-available').append(elementAppend);

                        $('.yelp-form-infomation').show();
                        $('.spa-filter-search').hide();
                        $('.success, .edit-spa-search').css('display', 'inline');

                        $('.aftersubmit-msg-error').hide();
                        if ($('input[name="rad"]:checked').length === 0) {
                            $('.selecthours-msg-error').show();
                            EnableBtnReserveNow(false);
                        }
                        else {
                            if ($('#spa-infor').valid() && $('#policy').is(':checked')) {
                                EnableBtnReserveNow(true);
                            } else {
                                EnableBtnReserveNow(false);
                            };
                        }

                    } else {
                        $('ul.list-time-available').empty();
                        turnOnErrorMessage(true);
                        EnableBtnReserveNow(false);
                        $('.aftersubmit-msg-error').hide();
                    }
                   
                }
                
            });
        });
    }
    function turnOnErrorMessage(isTurnOn) {
        if (isTurnOn) {
            $('#spa-infor').find('.infor-time-select').find('.infor-msg-error').attr('style', '');
            $('#spa-infor').find('.infor-time-select').find('.spa-form-title').attr('style', 'display:none');
            $('#spa-infor').find('.infor-time-select').find('.spa-form-content').attr('style', 'display:none');
            $('#spa-infor').attr('style', '');
        } else {
            $('#spa-infor').find('.infor-time-select').find('.infor-msg-error').attr('style', 'display:none');
            $('#spa-infor').find('.infor-time-select').find('.spa-form-title').attr('style', '');
            $('#spa-infor').find('.infor-time-select').find('.spa-form-content').attr('style', '');
            $('#spa-infor').attr('style', 'display:none');
        }
        
    }

    function addErrorMessageAfterCheck(message) {
        $('#spa-infor').find('.infor-msg-error.selecthours-msg-error').find('h4').text('');
        $('#spa-infor').find('.infor-msg-error.selecthours-msg-error').find('span:nth-child(2)').text(message);
        $('#spa-infor').find('.infor-msg-error.selecthours-msg-error').find('a').text('');

    }

    function addErrorMessageAfterSubmit(message) {
        $('#spa-infor').find('.infor-msg-error.aftersubmit-msg-error').find('h4').text('');
        $('#spa-infor').find('.infor-msg-error.aftersubmit-msg-error').find('span:nth-child(2)').text(message);
        $('#spa-infor').find('.infor-msg-error.aftersubmit-msg-error').find('a').text('');

    }
    function handleYelpReserve() {
        $(document).on('click', '.btn-yelp-rerseve-now', function (e) {
            e.preventDefault();
            var yelpForm = $('#spa-infor');
            if (yelpForm.valid() && $('#policy').is(':checked') && $('input[name="rad"]:checked').length > 0) {
                //build data
                var data = new Object();
                //ReservationDate
                data.ReservationDate = $('.spa-info-1 h4.datetime-submit').text();
                //ReservationTime
                var radioId = $('input[name="rad"]:checked').attr('id');
                var reservationTime = $('label[for="' + radioId + '"]').text().trim().split(" ");
                data.ReservationTimeDivider = reservationTime.length > 0 ? reservationTime[1] : ' ';
                data.ReservationTimeHour = reservationTime[0].split(':')[0];
                data.ReservationTimeMinute = reservationTime[0].split(':').length > 0 ? reservationTime[0].split(':')[1] : '00';
                //Reservation Covers
                data.NumberOfAdults = parseInt($('.spa-info-2 h4.guests-submit').text().split(" ")[0]);
                data.NumberOfChildren = 0;
                //Reservation Name
                data.RestaurantName = $('.view-head-content h1').text().trim();
                data.RestaurantAddress = $('#cultureInfo-name').attr('data-address');
                data.RestaurantPhone = $('#cultureInfo-name').attr('data-phone');
                //Pax info
                data.ReservationFirstName = $('input[name="firstName"]').val();
                data.ReservationLastName = $('input[name="lastName"]').val();
                data.MobileNumber = $('input[name="mobileNumber"]').val();
                data.Email = $('input[name="email"]').val();
                //call ajax
                var url = '/view-request/site/yelp/submitform';
                ajaxPost(url, data, function (error, responseData) {
                    if (error) {
                        return;
                    }

                    //bind infor confirmed
                    var dateConfirmed = $('.date-confirmed').text();
                    $('.date-confirmed').text(dateConfirmed.replace('{0}', $('.spa-info-1 h4.datetime-submit').text()));

                    var timeConfirmed = $('.time-comfirmed').text();
                    var radioCheckedId = $('input[name="rad"]:checked').attr('id');
                    $('.time-comfirmed').text(timeConfirmed.replace('{0}', $('label[for="' + radioCheckedId + '"]').text().trim()));

                    var guestConfirmed = $('.guestno-confirmed').text();
                    $('.guestno-confirmed').text(guestConfirmed.replace('{0}', $('.spa-info-2 h4.guests-submit').text().split(" ")[0]));

                    if (responseData.BookingNumber !== '') {
                        var confirmText = $('h1.confirmationNumber').text();
                        $('h1.confirmationNumber').text(confirmText.replace('{0}', responseData.BookingNumber));
                    } else {
                        $('h1.confirmationNumber').hide();
                    }

                    $('.yelp-form-infomation').hide();
                    $('.reservation-details').hide();
                    $('.reservation-confirmation').show();
                });
            }
        });
    }

    function ajaxPost(url, data, callback) {
        $.ajax({
            type: "POST",
            url: url,
            headers: {
                'culture-name': $('#cultureInfo-name').length > 0 ? $('#cultureInfo-name').val() : '',
                'business-id': $('#cultureInfo-name').length > 0 ? $('#cultureInfo-name').attr('data-business-id') : ''
            },
            dataType: "json",
            data: data,
            async: true,
            beforeSend: function () {
                $("body").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
            },
            complete: function () {
                $("#ajaxLoading").remove();
            },
            success: function (response) {
                if (response.Success) {
                    callback(null, response);
                }
                else {
                    displayError(response.DataReturn, response.Error);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                callback(new Error(thrownError), null);
            }
        });
    }

    function displayError(caseReturn, errorMessage) {
        if (caseReturn === 'Reservation Fail' || caseReturn === 'Hold Existed' || caseReturn === 'Hold Fail') {
            //bind error from api
            addErrorMessageAfterSubmit(errorMessage);
            $('.aftersubmit-msg-error').show();
            $('.selecthours-msg-error').hide();
        } else if (caseReturn === 'CheckAvaiable Fail') {
            $('.selecthours-msg-error').show();
            $('.aftersubmit-msg-error').hide();
        }
    }
    /* ----------------- YELP Detail - end ----------------- */
    

    return {
        init: function () {
            boxBookSticky();
            autoScrollItinerary();
            yelpInitControl();
            yelpHandleButtonEdit();
            handleYelpCheckAvailable();
            handleYelpReserve();
            // Handle button book
            $(document).ready(function () {
                //insert logo Yelp
                if ($('.reservationDateYelp').length > 0) {
                    var yelpUrl = $('#cultureInfo-name').length > 0 ? $('#cultureInfo-name').attr('data-alias') : '';
                    var textDisplay = $('#cultureInfo-name').length > 0 ? $('#cultureInfo-name').attr('data-powerredbyyelp') : '';
                    appendYelpLogo(textDisplay, yelpUrl);
                }

                //Date picker init
                $('.yelp-filter-date-time').validation({});
                $('.reservationDateYelp').datepicker({
                    minDate: new Date(),
                });

                var initDate = new Date();
                var dateParsed = new Date($('.spa-info-1 h4.datetime-display').text().trim());
                if (!isNaN(dateParsed.getTime())) {
                    initDate = dateParsed;
                }
                $('.reservationDateYelp').datepicker('setDate', initDate);

                if($(".box-btn-book .item").length == 1) {
                    $(".box-btn-book .item").css("width","100%");
                }

                if ($('input[name="rad"]:checked').length === 0) {
                    $('.selecthours-msg-error').show();
                }

                /*remove last item in view mobile*/
                var windowsize = $(window).width();
                if (windowsize < 767 && $('.time-spa').find('li').length === 7) {
                    $('.time-spa').find('li').last().remove();
                }
                //Liem Buu forward ticket 1065
                //var b = ['blue', 'white', 'black','yellow'];
                //$('.slick-slide').each(function (index, item) {
                //    $(this).addClass(b[Math.floor(Math.random() * b.length)]);
                //})
                setTimeout(function () {
                    $('.slick-slide').each(function (index, item) {
                        $(this).clone().removeAttr('class').addClass('slider-2').appendTo($(this)).children().remove();
                    })
                }, 100);
                
                // Module-hero automatically set to height of image
                $(".module-hero, .top-slider").each(function () {
                    var el = $(this);
                    if (el.hasClass('auto-cropped')) {
                        // Set Interval set max heigth when all image is completely loaded
                        var checkFunction = setInterval(function () {
                            if (checkImageCompleteLoading(el)) {
                                consistencyHeroBanner(el);
                                clearInterval(checkFunction);
                            }
                        }, 100);
                    }
                });
                $(".top-slider").each(function () {
                    var el = $(this);
                    if (el.hasClass('auto-cropped')) {
                        // Set Interval set max heigth when all image is completely loaded
                        var checkFunction = setInterval(function () {
                            if (checkImageCompleteLoading(el)) {
                                consistencyHeroBanner(el);
                                clearInterval(checkFunction);
                            }
                        }, 1000);
                    }
                });

                $('[data-dd-toggle="spa-view-edit"]').on('click', function (e) {
                    e.preventDefault();
                    $('.selecthours-msg-error').hide();
                    $('.success, .edit-spa-search').css('display', 'none');
                    $('.yelp-form-infomation').css('display', 'none');
                    $('.spa-filter-search').removeAttr('style');
                    //
                    var radioId = $('input[name="rad"]:checked').attr('id');
                    var reservationTime = $('label[for="' + radioId + '"]').text().trim().toUpperCase();
                    //$('#reservationTime').val(reservationTime);
                    $('#reservationTime option').each(function () {
                        if ($(this).text().toUpperCase() === reservationTime) {
                            $(this).attr('selected', 'selected');
                            $('#reservationTime').next('div.btn-group-selectbox').find('button').attr('title', reservationTime);
                            $('#reservationTime').next('div').find('span.multiselect-selected-text').text(reservationTime);
                            return false;
                        }
                    });
                    //JRCW-431
                    var peopleSelect = $('.spa-info-2 h4.guests-submit').text().split(" ")[0];
                    var person = $('#reservationPeople').attr('person-select');
                    var people = $('#reservationPeople').attr('people-select');

                    var suffix = peopleSelect === "1" ? person : people;
                    var compareText = peopleSelect + suffix;
                    $('#reservationPeople option').each(function () {
                        var self = $(this);
                        if (self.text() === compareText) {
                            self.attr('selected', 'selected');
                            $('#reservationPeople').next('div.btn-group-selectbox').find('button').attr('title', compareText);
                            $('#reservationPeople').next('div').find('span.multiselect-selected-text').text(compareText);
                            return false;
                        }
                    });

                    setTimeout(autoScrollToSelectedItem('#timeDividerNewDinningForm, #reservationTime, #reservationPeople'), 0);
                });

                $('[data-dd-toggle="search-spa"]').on('click', function (e) {
                    e.preventDefault();
                    $('.success, .edit-spa-search').removeAttr('style');
                    $('.spa-search').css('display', 'none');
                });

                $('select[name="peopleSelect"]').on('change', function () {
                    var noOfGuest = this.value.trim().split(" ")[0];
                    var currentText = $('.spa-info-2 h4.guests-display').text();
                    var replaceText = currentText.split(" ")[0];
                    var newText = currentText.replace(replaceText, noOfGuest);
                    $('.spa-info-2 h4').text(newText);
                });

                //hide messge error when chose new time
                $(document).on('change', 'input[name="rad"]', function (e) {
                    if (this.checked) {
                        $('.selecthours-msg-error').hide();
                    }
                    //when user select one, change reservation-time and reservation-date on book on concierge href attribute
                    var reservationTime = $('label[for="' + $('input[name="rad"]:checked').attr('id') + '"]').text().trim();
                    var reservationDate = $('.datetime-submit')[0].textContent;
                    var guestNo = $('.guests-display')[0].textContent;
                    updateBookThroughConciergeLink(reservationTime, reservationDate, guestNo);
                   
                    if ( $('#spa-infor').valid() && $('#policy').is(':checked') ) {
                        EnableBtnReserveNow(true);
                    } else {
                        EnableBtnReserveNow(false);
                    };

                });                
                

                $('.spa-infor').validation({
                    MobileNumber: {
                        name: 'mobileNumber'
                    },
                    txtEmail: {
                        name: 'email'
                    },
                    txtFirstName: {
                        name: 'firstName'
                    },
                    txtLastName: {
                        name: 'lastName'
                    }
                });

                //Check Ticket 281 
                if ($('#spa-infor').length > 0) {
                    if ($('#spa-infor').valid() && $('#policy').is(':checked') && $('input[name="rad"]:checked').length > 0)
                        EnableBtnReserveNow(true);
                    else
                        EnableBtnReserveNow(false);

                    $('#spa-infor').find(':input[required]:visible').keyup(function () {
                        if ($('#spa-infor').valid() && $('#policy').is(':checked') && $('input[name="rad"]:checked').length > 0) {
                            EnableBtnReserveNow(true);
                        } else {
                            EnableBtnReserveNow(false);
                        };
                    });

                    $('#policy').change(function () {
                        if ($(this).is(':checked') && $('#spa-infor').valid() && $('input[name="rad"]:checked').length > 0)
                            EnableBtnReserveNow(true);
                        else
                            EnableBtnReserveNow(false);
                    });
                }

            });

          
        } 
    }
}();