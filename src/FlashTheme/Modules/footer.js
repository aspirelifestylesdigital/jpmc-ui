﻿//"use strict";
var Footer = function () {
    function renderModalVerifyCreditCard(ccNumberLabel, ccNumberPlaceHolder, ccNumberSubmitButton) {
        if ($('#modalVerifyCreditCard').length == 0) {
            var $footer = $('footer#footer');
            if ($footer.length > 0) {
                var modalContent = '<div id="modalVerifyCreditCard" class="modal fade apr-modal" data-backdrop="static">'
                    + '<div class="modal-dialog">'
                    + '<div class="modal-content">'
                    + '<div class="row">'
                    + '<div class="col-lg-12 col-md-12 col-sm-12" style="alignment: center; text-align: center">'
                    + '<div class="form-group">'
                    + '<label for="">'
                    + ccNumberLabel
                    + '<span></span>'
                    + '</label>'
                    + '<label id="error-message-modalVerifyCreditCard-footer" for="">'
                    + '<span></span>'
                    + '</label>'
                    + '<div id="resultMessageVerifyCreditCard" class="resultMessage  error" style="display:none;"></div>'
                    + '<div class="box-group" style="margin-top: 15px">'
                    + '<div class="box-item">'
                    + '<label for="" class="input-group-before">'
                    + '<span class="sprites icon-credit-card"></span>'
                    + '</label>'
                    + '<input type="password" maxlength="16" class="form-control" name="footerTxtCreditCardNumber" id="footerTxtCreditCardNumber" placeholder="' + ccNumberPlaceHolder + '">'
                    + '</div>'
                    + '</div>'
                    + '<div class="col-lg-12 col-md-12 col-sm-12 wrap-form-group check-availability">'

                    //+ '< div class="apr-modal-content" >'
                    + '<div class="form-group" style="margin-bottom: 0px">'

                    + '<div class="row col-lg-12 col-md-12 col-sm-12 text-center">'
                    //+ '<button type="submit" class="btn btn-apr btn-primary" id="btn-submit-new-bin">Ok</button>'
                    + '<button type="submit" class="btn btn-apr btn-primary btn-submit-CreditCardNumber-footer" name="typeSubmit" value="Validate Credit Card Number">'
                    + ccNumberSubmitButton
                    + '</button>'
                    + '</div>'


                    //+ '<div class="row col-lg-5 col-md-5 col-sm-5 text-center">'
                    ////+ '<button type="submit" class="btn btn-apr btn-primary" id="btn-submit-new-bin">Ok</button>'
                    //+ '<button type="submit" class="btn btn-apr btn-primary btn-submit-CreditCardNumber-footer" name="typeSubmit" value="Validate Credit Card Number">'
                    //+ ccNumberSubmitButton
                    //+ '</button>'
                    //+ '</div>'
                    //+ '<div class="row col-lg-2 col-md-2 col-sm-2 text-center">'
                    //+ '</div>'
                    //+ '<div class="row col-lg-5 col-md-5 col-sm-5 text-center">'
                    //+ '<button class="btn btn-apr btn-primary btn-cancel-CreditCardNumber-footer" name="typeSubmit" value="Cancel Validate Credit Card Number">'
                    //+ 'Cancel'
                    //+ '</button>'
                    //+ '</div>'


                    + '</div >'

                    //+ '<div class="apr-modal-content">'
                    //+     '<div class="row pdt-10 text-center">'
                    //+         '<button type="submit" class="btn btn-apr btn-primary" id="btn-submit-new-bin">Ok</button>'
                    //+     '</div>'
                    //+     '<div class="row pdt-10 text-center">'
                    //+         '<button type="submit" class="btn btn-apr btn-primary" id="btn-return-to-home-page">Cancel</button>'
                    //+     '</div>'
                    //+  '</div>'

                    //+ '<div class="form-group" style="margin-bottom: 0px">'
                    //+ '<button type="submit" class="btn btn-apr btn-red btn-submit-CreditCardNumber-footer" name="typeSubmit" value="Validate Credit Card Number">'
                    //+ ccNumberSubmitButton
                    //+ '</button>'
                    //+ '<button class="btn btn-apr btn-red btn-cancel-CreditCardNumber-footer" name="typeSubmit" value="Cancel Validate Credit Card Number">'
                    //+ 'Cancel'
                    //+ '</button>'
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    + '</div>';
                $footer.append(modalContent);
            }
        }
        //console.log('Modal has exist: ' + $('#modalVerifyCreditCard').length);
    }
    function autoModifyHoursOfOperationWidth() { //created: 03-05-2019, updated: 06-05-2019 - test with spas, nightlife, yelp, featured restaurant

        //auto Update hours of operation width
        function autoUpdateHoursOfOperationWidth() {
            var total = $('.text-right.hours-of-operation').width();
            //var total = $('.toggle-box').find('div').find('.text-right.hours-of-operation').width();
            var col1 = $('.hours-of-operation-col-1-yelp').width();
            var col4 = $('.hours-of-operation-col-4-yelp').width();
            var col2A3 = total - col1 - col4;
            //console.log(total, col1, col2A3, col4);
            var totalP = 100;
            var col1P = (col1 * 100) / total;
            var col2A3P = (col2A3 * 100) / total;
            var col4P = (col4 * 100) / total;
            //console.log(totalP, col1P, col2A3P, col4P);

            function autoUpdateWithOverSize() {
                //check if there is any break row situation
                //var sample = $('.toggle-box').find('div').find('.text-left.hours-of-operation').height();
                var sample = $('.text-left.hours-of-operation').height();
                var list = $('.hours-of-operation-col-4-yelp');
                var col4Height = list[0].clientHeight;
                for (let i = 0; i < list.length; i++) {
                    //console.log(list[i].clientHeight);
                    if (list[i].clientHeight > col4Height) {
                        col4Height = list[i].clientHeight;
                        //console.log(list[i].clientHeight);
                    }
                }

                //console.log(sample, col4Height);
                if (col4Height > sample) {
                    var changeValue = 1;
                    //increase width of col4 and decrease width of col1
                    var currentCol1Width = $('.hours-of-operation-col-1-yelp').width();
                    var currentCol4Width = $('.hours-of-operation-col-4-yelp').width();
                    //increase col 4 width by 1
                    $('.hours-of-operation-col-4-yelp').width(currentCol4Width + changeValue);
                    //decrease col 1 width by 1
                    $('.hours-of-operation-col-1-yelp').width(currentCol1Width - changeValue);
                    setTimeout(function () {
                        autoUpdateWithOverSize();
                    }, 5);
                } else {
                    //console.log('Finish modifying width of Hours of operation.');
                }

            }
            function autoUpdateWithTooSmallSize() {
                //check if there is any break row situation
                //var sample = $('.toggle-box').find('div').find('.text-left.hours-of-operation').height();
                var sample = $('.text-left.hours-of-operation').height();
                var list = $('.hours-of-operation-col-4-yelp');
                var col4Height = list[0].clientHeight;
                for (let i = 0; i < list.length; i++) {
                    //console.log(list[i].clientHeight);
                    if (list[i].clientHeight > col4Height) {
                        col4Height = list[i].clientHeight;
                        //console.log(list[i].clientHeight);
                    }
                }
                var changeValue = 1;
                if (col4Height <= sample) {
                    //reduce by 1 until it breaks line

                    //decrease width of col4 and increase width of col1
                    var currentCol1Width = $('.hours-of-operation-col-1-yelp').width();
                    var currentCol4Width = $('.hours-of-operation-col-4-yelp').width();
                    //decrease col 4 width by 1
                    $('.hours-of-operation-col-4-yelp').width(currentCol4Width - changeValue);
                    //increase col 1 width by 1
                    $('.hours-of-operation-col-1-yelp').width(currentCol1Width + changeValue);
                    setTimeout(function () {
                        autoUpdateWithTooSmallSize();
                    }, 5);
                } else {
                    autoUpdateWithOverSize();
                }

            }
            autoUpdateWithOverSize();
            autoUpdateWithTooSmallSize();

        }
        //if ($('.toggle-box').find('div').find('.text-right.hours-of-operation').width() != null) {
        if ($('.text-right.hours-of-operation').width() != null) {
            autoUpdateHoursOfOperationWidth();
        }
    }
    return {
        init: function (ccNumberLabel, ccNumberPlaceHolder, ccNumberSubmitButton) {
            renderModalVerifyCreditCard(ccNumberLabel, ccNumberPlaceHolder, ccNumberSubmitButton);
            //autoModifyHoursOfOperationWidth();
        },
        addin: function () { autoModifyHoursOfOperationWidth(); }
    }
}();

//helper function
function submitCreditCardNumber(verifyCCBeforeSubmit) {
    var creditNumber = $('#footerTxtCreditCardNumber').val();
    //console.log(creditNumber);
    UpdateCCN(creditNumber, verifyCCBeforeSubmit);
}
function UpdateCCN(creditNumber, verifyCCBeforeSubmit) {
    $.ajax({
        type: "POST",
        url: '/ServiceProvider_UpdateCCNumber',
        dataType: "json",
        data: { ccn: creditNumber },
        beforeSend: function () {
            $(".btn-submit-CreditCardNumber-footer").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
        },
        complete: function () {
            $("#ajaxLoading").remove();
        },
        success: function (response) {            
            //console.log("Success");
            if (response.CCNIsEmpty != null && response.CCNIsEmpty) {
                //console.log("CCNIsEmpty");
                //$('#resultMessageWithBin').css('display', 'none');
                //$('#resultMessageWithBin').next('br').remove();
                AddErrorMessage('modalVerifyCreditCard',
                    'modalVerifyCreditCard-error',
                    'error-message-modalVerifyCreditCard-footer',
                    response.MessageResult);
            }
            else {
                //console.log("CCNIs NOT Empty");
                //console.log(response);
                if (response.IsValidCCNumber != null && response.IsValidCCNumber) {
                    if (response.IsUpdated != null && response.IsUpdated) {
                        //Close modal
                        $("#modalVerifyCreditCard").modal('hide');
                        if (verifyCCBeforeSubmit != undefined && verifyCCBeforeSubmit != null && verifyCCBeforeSubmit.isCheckingCC) {
                            verifyCCBeforeSubmit.run();
                        }
                    } else {
                        //console.log("Fail to update CC Number");
                        if (response.MessageResult != null && response.MessageResult) {
                            AddErrorMessage('modalVerifyCreditCard',
                                'modalVerifyCreditCard-error',
                                'error-message-modalVerifyCreditCard-footer',
                                response.MessageResult);
                            //console.log(response.MessageResult);
                        }

                    }
                } else {
                    //showMessageVerifyBin(response.MessageResult);
                    //console.log("Fail to update CC Number");
                    if (response.MessageResult != null && response.MessageResult) {
                        AddErrorMessage('modalVerifyCreditCard',
                            'modalVerifyCreditCard-error',
                            'error-message-modalVerifyCreditCard-footer',
                            response.MessageResult);
                        //console.log(response.MessageResult);
                    }
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(ajaxOptions, thrownError);
            //console.log("Fail");
        }
    });
}
function AddErrorMessage(modalId, errorId, errorMessageId, errorMessage) {
    //$('#' + modalId).addClass("error").addClass("lt-error");
    //$('#' + modalId).attr('aria-invalid', 'true');
    if ($('#' + errorId).length === 0) {
        $('#' + errorMessageId).after('<label id="' + errorId + '" class="error lt-error-cc" for="' + modalId + '">' + errorMessage + '</label>');
    } else {
        if ($('#' + errorMessageId).css('display') === 'none') {
            $('#' + errorMessageId).css('display', 'inline-block');
        }
        $('#' + errorId).text(errorMessage);


    }
}

function defer() {
    if (window.jQuery && typeof ($.fn.modal) == 'function' && binValidated !== "") {

        var isBinValidate = binValidated;
        if (isBinValidate == false) {
            $("#modal-no-account-existed-2").modal('show');
        }
        if (ccnValidate == false) {
            $("#modalVerifyCreditCard").modal('show');
        }
        //when press on submit credit card number on modalVerifyCreditCard
        $(".btn-submit-CreditCardNumber-footer").on("click", function (event) {
            event.preventDefault();
            submitCreditCardNumber();

        });
        document.querySelector("input").addEventListener("keydown", function (e) {
            var charCode = e.charCode || e.keyCode || e.which;
            if (charCode == 27) {
                //alert("Escape is not allowed!");
                return false;
            }
        });

        $('input[name="footerTxtCreditCardNumber"]').on('change keyup, keydown, keypress', function (e) {
            if (e.keyCode == 13) {
                $('#modalVerifyCreditCard').find('.btn-submit-CreditCardNumber-footer').trigger('click')
            }
        });

        //$(".btn-cancel-CreditCardNumber-footer").on("click", function (event) {
        //    event.preventDefault();
        //    $("#modalVerifyCreditCard").modal('hide');
        //});

        function OpenTimeOutPopupModal() {
            if ($('#modal-timeout').hasClass('in')) {
                //console.log("modal-timeout is on");
            } else {
                setTimeout(function () { OpenTimeOutPopupModal() }, 50);
                $('#modal-timeout').modal('show');
            }
        }

        //If it is Chase Domain, ccnValidate will true or false, it will never be null or undefined
        //This line is to improve UX
        if (ccnValidate != undefined && ccnValidate != null) {
            $("#btnSessionContinue").attr("data-dismiss", "");
        }

        //btnSessionContinue when press on this button, check validate , if not validate show pop up to update new cc number
        $("#btnSessionContinue").on("click", function (event) {
            event.preventDefault();
            if (ccnValidate != undefined && ccnValidate != null) {

                //run the validate function in sign in
                //OpenTimeOutPopupModal();
                $.ajax({
                    type: "POST",
                    url: '/ServiceProvider_ValidateCCTimeOut',
                    dataType: "json",
                    data: {},
                    beforeSend: function () {
                        $("#btnSessionContinue").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
                        //OpenTimeOutPopupModal();
                    },
                    complete: function () {
                        //$("#ajaxLoading").remove();
                        OpenTimeOutPopupModal();
                        location.reload();

                    },
                    success: function (response) {
                        //console.log("Successfully ExeCute Validate After Time Out.");
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        //console.log(ajaxOptions, thrownError);
                        //console.log("Fail to ExeCute Validate After Time Out.");
                    }
                });
            }


        });

        //press ok
        $("#btn-submit-new-bin").on("click",
            function (event) {
                event.preventDefault();
                $("#modal-check-bin-pop-up-footer").modal('show');
                $("#modal-no-account-existed-2").modal('hide');
            });

        //press cancel
        $("#btn-return-to-home-page").on("click",
            function (event) {
                event.preventDefault();
                location.reload();
            });

        $(".btn-submit-binNumber-footer").on("click", function (event) {
            event.preventDefault();
            var creditNumber = $('#footerTxtBinNumber').val();
            $.ajax({
                type: "POST",
                url: '/ServiceProvider_ValidateBinNumber',
                dataType: "json",
                data: { binNumber: creditNumber },
                beforeSend: function () {
                    $(".btn-submit-binNumber-footer").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
                },
                complete: function () {
                    $("#ajaxLoading").remove();
                },
                success: function (response) {
                    if (response.BinIsEmpty != null && response.BinIsEmpty) {
                        //$('#resultMessageWithBin').css('display', 'none');
                        //$('#resultMessageWithBin').next('br').remove();
                        $('#footerTxtBinNumber').addClass("error").addClass("lt-error");
                        $('#footerTxtBinNumber').attr('aria-invalid', 'true');
                        if ($('#footerTxtBinNumber-error').length === 0) {
                            $('#footerTxtBinNumber').after('<label id="footerTxtBinNumber-error" class="error lt-error" for="footerTxtBinNumber">' + response.MessageResult + '</label>');
                        } else {
                            if ($('#footerTxtBinNumber-error').css('display') === 'none') {
                                $('#footerTxtBinNumber-error').css('display', 'inline-block');
                            }
                        }
                    }
                    else {
                        if (response.IsValidBinNumber != null && response.IsValidBinNumber) {
                            showContentSignUpForm();
                        } else {
                            showMessageVerifyBin(response.MessageResult);
                        }
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //console.log(ajaxOptions, thrownError);
                }
            });
        });
        //
        function showMessageVerifyBin(message) {
            //$('#resultMessageWithBin').html(message);
            //$('#resultMessageWithBin').next('br').remove();
            //$('#resultMessageWithBin').after('<br />');
            //$('#resultMessageWithBin').css('display', 'block');
        }

        function showContentSignUpForm() {
            //$('#resultMessageWithBin').css('display', 'none');
            //$('#resultMessageWithBin').next('br').remove();
            //$('.check-availability-bin-first').css('display', 'none');
            //$('.check-availability-email-first').css('display', 'block');
            //$('.noticeEnterInfo').css('display', 'block');
            //$('.noticeEnterInfoWithBin').css('display', 'none');
            updateBinNumber();
        }

        function updateBinNumber() {
            var creditNumber = $('#footerTxtBinNumber').val();
            var email = binUserName;
            var key = binKey;
            //var email = '@Model.UserName';
            //var key = '@Model.Password';
            $.ajax({
                type: "POST",
                url: '/ServiceProvider_UpdateBinNumber',
                dataType: "json",
                data: { binNumber: creditNumber, email: email, key: key },
                beforeSend: function () {
                    $(".btn-submit-binNumber-footer").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
                },
                complete: function () {
                    $("#ajaxLoading").remove();
                    $("#btn-return-to-home-page").click();
                },
                success: function (response) {
                    //reLogin();
                    $("#btn-return-to-home-page").click();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //console.log(ajaxOptions, thrownError);
                }
            });
        }
        
    } else {
        setTimeout(function () { defer() }, 50);
    }
}