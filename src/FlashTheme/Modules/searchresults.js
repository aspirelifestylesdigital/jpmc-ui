
var SearchResults = function () {
    function initSlick() {
        $(document).ready(function(){
            $('.slides-results').slick({
                dots: true,
                arrows: false,
                infinite: false,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 4,
                rows: 2,
                slidesPerRow : 1,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            slidesPerRow : 1
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            slidesPerRow : 1
                        }
                    }
                ]
            });
        });
    }

    function moduleFilter() {
        $('.list-checkbox .check-status').on("click", function() {
            var self = $(this);
            $(this).toggleClass('active');
            var subList = $(this).siblings('.sub-list-checkbox');
            if(subList.hasClass('active')){
                $(this).removeClass('active');
                subList.removeClass('active');
                subList.find('input').prop("checked", false);
            } else{
                $(this).addClass('active');
                subList.addClass('active');
                subList.find('input').prop("checked", true);
            }
            self.on("mouseup", function() {
                if(subList.find("input[type='checkbox']:checked").length == 0){
                    self.siblings("input").prop("checked", false);
                }
            });
        });
        $('.list-checkbox .sub-list-checkbox').on("click",function(){
            var self = $(this);
            if($(this).find("input[type='checkbox']:checked").length == 0 ){
                self.siblings("input").prop("checked", false);
            } else {
                self.siblings("input").prop("checked", true);
            }
        });
        $(document).on("click",".icon-arrow", function() {
            if($(this).hasClass('toggle')) {
                $(this).removeClass('toggle');
                $(this).parent().children('.list-checkbox').slideDown('fast');
            } else {
                $(this).addClass('toggle');
                $(this).parent().children('.list-checkbox').slideUp('fast');
            };
        });
    }
    return {
        init: function () {
            App.init();
            initSlick();
            moduleFilter();
        }
    }
}();