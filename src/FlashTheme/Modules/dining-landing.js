
var Gourment = function () {
    
    function moduleFilter() {
        $(document).on("click",".icon-arrow", function() {
            if($(this).hasClass('toggle')) {
                $(this).removeClass('toggle');
                $(this).parent().children('.list-checkbox').slideDown('fast');
            } else {
                $(this).addClass('toggle');
                $(this).parent().children('.list-checkbox').slideUp('fast');
            };
        });
    }

    return {
        init: function () {
            App.init();
            moduleFilter();
        }
    }
}();