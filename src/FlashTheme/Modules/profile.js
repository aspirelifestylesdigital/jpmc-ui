var Profile = function () {

    function moduleFilter() {
        $('.list-checkbox .check-status').on("click", function() {
            var self = $(this);
            $(this).toggleClass('active');
            var subList = $(this).siblings('.sub-list-checkbox');
            if(subList.hasClass('active')){
                $(this).removeClass('active');
                subList.removeClass('active');
                subList.find('input').prop("checked", false);
            } else{
                $(this).addClass('active');
                subList.addClass('active');
                subList.find('input').prop("checked", true);
            }
            self.on("mouseup", function() {
                if(subList.find("input[type='checkbox']:checked").length == 0){
                    self.siblings("input").prop("checked", false);
                }
            });
        });
        $('.list-checkbox .sub-list-checkbox').on("click",function(){
            var self = $(this);
            if($(this).find("input[type='checkbox']:checked").length == 0 ){
                // self.removeClass('active');
                self.siblings("input").prop("checked", false);
                // self.siblings("label").removeClass('active');
            } else {
                self.siblings("input").prop("checked", true);
            }
        });
        $(document).on("click",".icon-arrow", function() {
            if($(this).hasClass('toggle')) {
                $(this).removeClass('toggle');
                $(this).parent().children('.list-checkbox').slideDown('fast');
            } else {
                $(this).addClass('toggle');
                $(this).parent().children('.list-checkbox').slideUp('fast');
            };
        });
    }
    
    function searchableOption() {
        if($(".module-searchable-option").length > 0){
            var checkCurrentElement = [];
            $(".module-searchable-option").each(function(index){
                checkCurrentElement[index] = 0;
                var self = $(this);
                var placeHolder = $(this).find("select.show-option").attr("placeholder");
                var currentSearchableValue = $(this).find(".show-option").searchableOptionList({
                    maxHeight: '200px',
                    // data: function(){
                    //     return searchableData;
                    // },
                    texts: {
                        searchplaceholder: placeHolder,
                        selectAll: window.LabelSelectAllText,
                        selectNone: window.LabelNonSelectedText
                    },
                    events: {
                        onInitialized: function (sol, item, a) {
                            self.find(".sol-selection").addClass('scrollbar-outer');
                            self.find(".sol-selection").scrollbar();
                            if ($('.my-preferences')) {
                                var itemSelected = item.filter(function (e) {
                                    if (e.selected == true) return e.value;
                                })
                                self.find(".sol-input-container input").val(itemSelected[0].value);
                                self.find(".sol-option input").each(function (index, item) {
                                    if (item.value == itemSelected[0].value)
                                        $(item).parent().parent().addClass('active');
                                });
                            }
                        },
                        onChange: function(sol,changedElements){
                            var placeHolder = $(changedElements).closest("select.show-option").attr("placeholder");
                            var numSelected = $(changedElements).closest(".show-option").find(".sol-checkbox:checked").length;
                            if(numSelected > checkCurrentElement[index]){
                                $(changedElements).closest(".show-option").find(".sol-input-container input").val(numSelected + " selected");
                            } else{
                                $(changedElements).closest(".show-option").find(".sol-input-container input").val(placeHolder);
                            }
                            checkCurrentElement[index] = numSelected;
                            if ($('.my-preferences')) {    
                                $(changedElements).closest(".show-option").find(".sol-input-container input").val(changedElements[0].value);
                                $(changedElements).closest(".show-option").find('.active').removeClass('active');
                                changedElements.parent().parent().addClass('active');
                            }

                        }
                    }
                });
            });
        }
    }

    function formValidation() {
        $(".fr-profile").validation({
            firstName: {
                name: "FirstName",
                message: $('#txtFirstName').attr('data-message')
            },
            lastName: {
                name: "LastName",
                message: $('#txtLastName').attr('data-message')
            },
            salutation: {
                name: "Salutation",
                message: $('#salutation').attr('data-message')
            },
            selectCreditName: {
                name: "NameOnCard"
            },
            emailAddress: {
                name: "EmailAddress",
                message: $('#txtEmail').attr('data-message')
            },
            birthday: {
                name: "DateOfBirth"
            },
            txtContactNumber: {
                name: "MobileNumber"
            },
            txtCreditCard: {
                name: "CreditCardNumber"
            },
            txtExpiryDate: {
                name: "DateOfCard"
            },
            txtPassword: {
                name: "password",
                rule: {
                    required: false
                }
            },
            txtConfirmPassword: {
                rule: {
                    required: false,
                    equalTo: "#txtPassword"
                }
            },
            selectChallengeQuestion: {
                name: 'ChallengeQuestion'
            },
            txtChallengeAnswer: {
                name: 'ChallengeAnswer'
            }
        });
    }

    function handleChangeInputValue(elements) {
        $('#btnUpdate').attr('disabled', true);

        $('#frProfileId input:not(.fakeidfakename, #signup-check-password)').on('change paste', function () {
            var $this = $(this);
            if ($this.attr('id') === 'txtChallengeAnswer') {
                $this = $('#hiddenAnswer');
            }
            var defaultValue = $this.attr('data-default-value');
            var currentValue = $this.val().trim();
            if (typeof defaultValue === typeof undefined || defaultValue === false) {
                //input wrap inside dropdown
                var $dropdownEl = $this.closest('.btn-group-selectbox').prev();
                if ($dropdownEl.length > 0) {
                    defaultValue = $dropdownEl.attr('data-default-value');
                }
            }

            if (typeof defaultValue !== typeof undefined && defaultValue !== false && defaultValue.trim() === currentValue) {
                var elementCount = elements.length;
                var isChange = false;
                for (var i = 0; i < elementCount; i++) {
                    if (elements[i] !== $(this).attr('id')) {
                        var elementId = '#' + elements[i];
                        //use hidden field
                        if (elementId === '#txtChallengeAnswer') {
                            elementId = '#hiddenAnswer';
                        }
                        var elDefaultValue = $(elementId).attr('data-default-value');
                        var elCurrentValue = $(elementId).val();
                        if (elCurrentValue === '*******' && elementId === '#hiddenAnswer') {
                            elCurrentValue = '';
                        }
                        if (typeof elDefaultValue !== typeof undefined && typeof elCurrentValue !== typeof undefined && elDefaultValue.trim() !== elCurrentValue.trim()) {
                            //console.log('input value change same');
                            //if detect the 1st value change --> no need to check all elements 
                            isChange = true;
                            return false;
                        }
                    }
                }

                if (isChange) {
                    $('#btnUpdate').removeAttr('disabled');
                } else {
                    $('#btnUpdate').attr('disabled', true);
                }
            } else {
                $('#btnUpdate').removeAttr('disabled');
            }
            //$('#btnUpdate').removeAttr('disabled');
        });
    }
    //function validateTextbox() {
    //    $('.fr-preference').validation({
    //        txtdiningOthers: {
    //            name: 'OtherDiningPreference'
    //        },
    //        txtrtransferMess: {
    //            name: 'OtherRentalPreference'
    //        },
    //        txtfavouriteGolf: {
    //            name: 'favouriteGolf'
    //        }
    //    });
    //}
   	
    return {
        init: function () {
            var _elements = ['txtFirstName',
                'txtLastName',
                'txtEmail',
                'txtContactNumber',
                'salutation',
                'ChallengeQuestion',
                'txtChallengeAnswer'
            ];

            $(document).ready(function () {
                formValidation();
                handleChangeInputValue(_elements);
            });
            //App.init();
            // moduleFilter();
            searchableOption();
            //max length of textbox
            //validateTextbox();
            //event change radio filter
            $('input:radio[name=filterRight]').change(function () {
                if ($(this).val() == 'history') {
                    $(".request-actions").hide();
                }
                else{
                    $(".request-actions").show();
                }
            });

            $('.sol-label,.sol-radio').on('click', function () {
                alert('test');
            })
			
			// detect without area clicked
			jQuery(document).on('touchend click', function(e) {
				if(!jQuery(e.target).closest(".module-searchable-option").hasClass('module-searchable-option'))
					$('.sol-container').removeClass('sol-active');
			});
        }
    }
}();