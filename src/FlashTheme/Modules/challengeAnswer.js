﻿var challengeAnswer = function () {
    function checkAnswer(defaultValue) {
        var answer = $('input#txtChallengeAnswer');
        var hidden = '*******';
        var hiddenAnswer = $('#hiddenAnswer');
        var selectedValue = defaultValue;
        $('#txtChallengeAnswer').val(hidden);
        $('#txtChallengeAnswer').removeAttr('style');
        $('#fakeusername').val(' '); //Liem Buu fix auto fill in browser
        $('.fakeidfakename').css({ 'position': 'absolute', 'top': '-9999px' }); //Liem Buu fix auto fill in browser
        $('.challengeQuestions').find('.wrap-multiselect').addClass('challenge-select');
        $('.challenge-select a').click(function () {
            selectedValue = $(this).find('input').val();
            var answerValue = $('#txtChallengeAnswer');
            $('#selectChalengeQuestion').val();
            if (defaultValue != selectedValue) answerValue.val('');
            else {
                answerValue.val(hidden);
            }
            hiddenAnswer.val(answer.val());
            answerValue.blur();
        })
        answer
            .focusout(function () {
                if (defaultValue == selectedValue || answer.val() != '') answer.val(hidden);
            })
            .focus(function () {
                if (defaultValue == selectedValue && answer.val() == hidden) answer.val('');
                if (defaultValue == selectedValue && hiddenAnswer.val() == hidden) hiddenAnswer.val('');
                answer.val(hiddenAnswer.val());
            })
            .keyup(function () {
                hiddenAnswer.val(answer.val());
            })
    }

    function checkRequiredCurrentPassword() {
        $('.lt-check-password').change(function () {
            if ($('#txtPassword').val() == '' && $('#txtConfirmPassword').val() == '') $('#txtCurrentPassword').blur();
        })
    }

    return {
        init: function () {
            checkAnswer(defaultValue);
            checkRequiredCurrentPassword();
        }
    }
}();