function config(name) {
	return require('./tasks/' + name + '.js');
}

module.exports = function(grunt) {
	'use strict';

	// cach 1: automatic load Grunt plugins.
    require('matchdep')
        .filterDev('grunt-*')
        .forEach(grunt.loadNpmTasks);

	// Configuration
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		// concat: config('concat'),
		// jshint: config('jshint'),
		// uglify: config('uglify'),
		less: config('less'),
		watch: {
			options: { keepalive: true },
			files: [
				'less/**/*.less',
				'less/**/*.css',
				// 'scripts/**/*.js'
			],
			tasks: ['less']
		}
	});

	// Tasks - npm - grunt dist or grunt
	grunt.registerTask('dist', ['less']);
	grunt.registerTask('default', ['watch']);
};
