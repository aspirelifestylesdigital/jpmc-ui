
var Nightlife = function () {
    function initSlick() {
        $(document).ready(function(){

            createSlick();

        });
    }

    function moduleFilter() {
        $('.list-checkbox .check-status').on("click", function() {
            var self = $(this);
            $(this).toggleClass('active');
            var subList = $(this).siblings('.sub-list-checkbox');
            if(subList.hasClass('active')){
                $(this).removeClass('active');
                subList.removeClass('active');
            } else{
                $(this).addClass('active');
                subList.addClass('active');
            }
        });
    }
    function travelCommonAnimation(){
        var btnChangeThumnnail = $(".block-article-desc .box-btn button");
        btnChangeThumnnail.on("click", function(){
            btnChangeThumnnail.removeClass("active");
            $(this).addClass("active");
        });
        
    }
	function createSlick(){
		$('.slides-recommends').slick({
                dots: true,
                arrows: false,
                infinite: false,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 4,
                slidesPerRow : 1,
                rows:2,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            rows: 2,
                            slidesPerRow : 1
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            rows: 2,
                            slidesPerRow : 1
                        }
                    }
                ]
            });
	}
	function showBoxFilter() {
        var boxFilter = $(".form-filter-top"), filterBtn = $(".wrap-btn-filter .btn-filter-top"), btnCloseFilter = $(".form-filter-title .icon-close");
        $(window).on("load resize", function () {
            var winHeight = $(window).height();
            boxFilter.css("height", winHeight);
        });
        filterBtn.on("click", function () {
            boxFilter.addClass("active");
        });
        btnCloseFilter.on("click", function () {
            boxFilter.removeClass("active");
        });
    }
    return {
        init: function () {
            App.init();
            initSlick();
            moduleFilter();
        },
		appInit: App.init,
        uiInit: UI.init,
        initBoxFilter: showBoxFilter,
		createSlick: createSlick
    }
}();