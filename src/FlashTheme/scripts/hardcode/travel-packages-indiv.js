
var TravelPackagesIndiv = function () {

    function boxBookSticky(){
        $('body, html').css('overflow', 'visible');
        var positionContent = $(".content-indiv").offset().top;
        $(window).scroll(function(){
            if($(window).scrollTop() > positionContent){
                $(".box-btn-book").addClass("sticky");
            } else {
                $(".box-btn-book").removeClass("sticky");
            }
        });
    }

    function autoScrollItinerary(){
        $(document).ready(function(){
            // Handle when first-time loading
            var activeTab = $(".tab-pane.active");
            var leftHeight = activeTab.find(".tab-inner-left").outerHeight();
            var articleHeight = leftHeight - activeTab.find(".tab-inner-right .itinerary-title").height() - 30;
            activeTab.find(".tab-inner-right .itinerary-article").addClass('scrollbar-outer');
            if($(window).width() > 991){
                activeTab.find(".tab-inner-right .itinerary-article").scrollbar({
                    onUpdate: function(){
                        activeTab.find(".tab-inner-right .itinerary-article").css("max-height", articleHeight);
                    }
                });
            } else{
                activeTab.find(".tab-inner-right .itinerary-article").scrollbar();
            }

            // Handle when switch tab
            $('.nav-tabs a').on('shown.bs.tab', function(event){
                // var currentTab = $(event.target);
                var $this = $($(event.target).attr("href"));
                var leftHeight = $this.find(".tab-inner-left").outerHeight();
                var articleHeight = leftHeight - $this.find(".tab-inner-right .itinerary-title").height() - 30;
                $this.find(".tab-inner-right .itinerary-article").addClass('scrollbar-outer');
                if($(window).width() > 991){
                    $this.find(".tab-inner-right .itinerary-article").scrollbar({
                        onUpdate: function(){
                            $this.find(".tab-inner-right .itinerary-article").css("max-height", articleHeight);
                        }
                    });
                } else{
                    $this.find(".tab-inner-right .itinerary-article").scrollbar();
                }
            });
        });
    }

    return {
        init: function () {
            App.init();
            boxBookSticky();
            autoScrollItinerary();
            // Handle button book
            $(document).ready(function(){
                if($(".box-btn-book .item").length == 1) {
                    $(".box-btn-book .item").css("width","100%");
                }
            });
        } 
    }
}();
