var Faqs = function () {

    function  forceNoScrollBackgroundPopupIphone(){
        if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))) {
            $("body").css({"position":"fixed","overflow":"hidden", "width":"100%","top":"0"});
        }
    }

    function scrollAnimation(){
        $(".terms-index a").on("click", function(){
            $("html, body").animate({
                scrollTop: $($(this).attr("href")).offset().top - 100
            }, 500);
            return false;
        });
    }

    return {
        init: function () {
            App.init();
            scrollAnimation();
        }
    }
}();