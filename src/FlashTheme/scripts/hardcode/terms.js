var Terms = function () {

    function  forceNoScrollBackgroundPopupIphone(){
        if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))) {
            $("body").css({"position":"fixed","overflow":"hidden", "width":"100%","top":"0"});
        }
    }

    return {
        init: function () {
            App.init();
        }
    }
}();