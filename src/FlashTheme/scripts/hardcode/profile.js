var Profile = function () {

    function moduleFilter() {
        $('.list-checkbox .check-status').on("click", function () {
            var self = $(this);
            $(this).toggleClass('active');
            var subList = $(this).siblings('.sub-list-checkbox');
            if (subList.hasClass('active')) {
                $(this).removeClass('active');
                subList.removeClass('active');
                subList.find('input').prop("checked", false);
            } else {
                $(this).addClass('active');
                subList.addClass('active');
                subList.find('input').prop("checked", true);
            }
            self.on("mouseup", function () {
                if (subList.find("input[type='checkbox']:checked").length == 0) {
                    self.siblings("input").prop("checked", false);
                }
            });
        });
        $('.list-checkbox .sub-list-checkbox').on("click", function () {
            var self = $(this);
            if ($(this).find("input[type='checkbox']:checked").length == 0) {
                // self.removeClass('active');
                self.siblings("input").prop("checked", false);
                // self.siblings("label").removeClass('active');
            } else {
                self.siblings("input").prop("checked", true);
            }
        });
    }
    $(".challengeQuestions .multiselect-selected-text").on('click', function (e) {

        var ele = $(this).find(".challengeQuestions .multiselect-selected-text");
        var eleHeight = ele.parent().height();
        debugger
        // addEllipsisEndOfContent(ele, eleHeight);
        // function addEllipsisEndOfContent (ele, eleHeight) {

        // }
      });

    // var searchableData = [
    //     {"type": "option", "label": "Another option1", "value": "entry-1", "selected": true},
    //     {"type": "option", "label": "Another option2", "value": "entry-2"},
    //     {"type": "option", "label": "Another option3", "value": "entry-3", "selected": true},
    //     {"type": "option", "label": "Another option4", "value": "entry-4"},
    //     {"type": "option", "label": "Another option5", "value": "entry-5", "selected": true},
    //     {"type": "option", "label": "Another option6", "value": "entry-6", "selected": true},
    //     {"type": "option", "label": "Another option7", "value": "entry-7", "selected": true}
    // ];
    function searchableOption() {
        if ($(".module-searchable-option").length > 0) {
            var checkCurrentElement = [];
            $(".module-searchable-option").each(function (index) {
                checkCurrentElement[index] = 0;
                var self = $(this);
                var placeHolder = $(this).find("select.show-option").attr("placeholder");
                var currentSearchableValue = $(this).find(".show-option").searchableOptionList({
                    maxHeight: '200px',
                    // data: function(){
                    //     return searchableData;
                    // },
                    texts: {
                        searchplaceholder: placeHolder
                    },
                    events: {
                        onInitialized: function (sol) {
                            self.find(".sol-selection").addClass('scrollbar-outer');
                            self.find(".sol-selection").scrollbar();
                        },
                        onChange: function (sol, changedElements) {
                            var placeHolder = $(changedElements).closest("select.show-option").attr("placeholder");
                            var numSelected = $(changedElements).closest(".show-option").find(".sol-checkbox:checked").length;
                            if (numSelected > checkCurrentElement[index]) {
                                $(changedElements).closest(".show-option").find(".sol-input-container input").val(numSelected + " selected");
                            } else {
                                $(changedElements).closest(".show-option").find(".sol-input-container input").val(placeHolder);
                            }
                            checkCurrentElement[index] = numSelected;
                        }
                    }
                });
            });
        }
    }

    function checkAnswer(defauleValue) {
        var answer = $('input#Answer');
        var hidden = '*******';
        answer
            .blur(function () {
                if (answer.val() != '' && answer.val() != hidden) {
                    answer.attr('type', 'password');
                }
            })
            .focus(function () {
                if (answer.val() == hidden) {
                    answer.attr('type', 'text');
                    answer.val('')
                }
                if (answer.val() != '' && answer.val() != hidden) {
                    answer.attr('type', 'text');
                }
            })
        $('.challengeQuestions').find('.wrap-multiselect').addClass('challenge-select');
        // var defauleValue = $('#selectChalengeQuestion option[value="question3"]').val();
        $('.challenge-select a').click(function () {
            var selectedValue = $(this).find('input').val();
            var answerValue = $('#Answer');
            answerValue.attr('type', 'text');
            answerValue.blur();
            $('#selectChalengeQuestion').val();
            if (defauleValue != selectedValue) answerValue.val('');
            else answerValue.val(hidden);
        })
    }

    return {
        init: function () {
            App.init();
            // moduleFilter();
            searchableOption();
            checkAnswer();
            //event change radio filter
            $('input:radio[name=filterRight]').change(function () {
                if ($(this).val() == 'history') {
                    $(".request-actions").hide();
                }
                else {
                    $(".request-actions").show();
                }
            });
        }
    }

    
}();
