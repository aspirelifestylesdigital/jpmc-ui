$(document).ready(function () {
    $.when(UI.init()).done(setTimeout(autoScrollToSelectedItem('#timeDividerNewDinningForm, #reservationTime, #cuisineSelect'), 0)).done(setTimeout(hiddenScrollbar('#frPreferenceId')), 0);
    setTimeout(setBackgroundSlideIE, 0);

    setTimeout(blurBackgroundSlideIE, 200);
});
var UI = function () {
    function slickSlides() {
        if ($('.slides-intro').length > 0) {
            $('.slides-intro').slick({
                dots: true,
                arrows: true,
                speed: 300
            });
        }

        if ($('.slide-recommendations').length > 0) {
            $('.slide-recommendations').slick({
                dots: true,
                arrows: false,
                infinite: false,
                speed: 300,
                slidesToShow: 3,
                slidesToScroll: 3,
                responsive: [
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    }
                ]
            });
        }
    }

    // check if all images is completely loaded?
    function checkImageCompleteLoading(element) {
        var imageNumber = element.find('.slide').length;
        var count = 0;
        element.find('.slide').each(function () {
            var self = $(this);
            var img = new Image;
            var imgSrc = self.css('background-image').replace('url("', '');
            imgSrc = imgSrc.substring(0, imgSrc.length - 2)
            img.src = imgSrc
            if (img.complete) { count++; } else { return; }
        });
        if (count == imageNumber) { return true; }
    }

    // Set max height to images
    function consistencyHeroBanner(element) {
        if (!element.hasClass('module-hero-switcher')) {
            var maxHeight = 446;
            // Count maximum height of the biggest image
            element.find('.slide').each(function () {
                var self = $(this);
                var img = new Image;
                var imgSrc = self.css('background-image').replace('url("', '');
                imgSrc = imgSrc.substring(0, imgSrc.length - 2);
                img.src = imgSrc;
                var widthCoefficience = img.width / element.width();
                var selfHeight = img.height / (widthCoefficience > 1 ? widthCoefficience : 1);
                maxHeight = selfHeight >= maxHeight ? selfHeight : maxHeight;
            });

            // Set background-size: cover to the biggest image and background-size: 100% auto to others;
            //element.find('.slide').css('height', maxHeight);
            element.find('.slide').each(function () {
                var self = $(this);
                var img = new Image;
                var imgSrc = self.css('background-image').replace('url("', '');
                imgSrc = imgSrc.substring(0, imgSrc.length - 2);
                img.src = imgSrc;
                var widthCoefficience = img.width / element.width();
                //if (img.height > maxHeight || widthCoefficience > 1) {
                if (img.height > maxHeight) {
                    $(this).css({ 'background-size': 'contain', 'background-position': '50% 60%' });
                } else {
                    $(this).css({ 'background-size': 'auto', 'background-position': '50% 60%' });
                }
            });
        }
    }


    function changeInfoBook() {
        
        if ($('.info-book').length > 0) {
            var adult = $('.infoAdults').data("singular") || 'adult';
            var adults = $('.infoAdults').data("many") || 'adults';
            var child = $('.infoKids').data("singular") || 'child';
            var children = $('.infoKids').data("many") || 'children';
            var textReservation = typeof placeHolderSumPreferred == 'undefined' ? 'Preferred' : placeHolderSumPreferred;
            var textAlternative = typeof placeHolderSumAlternative == 'undefined' ? 'Alternative' : placeHolderSumAlternative;
            //ready when loading
            //Restaurant Name
            if ($(".nameOfRestaurant").val() != "") {
                $(".infoRestaurant").text($(".nameOfRestaurant").val());
            } else {
                $(".infoRestaurant").closest("p").hide();
                $(".infoRestaurant").css('display', 'none');
            }

            //Golf Club Name
            if ($(".nameOfGolfClub").val() != "") {
                $(".infoRestaurant").text($(".nameOfGolfClub").val());
            } else {
                $(".infoRestaurant").closest("p").hide();
            }

            //Reservation Time
            if ($(".reservationTime").val() != "") {
                $(".reservation").text($(".reservationTime").val());
            } else {
                $(".reservation").closest("p").hide();
                if ($('#fr-limoTransferForm').length > 0) {
                    $(".infoTime").closest("p").hide();
                }
            }
            //Preferred Time -- GolfForm
            if ($(".reservationTime").val() != "") {
                $(".infoReservation").text($(".reservationTime").val() + ' ' + '(' + textReservation + ')');
            } else {
                $(".infoReservation").closest("p").hide();
            }

            //Alternative Time
            if ($(".alternativeTime").val() != "") {
                $(".infoAlternative").text($(".alternativeTime").val() + ' ' + '(' + textAlternative + ')');
            } else {
                $(".infoAlternative").closest("p").hide();
            }

            //Pickup Time
            if ($(".getPickupTime").val() != "") {
                $(".infoPickupTime").text($(".getPickupTime").val());
            } else {
                $(".infoPickupTime").closest("p").hide();
            }

            //Dropoff Time
            if ($(".getDropoffTime").val() != "") {
                $(".infoDropOffTime").text($(".getDropoffTime").val());
            } else {
                $(".infoDropOffTime").closest("p").hide();
            }

            //Reservation Date                    
            if ($(".reservationDate").val() != "") {
                if ($(".reservationDate").hasClass("check-in-date")) {
                    $('.check-in-time').text($.datepicker.formatDate('D, M d, yy', new Date($(".reservationDate.check-in-date").val())));
                }
                if ($(".reservationDate").hasClass("check-out-date")) {
                    $('.check-out-time').text($.datepicker.formatDate('D, M d, yy', new Date($(".reservationDate.check-out-date").val())));
                }
                $('.infoDate').text($.datepicker.formatDate('D, M d, yy', new Date($(".reservationDate").val())));
                $(".infoDate").closest("p").show();
            } else {
                $(".infoDate, .check-in-time, .check-out-time").closest("p").hide();
                $(".infoDate").closest("p").hide();
            }
            // Location
            if ($(".nameOfLocation").val() != "") {
                $(".infoAddressRestaurant").text($(".nameOfLocation").val());   
                var locationValue = $(".nameOfLocation").val();
                if (locationValue) {
                    UpdateLocationData(locationValue.replace(",", "|"));
                }
                
            }
            //// Course Type
            //if ($(".courseType").val() != "") {
            //    $(".infoAddressRestaurant").text($(".courseType").val());
            //}
            // Number of Adults, Children, Players, Passengers
            var textAdults = ($('.numberOfAdults').val() <= 1)
                ? $('.numberOfAdults').val() + ' ' + adult
                : $('.numberOfAdults').val() + ' ' + adults;

            var textKids = ($('.numberOfKids').val() <= 1)
                ? $('.numberOfKids').val() + ' ' + child
                : $('.numberOfKids').val() + ' ' + children;

            var textPlayers = ($('.numberOfPlayers').val() <= 1)
                ? $('.numberOfPlayers').val() + ' ' + 'Player'
                : $('.numberOfPlayers').val() + ' ' + 'Players';

            //var textPlayers = ($('.numberOfPlayers').val() <= 1) ? $('.numberOfPlayers').val() + ' Player' : $('.numberOfPlayers').val() + ' Players';
            //var playersPlaceholder = $('.infoPlayers').data("placeholder");
            //if (playersPlaceholder) {
            //    textPlayers = $('.numberOfPlayers').val() + ' ' + playersPlaceholder;
            //}
            
            var numberOfPlayerSingularPlaceholder = $('.infoPlayers').data("placeholder-singular");
            var numberOfPlayerPluralPlaceholder = $('.infoPlayers').data("placeholder-plural");

            var textPassengers = ($('.getPassengerNum').val() <= 1) ? $('.getPassengerNum').val() + ' Passenger' : $('.getPassengerNum').val() + ' Passengers';
            var textPassengersMsg = $('.infoPassengerNum').attr('data-placeholder'); //infoPassengerNumMsgTranslate
            var textPassengersPluralMsg = $('.infoPassengerNum').attr('data-placeholder-plural');
            if (textPassengersMsg) {
                textPassengers = ($('.getPassengerNum').val() <= 1) ? $('.getPassengerNum').val() + ' ' + textPassengersMsg : $('.getPassengerNum').val() + ' ' + textPassengersPluralMsg;
            }


            $('.infoAdults').text(textAdults);
            $('.infoKids').text(textKids);
            $('.infoPlayers').text(textPlayers);

            //$('.infoPlayers').text($('.numberOfPlayers').val());
            $('.infoPassengerNum').text(textPassengers);
            // Car Type
            if ($(".getCarType").val() == "") {
                $(".infoCarType").closest("p").hide();
            }
            // Passenger Number
            if ($(".getPassengerNum").val() == "") {
                $(".infoPassengerNum").closest("p").hide();
            } else {
                var passNum = $(".getPassengerNum").val();
                if (passNum > 1) {
                    $('.infoPassengerNum').text(passNum + " Passengers");
                } else {
                    $('.infoPassengerNum').text(passNum + " Passenger");
                }
            }
            // Pickup location
            if ($(".getPickupLocation").val() == "") {
                $(".infoPickupLocation").closest("p").hide();
            }
            // Dropoff location
            if ($(".getDropoffLocation").val() == "") {
                $(".infoDropoffLocation").closest("p").hide();
            }

            var newDinningLayout = $('#timeDividerNewDinningForm');
            if (typeof (newDinningLayout) != 'undefined' && newDinningLayout.length > 0) {
                
                var currentTimeWithDevider = $('#timeDividerNewDinningForm option:selected').text().trim();
                var divider = currentTimeWithDevider.trim().substring(currentTimeWithDevider.length - 2, currentTimeWithDevider.length);
                $('.infoTime').text(currentTimeWithDevider);
                $('#ReservationTimeDivider').val(divider);

                $('#timeDividerNewDinningForm').change(function () {
                    var currentTimeWithDevider = $('#timeDividerNewDinningForm option:selected').text().trim();
                    var divider = currentTimeWithDevider.trim().substring(currentTimeWithDevider.length - 2, currentTimeWithDevider.length);
                    $('.infoTime').text(currentTimeWithDevider);
                    $('#ReservationTimeDivider').val(divider);
                });

                $('.timeDivider').next().find('label').on('click', function () { //'.btn-group.btn-group-selectbox'
                    $('.infoTime').text($(this).first().text());
                });

            }
            else {
                
                var currentTimeWithDevider = $('.form-control.reservationTime.clearable-select-box.valid option:selected').text().trim(); //$('#timeDividerNewDinningForm option:selected').text().trim();
                var divider = currentTimeWithDevider.trim().substring(currentTimeWithDevider.length - 2, currentTimeWithDevider.length);
                $('.infoTime').text(currentTimeWithDevider);
                $('#ReservationTimeDivider').val(divider);
                //$('.form-control.showDefaultVal.timeDivider.valid option:selected').text().trim() //

                $('#timeDividerNewDinningForm').change(function () {
                    var currentTimeWithDevider = $('.form-control.reservationTime.clearable-select-box.valid option:selected').text().trim();//$('#timeDividerNewDinningForm option:selected').text().trim();
                    var divider = currentTimeWithDevider.trim().substring(currentTimeWithDevider.length - 2, currentTimeWithDevider.length);
                    $('.infoTime').text(currentTimeWithDevider);
                    $('#ReservationTimeDivider').val(divider);
                });

                $('.timeDivider').next().find('label').on('click', function () { //'.btn-group.btn-group-selectbox'
                    $('.infoTime').text($(this).first().text());
                });
                // Display time
                var hourVal = parseInt($('.inputHour').val()),
                    minutesVal = parseInt($('.inputMinutes').val()),
                    timeDivider = "AM",
                    inputHour = $('.inputHour').val(),
                    inputMinutes = $('.inputMinutes').val();

                $('.inputHour').change(function () {
                    hourVal = parseInt($(this).val());
                    inputHour = $(this).val();
                    if ((hourVal >= 0 && hourVal <= 12) && (minutesVal >= 0 && minutesVal < 60)) {
                        $(".infoTime").closest("p").show();
                        $('.infoTime').text(inputHour + " : " + inputMinutes + " " + timeDivider);
                    }
                });
                $('.inputMinutes').change(function () {
                    minutesVal = parseInt($(this).val());
                    inputMinutes = $(this).val();
                    if ((hourVal >= 0 && hourVal <= 12) && (minutesVal >= 0 && minutesVal < 60)) {
                        $(".infoTime").closest("p").show();
                        $('.infoTime').text(inputHour + " : " + inputMinutes + " " + timeDivider);
                    }
                });
                $('.timeDivider').change(function () {
                    timeDivider = $(this).val();
                    if ((hourVal >= 0 && hourVal <= 12) && (minutesVal >= 0 && minutesVal < 60)) {
                        $('.infoTime').text(inputHour + " : " + inputMinutes + " " + timeDivider);
                    }
                });

                //
                var golfForm = $('#golfForm');
                if (golfForm && golfForm.length > 0) {
                    $('.reservationTime ').change(function () {                        
                        var data = $(this);
                        if (data && data != null && data != undefined && data.length > 0) {
                            var value = data.val();
                            if (value && value != null && value != undefined && value.length > 0) {
                                $(".infoTime").closest("p").show();
                                $('.infoTime').text(value);
                            }
                        }
                    });
                }

            }



            //change
            $(".getPickupTime").change(function () {
                $(".infoPickupTime").closest("p").show();
                $('.infoPickupTime').text($(this).val());
            });

            $(".getDropoffTime").change(function () {
                $(".infoDropOffTime").closest("p").show();
                $('.infoDropOffTime').text($(this).val());
            });

            $(".getPickupLocation").change(function () {
                $(".infoPickupLocation").closest("p").show();
                $('.infoPickupLocation').text($(this).val());
            });
            $(".getDropoffLocation").change(function () {
                $(".infoDropoffLocation").closest("p").show();
                $('.infoDropoffLocation').text($(this).val());
            });
            $('.nameOfRestaurant').change(function () {
                $(".infoRestaurant").closest("p").show();
                $('.infoRestaurant').text($(this).val());
                //if ($('.nameOfRestaurant').val() == "") {
                //    $(".infoRestaurant").closest("p").hide();

                //}
                $('.infoRestaurant').text() == "" ? $('.infoRestaurant').css('display', 'none') : $('.infoRestaurant').removeAttr('style');
                ($('.infoRestaurant').text() == "" && $('.infoAddressRestaurant').text() == "") ? $(".icon-maps").css('display', 'none') : $('.icon-maps').removeAttr('style')
            });
            $('.nameOfGolfClub').change(function () {
                $(".infoRestaurant").closest("p").show();
                $('.infoRestaurant').text($(this).val());
            });

            //$('.courseType').change(function () {
            //    $(".infoAddressRestaurant").closest("p").show();
            //    $('.infoAddressRestaurant').text($(this).val());
            //});

            $('.nameOfLocation').change(function () {
                $(".infoAddressRestaurant").closest("p").show();
                $('.infoAddressRestaurant').text($(this).val());
                ($('.infoRestaurant').text() == "" && $('.infoAddressRestaurant').text() == "") ? $(".icon-maps").css('display', 'none') : $('.icon-maps').removeAttr('style')
            });

            $('.reservationTime').change(function () {
                $(".infoReservation").closest("p").show();
                $('.infoReservation').text($(this).val() + ' ' + '(' + textReservation + ')');
                //for limo form only
                if ($('#fr-limoTransferForm').length > 0) {                    
                    //write to summary  
                    $(".infoTime").closest("p").show();
                    $('.infoTime').text($(this).val());
                }
            });
            
            $('.alternativeTime').change(function () {
                $(".infoAlternative").closest("p").show();
                $('.infoAlternative').text($(this).val() + ' ' + '(' + textAlternative + ')');
            });


            // get number of passengers
            if (textPassengersMsg) {
                printSummaryInfo('.getPassengerNum', '.infoPassengerNum', textPassengersMsg, textPassengersPluralMsg);
            } else {
                printSummaryInfo('.getPassengerNum', '.infoPassengerNum', textPassengersMsg, textPassengersPluralMsg);
            }

            // get number of adults
            printSummaryInfo('.numberOfAdults', '.infoAdults', adult, adults);

            // get number of children
            printSummaryInfo('.numberOfKids', '.infoKids', child, children);

            // get number of players
            if (numberOfPlayerSingularPlaceholder != null && numberOfPlayerPluralPlaceholder != null && numberOfPlayerSingularPlaceholder != undefined && numberOfPlayerPluralPlaceholder != undefined
                && numberOfPlayerSingularPlaceholder.length > 0 && numberOfPlayerPluralPlaceholder.length > 0) {            
                printSummaryInfo('.numberOfPlayers', '.infoPlayers', numberOfPlayerSingularPlaceholder, numberOfPlayerPluralPlaceholder);
            }
            else {
                printSummaryInfo('.numberOfPlayers', '.infoPlayers', 'Player', 'Players');
            }
        }
    }

    function printSummaryInfo(getClass, setClass, singularUnit, pluralUnit) {
        $(getClass).on('focus keyup', function (e) {
            var info = ($(getClass).val() <= 1) ? $(getClass).val() + ' ' + singularUnit : $(getClass).val() + ' ' + pluralUnit;
            $(setClass).text(info);
        });
        $(getClass).parent().find('.lt-spinner-prev').on('click touchstart', function () {
            setTimeout(function () {
                var info = ($(getClass).val() <= 1) ? $(getClass).val() + ' ' + singularUnit : $(getClass).val() + ' ' + pluralUnit;
                $(setClass).text(info);
            }, 100);
        });
        $(getClass).parent().find('.lt-spinner-next').on('click touchstart', function () {
            setTimeout(function () {
                var info = ($(getClass).val() <= 1) ? $(getClass).val() + ' ' + singularUnit : $(getClass).val() + ' ' + pluralUnit;
                $(setClass).text(info);
            }, 100);
        });
    }

    var FormSetting = new Object();
    FormSetting.FlightFormSetting =
        {
            maxAdultNumber: 10,
            maxKidNumber: 10,
            minAdultNumber: 1
        };
    FormSetting.HotelFormSetting =
        {
            minAdultNumber: 1
        };
    FormSetting.DiningFormSetting =
        {
            minAdultNumber: 1
        };
    function spinner() {
        if ($('.lt-spinner').length > 0) {
            $('.lt-spinner-prev').on('click touchstart', function (event) {
                event.preventDefault();
                var x = $(this).parent().find('.lt-spinner').val();
                //var checkLimit = ($(this).hasClass('flight-adults-number'));
                //if (checkLimit && (x == 1)) return;
                
                if (x > 0) {
                    x = parseInt(x) - 1;
                };
                if (!isNaN(x)) {
                    $(this).parent().find('.lt-spinner').val(x);
                }
                var $this = $(this).siblings(".lt-spinner").attr("id");
                if ($('.fr-booking').length) {
                    var valid = $('.fr-booking').validate().element("#" + $this);
                }
                
            });
            $('.lt-spinner-next').on('click touchstart', function (event) {
                event.preventDefault();
                var x = $(this).parent().find('.lt-spinner').val();
                //var checkAdultLimit = ($(this).hasClass('flight-adults-number'));
                //var checkKidsLimit = ($(this).hasClass('flight-kids-number'));
                //if (checkAdultLimit && (x > 9)) return;
                //if (checkKidsLimit && (x > 9)) return;
                //if (x > 9) return;
                x = parseInt(x) + 1;
                
                if (!isNaN(x)) {
                    $(this).parent().find('.lt-spinner').val(x);
                }
                var $this = $(this).siblings(".lt-spinner").attr("id");
                if ($('.fr-booking').length) {
                    var valid = $('.fr-booking').validate().element("#" + $this);
                }
            });            
            setMinValueForInputWithLtSpinner('#flight-fr-booking #numberOfAdults', '#numberOfAdults', FormSetting.FlightFormSetting.minAdultNumber)
            setMinValueForInputWithLtSpinner('#fr-travelForm #NumberOfAdults', '#NumberOfAdults', FormSetting.HotelFormSetting.minAdultNumber);
            setMinValueForInputWithLtSpinner('#diningForm #numberOfAdults', '#numberOfAdults', FormSetting.DiningFormSetting.minAdultNumber);

            setMaxValueForInputWithLtSpinner('#flight-fr-booking #numberOfAdults', '#numberOfAdults', FormSetting.FlightFormSetting.maxAdultNumber);
            setMaxValueForInputWithLtSpinner('#flight-fr-booking #numberOfKids', '#numberOfKids', FormSetting.FlightFormSetting.maxKidNumber);
        }
    }
    function hidePhoto() {
        if ($('.box-add-photo').length > 0) {
            $('.box-add-photo').on('click touchstart', '.figure', function () {
                $(this).hide();
            });
        }
    }
    function LightboxGoogleMap() {
        if ($('.google-map').length > 0) {
            $('.google-map').lightGallery({
                selector: 'this',
                appendSubHtmlTo: '.lg-item',
                addClass: 'info-maps',
                iframeMaxWidth: '95%',
                thumbnail: false,
                download: false,
                controls: false,
                autoplayControls: false,
                zoom: false,
                fullScreen: false,
                counter: false,
                hideBarsDelay: 10000,
                startClass: 'no-animation'
            });
            $('.google-map').on('onAfterOpen.lg', function (event, index) {
                $('.info-maps .scrollbar-outer').scrollbar();
                var height = $('.lg-video-cont').height();
                $('.info-map .scroll-wrapper').css('height', height - 30 + 'px');
                $('.lg-inner').append('<div class="lightbox-overlay"></div>');
                $('.lightbox-overlay').on('click', function () {
                    $(this).data('lightGallery').destroy(false);
                });
            });
            $('.google-map').on('onSlideItemLoad.lg', function (event, index) {
                $('.info-map').css('opacity', '1');
            });
            $('.google-map').on('onBeforeClose.lg', function (event, index) {
                $('.info-map').css('opacity', '0');
            });
        }
    }
    function LightBoxGallery() {
        if ($('.lightgallery').length > 0) {
            $('.lightgallery').lightGallery({
                thumbnail: false,
                download: false,
                controls: true,
                counter: false,
                autoplayControls: false,
                zoom: false,
                fullScreen: false,
                hideBarsDelay: 60000,
                appendSubHtmlTo: '.lg-item',
                addClass: 'gallery',
                startClass: 'no-animation',
                nextHtml: '<span class="sprites icon-next"></span>',
                prevHtml: '<span class="sprites icon-prev"></span>'
            });
            $('.lightgallery').on('onSlideItemLoad.lg', function (event, index) {
                var $this = $('.gallery .lg-item').eq(index);
                $this.find('.caption-gallery').css('width', $this.find('img').width() + 'px');
                $this.find('#lg-counter').css('width', $this.find('img').width() + 'px');

            });
            $('.lightgallery').on('onBeforeSlide.lg', function (event, index, prevIndex) {
                var $this = $('.gallery .lg-item').eq(prevIndex);
                $this.find('.caption-gallery').css('width', $this.find('img').width() + 'px');
                $this.find('#lg-counter').css('width', $this.find('img').width() + 'px');


            });
            $('.lightgallery').on('onBeforePrevSlide.lg onBeforeNextSlide.lg', function (event, index) {

                var $this = $('.gallery .lg-item').eq(index);
                setTimeout(function () {
                    $this.find('.caption-gallery').css('width', $this.find('img').width() + 'px');
                    $this.find('#lg-counter').css('width', $this.find('img').width() + 'px');
                }, 1);
            });
            getLeftItems($('.lightgallery'));
            $(window).resize(function () {
                getLeftItems($('.lightgallery'));
            });

            $(document).on('click', '.btn-close-gallery', function (event) {
                $('.lg-close').click();
            });
        }

        if ($('.lightbox-menu').length > 0) {
            $('.lightbox-menu').lightGallery({
                thumbnail: false,
                download: false,
                controls: true,
                counter: false,
                autoplayControls: false,
                selector: '.img-responsive',
                zoom: false,
                fullScreen: false,
                hideBarsDelay: 60000,
                appendSubHtmlTo: '.lg-item',
                addClass: 'gallery',
                startClass: 'no-animation',
                nextHtml: '<span class="sprites icon-next"></span>',
                prevHtml: '<span class="sprites icon-prev"></span>'
            });
            $('.lightbox-menu').on('onSlideItemLoad.lg', function (event, index) {
                var $this = $('.gallery .lg-item').eq(index);
                $this.find('.caption-gallery').css('width', $this.find('img').width() + 'px');
                $this.find('#lg-counter').css('width', $this.find('img').width() + 'px');

            });
            $('.lightbox-menu').on('onBeforeSlide.lg', function (event, index, prevIndex) {

                var $this = $('.gallery .lg-item').eq(prevIndex);
                $this.find('.caption-gallery').css('width', $this.find('img').width() + 'px');
                $this.find('#lg-counter').css('width', $this.find('img').width() + 'px');

            });
            $('.lightbox-menu').on('onBeforePrevSlide.lg onBeforeNextSlide.lg', function (event, index) {;
                var $this = $('.gallery .lg-item').eq(index);
                setTimeout(function () {
                    $this.find('.caption-gallery').css('width', $this.find('img').width() + 'px');
                    $this.find('#lg-counter').css('width', $this.find('img').width() + 'px');
                }, 1);
            });
            getLeftItems($('.lightbox-menu'));
            $(window).resize(function () {
                getLeftItems($('.lightbox-menu'));
            });

            $(document).on('click', '.btn-close-gallery', function (event) {
                $('.lg-close').click();
            });
        }
    }

    var getLeftItems = function (el) {
        el.each(function () {
            if ($(this).attr('data-screen')) {
                var totalItms = $(this).find('.item').length;
                var srcItms = $(this).attr('data-screen').split(',');
                var rsWidth = [];
                var rsNumItems = [];
                for (var i = 0; i < srcItms.length; i++) {
                    var __src = srcItms[i].split('|');
                    if (__src[0] === '') {
                        __src.splice(0, 1);
                    }
                    rsNumItems.push(__src[0]);
                    rsWidth.push(__src[1]);
                }
                var wWidth = $(window).width();
                for (var j = 0; j < rsWidth.length; j++) {
                    var leftItems = totalItms - parseInt(rsNumItems[j]);

                    if (parseInt(rsWidth[j], 10) < wWidth) {
                        $(".left-item").remove();
                        if (leftItems > 0) {
                            $(this).find('.item:nth-child(' + rsNumItems[j] + ')').prepend('<div class="left-item"><div><span>+' + leftItems + '</span></div></div>');
                        }
                        break;
                    }


                }
            }
        });
    };

    function limitChar() {
        if ($('.slides-intro').length > 0) {
            $('.slides-intro .slide .inner .title').dotdotdot();
            $('.slides-intro .slide .inner .hide-xs').dotdotdot();
        }
        if ($('.slides-recommends').length > 0) {
            $('.slides-recommends .slide .inner p').dotdotdot();
            $('.slides-recommends .slide .inner .desc').dotdotdot();
        }
        if ($('.slides-product').length > 0) {
            $('.slides-product .slide .content h3').dotdotdot();
            $('.slides-product .slide .content p').dotdotdot();
        }
        if ($('.block-blog').length > 0) {
            $('.block-blog .article .inner h3').dotdotdot();
            $('.block-blog .article .inner p').dotdotdot();
        }
        if ($('.tiles_module_title.dot-added').length > 0) {
            $('.tiles_module_title.dot-added').dotdotdot();
        }

    }


    //FIX JRCW-385
    function showCap(moduleid) {
        $(".capslide_img_cont").capslide({
                caption_color: '#fff',
                caption_bgcolor: '#549360',
                overlay_bgcolor: '#549360',
                border: '10px solid #549360',
                showcaption: true
        });

        $('.star-rate-link').on('click', function (e) { e.stopPropagation() });
    }

    function selectBox() {
        //single select
        
        
        if ($('[data-spy="singleSelectBox"]').length > 0) {
            $('[data-spy="singleSelectBox"]').each(function () {
                var pleaseSelectText = $(this).attr('placeholder');
               
                var noneSelectedPlaceHolder = typeof pleaseSelectText == 'undefined' ? 'Non selected' : pleaseSelectText;
                var $spy = $(this);

                $spy.multiselect({
                    onInitialized: function (event) {

                        $spy.siblings().find(".multiselect-selected-text").html($spy.data('placeholder'));
                        $spy.next().find(".container-multiselect").addClass('scrollbar-outer');

                        // Check if container have scroll bar or not.
                        var $container = $spy.next().find(".container-multiselect");
                        if ($container[0].scrollHeight > $container.height()) {
                            $container.scrollbar();
                        }

                        if ($spy.hasClass("lazy-loading")) {
                            $container.scroll(function (event) {
                                $spy.trigger("onSelectScroll", [event]);
                            });
                        }

                    },
                    onChange: function (option, checked, select) {

                        $spy.siblings().find(".multiselect-selected-text").css("color", "#555");
                        if ($spy.attr('id') == 'cuisineSelect') {
                            return false;
                        } else {
                            $spy.siblings().removeClass('open opened');
                        }
                    },
                    onDropdownShow: function (event) {
                        if ($('.fr-booking').length > 0) {
                            // $('#mainform').validate().element($spy);
                            $('.fr-booking').validate().element($spy);
                        }
                        if ($('.signUpForm').length > 0) {
                            // $('#mainform').validate().element($spy);
                            $('.signUpForm').validate().element($spy);
                        }

                        if ($spy.attr('id') == 'cuisineSelect') {
                            if ($(document).width() <= 992) {
                                $('.lt-overlay').addClass('active');
                                setTimeout(function () {
                                    $($spy).next().find('btn-group-selectbox').css('top', $('#cuisineSelect').offset().top + 'px');
                                    $('#cuisineSelect + .btn-group-selectbox').css({ 'z-index': '107156', 'background-color':'#ECE9E4'});
                                }, 1);
                            }

                        }
                        autoScrollToSelectedItem('#timeDividerNewDinningForm, #reservationTime, #cuisineSelect');
                    },
                   
                    nonSelectedText: noneSelectedPlaceHolder
                    // disableIfEmpty: true
                });

                if ($spy.attr('id') == 'cuisineSelect') {
                    initCuisineSelectBox($spy);
                }

            });
        }

        if ($(".noautocomplete_ak_dining").length > 0 || $(".noautocomplete_ak_sightseeing_activity").length > 0) {
            $(document).on("change", ".noautocomplete_ak_dining", function () {
                var filterLocation = $('.noautocomplete_ak_dining');
                var form = filterLocation.closest('form');
                var searchBox = null;
                if (isNotNull(form)) {
                    searchBox = form.find('input[name=filter_location]');
                    if (isNotNull(searchBox)) {
                        var newLocation = filterLocation.val();
                        searchBox.attr('data', newLocation);
                    }
                }
            });
            $(document).on("change", ".noautocomplete_ak_sightseeing_activity", function () {
                var filterLocation = $('.noautocomplete_ak_sightseeing_activity');
                var form = filterLocation.closest('form');
                var searchBox = null;
                if (isNotNull(form)) {
                    searchBox = form.find('input[name=filter_location]');
                    if (isNotNull(searchBox)) {
                        var newLocation = filterLocation.val();
                        searchBox.attr('data', newLocation);
                    }
                }
            });
        }

        // close singleSelectBox(remove class: opened) when focusing out
        $(document).on("click", ".btn-group-selectbox, .wrap-multiselect", function (e) {
            e.stopPropagation();
        });
        $(document).on("mousedown", function (e) {
            if ($(".btn-group-selectbox").hasClass("opened")) {
                $(".btn-group-selectbox").removeClass("opened");
            }
        });

        //muti select
        if ($('[data-spy="mutiSelectBox"]').length > 0) {
            $('[data-spy="mutiSelectBox"]').each(function () {
                var $spy = $(this);
                $spy.multiselect({
                     onInitialized: function(select, container) {
                         $spy.siblings().find(".multiselect-selected-text").html($spy.data('placeholder'));
                     },
                    onInitialized: function (event) {
                        $spy.next().find(".container-multiselect").addClass('scrollbar-outer');
                        $spy.next().find(".container-multiselect").scrollbar();
                    }
 
                });
            });
        }

        // menuSelect apply for selecting country and city
        if ($("[data-spy='menuSelect']").length > 0) {
            $("[data-spy='menuSelect']").each(function () {
                var selectMenu = $(this).selectmenu().closest("body").find(".ui-selectmenu-menu .ui-menu");
            });
        }
        // autoComplete apply for selecting country and city
        var countries = [
            { value: 'Andorra, Viet Nam', data: 'AD' }, { value: 'Indorra', data: 'ID' },
            { value: 'Bndorra', data: 'BD' }, { value: 'Jndorra', data: 'JD' },
            { value: '&&Cndorra', data: 'CD' }, { value: 'Kndorra', data: 'KD' },
            { value: 'Dndorra', data: 'DD' }, { value: 'Lndorra', data: 'LD' },
            { value: ';;Endorra', data: 'ED' }, { value: 'Mndorra', data: 'MD' },
            { value: 'Fndorra', data: 'FD' }, { value: 'Nndorra', data: 'ND' },
            { value: 'Gndorra', data: 'GD' }, { value: 'Rndorra', data: 'RD' },
            { value: 'Hndorra', data: 'HD' }, { value: 'Timbabwe', data: 'ZZ' },
            { value: 'Gndorra', data: 'GD' }, { value: 'Rndorra', data: 'RD' },
            { value: 'Wndorra', data: 'WD' }, { value: 'Sndorra', data: 'SD' },
            { value: 'Zndorra', data: 'ZD' }, { value: 'Pndorra', data: 'PD' },
            { value: 'Yndorra', data: 'YD' }, { value: 'Qndorra', data: 'QD' }
        ];
        
    }

   

    function initCuisineSelectBox(selectBox) {
        var wrap = selectBox.next().find('.wrap-multiselect');
        if ($(wrap).find('.select-action').length > 0) return;
        if (wrap && wrap.length > 0) {
            wrap.append(
                '<div class="select-action">' +
                ' <input type ="button" value="Cancel" class="select-button-cancel">' +
                '<input type="button" value="Done" class="select-button-done">' +
                '</div>'
            );
        }

    }

    function RemoveRow() {
        if ($('[data-spy="remove-row"]').length > 0) {
            $(document).on('click', '[data-spy="remove-row"]', function (event) {
                event.preventDefault();
                $(this).parent().remove();
            });
        }
    }

    function addMemberShip() {
        if ($('.btn-add-member').length > 0) {
            var numId = $("[data-spy='remove-row']").length + 1;
            $(document).on('click', '.btn-cofirm-add-member', function (event) {
                event.preventDefault();
                var $modal = $('#modalAddMembership');
                var category = $modal.find('#categoryName').val(),
                    nameProgramme = $modal.find('#programName').val(),
                    MembershipNumber = $modal.find('#membershipNum').val(),
                    dataCategory = category.replace(/\s/g, '-'),
                    $dataCategory = $('[data-category="' + dataCategory + '"]');
                if (dataCategory != '') {
                    var data = '<div class="row programme-item" data-category="' + dataCategory + '">'
                        + '<input type="hidden" id="loyaltId' + numId + '" name="loyaltyId' + numId + '" />'
                        + '<input type="hidden" id="categoryName' + numId + '" name="categoryName' + numId + '" value="' + category + '" />'
                        + '<div class="text-bold" id="categoryName' + numId + '" name="categoryName' + numId + '" value="' + category + '">' + category + '</div>'
                        + '<div class="btn-remove" data-spy="remove-row">'
                        + '<span>-</span> Remove Membership'
                        + '</div>'
                        + '<div class="col-md-6 col-sm-5 col-xs-12">'
                        + '<div class="form-group">'
                        + '<label for="loyaltyName">Name of Loyalty Programme</label>'
                        + '<div class="box-group">'
                        + '<label class="input-group-before" for=""><span class="ico ico-membership"></span></label>'
                        + '<input type="text" class="form-control" name="loyaltyName' + numId + '" id="loyaltyName' + numId + '" value="' + nameProgramme + '" placeholder="Please enter loyalty program category" required>'
                        + '</div>'
                        + '</div>'
                        + '</div>'
                        + '<div class="col-md-6 col-sm-5 col-xs-12">'
                        + '<div class="form-group">'
                        + '<label for="membershipNum">Membership Number</label>'
                        + '<div class="box-group">'
                        + '<label class="input-group-before" for=""><span class="ico ico-membership"></span></label>'
                        + '<input type="text" class="form-control" name="membershipNum' + numId + '" id="membershipNum' + numId + '" value="' + MembershipNumber + '" placeholder="Please enter membership number" required>'
                        + '</div>'
                        + '</div>'
                        + '</div>'
                        + '</div>';
                    if ($('.modal-form').valid()) {
                        $('[data-spy="ProgramSection"]').append(data);
                        numId++;
                        var index = numId - 1;
                        $('#numId').val(index);
                        $('#modalAddMembership').modal('hide');
                    }
                }
                return false;
            });
        }
    }


    //show textbox select Option
    function textBoxOption() {
        var generalList = [];
        if ($('.module-show-option').length > 0) {
            $('.module-show-option').each(function (index) {
                var currentList = [];

                // Creating a global array when loading
                var inputValue = $(this).find('[data-spy="boxData"] input');
                if (inputValue.length > 0) {
                    inputValue.each(function () {
                        currentList.push($(this).val());
                    });
                }
                generalList.push(currentList);

                // Handle when pressing enter or tab
                $(this).find('.textBoxOption').on('keydown', function (e) {
                    if ((e.keyCode == 13) || (e.keyCode == 9)) {
                        textContent = $(this).val();
                        if ((textContent != "") && ($.inArray(textContent, generalList[index]) == -1)) {
                            var ContentShow = '<span class="label-option">'
                                + '<input type="text" hidden value="' + textContent + '">'
                                + textContent
                                + '<i class="mask-close">x</i></span>';
                            $(this).closest(".module-show-option").find('.box-show-option').append(ContentShow);
                            generalList[index].push(textContent);
                        }
                        $(this).val("");
                    }
                });

                // Handle when focusout
                $(this).find('.textBoxOption').on('focusout', function (e) {
                    textContent = $(this).val();
                    if ((textContent != "") && ($.inArray(textContent, generalList[index]) == -1)) {
                        var ContentShow = '<span class="label-option">'
                            + '<input type="text" hidden value="' + textContent + '">'
                            + textContent
                            + '<i class="mask-close">x</i></span>';
                        $(this).closest(".module-show-option").find('.box-show-option').append(ContentShow);
                        generalList[index].push(textContent);
                    }
                    $(this).val("");
                });
            });

            // Handle event when remove item from .label-option
            $('.module-show-option').each(function (index) {
                $(this).on('click', '.label-option', function () {
                    var Clone = $(this).clone();
                    $(Clone.find('i'), Clone).remove();
                    var optionValue = Clone.text();
                    $(this).remove();
                    generalList[index].splice($.inArray(optionValue, generalList[index]), 1);
                });
            });
        }
    }

    function clearInputFields() {
        $(".clearable-field").val("").removeProp("readonly");
        $(".only-clearable-value").val("");
        $(".only-clearable-number").val("0");

        $('.custom-dining-form input.reservationDate').datepicker('setDate', new Date());
        setSelectValueSelect('timeDividerNewDinningForm', '19:00');        
        updateReservationTimeOnDiningForm('19:00', '7:00 PM');
        $('textarea[name=SpecialRequirements]').val('');
        $('input[name=NumberOfAdults]').attr('value');

        $('input[name=NumberOfAdults]').val($('input[name=NumberOfAdults]').attr('value'));
        $('input[name=NumberOfChildren]').val($('input[name=NumberOfChildren]').attr('value'));

        //JRCW-408
        //Hotel form
        $('input[name=NumberOfRooms]').val($('input[name=NumberOfRooms]').attr('value')); 
        $('input[name=NumberOfPlayers]').val($('input[name=NumberOfPlayers]').attr('value'));


        $('input[name=IsSmoking]').removeAttr('checked');

        $('input[id=nonSmokingRoom]').prop('checked', 'checked');

        $('input[name=CheckInDate]').val('');
        $('input[name=CheckOutDate]').val('');
        $('input[name=Region]').val('');
        $('input[name=MinimumPrice]').val('');

        $('input[name=MaximumPrice]').val('');
        $('input[name=LoyatlyProgram]').val('');
        $('input[name=LoyatlyProgramMembership]').val('');



        $('input[name=IsHotelNameRecommend]').removeAttr('checked');
        $('input[id=IsHotelName]').prop('checked', 'checked');
        $('input[id=WithBreakfast]').prop('checked', 'checked');
        $('input[id=WithWifi]').prop('checked', 'checked');






        //Golf form
        $('input[name=Caddy]').removeAttr('checked');
        $('input[name=Buggy]').removeAttr('checked');
        $('input[name=MemberOfGolfClub]').removeAttr('checked');


        $('input[name=PreferredRegion]').val('');
        $('input[name=Handicap]').val('');

        $('input[id=cbxRecommendation]').prop('checked', 'checked');
        $('[data-spy=boxShow]').css('display', 'block');
        $('input[name=PlayDate]').val('');




        //Limo form
        $('input[name=NumberOfPassengers]').val($('input[name=NumberOfPassengers]').attr('value'));

        var adult = $('.infoAdults').data("singular") || 'adult';
        var adults = $('.infoAdults').data("many") || 'adults';
        var child = $('.infoKids').data("singular") || 'child';
        var children = $('.infoKids').data("many") || 'children';
        var textAdults = ($('.numberOfAdults').val() <= 1)
            ? $('.numberOfAdults').val() + ' ' + adult
            : $('.numberOfAdults').val() + ' ' + adults;

        var textKids = ($('.numberOfKids').val() <= 1)
            ? $('.numberOfKids').val() + ' ' + child
            : $('.numberOfKids').val() + ' ' + children;

        $('.infoAdults').text(textAdults);
        $('.infoKids').text(textKids);



        $(".clearable-select-box").each(function (index) {
            var emptyOption = $(this).find('option[value=""]');
            if (typeof (emptyOption) !== 'undefined' && emptyOption.length > 0) {
                $(this).val("");
            } else {
                var placeHolder = $(this).attr("placeHolder");
                if (typeof (placeHolder) === 'undefined' || placeHolder == null) {
                    placeHolder = "Please Select";
                }
                $(this).prepend("<option value='' selected='selected'>" + placeHolder + "</option>");
                $(this).multiselect('rebuild');
            }
            $(this).multiselect('refresh');
        });

        $("select").each(function (index) {
            var emptyOption = $(this).find('option[value=""]');
            if (typeof (emptyOption) !== 'undefined' && emptyOption.length > 0) {
                $(this).val("");
            } else {
                var placeHolder = $(this).attr("placeHolder");
                if (typeof (placeHolder) === 'undefined' || placeHolder == null) {
                    placeHolder = "Please Select";
                }
            }
            if ($(this).attr('name') == 'RoomType') {
                var options = $(this).children().clone();
                $(this).empty().html(options);
            }
            if ($(this).attr('name') == 'StarRating') {
                var options = $(this).children().clone();
                $(this).empty().html(options);
            }
            $(this).multiselect('refresh');
        });

        $(".clearable-checkbox").each(function (index) {
            $(this).prop('checked', false);
        });

        $(".summary-info").find("p").hide();
        UI.changeInfoBox();
    }


    function commonAnimation() {
        $(document).ready(function () {
            $(".ui-autocomplete-input").val("");
            $(".page-profile").closest("body").find(".ui-autocomplete").css("z-index", "4000");
        });
        $(document).on("click", ".box-category .list-checkbox li", function () {
            $(".box-category .list-checkbox li").removeClass('active');
            $(this).addClass('active');
        });

        // Remove non-numberic characters && Handle Hour and Minutes
        $("[data-spy='numberOnly']").on("keypress", function (evt) {
            var e = evt || event;
            // var code = e.keyCode ? e.keyCode : e.which;
            var code = e.which;
            if (code != 8 && code != 0 && (code < 48 || code > 57)) {
                return false;
            } else {
                if ($(this).hasClass("2characters") && $(this).val().length > 1 && code != 8 && code != 0) {
                    return false;
                } else {
                    return true;
                }
            }
        });

        // Remove non-numberic characters
        $(".numberOfAdults, .numberOfKids, .getPassengerNum, .numberOfPlayers, #MinimumPrice, #MaximumPrice, #LoyatlyProgramMembership").on("keypress", function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });

        // getTour and getCruise
        $(".getCruise, .getTour").on("change", function () {
            if ($(this).prop("checked") == true) {
                $(".getCruise, .getTour").siblings(".error").hide();
            }
        });

        // pagination
        $(document).on("keypress", '[data-spy="pagination"]', function (event) {
            var charCode = (event.which) ? event.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        });

        // close keyboard on ios after enter key is pressed
        $(document).on("keypress", ".simple-pagination input[type='number']", function () {
            var charCode = (event.which) ? event.which : event.keyCode
            if (charCode == 13) {
                $(this).blur();
            }
        });

        // Display Fields following new rules
        if ($("[data-field='name']").length > 0) {
            $("[data-field='name']").each(function () {
                $(this).fieldRule("name");
            });
        }
        if ($("[data-field='email']").length > 0) {
            $("[data-field='email']").each(function () {
                $(this).fieldRule("email");
            });
        }
        if ($("[data-field='birthday']").length > 0) {
            $("[data-field='birthday']").each(function () {
                $(this).fieldRule("birthday");
            });
        }

        // Handle defaultValue when loading
        $(document).ready(function (e) {
            $("[data-default='defaultValue']").each(function () {
                if ($(this).val() == "" || $(this).val() == undefined) {
                    $(this).removeProp("readonly");
                }
            });
        });

        // Handle clear button in booking form when clicking
        $(document).on("click", "[data-spy='btnClear']", function (e) {
            e.preventDefault();
            $(".infoRestaurant").parent().hide();
            $("[data-default='defaultValue']").val("").removeProp("readonly");
            clearInputFields();
            return false;
        });

        // Handle Showing and Hiding a div by using a checkbox
        $(document).ready(function () {
            if ($("[data-spy='cbxShowHide']").prop("checked") == true) {
                $("[data-spy='boxShow']").show();
                $("[data-spy='boxHide']").hide();
            } else {
                $("[data-spy='boxShow']").hide();
                $("[data-spy='boxHide']").show();
            }
        });
        $(document).on("change", "[data-spy='cbxShowHide']", function () {
            if ($(this).prop("checked") == true) {
                $("[data-spy='boxShow']").show();
                $("[data-spy='boxHide']").hide();
                $(".nameOfRestaurant").removeClass("error");
                $(".nameOfRestaurant").siblings(".error").hide();
                $('.nameOfRestaurant').prop('required', null);

                $(".nameOfGolfClub").removeClass("error");
                $(".nameOfGolfClub").siblings(".error").hide();
                $('.nameOfGolfClub').prop('required', null);
            } else {
                $("[data-spy='boxShow']").hide();
                $("[data-spy='boxHide']").show();
                $('.nameOfRestaurant').prop('required', 'required');
                $('.nameOfGolfClub').prop('required', 'required');
            }
        });

        // Remove error message of email and phone
        $("input.alterFirstValue, input.alterSecondValue").on("change", function () {
            $(this).closest(".box-choices").find("label.error").css("display", "none");
            $(this).closest(".box-choices").find("input.error, select.error").removeClass("error lt-error");
        });
        $("input.getContactedEmail").on("change", function () {
            $(this).closest(".contact-email").find("label.error").css("display", "none");
            $(this).closest(".contact-email").find("input.error, select.error").removeClass("error lt-error");
            $("input.getContactedMobile").siblings("label.error").css("display", "none");
        });
        $("input.getContactedMobile").on("change", function () {
            $(this).closest(".contact-phone").find("label.error").css("display", "none");
            $(this).closest(".contact-phone").find("input.error, select.error").removeClass("error lt-error");
            $("input.getContactedEmail").siblings("label.error").css("display", "none");
        });

        // Automatically change status of email and mobile phone checkbox when typing.
        $(".contact-email input[type='text']").on("keyup", function () {
            if ($(this).val() != "") {
                $(".getContactedEmail").prop("checked", true);
            }
        });
        $(".contact-phone input[type='text']").on("keyup", function () {
            if ($(this).val() != "") {
                $(".getContactedMobile").prop("checked", true);
            }
        });

        // Allow submit on form only one time
        $(document).on('submit', 'form', function (e) {
            $("button.btn-red").attr("disabled", true);
            // or change if to an loading gif image.
            return true;
        });

        //one-way Option Ticket
        $(document).on("click", ".onewayOption", function () {
            $("[data-ticket='oneway']").hide("fast");
        });
        $(document).on("click", ".returnOption", function () {
            $("[data-ticket='oneway']").show("fast");
        });

        //scrollTop when update info in preferences
        $(document).on("click", "[data-spy='updateBtn']", function () {
            $("html, body").animate({
                scrollTop: 0
            }, 300);
            return false;
        });

        // Partially editable field
        var queryStringValue = getParameterByName("requestType");
        if ($("[data-spy='partiallyEditableField']").length > 0 && queryStringValue) {
            $("[data-spy='partiallyEditableField']").each(function () {
                $(this).data().length = $(this).val().length;
                $(this).on('keypress, keydown', function (event) {
                    var readOnlyLength = $(this).data().length;
                    if ((event.which != 37 && (event.which != 39))
                        && ((this.selectionStart < readOnlyLength)
                            || ((this.selectionStart == readOnlyLength) && (event.which == 8)))) {
                        return false;
                    }
                });
            });
        }
    }

    // Generate passenger table
    function generatePassengerTable(maxNum) {
        $(".getAdult").on("change", function () {

            // Generate getChild field
            var adultNum = $(this).val();
            var currentChildNum = $('.getChild').val();
            var visualChildNum = maxNum - parseInt(adultNum);
            var options = [];

            for (var i = 0; i <= visualChildNum; i++) {
                var item = 0;
                if (i == currentChildNum) {
                    item = { label: i, title: i, value: i, selected: true };
                } else {
                    item = { label: i, title: i, value: i };
                }
                options.push(item);
            }
            $('.getChild').multiselect('dataprovider', options);
            $('.getChild').multiselect('rebuild');

            // Generate passenger table
            if (currentChildNum == "") {
                currentChildNum = 0;
            }
            functionGenerate(adultNum, currentChildNum);

        });
        $(".getChild").on("change", function () {

            // Generate getAdult field
            var childNum = $(this).val();
            var currentAdultNum = $('.getAdult').val();
            var visualAdultNum = maxNum - parseInt(childNum);
            var options = [];

            for (var i = 1; i <= visualAdultNum; i++) {
                var item = 0;
                if (i == currentAdultNum) {
                    item = { label: i, title: i, value: i, selected: true };
                } else {
                    item = { label: i, title: i, value: i };
                }
                options.push(item);
            }
            $('.getAdult').multiselect('dataprovider', options);
            $('.getAdult').multiselect('rebuild');

            // Generate passenger table
            if (currentAdultNum != "") {
                functionGenerate(currentAdultNum, childNum);
            }
        });

    }

    // Function Generate passenger table
    function functionGenerate(adultNum, childNum) {
        var travelerNum = parseInt(adultNum) + parseInt(childNum);
        $("[data-spy='tablePassenger']").find("tbody").html("");
        for (var i = 1; i <= travelerNum; i++) {
            if (i <= adultNum) {
                if (i == 1) {
                    var checked = "checked";
                } else {
                    var checked = "";
                }
                var adultItem =
                    '<tr>'
                    + '<td>Traveler ' + i + ' (Adult)<span class="txt-red">*</span></td>'
                    + '<td class="text-center">'
                    + '<input type="radio" name="leadPassenger" id="traveler-' + i + '"' + checked + '>'
                    + '<label for="traveler-' + i + '"></label>'
                    + '</td>'
                    + '<td>'
                    + '<div class="box-group">'
                    + '<label class="input-group-before" for="">'
                    + '<span class="sprites icon-user"></span>'
                    + '</label>'
                    + '<input type="text" class="form-control" name="firstName' + i + '" id="firstName' + i + '" placeholder="Please enter first name" required>'
                    + '</div>'
                    + '</td>'
                    + '<td>'
                    + '<div class="box-group">'
                    + '<label class="input-group-before" for="">'
                    + '<span class="sprites icon-user"></span>'
                    + '</label>'
                    + '<input type="text" class="form-control" name="lastName' + i + '" id="lastName' + i + '" placeholder="Please enter last name" required>'
                    + '</div>'
                    + '</td>'
                    + '</tr>';
            } else {
                var adultItem =
                    '<tr>'
                    + '<td>Traveler ' + i + ' (Child)<span class="txt-red">*</span></td>'
                    + '<td>'
                    + '</td>'
                    + '<td>'
                    + '<div class="box-group">'
                    + '<label class="input-group-before" for="">'
                    + '<span class="sprites icon-user"></span>'
                    + '</label>'
                    + '<input type="text" class="form-control" name="firstName' + i + '" id="firstName' + i + '" placeholder="Please enter first name" required>'
                    + '</div>'
                    + '</td>'
                    + '<td>'
                    + '<div class="box-group">'
                    + '<label class="input-group-before" for="">'
                    + '<span class="sprites icon-user"></span>'
                    + '</label>'
                    + '<input type="text" class="form-control" name="lastName' + i + '" id="lastName' + i + '" placeholder="Please enter last name" required>'
                    + '</div>'
                    + '</td>'
                    + '</tr>';
            }
            $("[data-spy='tablePassenger']").find("tbody").append(adultItem);
        }
    }

    function datepicker() {
        if ($('.datepicker').length > 0) {
            $('.datepicker').datepicker({
                showOtherMonths: true,
                selectOtherMonths: false,
                dateFormat: "DD, MM d, yy",
                setDate: new Date(),
                changeMonth: true,
                changeYear: true,
                minDate: new Date(),
                beforeShow: function (input, inst) {
                    $('#ui-datepicker-div').addClass("booking-datepicker");
                    if ($('.ui-datepicker').closest("body").find(".page-my-reminders").length > 0) {
                        $(this).closest("body").find('#ui-datepicker-div').addClass("bigger-ui-datepicker");
                    }
                    if ($(document).width() <= 992) {
                        $('.lt-overlay').addClass('active');
                    }
                    if ($(window).width() <= 767 && $('.btn-nav').is(':visible')) {
                        setTimeout(function () {
                            $('#ui-datepicker-div').css('top', $('.datepicker').offset().top + 'px');
                        }, 1);
                    }
                },
                onSelect: function (selectedDate) {
                    if ($(document).width() <= 992) {
                        $('.lt-overlay').removeClass('active');
                    }
                }
            });
            $('.lt-overlay').on('click touchstart', function () {
              

                if ($('#cuisineSelect+.btn-group-selectbox').hasClass('open')) {
                    return false;
                } 
                $('.datepicker').datepicker("hide");
                $(this).removeClass('active');
            });
        }
    }

    function resizeHeroModuleDepentOnNav() {
        $(window).resize(function () {
            var navHeight = $('.aspire-nav').height();
            $('.wrapper').css('padding-top', navHeight);
        });
    }
    function navAnimation() {
        // fix JRCW-428
        $('.module-help').hover(function () {
            $(this).css('transition', 'all 0.3s');
        });
    }

    //Liem Buu forward ticket 1065
    setTimeout(function () {
        $('.slick-slide').each(function (index, item) {
            $(this).clone().removeAttr('class').addClass('slider-2').appendTo($(this)).children().remove();
        });
        $('.top-slider .slick-slide > div').each(function (index, item) {
            $(this).clone().removeAttr('class').addClass('slider-2').appendTo($(this));
        });
    }, 100);

    // Module-hero automatically set to height of image
    $(".module-hero, .top-slider").each(function () {
        var el = $(this);
        // Set Interval set max heigth when all image is completely loaded
        var checkFunction = setInterval(function () {
            if (checkImageCompleteLoading(el)) {
                consistencyHeroBanner(el);
                clearInterval(checkFunction);
            }
        }, 100);
    });

    // Vy Vo
    // fix bug add ellipsis when the challenge question is over the box in FF/IE
    // DOMSubtreeModified: Chrome/FF
    // propertychange: IE
    $(".multiselect-selected-text").on('DOMSubtreeModified propertychange', function () {
        var ele = $(".challengeQuestions").find("span .multiselect-selected-text");
        var textHeight = parseInt(ele.css('line-height'));
        while (ele.height() > textHeight) {
            var newText = ele.text().replace(/(\w+)\W*$/, '...');
            ele.text(newText);
        }
    });

    $('input[name=IsHotelNameRecommend]').change(function () {
        if ($(this).hasClass('alterFirstValue')) {
            $('.infoRestaurant').text($('#HotelName').val());
        } else if ($(this).hasClass('alterSecondValue')) {
            $('.infoRestaurant').text($('#Region').val());

        }
    });

    $('#MinimumPrice, #MaximumPrice').on('change keydown keyup', function () {
        if ($('#MinimumPrice').val() > 0) {
            $('#MaximumPrice').attr('min', $('#MinimumPrice').val());
        }

        if ($('#MaximumPrice').val() > 0) {
            $('#MinimumPrice').attr('max', $('#MaximumPrice').val());
        }

        $('#fr-travelForm').validate().element($('#MinimumPrice'));
        $('#fr-travelForm').validate().element($('#MaximumPrice'));
    })

    //Liem Buu forward ticket 1065
    //setTimeout(function () {
    //    $('.slick-slide').each(function (index, item) {
    //        $(this).clone().removeAttr('class').addClass('slider-2').appendTo($(this)).children().remove();
    //    });
    //    $('.top-slider .slick-slide > div').each(function (index, item) {
    //        $(this).clone().removeAttr('class').addClass('slider-2').appendTo($(this));
    //    });
    //}, 100);

    // Module-hero automatically set to height of image
    //$(".module-hero, .top-slider").each(function () {
    //    var el = $(this);
    //    // Set Interval set max heigth when all image is completely loaded
    //    var checkFunction = setInterval(function () {
    //        if (checkImageCompleteLoading(el)) {
    //            consistencyHeroBanner(el);
    //            clearInterval(checkFunction);
    //        }
    //    }, 100);
    //});

    // Vy Vo
    // fix bug add ellipsis when the challenge question is over the box in FF/IE
    // DOMSubtreeModified: Chrome/FF
    // propertychange: IE
    //$(".multiselect-selected-text").on('DOMSubtreeModified propertychange', function () {
    //    var ele = $(".challengeQuestions").find("span .multiselect-selected-text");
    //    var textHeight = parseInt(ele.css('line-height'));
    //    while (ele.height() > textHeight) {
    //        var newText = ele.text().replace(/(\w+)\W*$/, '...');
    //        ele.text(newText);
    //    }
    //});

    function setMinValueForInputWithLtSpinner(identityClass, inputId, minValue) {
        if ($(identityClass).length > 0) {
            var limiter = minValue - 1;
            //implement limit adults number to minValue
            $(identityClass).prev().on('touchstart click', function () {
                if ($(inputId).length > 0) {
                    var valueOfAdult = parseInt($(inputId).val(), 10) - 1;
                    if (valueOfAdult <= limiter) {
                        $(inputId).val(minValue);
                        $(inputId).focus();
                        return;
                    }
                }


            });
            $(identityClass).on('change keyup keydown input', function () {
                if ($(inputId).length > 0) {
                    var valueOfAdult = parseInt($(inputId).val(), 10);
                    if (valueOfAdult <= limiter) {
                        $(inputId).val(minValue);
                        $(inputId).focus();
                    }
                }
            });
            $(identityClass).on('focusout', function () {
                if ($(inputId).val() == undefined || $(inputId).val() == null || $(inputId).val() == "") {
                    $(inputId).val(minValue);
                    $(inputId).focus();
                    //update info book on change
                    UI.changeInfoBox();
                }
            });
        }
        
    }
    function setMaxValueForInputWithLtSpinner(identityClass, inputId, maxValue) {
        if ($(identityClass).length > 0) {
            var limiter = maxValue - 1;
            //implement limit adults number to max
            $(identityClass).next().on('touchstart click', function () {
                if ($(inputId).length > 0) {
                    var valueOfAdult = parseInt($(inputId).val(), 10) + 1;
                    if (valueOfAdult > maxValue) {
                        $(inputId).val(maxValue);
                        $(inputId).focus();
                        return;
                    }
                }
            });
            $(identityClass).on('change keyup keydown input', function () {
                if ($(inputId).length > 0) {
                    var valueOfAdult = parseInt($(inputId).val(), 10);
                    if (valueOfAdult > maxValue) {
                        $(inputId).val(maxValue);
                        $(inputId).focus();
                    }
                }
            });
        }
    }
    return {
        init: function () {
            // LightBoxGallery();
            slickSlides();
            LightboxGoogleMap();
            selectBox();
            changeInfoBook();
            hidePhoto();
            spinner();
            RemoveRow();
            addMemberShip();
            limitChar();
            //lineClamp();
            // showSelectOption();
            textBoxOption();
            commonAnimation();
            generatePassengerTable(9);
            datepicker();
            navAnimation();
            //resizeHeroModuleDepentOnNav();
        },
        changeInfoBox: changeInfoBook,
        renderSelectBox: selectBox,
        LimitChar: limitChar,
        LightBoxGallery: LightBoxGallery,
        ShowCap: showCap,
        setMinValueForInputWithLtSpinner: setMinValueForInputWithLtSpinner,
        setMaxValueForInputWithLtSpinner: setMaxValueForInputWithLtSpinner,
        FormSetting: FormSetting
    };
}();


//getParameterByName
//=============
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


//plugin toogle
//=============
(function ($) {
    //TOGGLECONTENT CONTRUCTOR FUNCTION DEFINITION
    //============================================
    var ToggleContent = function (element, option) {
        this.options = $.extend({}, ToggleContent.DEFAULTS, option);

        this.$target = $(element).parents().eq(1).find(this.options.target);
        this.$element = $(element);
        this.$elementClick = this.$element
            .on('click.LT.toggle', $.proxy(this.LTtoogle, this));
        this.statusDefault = function () {
        }();
    }

    //INFO PLUGIN
    //===========
    ToggleContent.VERSION = '1.0';
    ToggleContent.AUTHOR = 'loc.tran@s3corp.com.vn';
    ToggleContent.DEFAULTS = {
        target: '.inner',
        speed: 'fast',
        fade: false         //effects toogle (slide, fade). Defaults is SLIDE
    }

    //PUBLIC METHOD
    //=============
    ToggleContent.prototype.LTtoogle = function () {
        if (this.options.fade) {
            this.$target.toggleClass('active').fadeToggle(this.options.speed);
            this.$element.toggleClass('active');
        } else {
            this.$target.toggleClass('active').slideToggle(this.options.speed);
            this.$element.toggleClass('active');
        }
    }

    //TOGLE PLUGIN DEFINITION
    //=======================
    function Plugin(option) {
        return this.each(function () {
            var $this = $(this);
            var options = typeof option == 'object' && option
            var data = new ToggleContent(this, options);
        });
    }
    $.fn.toogleContent = Plugin;


    //USING DATA HTML5
    //================
    $(window).on('load', function () {
        if ($('[data-spy="toggleContent"]').length > 0) {
            $('[data-spy="toggleContent"]').each(function () {
                var $spy = $(this);
                var data = $spy.data();
                Plugin.call($spy, data);
            })
        }
    });
    
}(jQuery));


// PLUGIN DISPLAY FIELDS
//======================
(function ($) {
    // FIELD DISPLAY PLUGIN
    $.fn.fieldRule = function (fieldName) {
        // PRIVATE VARIABLE
        var realValue = "";
        var check = 0;

        // DISPLAY NAME VALUE FOLLOWING NEW RULE
        // RULES:
        // For more than 3 characters: ***itt
        // For 2 and 3 characters: **n, *n
        // For 1 Character: *
        var nameRule = function (obj) {
            var realLength = obj.val().length;
            var maskLength = 0;
            var stars = "";
            if (realLength <= 2) {
                maskLength = 1;
            } else if (realLength == 3) {
                maskLength = 2;
            } else {
                maskLength = realLength - 3;
            }
            for (var i = 0; i < maskLength; i++) {
                stars += "*";
            }
            var patt = new RegExp("^.{" + maskLength + "}", "g");
            var fakeValue = realValue.replace(patt, stars);
            return fakeValue;
        }

        // FUNCTION DISPLAY EMAIL VALUE FOLLOWING NEW RULE
        // RULES:  
        // For more than 4 characters: *****xyz@xyz.com
        // For 1, 2, 3 and 4 characters: *@sign, *x@sign, *xy@sign, *xyz@sign
        var emailRule = function () {
            // index of @ character in email value
            var index = realValue.indexOf("@");
            var stars = "", fakeValue, subStr;
            if (index <= 4) {
                if (index == 3) {
                    subStr = realValue.substring(0, 2);
                    fakeValue = realValue.replace(subStr, "**");
                } else {
                    subStr = realValue[0];
                    fakeValue = realValue.replace(subStr, "*");
                }
            } else {
                for (i = 1; i <= index - 3; i++) { stars += "*"; }
                subStr = realValue.substring(0, index - 3);
                fakeValue = realValue.replace(subStr, stars);
                fakeValue = realValue.replace(subStr, stars);
            }
            return fakeValue;
        }

        // FUNCTION DISPLAY BIRTHDAY VALUE FOLLOWING NEW RULE
        // RULES: 
        var birthdayRule = function (obj) {
            var length = obj.val().length - 3;
            var stars = "";
            for (var i = 0; i < length; i++) {
                stars += "*";
            }
            var patt = new RegExp(".{" + length + "}$", "g");
            var fakeValue = realValue.replace(patt, stars);
            return fakeValue;
        }

        // GENERATE WHEN LOADING
        realValue = this.val();
        if (realValue != "") {
            this.siblings("input").val(realValue);
            if (fieldName == "name") {
                this.val(nameRule(this));
            }
            if (fieldName == "email") {
                this.val(emailRule(this));
            }
            if (fieldName == "birthday") {
                this.val(birthdayRule(this));
            }
        }

        // HANDLE WHEN SELECTING
        this.on("focusin", function () {
            realValue = $(this).siblings("input").val();
            $(this).val(realValue);
        });
        if (fieldName != "birthday") {
            this.on("focusout", function () {
                realValue = $(this).val();
                $(this).siblings("input").val(realValue);
                if (realValue != "") {
                    var self = $(this);
                    if (fieldName == "name") {
                        self.val(nameRule(self));
                    }
                    if (fieldName == "email") {
                        self.val(emailRule());
                    }
                }
            });
        }
        return this;
    }
}(jQuery));


function setSelectValueSelect(selectId, value) {
    //IE issue - remove selected before add new =.=
    if ($('#' + selectId + ' option:selected').length > 1) {
        $('#' + selectId + ' option:selected').remove();
    }
    $('#' + selectId + ' option').each(function () {
        var currentValue = $(this).val();
        var currentText = $(this).text();        
        if (currentValue === value) {
            $(this).attr('selected', 'selected');
            //set timeout for delay render element in IE
            setTimeout(function () {
                $('#' + selectId).next('div.btn-group-selectbox').find('button').attr('title', currentText);
            }, 0);
            setTimeout(function () {
                $('#' + selectId).next('div').find('span.multiselect-selected-text').text(currentText);
            }, 0);
            return false;
        }
    });
}

function setBackgroundSlideIE() {
    var listSlider = $('.module-hero .slide');
    listSlider.each(function () {
        var self = this;
        var imgSrc = $(self).css('background-image');
        //$(self).css('z-index', '-1');
        var parentEle = self.parentElement.parentElement;
        $(parentEle).addClass('slide');
        $(parentEle).css({
            'background-image': imgSrc, 'background-size': 'auto',
            'background-position': '50% 60%'
        });
        if ($(self).hasClass('slide-resize-hero')) {
            $(parentEle).css({
                'background-size': 'cover'
            });
        }
    });

    var topSlider = $('.top-slider .slick-track');
}
function blurBackgroundSlideIE() {
    var isIE11 = !!navigator.userAgent.match(/Trident.*rv\:11\./);
    if ((!!isIE11)) {
        $('.slider-2').css('opacity', '0.05');
    }
}

//Add logo Yelp for Landing/Detail
function appendYelpLogo(textDisplay, yelpUrl) {
    //for desktop only
    var $breadCrumb = $('.bread-crumb').children();
    if ($breadCrumb.length > 0) {
        var $yelpLogo = '<div class="yelp-logo-wrapper">'
            + '<span class="yelp-logo-text">' + textDisplay + '</span>'
            + '<a href="' + yelpUrl + '" target="_blank"><span class="icon-yelp-logo"></span></a>'
            + '</div>';
        if ($('.yelp-logo-wrapper').length === 0) {
            //$($yelpLogo).insertAfter($breadCrumb);
            $breadCrumb.css("position", "relative");
            $breadCrumb.append($yelpLogo);
        }
    }
    //for table/mobile
    var $footer = $('#footer');
    if ($footer.length > 0) {
        var $yelpMobile = '<div class="yelp-logo-mobile row">'
            + '<span>' + textDisplay + '</span>'
            + '<a href="' + yelpUrl + '" target="_blank"><span class="icon-yelp-logo"></span></a>'
            + '</div>';
        if ($('.yelp-logo-mobile').length === 0) {
            $($yelpMobile).insertBefore($footer);
        }
    }
}