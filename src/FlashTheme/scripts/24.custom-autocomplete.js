﻿$(function () {
    $("[data-auto-complete='true']").each(function () {
        var self = $(this);
        //var dataUrl = self.attr('data-url');
        var itemType = self.attr('data-item-type');
        var limit = self.attr('data-item-limit');
        dataUrl = "/DCRApi_AutoCompleteSearch";

        if (limit == "") {
            limit = 10;
        }

        self.autocomplete({
            serviceUrl: dataUrl,
            params: { itemType: itemType, limit: limit },
            minChars: 3,
            appendTo: self.parent(),
            autoSelectFirst: true,
            onSelect: function (suggestion) {
                var value = suggestion.value;
                if (self.hasClass("nameOfRestaurant")) {
                    $(".infoRestaurant").text(value);
                    $(".infoRestaurant").closest("p").show();
                } else if (self.hasClass("nameOfLocation")) {
                    $(".infoAddressRestaurant").text(value);
                    $(".infoAddressRestaurant").closest("p").show();
                } else if (self.hasClass("getPickupLocation")) {
                    $(".infoPickupLocation").text(value);
                    $(".infoPickupLocation").closest("p").show();
                } else if (self.hasClass("getDropoffLocation")) {
                    $(".infoDropoffLocation").text(value);
                    $(".infoDropoffLocation").closest("p").show();
                }
            },
            onSearchComplete: function (query, suggestions) {
                $(this).siblings(".autocomplete-suggestions").addClass('scrollbar-outer');
                $(this).siblings(".autocomplete-suggestions").scrollbar();
            }
        });
    });
});