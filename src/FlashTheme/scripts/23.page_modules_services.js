﻿"use strict";
var pageurl = "";
var langPrevText = $('#paging_prev_text').val() ? $('#paging_prev_text').val() : "Prev";
var langNextText = $('#paging_next_text').val() ? $('#paging_next_text').val() : "Next";
var $modulePaginationCarousel = $('.tiles_module_pagination_carousel');
//Const
var _carouselConst = 'carousel';
var _slickContentClassNameConst = '.multiple-items-responsive_';
var _urlGetPageModuleContentConst = '/view-request/site/GetPageModuleContent?';
var _filterElementClassnameConst = '.filter-tilemodule-';
var moduleIdClicked = '';
//Open modal
$('body').on('click', '[data-modal-open]', function (e) {
    e.preventDefault();

    let $target = $(e.currentTarget),
        mId = $target.attr('data-modal-open'),
        $md = $('[data-modal=' + mId + ']');

    if (!$md.length) return false;

    appendFilterTag(mId);
    divideListFilterOnModal(mId, 3);

    $($md[0]).addClass('_open');
    $('body').addClass('_mdopen');
});

//Close modal
$('body').on('click', '[data-close-modal]', function (e) {
    e.preventDefault();

    $('[data-modal]._open').removeClass('_open');
    $('body').removeClass('_mdopen');
});

$(document).ready(function (e) {
    //$('.reservationDate').datepicker({
    //    minDate: new Date()
    //});

    $('section.block-recommendations').css('display', 'none');

    initReservationDateForYelpLanding('.yelp-reservation-date-landing');
    page_modules_services.init();
    //seperate new file js for slick
    //SlickPaggingCustomization.init($modulePaginationCarousel, _carouselConst, _slickContentClassNameConst, _urlGetPageModuleContentConst);
    GetDataFromParamUrl(page_modules_services);
   /* ----------------- Slick carousel pagging - start ----------------- */
    if ($modulePaginationCarousel.length > 0) {
        $.each($modulePaginationCarousel, function (idx, module) {
            var $slickModule = $(module);
            if ($slickModule.val() === _carouselConst) {
                var itemsDisplay = parseInt($slickModule.attr('data-items-on-page'));
                var itemsTotal = parseInt($slickModule.attr('data-total-items'));
                //var itemTotals = parseInt(slickModule.attr('data-total-items'));
                var currentPage = 1;
                var rowDisplay = 1;
                if (itemsDisplay > 4) {
                    rowDisplay = calcRowsDisplayFollowItemsDisplay(itemsDisplay, itemsTotal);
                    itemsDisplay = 4;
                }
                //add attribute to store var
                var itemId = $slickModule.attr('data-item-id');
                //addAttributeForSlickTile(slickModule, rowDisplay, itemsDisplay, itemId);
                //init slick
                initSlick(itemsDisplay, itemsDisplay, rowDisplay, itemId);
                //handle event
                handleEventSlick(itemId, currentPage, rowDisplay, $slickModule);
            }
        });
    }
    /* ----------------- Slick carousel pagging - end ----------------- */

    /* ----------------- New filter - start ----------------- */
    $(document).on('change', 'input[name="filter_data"]', function () {
        //calcNumberOfFilterApplied();
        var _this = $(this);
        var value = _this.val();
        var moduleId = _this.attr('data-moduleID');
        var $tagId = 'filter-tag-' + moduleId + '_' + value.replace(/[^A-Z0-9]+/ig, '-');
        if (this.checked) {
            if ($('#' + $tagId).length === 0) {
                $('.modal-filter-subhead[data-moduleID="' + moduleId + '"]').append('<div class="filter-tag" id="' + $tagId + '"><span>' + value
                    + '</span><a class="__del" href="#"></a></div>');
            }
            if ($('input[value="' + value + '"]').length > 1) {
                $('input[value="' + value + '"]').prop('checked', true);
            }
        } else {
            $('#' + $tagId).remove();
            if ($('input[value="' + value + '"]').length > 1) {
                $('input[value="' + value + '"]').removeAttr('checked');
            }
        }
        if (_this.attr('data-position') === 'filter') {
            //call ajax every single checkbox
            newFilter(_this, '');
        }
    });

    $(document).on('change', 'input[name="price_max"]', function () {
        //calcNumberOfFilterApplied();
        newFilter($(this), '');
    });

    $(document).on('click', '.clear-all-filters', function (e) {
        e.preventDefault();

        var $subhead = $(this).parent().parent().next('.modal-filter-subhead');
        if ($subhead.length > 0) {
            $subhead.empty();
            var moduleId = $subhead.attr('data-moduleID');
            $('input[name="filter_data"][data-moduleID="' + moduleId +' "]').removeAttr('checked');
        }
    });

    $(document).on('click', '.__del', function () {
        var currentId = '#' + $(this).attr('id');
        $(this).parent().remove();
        $(currentId).trigger('click');
    });

    $(document).on('click', '.btn-search-filter-modal', function (e) {
        e.preventDefault();
        //close modal
        $('[data-modal]._open').removeClass('_open');
        $('body').removeClass('_mdopen');

        var _btn = $(this);
        newFilter(_btn, '');
    });

    //calcNumberOfFilterApplied();
    resetAllFilter();
    clickXOnFilterBar();
    searchGlobal();
    isShowFilter(function(){
        HideFilterInModule();
    });

    $(document).on('click touchstart', '.__foot-search', function (e) {
        customDestinationSearchSpecific();
    });

    /* ----------------- New filter - end ----------------- */
});
//$(window).bind('popstate', function () {
//    window.location.href = window.location.href;
//});
var page_modules_services = {
    init: function () {
        this.tiles_module_paging_loading();
        this.tiles_module_filter_action();
        this.tiles_module_search_action();
        this.tiles_module_yelpsearch_action();
        //this.tiles_module_back();
    },

    tiles_module_back: function () {
        /*TinhTran*/
        var _listTileModule = $('div[class^="tile_module_"]').toArray();
        // managage back button click (and backspace)
        var count = 0; // needed for safari\
        if (_listTileModule.length != 0) {
            var countBack = -1 - _listTileModule.length;
            window.onload = function () {
                if (typeof history.pushState === "function") {
                    history.pushState("back", null, null);
                    window.onpopstate = function () {
                        history.pushState('back', null, null);
                        if (count == 1) { window.history.go(countBack); }
                    };
                }
            }
            setTimeout(function () { count = 1; }, 200);
        } else {
            return;
        }

    },
    tiles_module_paging_loading: function () {
        $(".tiles_module_pagination").each(this.tiles_module_init_pagination);
    },
    tiles_module_init_pagination: function (index, itemPaging) {

        var items = parseInt($(itemPaging).attr('data-total-items'));
        var itemsOnPage = parseInt($(itemPaging).attr('data-items-on-page'));
        var item_Id = $(itemPaging).attr('data-item-id');
        var queryString = $(itemPaging).attr('data-query-string');
        var currentpage = 1;
        if ($(itemPaging).closest("section").find("._tiles_m_filtering_aciton").length > 0) {
            currentpage = getParameterByName("pagenumber") != "" ? parseInt(getParameterByName("pagenumber")) : 1;
        }
        if (!item_Id) {
            return;
        }
        $(itemPaging).pagination({
            items: items,
            itemsOnPage: itemsOnPage,
            item_Id: item_Id,
            queryString: queryString,
            localCurrentPage: 1,
            currentPage: currentpage,
            prevText: langPrevText,
            nextText: langNextText,
            onPageClick: function (pageNumber, event) {
                if (pageNumber <= 0 || pageNumber > this.items) {
                    return false;
                }

                if (event) {
                    event.preventDefault();
                }
                var _item_id = this.item_Id;

                $('#' + _item_id + ' .tiles_module_pagination').addClass('disabled');
                var animationTime = 100;

                //get data for yelp landing
                var params = buildParamsWhenPagging(this.item_Id, pageNumber);
                if (pageNumber > this.localCurrentPage) {
                    page_modules_services.ajax_get(_urlGetPageModuleContentConst + this.queryString, params, function (error, responseData) {
                        if (error) {
                            return;
                        }
                        $('#' + _item_id).find(".data-container").eq(1).animate({ 'margin-left': "-100%" }, animationTime, function () {
                            $(this).css("margin-left", "100%");
                            $(this).css("position", "absolute");
                            $(this).css("left", "-10000000px");
                            $(this).appendTo($('#' + _item_id).find("[data-spy='blockSlide']"));
                            $(this).appendTo($('#' + _item_id).find("[data-spy='blockSlideS']"));
                        });
                        var classes = $('#' + _item_id).find(".data-container").eq(1).attr('class');
                        var styles = $('#' + _item_id).find(".data-container").eq(1).attr('style');
                        responseData.data = $(responseData.data).find(".data-container").eq(1).html();
                        $('#' + _item_id).find(".data-container").eq(1).next().html(responseData.data);
                        $('#' + _item_id).find(".data-container").eq(1).next().animate({ 'margin-left': "0" }, animationTime);
                        $('#' + _item_id + ' .tiles_module_pagination').removeClass('disabled');
                        $('#' + _item_id).find(".data-container").eq(1).next().attr("class", classes);
                        $('#' + _item_id).find(".data-container").eq(1).next().removeAttr("style").attr("style", styles);

                    });
                }
                if (pageNumber < this.localCurrentPage) {
                    page_modules_services.ajax_get(_urlGetPageModuleContentConst + this.queryString, params, function (error, responseData) {

                        if (error) {
                            return;
                        }
                        $('#' + _item_id).find(".data-container").eq(1).animate({ 'margin-left': "100%" }, animationTime, function () {
                            $(this).css("margin-left", "-100%");
                            $(this).css("position", "absolute");
                            $(this).css("left", "-10000000px");
                            $(this).prependTo($('#' + _item_id).find("[data-spy='blockSlide']"));
                            $(this).prependTo($('#' + _item_id).find("[data-spy='blockSlideS']"));
                        });
                        var classes = $('#' + _item_id).find(".data-container").eq(1).attr('class');
                        var styles = $('#' + _item_id).find(".data-container").eq(1).attr('style');
                        responseData.data = $(responseData.data).find(".data-container").eq(1).html();
                        $('#' + _item_id).find(".data-container").eq(1).prev().html(responseData.data);
                        $('#' + _item_id).find(".data-container").eq(1).prev().animate({ 'margin-left': "0" }, animationTime);
                        $('#' + _item_id + ' .tiles_module_pagination').removeClass('disabled');
                        $('#' + _item_id).find(".data-container").eq(1).prev().attr("class", classes);
                        $('#' + _item_id).find(".data-container").eq(1).prev().removeAttr("style").attr("style", styles);
                    });
                }
                this.localCurrentPage = pageNumber;
                if ($(itemPaging).closest("section").find("._tiles_m_filtering_aciton").length > 0) {
                    //get data filter data and filter search

                    pageurl = "?{0}".f(this.queryString) + "&pagenumber=" + pageNumber;
                    PushStateUrl(pageurl);
                }
                return false;
            },
            cssStyle: 'light-theme'
        });
    },
    ajax_get: function (url, data, callback) {

        $.ajax({
            type: "GET",
            url: url,
            dataType: "json",
            data: data,
            async: true,
            cache: false,
            success: function (response) {
                if (response.success) {
                    if (response.DataSource === 'yelp_dining') {
                        appendYelpLogo(response.ReservationByPlaceHolder, response.YelpHomePageUrl);
                    }
                    callback(null, response);
                    App.ChangeScrollBar();

                    rebind_autocomplete(data.filter_datasource);
                    UI.ShowCap();
                    UI.renderSelectBox();

                    var initDate = new Date();
                    if (response.reservationdate !== null) {
                        initDate = new Date(response.reservationdate);
                    }
                    initReservationDateForYelpLanding('.yelp-reservation-date-landing', initDate);
                    rebindReservationHour(response.reservationTime);
                    rebindReservationCover(response.reservationCovers);
                    alignGrayBar(0);
                    alignGrayBarTablet();
                }
                else {
                    var pathname = window.location.pathname;
                    pathname = pathname != '' ? pathname.toLowerCase() : "";
                    if (response.code == '401' && (pathname != "/loggedout" && pathname != "loggedout")) {
                        var url = response.returnUrl;
                        if (!url || url == '') {
                            url = "/LoggedOut";
                        }

                        window.location.href = url;
                    }
                    else {
                        UI.ShowCap();
                        UI.renderSelectBox();
                        $('.yelplanding-filter-date-time').validation({});
                        callback(new Error(response.error), null);
                    }
                }
                hideLoading();
                setTimeout(function (parameters) {
                    autoScrollToSelectedItem('#timeDividerNewDinningForm, #reservationTime, #cuisineSelect');
                }, 0);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                hideLoading();
                if (xhr.readyState == 4 && xhr.responseText != '') {
                    return console.log(xhr.responseText);
                }
            }
        });
    },

    tiles_module_filter_action: function () {
        $(document).on("click", "._tiles_m_filtering_aciton", function () {
            var form = $(this).closest('form');
            filter(form);

        });
        $(document).on("submit", ".form-filter-top", function (event) {
            event.preventDefault();
            filter($(this));
        });

        $('.des-item').off('click').on('click',
            function () {
                var form = $('form[name^="frmFilterTop"]');
                var desName = $(this).text().trim();
                $.when(searchDestination(form, desName)).done(isShowFilter(function(){
                    HideFilterInModule();
                })).done(function () {
                    $('.aspire-nav').removeClass('active'); //Fix for mobile and tablet
                    $('.unscroll').removeClass('unscroll'); //Fix for mobile and tablet
                });
            });

        $('.__foot-search-btn').on('click', function (e) {
            e.preventDefault();
            var form = $('form[name^="frmFilterTop"]');
            var $target = $('.__foot-search .__foot-search-input').val();
            var desName = $target.trim();
            $.when(searchDestination(form, desName)).done(isShowFilter());
        });
        $('.__foot-search-input').on('keypress', function (e) {
            if (e.which == 13) {
                var $target = $(e.currentTarget);
                var form = $('form[name^="frmFilterTop"]');
                cityName = $target.val();
                var desName = cityName.trim();
                $.when(searchDestination(form, desName)).done(isShowFilter());
            }
        });
        function filter(form) {
            var filter_datasource = form.attr('filter_datasource');
            var item_Id = form.attr('data-item-id');
            var datafilter = form.serializeArray();

            //encode categories value
            datafilter.forEach(function (currentValue, index, array) {
                if (currentValue.name === "filter_location" && isNotNullAndEmpty(currentValue.value)) {
                    var searchBox = form.find('input[name=filter_location]');
                    if (isNotNull(searchBox)) {
                        var searchLocation = searchBox.attr('data');
                        if (isNotNull(searchLocation)) {
                            currentValue.value = encodeURIComponent(searchLocation.replace(/\|/g, ','));
                        }
                    }
                } else {
                    currentValue.value = encodeURIComponent(currentValue.value);
                }
            });

            var data;
            if ($(window).width() > 992) {
                var formsearch = $('form[name="frmSearch-' + item_Id + '"]');
                var datasearch = formsearch.serializeArray();
                data = datasearch.concat(datafilter);
            } else {
                data = datafilter;
            }

            data.push({ name: 'filter_datasource', value: filter_datasource });
            data[data.length] = { name: "pageModuleID", value: item_Id };

            //pushState start
            TilesPushState(data, 1);

            if (!isModuleSlickCarousel(item_Id)) {
                AjaxGetData(page_modules_services, data, filter_datasource, item_Id, 1);
            }
        };
    },
    //Please don't use it
    tiles_module_search_action: function () {
        $(document).on("click", ".btn-search", function () {
            var formsearch = $(this).closest('form');
            search(formsearch);
        });
        $(document).on("submit", ".js-form-Search", function (event) {
            event.preventDefault();
            search($(this));
        });
        function search(formsearch) {
            var itemId = formsearch.attr('data-item-id');
            var datasearch = formsearch.serializeArray();
            if (datasearch.length > 0) {
                var searchBox = formsearch.find('input[name=filter_location]');
                if (isNotNull(searchBox)) {
                    var searchLocation = searchBox.attr('data');
                    if (isNotNullAndEmpty(searchLocation)) {
                        datasearch[0].value = encodeURIComponent(searchLocation.replace(/\|/g, ','));
                    } else {
                        datasearch[0].value = encodeURIComponent(datasearch[0].value);
                    }
                }
            }
            var formfilter = $('form[name="frmFilter-' + itemId + '"]');
            var filter_datasource = formfilter.attr('filter_datasource');
            var datafilter = formfilter.serializeArray();

            //Tam.Hua encode categories value
            datafilter.forEach(function (currentValue, index, array) {
                currentValue.value = encodeURIComponent(currentValue.value);
            });
            //end Tam.Hua encode categories value

            var data = datasearch.concat(datafilter);
            data.push({ name: 'filter_datasource', value: filter_datasource });
            data[data.length] = { name: "pageModuleID", value: itemId };

            //pushState start
            TilesPushState(data, 1);

            AjaxGetData(page_modules_services, data, filter_datasource, itemId, 1);
        }
    },
    tiles_module_yelpsearch_action: function () {

        $(document).on("click", ".yelplanding-filter-date-time .yelp-checkavailability", function (e) {
            showLoading();
            var formsearch = $(this).closest('form');
            $.when(checkYelpAvailability(formsearch)).done(hideTileWithOutData());
        });
    }
}

function checkYelpAvailability(formsearch) {

    searchYelp(formsearch);
}

//Add you function here, Pooja//
function searchYelp(formsearch) {

    var itemId = formsearch.attr('data-item-id');
    var fieldValuePairs = $('.yelplanding-filter-date-time').serializeArray();
    //var datasearch = formsearch.serializeArray();
    //if (fieldValuePairs.length > 0) {
    //    var reservationDate = fieldValuePairs.attr('date');
    //    var reservationTime = fieldValuePairs.attr('ReservationTime');
    //    var reservationCovers = fieldValuePairs.attr('peopleSelect');

    //}
    var filterlocation = decodeURIComponent(getParameterByName("filter_location"));
    var formfilter = $('form[name="frmFilter-' + itemId + '"]');
    var filter_datasource = formsearch.attr('filter_datasource');
    var datafilter = formfilter.serializeArray();

    //Tam.Hua encode categories value
    datafilter.forEach(function (currentValue, index, array) {
        currentValue.value = encodeURIComponent(currentValue.value);
    });
    //end Tam.Hua encode categories value

    var data = fieldValuePairs.concat(datafilter);
    data.push({ name: 'filter_datasource', value: filter_datasource });
    data[data.length] = { name: "pageModuleID", value: itemId };



    //pushState start
    TilesPushState(data, 1);

    AjaxGetData(page_modules_services, data, filter_datasource, itemId, 1);

}

var pushstateDesName = "";
function searchDestination(formsearch, desName) {

    //end Tam.Hua encode categories value
    var datafilter = [];
    desName = encodeURIComponent(desName);
    pushstateDesName = desName; //Do not delete this line, it is use for pushStateUrl Function
    var listTileModule = $('div[class^="tile_module_"]').toArray();
    if (listTileModule.length > 0) {
        listTileModule.forEach(function (currentTile, index, array) {
            datafilter = [];
            datafilter.push({ "name": "filter_location", "value": desName });
            var itemId = currentTile.id;
            var formfilter = $('form[name="frmFilter-' + itemId + '"]');
            var filter_datasource = formfilter.attr('filter_datasource');

            var data = datafilter;
            data.push({ name: 'filter_datasource', value: filter_datasource });
            data[data.length] = { name: "pageModuleID", value: itemId };

            //pushState start
            TilesPushState(data, 1);

            //handle search for tile module carousel
            var $tileSlickCarousel = $(_slickContentClassNameConst + itemId);
            var $inputSlickCarousel = $('#' + itemId + ' input.tiles_module_pagination_carousel');
            if ($inputSlickCarousel.length > 0 && $inputSlickCarousel.val() === _carouselConst) {
                ajaxGetDataForCarousel('/view-request/site/GetPageModuleContent', data, function (error, responseData) {
                    if (error) {
                        return;
                    }
                    if (responseData.data != "") {
                        //FIX 433
                        var itemDisplay = parseInt($(responseData.data).eq(2).find('input.tiles_module_pagination_carousel').attr('data-items-on-page'));
                        var itemsTotal = parseInt($(responseData.data).eq(2).find('input.tiles_module_pagination_carousel').attr('data-total-items'));
                        var rowsDisplay = 1;
                        if (itemDisplay > 4) {
                            rowsDisplay = calcRowsDisplayFollowItemsDisplay(itemDisplay, itemsTotal);
                            itemDisplay = 4;
                        }

                        $('#' + itemId).replaceWith($(responseData.data));
                        if (responseData.DataSource === 'yelp_dining') {
                            appendYelpLogo(responseData.ReservationByPlaceHolder, responseData.YelpHomePageUrl);
                        }

                        App.ChangeScrollBar();

                        rebind_autocomplete(data.filter_datasource);
                        UI.ShowCap();
                        UI.renderSelectBox();


                        var initDate = new Date();
                        //initDate.setDate(initDate.getDate() + 30);
                        if (typeof responseData.reservationdate != 'undefined' && responseData.reservationdate != null) {
                            initDate = new Date(responseData.reservationdate);
                        }

                        initReservationDateForYelpLanding('.reservationDate', initDate);


                        var $slickEl = $('#' + itemId + ' input.tiles_module_pagination_carousel');
                        if ($slickEl.length > 0 && $slickEl.val() === _carouselConst) {
                            //addAttributeForSlickTile(slickEl, rowsDisplay, itemDisplay, itemId);
                            //re-slick
                            if ($tileSlickCarousel.length > 0) {
                                if ($tileSlickCarousel.find('.slick-slide').length > 0) {
                                    $tileSlickCarousel.slick('unslick');
                                }
                            }
                            initSlick(itemDisplay, itemDisplay, rowsDisplay, itemId);
                            //trigger event
                            handleEventSlick(itemId, 1, rowsDisplay, $slickEl);
                        }
                    }
                    hideTileWithOutData();
                    if (responseData.totalitem == 0) {
                        var notfounditem = $("#" + itemId).find(".wrap-experiences");
                        $('#' + itemId + ' div.wrap-experiences p.error').remove();
                        //var conciergeContactNumber = $("#ConciergeContactNumber").val();

                        if (responseData.error) {

                            $("#" + itemId).find(".wrap-experiences").children(".slides-recommends, .pagination").remove();
                            (!!$("#" + itemId).find('.contact_concierge')) ? $("#" + itemId).find('.contact_concierge').addClass('active') : ''; /*TinhTran*/
                        }
                        else {
                            $("<p class='error text-center'>Our apologies, but it appears that what you've searched for is not included in our curated content. Our concierge team is, however, available to help you. As our Concierge customer, you have access to your our Concierge team 24 hours per day, every day of the year. You may reach the concierge team by calling in our number or by placing a new request from the concierge website. Either way it would be our pleasure to assist you.</p>").appendTo(notfounditem);
                        }
                    }
                });
            } else {
                AjaxGetData(page_modules_services, data, filter_datasource, itemId, 1, function(){ 
                    HideFilterInModule();
                });
            }
        });
    }
};

function AjaxGetData(page_modules_services, data, filter_datasource, item_Id, pagenumber, callback) {
    data.push({ name: 'page', value: pagenumber });
    page_modules_services.ajax_get('/view-request/site/GetPageModuleContent', data, function (error, responseData) {
        if (error) {
            return;
        }
        if (responseData.data != "") {
            $('#' + item_Id).html(responseData.data); //Show error when using replaceWith instead of html

            //fill no. of filter applied            
            calcNumberOfFilterApplied(item_Id);

            //init slick when load page
            //SlickPaggingCustomization.reInitSlickWhenAjaxLoad(_slickContentClassNameConst, item_Id);
            var $tileSlickCarousel = $(_slickContentClassNameConst + item_Id);
            if ($tileSlickCarousel.length > 0) {
                if ($tileSlickCarousel.find('.slick-slide').length == 0) {
                    var $inputSlickCarousel = $('#' + item_Id + ' input.tiles_module_pagination_carousel');
                    if ($inputSlickCarousel.length > 0 && $inputSlickCarousel.val() === _carouselConst) {
                        var itemDisplay = parseInt($inputSlickCarousel.attr('data-items-on-page'));
                        var itemsTotal = parseInt($inputSlickCarousel.attr('data-total-items'));
                        var rowsDisplay = 1;
                        if (itemDisplay > 4) {
                            rowsDisplay = calcRowsDisplayFollowItemsDisplay(itemDisplay, itemsTotal);
                            itemDisplay = 4;
                        }
                        initSlick(itemDisplay, itemDisplay, rowsDisplay, item_Id);
                        //trigger event
                        handleEventSlick(item_Id, 1, rowsDisplay, $inputSlickCarousel);
                        alignGrayBar(0);
                    }
                }
            }
            hideCarouselPaging();
            hideTileWithOutData();

            //if (responseData.totalitem == 0) {
            //    var notfounditem = $("#" + item_Id).find(".wrap-experiences");
            //    var conciergeContactNumber = $("#ConciergeContactNumber").val();
            //    if (responseData.error) {
            //        $("#" + item_Id).find(".wrap-experiences").children(".slides-recommends, .pagination").remove();
            //        $("<p class='error text-center'>" + responseData.error + "</p>").appendTo(notfounditem);
            //    }
            //    else {
            //        $("<p class='error text-center'>Our apologies, but it appears that what you've searched for is not included in our curated content. Our concierge team is, however, available to help you. As our Concierge customer, you have access to your our Concierge team 24 hours per day, every day of the year. You may reach the concierge team by calling in our number or by placing a new request from the concierge website. Either way it would be our pleasure to assist you.</p>").appendTo(notfounditem);
            //    }
            //}
        }
        if (responseData.totalitem == 0) {
            //hide filter
            //$(_filterElementClassnameConst + item_Id).hide(); //Show or hide filter after search /*JRCW-232 */
           
            var notfounditem = $("#" + item_Id).find(".wrap-experiences");
            var conciergeContactNumber = $("#ConciergeContactNumber").val();
            $('#' + item_Id + ' div.wrap-experiences p.error').remove();
            if (responseData.error) {
                $("#" + item_Id).find(".wrap-experiences").children(".slides-recommends, .pagination").remove();
                (!!$("#" + item_Id).find('.contact_concierge')) ? $("#" + item_Id).find('.contact_concierge').addClass('active') : ''; /*TinhTran*/
            }
            else {
                $("<p class='error text-center'>Our apologies, but it appears that what you've searched for is not included in our curated content. Our concierge team is, however, available to help you. As our Concierge customer, you have access to your our Concierge team 24 hours per day, every day of the year. You may reach the concierge team by calling in our number or by placing a new request from the concierge website. Either way it would be our pleasure to assist you.</p>").appendTo(notfounditem);
            }

            //find contact_concierge
            var isConciergeActive = $("#" + item_Id).find('.contact_concierge').hasClass('active');
            if (!isConciergeActive) {
                $("#" + item_Id).css('display', 'none');
            }

          
          
        }
       
        //JRCW-417
        ArrModuleId.push(item_Id);
        if (typeof(callback) == 'function')
            callback();

        page_modules_services.tiles_module_init_pagination(null, $('#' + item_Id).find('.tiles_module_pagination')[0]);
        AutocompleteDining();
        rebind_autocomplete(filter_datasource);

        turnOffFilterPopUp();
        //hideTileWithOutData();
        rebindReservationHour(responseData.reservationTime)
        rebindReservationCover(responseData.reservationCovers);

    });
}
function TilesPushState(data, pagenumber, element) {
    pageurl = "";
    $.each(data, function (key, obj) {
        var encodeValueObj = (obj.value);
        switch (obj.name) {
            case "filter_location":
                pageurl = encodeValueObj != "" ? pageurl + "?filter_location={0}".f(encodeValueObj) : pageurl;
                break;
            case "filter_data":
                if (pageurl.indexOf("filter_data") >= 0) {
                    pageurl = pageurl + ",{0}".f(encodeValueObj);
                } else {
                    pageurl = pageurl == "" ? pageurl + "?filter_data={0}".f(encodeValueObj) : pageurl + "&filter_data={0}".f(encodeValueObj);
                }
                break;
            case "price_max":
                if (pageurl.indexOf("price_max") >= 0) {
                    pageurl = pageurl + ",{0}".f(encodeValueObj);
                } else {
                    pageurl = pageurl == "" ? pageurl + "?price_max={0}".f(encodeValueObj) : pageurl + "&price_max={0}".f(encodeValueObj);
                }
                break;
            case "peopleSelect":
                pageurl = pageurl == "" ? pageurl + "?ReservationCovers={0}".f(encodeValueObj) : pageurl + "&ReservationCovers={0}".f(encodeValueObj);
                break;
            case "date":
                pageurl = pageurl == "" ? pageurl + "?ReservationDate={0}".f(encodeValueObj) : pageurl + "&ReservationDate={0}".f(encodeValueObj);
                break;
            case "ReservationTime":
                pageurl = pageurl == "" ? pageurl + "?ReservationTime={0}".f(encodeValueObj) : pageurl + "&ReservationTime={0}".f(encodeValueObj);
                break;
            default:
                break;
        }
    });
    if (element != undefined && element.closest("section").find("._tiles_m_filtering_aciton").length > 0) {
        pageurl = data[0].name != "filter_location" && data[0].name != "filter_data" ? pageurl + "?pagenumber={0}".f(pagenumber) : pageurl + "&pagenumber={0}".f(pagenumber);
    }

    PushStateUrl(pageurl);
}

function GetDataFromParamUrl(page_modules_services) {
    $('input[name=filter_data]').removeAttr('checked');

    var filter_data = decodeURIComponent(getParameterByName("filter_data"));
    var filter_search = decodeURIComponent(getParameterByName("filter_location"));
    var price_max = getParameterByName("price_max");
    var pagenumber = getParameterByName("pagenumber");

    if (filter_search != "null") {
        $('input[name=filter_location]').val(filter_search);
    }
    if (filter_data != "null") {
        filter_data = "," + filter_data + ",";
        $('input[name=filter_data]').each(function (key, obj) {
            var itemValue = "," + obj.value + ",";
            if (filter_data.indexOf(itemValue) != -1) {
                $(this).prop('checked', true);
            }
        });
    }
    if (price_max != null && price_max != "") {
        $("#likelyPreferRange").val(price_max).change();
    }

    // get data from forms
    var formsearch = $(".btn-search").closest('form');
    var datasearch = formsearch.serializeArray();

    //var formfilter = $('form[name="frmFilter-' + item_Id + '"]');
    var formfilter = $('form[name^="frmFilter-"]');
    var item_Id = formfilter.attr('data-item-id');
    var filter_datasource = formfilter.attr('filter_datasource');
    var datafilter = formfilter.serializeArray();

    if (formsearch.length > 0) {

        //Tam.Hua encode categories value
        datafilter.forEach(function (currentValue, index, array) {
            currentValue.value = encodeURIComponent(currentValue.value);
        });
        //end Tam.Hua encode categories value

        var data = datasearch.concat(datafilter);
        data.push({ name: 'filter_datasource', value: filter_datasource });
        data[data.length] = { name: "pageModuleID", value: item_Id };

        if ((price_max == null || price_max == "") && data.length > 0) {
            data = jQuery.grep(data, function (item) {
                return item.name != "price_max";
            });
        }

        if (filter_search == "null" && data.length > 0) {
            data = jQuery.grep(data, function (item) {
                return item.name != "filter_location";
            });
        }
        
        AjaxGetData(page_modules_services, data, filter_datasource, item_Id, pagenumber);
    }
    else // Trường hợp chỉ có selectbox
    {
        //gán lại giá trị select từ url
        var form = $('form[name^="frmFilterTop"]');
       $.when(searchDestination(form, '')).then(isShowFilter(function(){
            HideFilterInModule();
       }));
        
    }
  
}

//Tam.Hua add trick rebind event for location filter (lost location filter event after filtering)
function rebind_autocomplete(filter_datasource) {
    if (filter_datasource == 'kb_sightseeing_activity' || filter_datasource == 'kb_event' || filter_datasource == 'kb_dining' || filter_datasource == 'ak_dining' || filter_datasource == 'ak_sightseeing_activity') {
        DCRFilterAutoComplete();
    }
}

function turnOffFilterPopUp() {
    $('body').css('padding-right', '');
    $('body').removeClass('modal-open');
    $('.header').show('fast');
    $('#footer').show('fast');
}

function PushStateUrl(pageurl) {

    if (pushstateDesName.length > 0) { //If your pushStateurl is not working correctly, please add your condition here     
        PushStateUrlFunc(pageurl);
    } else {
        //This part is to fix return url error when user has not logined yet but go to a required-login page
        var checklength = $(".user-name .full-name").length;
        if (checklength > 0) {
            var check = $(".user-name .full-name")[0].textContent;
            if (check.length > 0) {
                PushStateUrlFunc(pageurl);
            }
        }
    }
}

function PushStateUrlFunc(pageurl) {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    var ieVersion = parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));

    if (ieVersion > 9 || isNaN(ieVersion)) {
        if (pageurl != "") {
            window.history.pushState({ path: pageurl }, document.title, pageurl);
        }
        else {
            window.history.pushState({ path: pageurl }, document.title, window.location.pathname);
        }
    }
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

/* ----------------- Slick carousel pagging - start ----------------- */
function initSlick(slidesToShowDefault, slidesToScrolllDefault, rowsDisplay, moduleItemId) {
    if (rowsDisplay === 1) {
        $(_slickContentClassNameConst + moduleItemId).slick({
            dots: true,
            infinite: false,
            speed: 300,
            mobileFirst: true,
            //slidesPerRow: slidesToShowDefault,
            //slidesToShow: slidesToShowDefault,
            //slidesToScroll: slidesToScrolllDefault,
            rows: rowsDisplay,
            slidesPerRow: 2,
            //slidesToScroll: 2,
            responsive: [
                {
                    breakpoint: '767', // tablet breakpoint
                    settings: {
                        //slidesToShow: 3,
                        //slidesToScroll: 3,
                        rows: 1,
                        slidesPerRow: 3
                    }
                },
                {
                    breakpoint: '1024', //  breakpoint
                    settings: {
                        //slidesToShow: 3,
                        //slidesToScroll: 3,
                        rows: 1,
                        slidesPerRow: slidesToShowDefault
                    }
                }
                //{
                //    breakpoint: 980, // tablet breakpoint
                //    settings: {
                //        slidesToShow: 3,
                //        slidesToScroll: 3
                //        //slidesPerRow: 3,
                //        //rows: 1
                //    }
                //},
                //{
                //    breakpoint: 480, // mobile breakpoint
                //    settings: {
                //        slidesToShow: 2,
                //        slidesToScroll: 2
                //        //slidesPerRow: 2,
                //        //rows: 1
                //    }
                //}
            ]
        });
    } else {
        $(_slickContentClassNameConst + moduleItemId).slick({
            dots: true,
            infinite: false,
            speed: 300,
            //slidesPerRow: slidesToShowDefault,
            mobileFirst: true,
            //rows: 1,
            slidesPerRow: 1,
            //slidesToShow: 4,
            //slidesToScroll: 4,
            rows: 1,
            responsive: [
                {
                    breakpoint: '767', // tablet breakpoint
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        //rows: 1
                        //slidesPerRow: slidesToShowDefault,
                        slidesPerRow: 1,
                        rows: 1,
                    }
                },
                {
                    breakpoint: '1024', //  breakpoint
                    settings: {
                        //slidesToShow: 3,
                        //slidesToScroll: 3,
                        //rows: 1
                        slidesPerRow: slidesToShowDefault, //4
                        rows: rowsDisplay, //3
                        //rows: rowsDisplay
                    }
                },


            ]
        });
    }
    //hide dot indicators
    hideCarouselPaging();
    //set status for navigation
    setArrowNavigationStats(true, moduleItemId, rowsDisplay, 1, 1);
}

function setArrowNavigationStats(isInit, moduleItemId, noOfRowDisplay, nextSlide, currentPage) {
    var $inputSlick = $('input.tiles_module_pagination_carousel[data-item-id="' + moduleItemId + '"]');
    if ($inputSlick.length > 0) {
        var totalItem = parseInt($inputSlick.attr('data-total-items'));
        var itemDisplay = parseInt($inputSlick.attr('data-items-on-page'));

        var rowCalculate = 0;

        if (itemDisplay % 2 == 0) {
            rowCalculate = Math.floor((itemDisplay / 4));
        }
        else {
            rowCalculate = Math.floor((itemDisplay / 4)) + 1;
        }

        var finalDisplay = 4 * rowCalculate;
        if (isInit) {
            //alway hide previous arrow when init
            $(_slickContentClassNameConst + moduleItemId + ' button.slick-prev').hide();
            
            if ($(window).width() > 991) { //FOR PC
                if (itemDisplay >= totalItem) {
                    $(_slickContentClassNameConst + moduleItemId + ' button.slick-next').hide();
                }
            }
            else if (($(window).width() > 767) && ($(window).width() < 992) ){ //FOR TABLET
                if (itemDisplay <= 3) {
                    $(_slickContentClassNameConst + moduleItemId + ' button.slick-next').hide();
                }
            } else {// FOR MB
                if (itemDisplay == 1) {
                    $(_slickContentClassNameConst + moduleItemId + ' button.slick-next').hide();
                }
            }
        } else {
            if (nextSlide === 0) {
                //go to the 1st slide
                $(_slickContentClassNameConst + moduleItemId + ' button.slick-prev').hide();
                $(_slickContentClassNameConst + moduleItemId + ' button.slick-next').show();
            } else if (((nextSlide * noOfRowDisplay) + itemDisplay >= totalItem) || ((currentPage * finalDisplay) >= totalItem)) {
                //go to the last slide
                $(_slickContentClassNameConst + moduleItemId + ' button.slick-next').hide();
                $(_slickContentClassNameConst + moduleItemId + ' button.slick-prev').show();
            } else {
                $(_slickContentClassNameConst + moduleItemId + ' button.slick-prev').show();
                $(_slickContentClassNameConst + moduleItemId + ' button.slick-next').show();
            }
        }
    }
}

function rebuildSlideInCaseMoreRowDisplay(rowsDisplay, data, datasource, yelItemOffset, totalItemResponse) {
    var result = '';
    var dataReturnCount = data.children().length;
    var totalItem = rowsDisplay * 4;
    //note data return some case does not = displayItem
    //set missing item is empty
    var col1Start = '<div class="slick-slide"><div>';
    var col1End = '</div></div>';

    var defaultTile = '';

    for (var i = 0; i < totalItem; i++) {
        if (dataReturnCount > 0) {
            if (defaultTile == '') {
                $(data.children()[i]).css({
                    'visibility': 'hidden'
                });
                defaultTile = data.children()[i].outerHTML;
            }

            if (i < dataReturnCount) {
                $(data.children()[i]).css({
                    'display': 'inline-block'
                });
                col1Start += data.children()[i].outerHTML;
            }
            else {
                //just use this case for Yelp landing
                if (datasource === 'yelp_dining') {
                    var countItem = parseInt(yelItemOffset) + dataReturnCount;
                    if (countItem !== parseInt(totalItemResponse)) {
                        col1Start += defaultTile;
                    }
                }
            }
        }
    }
    col1Start += col1End;
    result = col1Start;

    return result;
}

//JRCW-426
function rebuildSlideInCaseMoreRowDisplayForMobile(itemOnDisplay, data, moduleId) {
    var result = '';
    var dataReturnCount = data.children().length;
    if(dataReturnCount>0){
        for (var i = 0; i < itemOnDisplay; i++) {
                if(data.children()[i]!=undefined || data.children()[i]!=null)
                {
                    $(data.children()[i]).css({
                            display: 'inline-block',
                            width:"100%"
                    });
                    result+='<div><div>'+data.children()[i].outerHTML+'</div></div>';
                }
        }
    }
    return result;
}

function calcRowsDisplayFollowItemsDisplay(itemsDisplay, totalItems) {
    var rowDisplay = 1;
    var itemBasedToCalc = itemsDisplay <= totalItems ? itemsDisplay : totalItems;
    if (parseInt(itemBasedToCalc) % 4 === 0) {
        rowDisplay = parseInt(itemBasedToCalc / 4);
    } else {
        rowDisplay = parseInt(itemBasedToCalc / 4) + 1;
    }

    return rowDisplay;
}

function ajaxGetDataForCarousel(url, data, callback) {
    $.ajax({
        type: "GET",
        url: url,
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function () {
            showLoading();
        },
        complete: function () {
            hideLoading();
            setTimeout(function (parameters) {
                autoScrollToSelectedItem('#timeDividerNewDinningForm, #reservationTime, #cuisineSelect');
            }, 0);
        },
        success: function (response) {
            if (response.success) {
                callback(null, response);
                App.ChangeScrollBar();
                rebind_autocomplete(data.filter_datasource);
                UI.ShowCap(moduleIdClicked);
                alignGrayBar(0);
                alignGrayBarTablet(0);
            }
            else {
                var pathname = window.location.pathname;
                pathname = pathname != '' ? pathname.toLowerCase() : "";
                if (response.code == '401' && (pathname != "/loggedout" && pathname != "loggedout")) {
                    var url = response.returnUrl;
                    if (!url || url == '') {
                        url = "/LoggedOut";
                    }

                    window.location.href = url;
                }
                else {
                    UI.ShowCap();
                    callback(new Error(response.error), null);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            callback(new Error(thrownError), null);
        }
    });
}

function isModuleSlickCarousel(moduleId) {
    return $(_slickContentClassNameConst + moduleId).length !== 0;
}

function addAttributeForSlickTile(slickModule, rowDisplay, itemsDisplay, moduleId) {
    var attrItemsDisplay = slickModule.attr('data-itemsDisplay');
    if (typeof attrItemsDisplay === typeof undefined || attrItemsDisplay === false) {
        slickModule.attr('data-itemsDisplay', itemsDisplay);
    }
    var attrRowDisplay = slickModule.attr('data-rowDisplay');
    if (typeof attrRowDisplay === typeof undefined || attrRowDisplay === false) {
        slickModule.attr('data-rowDisplay', rowDisplay);
    }
    //var attrModuleCarouselId = slickModule.attr('data-moduleCarouselId');
    //if (typeof attrModuleCarouselId === typeof undefined || attrModuleCarouselId === false) {
    //    slickModule.attr('data-moduleCarouselId', 'tiles_module_pagination_carousel_' + moduleId);
    //}
}
function alignGrayBar(currentSlideIndex) {

    if ($(window).width() > 991) {
        var arrayHeight = new Array();
        var maxHeight;
        if (moduleIdClicked.length == 0 || moduleIdClicked == "") {
            $('.slides-recommends').each(function (index) { // get LIST SLIDE in PAGE
                $(this).find('.slick-slide').each(function (i) {
                    var listItem = $(this).find('div > .view-type-tilesview'); //get list item each page in each slide module....
                    listItem.find('.ic_content').each(function (ii) {
                        var contentItem = $(this).find('.ic_text');
                        contentItem.css({ 'display': 'block' });

                        var HeightContentItem = contentItem.find('.ic_height-content').outerHeight();
                        arrayHeight.push(HeightContentItem);

                        contentItem.css({ 'display': 'none' });

                        $(this).find('.capslide-more').css('display', 'inline-block');
                        $(this).find('.capslide-less').css('display', 'none');
                    });

                    if (arrayHeight.length > 0) {
                        maxHeight = Math.max.apply(null, arrayHeight);
                        $(this).find('div > .view-type-tilesview').find('.ic_height-content').each(function (i) {
                            $(this).css('height', maxHeight);
                        });

                    }
                    arrayHeight = [];
                });
            });
        }
        else {
            $('.multiple-items-responsive_' + moduleIdClicked).find('.slick-slide').each(function (i) {
                var listItem = $(this).find('div > .view-type-tilesview'); //get list item each page in each slide module....
                listItem.find('.ic_content').each(function (ii) {
                    var contentItem = $(this).find('.ic_text');
                    contentItem.css({ 'display': 'block' });

                    var HeightContentItem = contentItem.find('.ic_height-content').outerHeight();
                    arrayHeight.push(HeightContentItem);

                    contentItem.css({ 'display': 'none' });

                    $(this).find('.capslide-more').css('display', 'inline-block');
                    $(this).find('.capslide-less').css('display', 'none');
                });

                if (arrayHeight.length > 0) {
                    maxHeight = Math.max.apply(null, arrayHeight);
                    $(this).find('div > .view-type-tilesview').find('.ic_height-content').each(function (i) {
                        $(this).css('height', maxHeight);
                    });

                }
                arrayHeight = [];

            });
        }
    }
}

function handleEventSlick(moduleId, currentPage, rowDisplay, $inputSlickModule) {
    $(_slickContentClassNameConst + moduleId).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        moduleIdClicked = moduleId;

        var itemTotals = parseInt($inputSlickModule.attr('data-total-items'));
        var itemOnDisplay = parseInt($inputSlickModule.attr('data-items-on-page'));

        var pageNumber = parseInt($(_slickContentClassNameConst + moduleId + ' .slick-dots li.slick-active button').text());
        var totalCurrentItem = slick.$slides.length * itemOnDisplay;//pageNumber * rowDisplay * 4;

        var direction = '';
        var queryString = '';
        var totalItemLoaded = '';

        if ($(window).width() > 991) { //FOR PC
            //var currentPage = 1;
            if (currentSlide === 0 && nextSlide === slick.$slides.length - 1 && totalCurrentItem > itemTotals) {
                // its going from the first slide to the last slide (backwards)
                direction = 'prev';
                currentPage = 1;
                pageNumber = 1;
                setTimeout(alignGrayBar(nextSlide), 0);

            } else if ((nextSlide > currentSlide || (currentSlide === (slick.$slides.length - 1) && nextSlide === 0)) && totalCurrentItem < itemTotals) {
                // its either going normally forwards or going from the last slide to the first
                direction = 'next';
                pageNumber += 1;
                setTimeout(alignGrayBar(nextSlide), 0);

            } else {
                direction = 'prev';
                pageNumber -= 1;
                setTimeout(alignGrayBar(nextSlide), 0);
                //alignGrayBar(nextSlide);
            }
            currentPage = pageNumber;
            queryString = $inputSlickModule.attr('data-query-string');
            //update curent item
            totalItemLoaded = $(_slickContentClassNameConst + moduleId).find('.view-type-tilesview').length;
        }
        else if (($(window).width() > 767) && ($(window).width() < 992)) { //FOR TABLET
            var totalpages = slick.$slides.length % 3 == 0 ? slick.$slides.length / 3 : parseInt(slick.$slides.length / 3) + 1;
            var dataTotalRequest = $('.multiple-items-responsive_' + moduleId).attr('data-total-request');

            if ($inputSlickModule.attr('pagenumberapi') == undefined)
                $inputSlickModule.attr('pagenumberapi', 1);

            if ((totalpages - pageNumber == 2) && currentSlide < nextSlide) {
                direction = 'next';
                setTimeout(alignGrayBar(nextSlide), 0);
                pageNumber = parseInt($inputSlickModule.attr('pagenumberapi')) + 1;
                $inputSlickModule.attr('pagenumberapi', pageNumber);

                currentPage = pageNumber;
                queryString = $inputSlickModule.attr('data-query-string');
                // totalItemLoaded = $('.multiple-items-responsive_' + moduleId).attr('TotalItemLoaded');
                totalItemLoaded = $(_slickContentClassNameConst + moduleId).find('.view-type-tilesview').length;
            }
            else {
                direction = 'prev';
            }
        }
        else { //For Mobile 
            if ($inputSlickModule.attr('pagenumberapi') == undefined) {
                $inputSlickModule.attr('pagenumberapi', 1);
            }

            var dataTotalRequest = $('.multiple-items-responsive_' + moduleId).attr('data-total-request');

            if ((pageNumber == slick.$slides.length - 1) && currentSlide < nextSlide) {
                direction = 'next';
                setTimeout(alignGrayBar(nextSlide), 0);
                pageNumber = parseInt($inputSlickModule.attr('pagenumberapi')) + 1;
                $inputSlickModule.attr('pagenumberapi', pageNumber);

                currentPage = pageNumber;
                queryString = $inputSlickModule.attr('data-query-string');
                //update curent item
                totalItemLoaded = $(_slickContentClassNameConst + moduleId).find('.view-type-tilesview').length;
            } else {
                direction = 'prev';
            }
        }

        if (direction === 'next' && totalItemLoaded < itemTotals) {
            var urlGet = _urlGetPageModuleContentConst + queryString;
            var params = buildParamsWhenPagging(moduleId, pageNumber + 1);
            //carousel only
            params.Direction = direction;
            params.TotalItemLoaded = totalItemLoaded;
            ajaxGetDataForCarousel(urlGet, params, function (error, responseData) {
                if (error) {
                    return;
                }
                if (responseData.data != "") {
                    var slideToAdd = '';
                    if ($(window).width() > 991) {  // FOR PC
                        slideToAdd = rebuildSlideInCaseMoreRowDisplay(rowDisplay, $(responseData.data).find(".data-container").eq(1), responseData.DataSource,
                            responseData.YelpItemOffset, responseData.totalitem);
                    }
                    else {
                        slideToAdd = rebuildSlideInCaseMoreRowDisplayForMobile(itemOnDisplay, $(responseData.data).find(".data-container").eq(1), moduleId);
                    }

                    if (slideToAdd !== '') {
                        $('#' + moduleId + ' .multiple-items-responsive').slick('slickAdd', slideToAdd, null, false);
                    }
                    //Hide dot indicators
                    hideCarouselPaging();
                }
            });
        }

        setArrowNavigationStats(false, moduleId, rowDisplay, nextSlide, currentPage);
    });

    //force hide when slick set disabled
    $(_slickContentClassNameConst + moduleId).on('afterChange', function (event, slick, currentSlide, nextSlide) {
        if ($(_slickContentClassNameConst + moduleId).find('button').hasClass('slick-disabled')) {
            $(_slickContentClassNameConst + moduleId).find('button.slick-disabled').hide();
        } else {
            $(_slickContentClassNameConst + moduleId).find('button').show();
        }
        alignGrayBarTablet();
        alignGrayBar(nextSlide);
    });
}
/* ----------------- Slick carousel pagging - end ----------------- */

/* ----------------- New filter - start ----------------- */
function appendFilterTag(modalId) {
    var moduleId = modalId.replace('filtersCuisine-', '');
    var $filterEls = $('input[name="filter_data"][data-moduleID="' + moduleId + '"]:checked');
    if ($filterEls.length > 0) {
        var elLength = $filterEls.length;
        for (var i = 0; i < elLength; i++) {
            var value = $($filterEls[i]).val();
            var $tagId = 'filter-tag-' + moduleId + '_' + value.replace(/[^A-Z0-9]+/ig, '-');
            if ($('#' + $tagId).length === 0) {
                $('.modal-filter-subhead[data-moduleID="' + moduleId + '"]')
                    .append('<div class="filter-tag" id="' + $tagId + '"><span>' + value + '</span><a class="__del" href="#"></a></div>');
            }
        }
    }
}

function newFilter($element, moduleId) {
    var data;
    var filterDatasource = '';
    if (moduleId === '') {
        moduleId = $element.attr('data-moduleid');
    }
    var $modalFilterEl = $('div[data-modal="filtersCuisine-' + moduleId + '"]');
    if ($modalFilterEl.length > 0) {
        var formFilter = $modalFilterEl.find('form');
        filterDatasource = formFilter.attr('filter_datasource');
        var datafilter = formFilter.serializeArray();
        //encode categories value
        datafilter.forEach(function (currentValue, index, array) {
            if (currentValue.name === "filter_location" && isNotNullAndEmpty(currentValue.value)) {
                var searchBox = formFilter.find('input[name=filter_location]');
                if (isNotNull(searchBox)) {
                    var searchLocation = searchBox.attr('data');
                    if (isNotNull(searchLocation)) {
                        currentValue.value = encodeURIComponent(searchLocation.replace(/\|/g, ','));
                    }
                }
            } else {
                currentValue.value = encodeURIComponent(currentValue.value);
            }
        });

        //search global
        if ($(window).width() > 992) {
            var formsearch = $('form[name="frmSearch-' + moduleId + '"]');
            var datasearch = formsearch.serializeArray();
            data = datasearch.concat(datafilter);
        } else {
            data = datafilter;
        }

        data.push({ name: 'filter_datasource', value: filterDatasource });

        //price max
        //dynamic price element
        var $priceEl = $('input[name="price_max"][data-moduleID="' + moduleId + '"]:checked');
        if ($priceEl.length > 0) {
            var elLength = $priceEl.length;
            for (var i = 0; i < elLength; i++) {
                data.push({ name: 'price_max', value: $($priceEl[i]).val() });
            }
        }

        //old price element
        //if (filterDatasource === 'kb_dining') {
        //    $priceEl = $('input[name="price_max"][data-moduleID="' + moduleId + '"]');
        //    if ($priceEl.length > 0) {
        //        data.push({ name: 'price_max', value: $priceEl.val() });
        //    }
        //}

        data[data.length] = { name: "pageModuleID", value: moduleId };
        if (data != null) {
            TilesPushState(data, 1);
            AjaxGetData(page_modules_services, data, filterDatasource, moduleId, 1);
        }
    }
}

function calcNumberOfFilterApplied(moduleItemId) {
    var $filterEls = $('input[name="filter_data"][data-moduleID="' + moduleItemId + '"]:checked');
    var filterElsLength = $filterEls.length;
    //remove duplicate in modal
    var countDuplicate = 0;
    for (var i = 0; i < filterElsLength; i++) {
        var value = $($filterEls[i]).val();
        if ($('input[value = "' + value + '"][data-moduleID="' + moduleItemId + '"]:checked').length > 1) {
            countDuplicate++;
        }
    }
    if (countDuplicate > 0) {
        filterElsLength = filterElsLength - (countDuplicate / 2);
    }
    var $priceEls = $('input[name="price_max"][data-moduleID="' + moduleItemId + '"]:checked');
    var filterAppliedNumber = filterElsLength + $priceEls.length;
    var $filtersApplied = $('span[data-moduleID="' + moduleItemId + '"]');
    if ($filtersApplied.length > 0) {
        var textDefault = $('span[data-moduleID="' + moduleItemId + '"]').attr('data-value');
        var textPluralDefault = $('span[data-moduleID="' + moduleItemId + '"]').attr('data-plural-value');

        var firstChar = textDefault.substring(0, 1);

        var textApplied = filterAppliedNumber > 1 ? textPluralDefault : textDefault;
        $filtersApplied.text(textApplied.replace(firstChar, filterAppliedNumber));
    }
}

function divideListFilterOnModal(modalId, noOfColumn) {
    var $modalFilter = $('div[data-modal="' + modalId + '"]');
    if ($modalFilter.length === 0) {
        return false;
    }
    var columns = new Array();//['.col_1st', '.col_2nd', '.col_3rd'];
    for (var i = 1; i <= noOfColumn; i++) {
        columns.push('.column-div-' + i);
    }
    var $listitem = $modalFilter.find('label.filter-cb');
    var $total = Math.ceil($listitem.length / noOfColumn);
    var index = 0;
    var i = 0;
    while (index < $listitem.length) {
        var newItem = $listitem.slice(0 + index, $total + index);
        $('div[data-modal= "' + modalId + '"] ' + columns[i]).append(newItem);
        i++;
        index += $total;
    }
}
function clickXOnFilterBar() {
    $(document).on('click', '.del-search_global', function (e) {

        $('.del-search_global').parents().first().find('input').val("");
        resetAllFilterMain($('.reset-all-filters'));

    });
}
function resetAllFilter() {
    $(document).on('click', '.reset-all-filters', function (e) {
        e.preventDefault();
        var $this = $(this);
        resetAllFilterMain($this);

    });
}

function resetAllFilterMain($this) {
    var moduleID = $this.attr('data-moduleid');
    $('input[name="filter_data"][data-moduleID="' + moduleID + '"]').removeAttr('checked');
    $('input[name="price_max"][data-moduleID="' + moduleID + '"]').removeAttr('checked');
    calcNumberOfFilterApplied(moduleID);
    //select all when remove all filters
    newFilter($this, moduleID);
}

function searchGlobal() {
    $(document).on('focusout', 'input[name="terms"]', function (e) {
        var _this = $(this);
        if (_this.val() !== '') {
            var moduleId = _this.attr('id').replace('terms-', '');
            newFilter(_this, moduleId);
        }
    });

    $(document).on('keyup', 'input[name="terms"]', function (e) {
        var _this = $(this);
        if (e.keyCode == 13) {
            if (_this.val() !== '') {
                var moduleId = _this.attr('id').replace('terms-', '');
                newFilter(_this, moduleId);
            }
        }
    });
}
/* ----------------- New filter - end ----------------- */

String.prototype.format = String.prototype.f = function () {
    var s = this, i = arguments.length;
    while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
};

function hideTileWithOutData() {
    var listTileModule = $('div[class^="tile_module_"]').toArray();
    if (listTileModule.length > 0) {
        listTileModule.forEach(function (currentTile, index, array) {
            if (!$(currentTile).find('.wrap-experiences').find('.img-cover').length > 0) {
                $(currentTile).show();
                $(currentTile).find('.contact_concierge').show();
                $(currentTile).find('.contact_concierge').css('float', 'left');
            }
            else {
                if ($(currentTile).find('.wrap-experiences').find('.img-cover').length > 0) {
                    $(currentTile).show();
                    $(currentTile).find('.contact_concierge').hide();
                }
                else {
                    $(currentTile).show();
                    $(currentTile).find('.contact_concierge').show();
                    $(currentTile).find('.contact_concierge').css('float', 'left');

                }
            }
        });
    }
}

//JRCW-417
var ArrModuleId =new Array();
function HideFilterInModule(){
    if (ArrModuleId.length>0) {
        for (var i = 0; i < ArrModuleId.length; i++) {
            if($("#" + ArrModuleId[i]).find('.contact_concierge').is(':visible')){
                if($('.filter-tilemodule-'+ArrModuleId[i]).length>0)    
                { 
                     $('.filter-tilemodule-'+ArrModuleId[i]).hide();
                }
            }
          
        }
    }
}


function isShowFilter(callback) {
    if ($('div').hasClass('hero-choose-dd')) {
        var isUsOnly = !!($(".hero-choose-dd").find('._only-one').length);
        var lstModuleIds = $('div[class^="tile_module_"]');
        var pageModuleId = '';
        if (lstModuleIds.length > 0) {
            pageModuleId = lstModuleIds[0].id;
        }
        $.ajax({
            type: "GET",
            url: "/view-request/site/IsShowFilter",
            dataType: "json",
            data: {
                pageModuleId: pageModuleId,
                isUsOnly: isUsOnly
            },
            success: function (resultData) {
                //check current show or not
                if (resultData.isShowFilter == true) {
                    if (!($('[data-hero-city]').text() == "")) {
                        if (!$(".hero-choose-button").hasClass('_active active')) {
                            $('[data-dd-toggle="hero-choose-city"]').trigger('click');
                        }

                    }

                    $('form.yelplanding-filter-date-time').hide();
                    $('div.wrap-experiences div.filter').hide();
                }
                else {
                    $('form.yelplanding-filter-date-time').show();
                    $('div.wrap-experiences div.filter').show();
                }

                //Apply logic for button
                if (typeof resultData.button != "undefined") {

                    //Set new text for default and active span
                    $('.hero-body-choose').find('span.__default_text').text(resultData.button.defaultText); //Ticket JRCW-231
                    $('.hero-body-choose').find('span.__active_text').text(resultData.button.activeText); //Ticket JRCW-231
                    if (resultData.button.isDefault) {
                        $(".hero-body-choose").find(".hero-choose-button").removeClass('_active active'); //Ticket JRCW-231
                    }
                    else {
                        $(".hero-body-choose").find(".hero-choose-button").removeClass('_active active').addClass('_active active'); //Ticket JRCW-231
                    }
                }
                if (typeof(callback) == 'function')
                     callback();
            }
        });
    }
}

function rebindReservationHour(reservationCovers) {
    var paramHour = reservationCovers;
    if (paramHour === '' || paramHour === null) {
        paramHour = getParameterByName("ReservationTime");
    }
    if (paramHour !== null && paramHour !== '') {
        var hour = paramHour.trim().split(':')[0];
        if (!isNaN(parseInt(hour))){
            if (parseInt(hour) < 12) {
                paramHour = paramHour + ' AM';
            } else if (parseInt(hour) === 12) {
                paramHour = paramHour + ' PM';
            } else{
                var mins = paramHour.trim().split(':')[1];
                var hoursAMPM = parseInt(hour) - 12;
                paramHour = hoursAMPM + ':' + mins + ' PM';
            }

            //IE issue - remove selected before add new =.=
            if ($('#reservationTime option:selected').length > 1) {
                $('#reservationTime option:selected').remove();
            }
            $('#reservationTime option').each(function () {
                if ($(this).text() === paramHour) {
                    $(this).attr('selected', 'selected');
                    //set timeout for delay render element in IE
                    setTimeout(function () {
                        $('#reservationTime').next('div.btn-group-selectbox').find('button').attr('title', paramHour);
                    }, 0);
                    setTimeout(function () {
                        $('#reservationTime').next('div').find('span.multiselect-selected-text').text(paramHour);
                    }, 0);
                    return false;
                }
            });
        }
    }
}

function rebindReservationCover(reservationCovers) {
    var compareText = reservationCovers;
    if (compareText === '' || compareText === null) {
        var paramCover = getParameterByName("ReservationCovers");
        if (paramCover !== null && paramCover !== '') {
            var noOfPeople = paramCover.trim().split(' ')[0];
            if (!isNaN(parseInt(noOfPeople))) {
                var peopleText = parseInt(noOfPeople) === 1 ? ' person' : ' people';
                compareText = noOfPeople + peopleText;
            }
        }
    }
    if (compareText !== null && compareText !== '') {
        //IE issue - remove selected before add new =.=
        if ($('select[name="peopleSelect"] option:selected').length > 1) {
            $('select[name="peopleSelect"] option:selected').remove();
        }
        $('select[name="peopleSelect"] option').each(function () {
            var $this = $(this);
            if ($this.text().trim().toLowerCase() === compareText.trim().toLowerCase()) {
                $this.attr('selected', 'selected');
                setTimeout(function () {
                    $('select[name="peopleSelect"]').next('div.btn-group-selectbox').find('button').attr('title', compareText);
                }, 0);
                setTimeout(function () {
                    $('select[name="peopleSelect"]').next('div').find('span.multiselect-selected-text').text(compareText);
                }, 0);
                return false;
            }
        });
    }
}

function customDestinationSearchSpecific() {
    var cusSearch = $('.custom-search-des-spec');
    if ($(cusSearch).is('[readonly]'))  {
        $(cusSearch).removeAttr('readonly');
        $(cusSearch).blur();
        $(cusSearch).focus();
    }
}

function buildParamsWhenPagging(moduleId, pageNumber) {
    //get data for yelp landing
    var params = new Object();
    params.pageModuleID = moduleId;
    params.page = pageNumber;
    params.ReservationTime = getParameterByName("ReservationTime");
    params.date = getParameterByName("ReservationDate");
    params.peopleSelect = getParameterByName("ReservationCovers");
    return params;
}

function hideCarouselPaging() {
    $('ul.slick-dots').css('display', 'none');
}

/* Init control date Yelp */
function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}

function initReservationDateForYelpLanding(element, initDate) {
    var offset = Number($('#valueTimeZone').val() || 0);
    var minDateByOffset = getMinDateOffset(offset);
    if (typeof initDate == 'undefined' || initDate == null) {
        $(element).validation({});
        $(element).datepicker('option', { minDate: minDateByOffset, maxDate: addDays(minDateByOffset, 90) });
    }
    else {
        $(element).datepicker({
            showOtherMonths: true,
            selectOtherMonths: false,
            dateFormat: "DD, MM d, yy",
            setDate: initDate > minDateByOffset ? initDate : minDateByOffset,
            changeMonth: true,
            changeYear: true,
            minDate: minDateByOffset,
            beforeShow: function (input, inst) {
                $('#ui-datepicker-div').addClass("booking-datepicker");
                if ($('.ui-datepicker').closest("body").find(".page-my-reminders").length > 0) {
                    $(this).closest("body").find('#ui-datepicker-div').addClass("bigger-ui-datepicker");
                }
                if ($(document).width() <= 992) {
                    $('.lt-overlay').addClass('active');
                }
                if ($(window).width() <= 767 && $('.btn-nav').is(':visible')) {
                    setTimeout(function () {
                        $('#ui-datepicker-div').css('top', $('.datepicker').offset().top + 'px');
                    }, 1);
                }
            },
            onSelect: function (selectedDate) {
                if ($(document).width() <= 992) {
                    $('.lt-overlay').removeClass('active');
                }
            }
        });
        $('.lt-overlay').on('click touchstart', function () {
            $('.datepicker').datepicker("hide");
            if ($('#cuisineSelect+.btn-group-selectbox').hasClass('open')) {
                return false;
            }
            $(this).removeClass('active');
        });
        $(element).datepicker('setDate', initDate > minDateByOffset ? initDate : minDateByOffset);
        $(element).datepicker('option', { dateFormat: "D, d MM, yy" });
        $(element).datepicker('option', { minDate: minDateByOffset, maxDate: addDays(minDateByOffset, 90) });
    }
}

function getMinDateOffset(offset) {
    var nextDate = new Date(new Date(new Date().getTime() + (offset + 24) * 3600 * 1000).toUTCString().replace(/ GMT$/, ""));
    var date = new Date(new Date(new Date().getTime() + offset * 3600 * 1000).toUTCString().replace(/ GMT$/, ""));
    if (offset == 0) {
        date = new Date();
    }
    if (date.getHours() >= 16) {
        date = nextDate;
    }
    return date;
}

//Display ajaxLoading when redirect to detail page
$(document).on('click', 'div.article', function (e) {
    //Prevent ctrl + click when oepn new tab
    if (!e.ctrlKey) {
        showLoading();
    }
});

$(document).ready(function () {
    alignGrayBar(0);

    $("div.ic_more a").click(function (e) {
        //Prevent ctrl + click when oepn new tab
        if (!e.ctrlKey) {
            showLoading();
        }
    });

    setTimeout(function (parameters) {
        autoScrollToSelectedItem('#timeDividerNewDinningForm, #reservationTime, #cuisineSelect');
    }, 0);
  
});

function alignGrayBarTablet() {
    if (($(window).width() > 767) && ($(window).width() < 992)) {
        var arrayHeight = new Array();
        var maxHeight;

        if (moduleIdClicked.length == 0 || moduleIdClicked == "") {
            $('.slides-recommends').each(function (index) { // get LIST SLIDE in PAGE
                $(this).find('.view-type-tilesview').each(function (i) {
                    var Item = $(this).find('.ic_text'); //get list item each page in each slide module....
                    Item.css({ 'display': 'block' });

                    var HeightcontentItem = Item.find('.ic_height-content').outerHeight();
                    arrayHeight.push(HeightcontentItem);

                    Item.css({ 'display': 'none' });
                    $(this).find('.capslide-more').css('display', 'inline-block');
                    $(this).find('.capslide-less').css('display', 'none');
                });
             
                if (arrayHeight.length > 0) {
                    maxHeight = Math.max.apply(null, arrayHeight);
                    $(this).find('.view-type-tilesview').find('.ic_height-content').each(function (i) {
                        $(this).css('height', maxHeight);
                    });

                }
                arrayHeight = [];
            });
        }
        else
        {
            $('.multiple-items-responsive_' + moduleIdClicked).find('.view-type-tilesview').each(function (i) {
                var Item = $(this).find('.ic_text'); //get list item each page in each slide module....
                Item.css({ 'display': 'block' });

                var HeightcontentItem = Item.find('.ic_height-content').outerHeight();
                arrayHeight.push(HeightcontentItem);

                Item.css({ 'display': 'none' });
                $(this).find('.capslide-more').css('display', 'inline-block');
                $(this).find('.capslide-less').css('display', 'none');
            });

            if (arrayHeight.length > 0) {
                maxHeight = Math.max.apply(null, arrayHeight);
                 $('.multiple-items-responsive_' + moduleIdClicked).find('.view-type-tilesview .ic_height-content').each(function (i) {
                    $(this).css('height', maxHeight);
                });

            }
            arrayHeight = [];
        }
    }

}
function resetStateOfToggleTileIcon(arr) {
    for (var i = 0; i < arr.length; i++) {
        $(arr[i]).find('.ic_caption .ic_height_title .capslide-more').css('display', 'inline-block');
        $(arr[i]).find('.ic_caption .ic_height_title .capslide-less').css('display', 'none');
    }
}
