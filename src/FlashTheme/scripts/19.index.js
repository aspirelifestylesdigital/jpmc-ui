
var Home = function () {

    function initSlick() {
        $(document).ready(function(){
	       slideProduct();
        });
        $('.menu-change').find('a').click(function (event) {
            $('.name-change').text($(this).text());
        })
    }

    function  slideProduct(){
        
            $('.slides-product').slick({
                dots: true,
                arrows: false,
                infinite: false,
                speed: 300,
                slidesToShow: 3,
                slidesToScroll: 3,
                rows: 2,
                slidesPerRow : 1,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true,
                            rows: 2,
                            slidesPerRow : 1
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            dots: true,
                            rows: 2,
                            slidesPerRow : 1
                        }
                    }
                ]
            });
    }

	function  forceNoScrollBackgroundPopupIphone(){
		if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))) {
			$("body").css({"position":"fixed","overflow":"hidden", "width":"100%","top":"0"});
		}		
    }
	
    return {
    init: function (isAuthenticated) {

            App.init();
            initSlick();
            UI.init();

            //zoom screen when switch between lanscape and portail
            if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))) {
                $( window ).resize(function() {
                    $(".apr-form input[type='text']").blur();
                });
                window.document.addEventListener('orientationchange', function() {
                    var iOS = navigator.userAgent.match(/(iPad|iPhone|iPod)/g);
                    var viewportmeta = document.querySelector('meta[name="viewport"]');
                    if (iOS && viewportmeta) {
                        if (viewportmeta.content.match(/width=device-width/)) {
                            viewportmeta.content = viewportmeta.content.replace(/width=[^,]+/, 'width=1');
                        }
                        viewportmeta.content = viewportmeta.content.replace(/width=[^,]+/, 'width=' + window.innerWidth);
                    }
                    // If you want to hide the address bar on orientation change, uncomment the next line
                    // window.scrollTo(0, 0);
                }, false);
            }
        },	
        slideProduct: function () {
            slideProduct();
        }
    }
}();