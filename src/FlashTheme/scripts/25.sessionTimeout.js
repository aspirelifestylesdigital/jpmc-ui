﻿$(function () {
    if (typeof SESSION_TIMEOUT_MINUTES != 'undefined' && SESSION_TIMEOUT_MINUTES != undefined && SESSION_TIMEOUT_MINUTES != -1) {

        if (typeof GRACE_PERIOD_MINUTES == 'undefined' || GRACE_PERIOD_MINUTES == 0) {
            GRACE_PERIOD_MINUTES = 1;
        }
		
        var SESSION_TIMEOUT_SECONDS = SESSION_TIMEOUT_MINUTES * 60;
        var WARNING_TIMEOUT_IN_SECONDS = GRACE_PERIOD_MINUTES * 60;

        var showTimeoutWarningSeconds = SESSION_TIMEOUT_SECONDS - WARNING_TIMEOUT_IN_SECONDS;
        var showTimeoutWarningTimer = window.setTimeout(ShowSessionTimeoutWarningBox, showTimeoutWarningSeconds * 1000);
        var warningTimeoutTimer = null;

        $("#btnSessionContinue").click(function () {
            $.ajax({
                url: "/SSOKeepLogin",
                type: "POST",
                success: function (response) {
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });

            if (warningTimeoutTimer != null) {
                window.clearTimeout(warningTimeoutTimer);
            }
            showTimeoutWarningTimer = window.setTimeout(ShowSessionTimeoutWarningBox, showTimeoutWarningSeconds * 1000);
        });

        function ShowSessionTimeoutWarningBox() {
            $.ajax({
                url: "/GetRemainingSessionTimeout",
                type: "GET",
                success: function (response) {
                    var sessionTimeoutRemaining = 0;
                    if ($.trim(response)) {
                        var sessionTimeoutRemaining = response.ReturnValue;
                    }

                    showTimeoutWarningSeconds = sessionTimeoutRemaining - WARNING_TIMEOUT_IN_SECONDS;

                    if (showTimeoutWarningSeconds <= 0) {
                        warningTimeoutTimer = window.setTimeout(SessionTimedOut, WARNING_TIMEOUT_IN_SECONDS * 1000);
                        $("#modal-timeout").modal("show");
                    }
                    else {
                        window.clearTimeout(showTimeoutWarningTimer);
                        showTimeoutWarningTimer = window.setTimeout(ShowSessionTimeoutWarningBox, showTimeoutWarningSeconds * 1000);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    SessionTimedOut();
                    console.log(textStatus, errorThrown);
                }
            });
        }

        function SessionTimedOut() {
            $.ajax({
                url: "/GetRemainingSessionTimeout",
                type: "GET",
                success: function (response) {
                    var logOutUrl = '/LogOut';
                    var sessionTimeoutRemaining = 0;
                    if ($.trim(response)) {
                        sessionTimeoutRemaining = response.ReturnValue;
                        logOutUrl = response.LogOutUrl;
                    }

                    showTimeoutWarningSeconds = sessionTimeoutRemaining - WARNING_TIMEOUT_IN_SECONDS;

                    if (showTimeoutWarningSeconds <= 0) {
                        window.clearTimeout(warningTimeoutTimer);
                        window.clearTimeout(showTimeoutWarningTimer);
                        window.clearTimeout(showTimeoutWarningSeconds);
                        window.location.href = logOutUrl;
                    }
                    else {
                        $("#modal-timeout").modal("hide");
                        window.clearTimeout(showTimeoutWarningTimer);
                        showTimeoutWarningTimer = window.setTimeout(ShowSessionTimeoutWarningBox, showTimeoutWarningSeconds * 1000);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    SessionTimedOut();
                    console.log(textStatus, errorThrown);
                }
            });
        }
    }

});