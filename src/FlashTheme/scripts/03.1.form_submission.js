﻿$(function () {
    $.fn.serializeObject = function () {
        var jsonResults = {};
        var checkboxControls = [];
        var arrayData = this.serializeArray();

        $.each(arrayData, function () {
            var selectedElement = $("#" + this.name);
            selectedElement = selectedElement.length > 0 ?
                selectedElement : $("[name='" + this.name + "']").first();
            if (selectedElement.length > 0) {
                var isCheckbox = selectedElement.prop('type') === 'checkbox';
                var isNewCheckbox = false;
                if (isCheckbox) {
                    if (checkboxControls.indexOf(this.name) < 0) {
                        isNewCheckbox = true;
                        checkboxControls.push(this.name);
                    }
                }

                var beSubmitted = ((selectedElement.prop('disabled') == false &&
                    selectedElement.is('[readonly]') == false)
                    || selectedElement.hasClass("always-submitted")
                    || selectedElement.hasClass("reservationDate"))
                    && (!isCheckbox || isNewCheckbox);
                if (beSubmitted) {
                    var elementValue = this.value || '';
                    if (selectedElement.hasClass("reservationDate")
                        && isNotNullAndEmpty(elementValue)) {
                        var dt = new Date(elementValue);
                        elementValue = dt.getFullYear() + '-'
                            + formatMonthDay(dt.getMonth() + 1)
                            + '-' + formatMonthDay(dt.getDate());
                    }
                    if (selectedElement.hasClass("multiLanguageDateTime")
                        && isNotNullAndEmpty(elementValue)) {
                        elementValue = this.value || '';
                    }
                    if (jsonResults[this.name]) {
                        if (!jsonResults[this.name].push) {
                            jsonResults[this.name] = [jsonResults[this.name]];
                        }
                        jsonResults[this.name].push(elementValue);
                    } else {
                        jsonResults[this.name] = elementValue;
                    }
                }
            }
        });
        return jsonResults;
    };
});

function isFieldRequired(element) {
   if (isNotNull(element.getAttribute("required")) || isNotNull(element.getAttribute("data-val-required"))) {
        return true;
    }
    else {
        return false;
    }
}

function isNotNull(val) {
    return typeof (val) !== 'undefined' && val != null;
}

function isNotNullAndEmpty(strVal) {
    return typeof (strVal) !== 'undefined' && strVal != null && strVal != '';
}

function formatMonthDay(val) {
    return (val).toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false });
}

function showSubmissionErrorMessage() {
    var submitMsgElement = $("#submit_message");
    if (submitMsgElement.length > 0) {
        submitMsgElement.show();
        $('html, body').animate({
            scrollTop: 100
        }, 200);
    }
}

function redirectToPath(url) {
    window.location.href = url;
}

$(".booking-btn").click(function () {
    var attrClass = $(this).attr('class');
    var bookingForm = $(".fr-booking").first();
    if (bookingForm.length > 0) {
        bookingForm.validate();
        if (bookingForm.valid()) {
            var showAjaxLoading = false;
            if (isNotNullAndEmpty(attrClass)) {
                showAjaxLoading = attrClass.indexOf('showAjaxLoading') >= 0;
            }
            var bookingInfo = bookingForm.serializeObject();
            $.ajax({
                url: bookingForm.attr('action'),
                type: "POST",
                dataType: "json",
                data: JSON.stringify(bookingInfo),
                contentType: "application/json;charset=utf-8",
                beforeSend: function () {
                    if (showAjaxLoading) {
                        $("body").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
                    }
                },
                complete: function () {
                    if (showAjaxLoading) {
                        $("#ajaxLoading").remove();
                    }
                },
                success: function (response) {
                    var result = response.Result;
                    if (typeof (result) !== 'undefined' && result != null) {
                        if (result.success == true) {
                            redirectToPath(result.returnUrl);
                        } else {
                            console.log(response.Result.message);
                            showSubmissionErrorMessage();
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                    showSubmissionErrorMessage();
                }
            });
        }
    }
});