﻿"use strict";

if (isPrefilledRestaurantName == 'true' || isPrefilledRestaurantName == 'True') {
    $("#RestaurantName").attr('readonly', true);
}
$('.fr-booking').validation({
    selectCreditName: {
        name: 'ReservationName'
    },
    txtContactNumber: {
        name: 'MobileNumber'
    },
    txtEmail: {
        name: 'Email'
    },
    txtCreditCard: {
        name: 'CreditCardDigits'
    }
})
var placeholderDish = "You are entitled for 1 complimentary dishes";

$("#creditCardDigits").keyup(function () {
    var binNumber = $('#creditCardDigits').val();
    if (binNumber.length >= 6) {
        $.ajax({
            type: 'POST',
            url: "/sm/FormModule/MaxDishes",
            data: {
                binNumber
            },
            dataType: 'json',
            success: function (result) {
                var maxDishNumber = "";
                maxDishNumber = result.Result.maxDishesCanOrder;
                $('#PlaceholderNumberOfDish').text(placeholderDish.replace('1', maxDishNumber));
                for (var i = 0; i < dishCount; i++) {
                    $("#Dishes_" + i + "__Quantity").attr({ "max": maxDishNumber }).val('0');
                    $("#Dishes_" + i + "__Quantity-error").hide();
                    if ($("input").hasClass("error lt-error"))
                        $("input").removeClass("error lt-error");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }
})
