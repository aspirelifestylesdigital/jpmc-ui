var App = function () {

    function addBackToTop() {
        if (!$('#back-to-top').length > 0) {
            var htmlBackToTop = '<a id="back-to-top" href="#top" title="Back to top">' +
                '<span class="ico-arrow-up" />' +
                '</a>';

            $('body').prepend(htmlBackToTop);

            $(window).on('load scroll', function () {
                var $backToTop = $('#back-to-top');
                var right = $(window).width() - $(".top-nav .container").width() - 2;

                if ($(window).scrollTop() >= 100) {
                    $backToTop.addClass('active');
                    if (right > 0) {
                        $backToTop.css('right', right / 2 + 'px');
                    } else {
                        $backToTop.removeAttr('style');
                    }
                } else {
                    $backToTop.removeClass('active');
                    $backToTop.removeAttr('style');
                }
            });

            $('#back-to-top').on('click', function (event) {
                event.preventDefault();

                $("html, body").animate({
                    scrollTop: 0
                }, 300);
            });
        }
    }
    function isIE11Check() {
        return !!navigator.userAgent.match(/Trident.*rv\:11\./);
        
    }

    function LogoutNoRedirect() {
        //console.log(sfLogoutUrl);        
        $(".btn-close-window").on("click", function (event) {
            event.preventDefault();
            $.ajax({
                url: sfLogoutUrl,
                xhrFields: {
                    withCredentials: true
                },
                beforeSend: function () {
                    $("body").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
                },
                complete: function () {
                    $("#ajaxLoading").remove();

                },
                //cache: true,
                success: function (response) {
                    location.href = "/Logout"
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(ajaxOptions, thrownError);
                    location.href = "/Logout";
                }
            });
        });
    }
    function ShowForgetPasswordModal() { // when pre login
        if ($('.link-forget-password').length > 0) {
            //click link forget password => hide Model SIGN IN show Model FORGET PASSWORD
            $('.link-forget-password').on('click', function (event) {
                event.preventDefault();
                $("#modal-signin").modal('hide');
                $("#modal-forget-password").modal('show');
            });
        }
    }
    function ShowLogInModal() { // when pre login
        if ($('.link-log-in').length > 0) {
            //click link log in => hide Model FORGET PASSWORD show Model SIGN IN 
            $('.link-log-in').on('click', function (event) {
                event.preventDefault();
                $("#modal-signin").modal('show');
                $("#modal-forget-password").modal('hide');
            });
        }
    }
    function changeLocationIconDisk() {
        //if (atLeastOneHasIcon == "False") {
        //    $('.menu-aspire li.have-icon').removeClass('have-icon');
        //}
    }

    function calHeightWrapper() {
        var minHeight = $('.module-richtext').position().top + $('.content-filter').outerHeight(true);
        // if($('.content-filter').hasClass('bigger-filter')){
        //     var minHeight = minHeight + 100;
        // }
        $('.wrapper').css('min-height', minHeight + 'px');
    }

    function showBoxFilter() {
        // Module-filter show/hide when click on arrow
        $(document).on("click", ".icon-arrow", function () {
            var listCheckbox = $(this).parent().children('.list-checkbox');
            if (listCheckbox.length <= 0) {
                listCheckbox = $(this).parent().children('.multi-list');
            }

            if ($(this).hasClass('toggle')) {
                $(this).removeClass('toggle');

                listCheckbox.slideDown('fast');

            } else {
                $(this).addClass('toggle');
                listCheckbox.slideUp('fast');
            };
        });

        // Form filter
        $(document).on("click", ".wrap-btn-filter .btn-filter-top", function () {
            $(this).closest(".filtering").find(".form-filter-top").addClass("active");
            $('.header').hide('slow');
            $('#footer').hide('slow');
            $('body').addClass('unscroll');
        });
        $(document).on("click", ".form-filter-title .icon-close, .form-filter-top .btn-filter-main, .form-filter-top .btn-filter", function () {
            $("html .form-filter-top").removeClass("active");
            $('.header').show('fast');
            $('#footer').show('fast');
            $('body').removeClass('unscroll');
        });

        // Sticky btn-filter
        if ($(".module-filter .content-filter .btn-filter").length > 0) {
            var btnFilter = $(".module-filter .content-filter .btn-filter");
            var btnOffsetTop = btnFilter.offset().top;
            btnFilter.affix({
                offset: {
                    top: btnOffsetTop - 120
                }
            });
        }

        // Handle event when clicking on btn-filter
        var numOfClick = 0;
        $('.btn-filter-devices, .btn-filter').on("click", function () {
            if ($('.content-filter').hasClass('active') == true) {
                $('.content-filter').removeClass('active');
                $('.wrapper').css('min-height', 'auto');
            } else {
                $('.content-filter').addClass('active');
                calHeightWrapper();
            };

            // sticky menu if screen size is biger than filter size
            var stickyFilterPages = ".content-filter";
            var stickyFilter = $(stickyFilterPages);
            if (numOfClick == 0) {
                numOfClick++;
                var filterHeight = stickyFilter.outerHeight();
            }
            // filterHeight + 170 (is height of header + filter + chatbox + btn-back-to-top)
            if (stickyFilter.length > 0) {
                if ((filterHeight + 360) < $(window).height()) {
                    var filterOffsetTop = stickyFilter.offset().top;
                    stickyFilter.affix({
                        offset: {
                            top: filterOffsetTop - 120
                        }
                    });
                }
            }
        });

        // Btn-select-all
        $(document).on("click", ".btn-select-all", function () {
            var inputListCheck = $(this).closest(".item").find(".list-checkbox input");
            var inputAll = $(this).find("input");
            if (inputAll.prop("checked")) {
                inputListCheck.prop("checked", true);
            } else {
                inputListCheck.prop("checked", false);
            }
        });

        // Handle list item of Btn-select-all
        $(document).on("click", ".list-checkbox", function () {
            var btnCheckAll = $(this).closest(".item").find(".btn-select-all input");
            if ($(this).find("input:checked").length == $(this).find("input").length) {
                btnCheckAll.prop("checked", true);
            } else {
                btnCheckAll.prop("checked", false);
            }
        });
        // Handle list button check all group (tam.hua added 2017/04/12)
        // Handle multi-list checkbox when clicking
        $(document).on("click", '.multi-list .check-status', function () {
            var subList = $(this).siblings('.sub-list-checkbox');
            var inputCheck = $(this).siblings('input');
            var subFormFilter = $(".sub-form-filter-top");
            if ($(window).width() >= 992) {
                if (!inputCheck.prop("checked")) {
                    subList.find('input').prop("checked", true);
                } else {
                    subList.find('input').prop("checked", false);
                }
            } else {
                var dataShow = $(this).attr("data-show");
                // Keeping status when clicking checkbox
                if (inputCheck.prop("checked")) {
                    inputCheck.prop("checked", false);
                } else {
                    inputCheck.prop("checked", true);
                }
                subFormFilter.addClass("active");
                subFormFilter.find(".box-filter .item").removeClass("active");
                subFormFilter.find("[data-show='" + dataShow + "']").addClass("active");
            }
        });

        // Check status of sub-list-checkbox
        $(document).on('click', '.multi-list .sub-list-checkbox', function () {
            var inputCheck = $(this).closest('.item-list').children('input');
            if ($(this).find("input:checked").length == $(this).find("input").length) {
                inputCheck.prop("checked", true);
            } else {
                inputCheck.prop("checked", false);
            }
        });

        // .sub-form-filter-top: Handle btn-back, btn-done, btn-close
        var btnDoneSubFilter = ".sub-form-filter-top .sub-btn-filter";
        var btnCloseSubFilter = ".sub-form-filter-top .form-filter-title .icon-close";
        var btnBackSubFilter = ".sub-form-filter-top .form-filter-title label";
        var item = $(".sub-form-filter-top .box-filter .item");
        function btnInSubFormFilter(btnItem) {
            $(document).on('click', btnItem, function () {
                $("html .sub-form-filter-top").removeClass("active");
                item.each(function () {
                    var dataShow = $(this).attr("data-show");
                    var dataShowItem = $(".form-filter-top").find("[data-show*='" + dataShow + "']");
                    var input = dataShowItem.siblings('input');
                    if ($(this).find("input:checked").length > 0) {
                        input.prop('checked', true);
                    } else {
                        input.prop('checked', false);
                    }
                });
            });
        }
        // btn-done
        btnInSubFormFilter(btnDoneSubFilter);
        // btn-close
        btnInSubFormFilter(btnCloseSubFilter);
        // btn-back
        btnInSubFormFilter(btnBackSubFilter);


        // Handle btn-check-all in .sub-form-filter-top
        $(document).on('click', '.sub-form-filter-top .btn-check-all', function () {
            var btnCheckAll = $(this).siblings('input');
            var listCheckbox = $(this).siblings('.list-checkbox');
            if (!btnCheckAll.prop("checked")) {
                listCheckbox.find('input').prop('checked', true);
            } else {
                listCheckbox.find('input').prop('checked', false);
            }
        });

        // Check status of btn-check-all in .sub-form-filter-top
        $(document).on('click', '.sub-form-filter-top .list-checkbox', function () {
            var btnCheckAll = $(this).closest(".item").children('input');
            if ($(this).find("input:checked").length == $(this).find("input").length) {
                btnCheckAll.prop("checked", true);
            } else {
                btnCheckAll.prop("checked", false);
            }
        });
        // Handle list button check all group (tam.hua added 2017/04/12)
    }
    //double function showBoxFilter cho a Thanh lam tam. dung cho gourmet filter device

    function CalRangeSlide() {
        var rangeSlide = $('.price-range-slider input[type=range]');
        var winHeight = $(document).height();
        var winWidth = $(window).width();
        if ((winWidth < 1024) && (winWidth > 767)) {
            rangeSlide.css("padding-left", 0.096 * winWidth);
            rangeSlide.css("padding-right", 0.096 * winWidth);
        } else if ((winWidth < 767) && (winWidth > 450)) {
            rangeSlide.css("padding-left", 0.093 * winWidth);
            rangeSlide.css("padding-right", 0.093 * winWidth);
        } else if ((winWidth < 450) && (winWidth >= 320)) {
            rangeSlide.css("padding-left", 0.056 * winWidth);
            rangeSlide.css("padding-right", 0.056 * winWidth);
        }
    }

    // Fix scroll on filters
    function ChangeScrollBar() {
        if ($('[data-spy="scrollBar"]').length > 0) {
            $('[data-spy="scrollBar"]').each(function () {
                var _self = $(this);
                $(this).addClass('scrollbar-outer');
                $(this).scrollbar();
                $(this).focusout(function () {
                    $(this).focus();
                });
                $(document).on('touchstart touchend touchmove', function () {
                    _self.focus();
                })
            });
        }
    }
    function destinationChange() {
        $('.menu-change').find('a').click(function (event) {
            $('.name-change').text($(this).text());
        });
    }
    function commonAnimation() {
        $(".navigation-right ul li").on("click", function () {
            $(".navigation-right ul li").removeClass("active");
            $(this).addClass("active");

            //Smooth scroll when click on right navigation
            $("html, body").animate({
                scrollTop: $($(this).find("a").attr('href')).offset().top - 92
            }, 500);
            return false;
        });
        $("[data-spy='section-nav'] a").on("click", function () {
            //Smooth scroll when click on right navigation
            $("html, body").animate({
                scrollTop: $($(this).attr('href')).offset().top - 92
            }, 500);
            return false;
        });
    }
    function stickyMenu() {
        if ($('.info-book').length > 0) {
            var check = true;
            if (check) {
                var offsetTop = $('.info-book').offset().top - $('.aspire-nav').height() - 40; //40 = margin top
                check = false;
            }
            $('.info-book').affix({
                offset: {
                    top: offsetTop,
                    bottom: function () {
                        return (this.bottom = $('#footer').outerHeight(true) + 90)
                    }
                }
            });
        }
        if ($('.booking-box').length > 0) {
            var check = true;
            if (check) {
                var offsetTop = $('.booking-box').offset().top - $('.aspire-nav').height() - 22; //40 = margin top
                check = false;
            }
            if ($(window).width() < 768) {
                check = true;
            }
            $('.booking-box').affix({
                offset: {
                    top: offsetTop,
                    bottom: function () {
                        return (this.bottom = $('#footer').height() + $('.block-recommendations').height())
                    }
                }
            });
        }
    }

    function boxDateRange() {
        $(document).ready(function () {
            $(".earliestDate").datepicker({
                showOtherMonths: true,
                selectOtherMonths: false,
                dateFormat: "DD, d M, yy",
                changeMonth: true,
                changeYear: true,
                beforeShow: function (input, inst) {
                    $(".earliestDate").trigger('blur');
                    $('#ui-datepicker-div').addClass("booking-datepicker");
                    if ($('.ui-datepicker').closest("body").find(".page-my-reminders").length > 0) {
                        $(this).closest("body").find('#ui-datepicker-div').addClass("bigger-ui-datepicker");
                    }
                    if ($(document).width() <= 992) {
                        $('.datepicker-overlay').addClass('active');
                    }
                },
                onSelect: function (selectedDate) {
                    if ($(document).width() <= 992) {
                        $('.datepicker-overlay').removeClass('active');
                    }
                    $(".lastestDate").datepicker("option", "minDate", getDate(selectedDate));
                }
            });

            $(".lastestDate").datepicker({
                showOtherMonths: true,
                selectOtherMonths: false,
                dateFormat: "DD, d M, yy",
                changeMonth: true,
                changeYear: true,
                beforeShow: function (input, inst) {
                    $(".lastestDate").trigger('blur');
                    $('#ui-datepicker-div').addClass("booking-datepicker");
                    if ($('.ui-datepicker').closest("body").find(".page-my-reminders").length > 0) {
                        $(this).closest("body").find('#ui-datepicker-div').addClass("bigger-ui-datepicker");
                    }
                    if ($(document).width() <= 992) {
                        $('.datepicker-overlay').addClass('active');
                    }
                },
                onSelect: function (selectedDate) {
                    if ($(document).width() <= 992) {
                        $('.datepicker-overlay').removeClass('active');
                    }
                    $(".earliestDate").datepicker("option", "maxDate", getDate(selectedDate));
                }
            });

            var dateFormat = "DD, d M, yy";
            function getDate(element) {
                var date;
                try {
                    date = $.datepicker.parseDate(dateFormat, element);
                } catch (error) {
                    date = null;
                }
                return date;
            }
            $('.datepicker-overlay').on('click touchstart', function () {
                $(this).removeClass('active');
            });
        });
    }

    function setTimezoneCookie() {

        var timezone_cookie = "timezoneoffset";

        // if the timezone cookie not exists create one.
        if (!getCookie(timezone_cookie)) {

            // create a new cookie 
            set_cookie(timezone_cookie, new Date().getTimezoneOffset());

            // re-load the page
            location.reload();
        }
        // if the current timezone and the one stored in cookie are different
        // then store the new timezone in the cookie and refresh the page.
        else {

            var storedOffset = parseInt(getCookie(timezone_cookie));
            var currentOffset = new Date().getTimezoneOffset();

            // user may have changed the timezone
            if (storedOffset !== currentOffset) {
                set_cookie(timezone_cookie, new Date().getTimezoneOffset());
                location.reload();
            }
        }
    }

    /**
     * @description get error placeholder message translate by value
     * @param {string} apiMessage The error message response from API.
     * @param {string} formName The form name: tour / event / limo.
     * @param {function} callback The call back function.
     * @return {string} translated message 
     */
    function getPlaceholderSelfFormByValue(apiMessage, formName, callback) {
        $.ajax({
            type: "GET",
            url: '/sm/Base/GetPlaceholderSelfFormByValue',
            dataType: "json",
            data: { apiMessage: apiMessage, formName: formName },
            cache: false,
            success: function (response) {
                callback(response.translatedMessage);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                callback(apiMessage);
            }
        });
    }

    /**
     * @description get list error placeholder message translate by values
     * @param {string} apiMessages The list error message response from API.
     * @param {string} formName The form name: tour / event / limo.
     * @param {function} callback The call back function.
     * @return {string} translated message 
     */
    function getPlaceholderSelfFormByValues(apiMessages, formName, callback) {
        $.ajax({
            type: "POST",
            url: '/sm/Base/GetPlaceholderSelfFormByValues',
            dataType: "json",
            data: { apiMessages: apiMessages, formName: formName },
            cache: false,
            success: function (response) {
                callback(response.translatedMessages);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                callback(apiMessages);
            }
        });
    }

    function NavDestination() {
        function selectCity(cityId, cityName) {
            if (!(cityId && cityName)) return false; //Place selected city name into header

            $('[data-head-dest]').html(cityName); //Close header and hero destination dropdowns
            $('[data-dd-toggle="header-choose-city"]').removeClass('_active active');
            $('[data-dd="header-choose-city"]').removeClass('_active active');
            $('[data-dd-toggle="hero-choose-city"]').removeClass('_active active');
            $('[data-dd="hero-choose-city"]').removeClass('_active active'); //Update selected city in header and hero destination dropdowns

            $('[data-dest-city]').removeClass('_selected');
            $('[data-dest-city="' + cityId + '"]').addClass('_selected'); //Update hero button text when city is selected

            $('[data-dd-toggle="hero-choose-city"] .__default_text').html('CHANGE');
            $('[data-dd-toggle="hero-choose-city"] .__active_text').html('CHANGE LATER'); //Add city name into hero section text //CHANGE LATER

            $('.hero-body-choose').addClass('_selected');
            $('[data-hero-city]').html(cityName); //Add city name into benefits section text and categories buttons

            $('[data-benefits-city]').html(' IN ' + cityName.toUpperCase());
            $('[data-categorybtn-city]').html(' IN ' + cityName.toUpperCase()); //Hide Choose destination section

            $(".aspire-nav > .container").removeClass('selected-depth-1');
            $('.destination').addClass('_hidden');
        } //--Choose a city in header or hero dropdown

        $('body').on('click', '.prev-nav', function (e) {
            e.preventDefault();
            var $target = $(e.currentTarget);
            $target.parents('.header-menu-dest').find('._active.active').toggleClass('_active active');
            $(".aspire-nav > .container").removeClass('selected-depth-1');
        });

        $('body').on('click', '[data-dd-toggle]', function (e) { //remove touchstart to fix destination menu responsive
            e.preventDefault();
            var $target = $(e.currentTarget),
                ddId = $target.attr('data-dd-toggle'),
                $dd = $('[data-dd=' + ddId + ']');
            if (!$dd.length) return false;
            $target.toggleClass('_active active');
            $dd.toggleClass('_active active');
            $target.parents('.aspire-nav > .container').addClass('selected-depth-1');
        });

        $('body').on('click', '[data-dest-city]', function (e) {
            e.preventDefault();
            var $target = $(e.currentTarget),
                cityName = $target.text(),
                cityId = $target.attr('data-dest-city');
            selectCity(cityId, cityName);
        }); //--Hero dd search submit button

        $('body').on('click', '[data-hero-search-submit]', function (e) {
            e.preventDefault();
            $('[data-dd-toggle="choose-city"]').removeClass('_active active');
            $('[data-dd="choose-city"]').removeClass('_active active');
            $(".aspire-nav > .container").removeClass('selected-depth-1');
        }); //--Choose destination click

        $('.__foot-search-input').on('keypress', function (e) {
            if (e.which == 13) {
                var $target = $(e.currentTarget);
                cityName = $target.val();
                selectCity(' ', cityName);
                $(window).scrollTop(0);
            }
        });
        $('.__foot-search-btn').on('click', function (e) {
            e.preventDefault();
            var $target = $('.__foot-search .__foot-search-input').val();
            selectCity(' ', $target);
            $(window).scrollTop(0);
        })


        $('body').on('click', '[data-moveto-hero]', function (e) {
            e.preventDefault();
            $('[data-dd-toggle="hero-choose-city"]').trigger('click');
            $('html, body').animate({
                scrollTop: $('[data-dd-toggle="hero-choose-city"]').offset().top - 50
            }, 250);
        });
    }
    function OnlyOneDestination() {
        if (!$('._only-one')) return false;
        var columns = ['._ap', '._eu', '._na'];
        var $listitem = $('._only-one').find('li');
        var $total = Math.ceil($listitem.length / 3);
        var index = 0;
        var i = 0;
        while (index < $listitem.length) {
            var newItem = $listitem.slice(0 + index, $total + index);
            $('._only-one ' + columns[i]).append(newItem);
            i++;
            index += $total;
        }
    }
    function callPost(url, dataModels, successFunction, failFunction) {
        var showAjaxLoading = true;
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            async: true,
            data: dataModels,
            beforeSend: function () {
                if (showAjaxLoading) {
                    $("body").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
                }
            },
            complete: function () {
                if (showAjaxLoading) {
                    $("#ajaxLoading").remove();
                }
            },
            success: function (response) {                
                if (successFunction) {
                    successFunction(response);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#ajaxLoading").remove();                
            }
        });
    }

    function verifyCC(submitFunction) { //apply new flow - use this one as main function
        if ($('button[type="submit"]').attr("disabled") == undefined) {
            //call ajax
            var url = '/ServiceProvider_VerifyCC';
            function onFail() {
                //console.log("Here, we will active the popup to show CC update");
                $("#modalVerifyCreditCard").modal('show');
                $("#ajaxLoading").remove();
            }
            function onSuccess(res) {
                //console.log(res);
                if (res.IsSuccess) {
                    submitFunction();
                    $("#ajaxLoading").remove();
                } else {                    
                    $.ajax(
                        {
                            async: true,
                            beforeSend: function () {
                                $(".btn-submit-CreditCardNumber-footer").unbind();
                            },
                            complete: function () {
                                //add event, to submit function when update new CC number successfully
                                $('.btn.btn-apr.btn-primary.btn-submit-CreditCardNumber-footer').on('click', function () {
                                    event.preventDefault();
                                    //console.log(submitFunction);
                                    var data = {
                                        run: function () {
                                            submitFunction();
                                        },
                                        isCheckingCC: true
                                    }
                                    submitCreditCardNumber(data);
                                });
                                onFail();                                
                            }
                        });
                    
                }
            }
            callPost(url, { isForm: true }, onSuccess, onFail);
        }
    }
    
    return {
        init: function () {

            //detect IE 10
            var b = document.documentElement;
            var ie10 = /MSIE 10.0/g;
            var string = String(navigator.userAgent);
            if (ie10.test(string)) {
                b.setAttribute('data-useragent', navigator.userAgent);
                b.setAttribute('data-platform', navigator.platform);
            }
            var destination = $('[data-head-dest]').html();
            //end
            //autoCompleteSearch();
            //signinValidate();
            //stickyMenu();
            // addBackToTop();
            OnlyOneDestination();
            NavDestination();
            ShowForgetPasswordModal();
            ShowLogInModal();
            changeLocationIconDisk();
            ChangeScrollBar();
            commonAnimation();
            destinationChange();
            boxDateRange();
            showBoxFilter();
            helpModuleInit();
            LogoutNoRedirect();
            setTimezoneCookie();
            //$('.scrollbar-outer').scrollbar();
            if ($(window).width() <= 1024) {
                $(".nav-search").click(function (event) {
                    $(".nav-search ").addClass('open');
                    $("body").addClass('open-search');
                    event.stopPropagation();
                });
                $("body").on("click", function () {
                    if ($(this).hasClass("open-search")) {
                        $(".nav-search ").removeClass('open');
                    }
                });
                $(".search-text").on("blur", function () {
                    $(".nav-search ").removeClass('open');
                });
            }

            //$('.pure-range-slider input[type=range]').change(function () {$(this).attr('value', $(this).val())});
            //$('.price-range-slider input[type=range]').change(function () {$(this).attr('value', $(this).val())});
            $('[data-hero-city]').html() == "" ? $('[data-dd-toggle="hero-choose-city"]').trigger('click') : $('.hero-body-choose').addClass('_selected'); /*dev_sprint39_JRCW-70.1 TinhTran*/

            $(document).on('change', '.pure-range-slider input[type=range]', function (event) {
                event.preventDefault();
                $(this).attr('value', $(this).val());
            });

            $(document).on('change', '.price-range-slider input[type=range]', function (event) {
                event.preventDefault();
                $(this).attr('value', $(this).val());
            });

            stickyTop();
            $(window).resize(function () {
                stickyTop();
            });
        },
        renderBoxFilter: showBoxFilter,
        calRangeSlide: CalRangeSlide,
        ChangeScrollBar: ChangeScrollBar,
        getPlaceholderSelfFormByValue: getPlaceholderSelfFormByValue,
        getPlaceholderSelfFormByValues: getPlaceholderSelfFormByValues,
        isIE11Check: isIE11Check,
        callPost: callPost,
        verifyCC: verifyCC
    }
}();

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}

function set_cookie(name, value) {
    document.cookie = name + '=' + value + '; Path=/;';
}

var stickyTop = function () {
    if ($(window).width() >= 992) {
        $('[sticky-on*="md"], [sticky-on*="lg"]').each(function () {
            var $this = $(this);
            $this.data('sticky', { 'top': $this.offset().top, 'width': $this.width() + parseFloat($this.css('padding-left')) + parseFloat($this.css('padding-right')) });
        });

        $(window).scroll(function () {
            $('[sticky-on*="md"], [sticky-on*="lg"]').each(function () {
                var $this = $(this);
                var stickyData = $this.data('sticky');
                var restaurantListHeight = $this.parent().parent().outerHeight();
                var restaurantListOffsetTop = $this.parent().parent().offset().top;
                var stickyDataWidth = 0;
                if ($(window).width() >= 981) {
                    stickyDataWidth = stickyData.width;
                } else {
                    stickyDataWidth = 225;
                }


                var stickyTop = parseFloat($this.attr('sticky-top-md'));
                if ($(window).width() >= 1025) {
                    var stickyTop = parseFloat($this.attr('sticky-top-lg'));
                }

                if ($(window).scrollTop() + stickyTop > stickyData.top) {

                    if ($(window).scrollTop() + stickyTop < restaurantListHeight + restaurantListOffsetTop - $this.outerHeight()) {
                        $this.css({
                            'width': stickyDataWidth,
                            'margin-top': 0,
                            'top': stickyTop,
                            //'left': $this.offset().left, /*Fix JRCW-412:*/
                            'position': 'fixed',
                            'z-index': 1,
                            'bottom': ''
                        });
                    } else {
                        $this.parent().css("height", restaurantListHeight);
                        $this.css({
                            'width': '',
                            'margin-top': '',
                            'top': '',
                            'left': '',
                            'z-index': '',
                            'position': 'absolute',
                            'right': '4px',
                            'bottom': 0
                        });
                    }
                } else {
                    $this.css({
                        'width': '',
                        'margin-top': '',
                        'top': '',
                        'left': '',
                        'position': '',
                        'z-index': ''
                    });
                }
            });
        });
    } else {
        $("#main-nav .btn-link ").click(function (e) {
            e.stopPropagation();
        })
    }
}
var LoadFromServer = function () {
    $('.module-help').find('.title').on("click", function () {
        if ($(this).parent().hasClass('expand')) {
            $(this).parent().removeClass('expand');
            localStorage.setItem("liveChatStatus", "");
        } else {
            $(this).parent().addClass('expand');
            localStorage.setItem("liveChatStatus", "expand");
        };
    });
    $('.module-help').find('#myzopim').on("click", function () {
        $('.module-help').fadeOut('fast');
    });
    $('.module-help').on("click touchstart", function () {
        $(this).addClass('clicked');
    });
};
function helpModuleInit() {
    if (localStorage.getItem("liveChatStatus") === "expand") {
        $('.module-help').removeClass('expand');
    }
    LoadFromServer();
}

// code of BE: SIEUANH for add to wishlist
function generateGuid() {
    function _p8(s) {
        var p = (Math.random().toString(16) + "000000000").substr(2, 8);
        return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
    }
    return _p8() + _p8(true) + _p8(true) + _p8();
}

function post(url, jsondata, successCallback) {
    var API_NAMESPACE = '//' + document.location.host + '/api/';
    $.ajax({
        type: "POST",
        dataType: "json",
        data: JSON.stringify(jsondata),
        contentType: "application/json;charset=utf8",
        url: API_NAMESPACE + url,
        success: successCallback
    });
}
function get(url, successCallback) {
    var API_NAMESPACE = '//' + document.location.host + '/api/';
    $.ajax({
        type: "GET",
        url: API_NAMESPACE + url,
        success: successCallback
    });
}
function numberWithCommas(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
var WISHLIST_REMOVE_TYPE = {
    AddToWishList: 1,
    AddToWishListSingle: 2,
    AddToWishListWithoutDisplayLikes: 3
}
function addToWishList(item) {
    var self = item;
    if ($(".info-location >.btn-location >#lblRegion").length > 0) { //Check condition (must be logged in)
        //Adding to wishlist
        if ($(self).hasClass("voted") === false) {
            //*****Update UI first*************//
            //Set Heart Icon to red
            $(self).addClass("voted");
            //Number of Likes
            var likesElementValueString = $(self).parent().find(".lblNbOfWishList").text();
            var currentNbOfLike = parseInt(likesElementValueString.replace(",", "")) + 1;
            if (currentNbOfLike < 2) {
                $(self).parent().find(".lblNbOfWishList").parent().next().text("like");
            }
            else {
                $(self).parent().find(".lblNbOfWishList").parent().next().text("likes");
            }
            $(self).parent().find(".lblNbOfWishList").text(numberWithCommas(currentNbOfLike));
            //Change the text to ADDED TO WISHLIST
            if ($(self).find(".add-to-wishlist").html() !== undefined) {
                $(self).find(".add-to-wishlist").html("ADDED TO WISHLIST");
            } else {
                $(self).find(".added-to-wishlist").html("ADDED TO WISHLIST");
            }
            //*****Send data to Server then*************//
            var model = new Object();
            model.ItemId = $(self).parent().find("input[type=hidden]").val();
            model.UserId = "";
            model.NbOfLikes = "";
            $.post("/api/WishList/AddToWishList", model, function (data) { });
        }
        //Removing from wishlist
        else {
            //Set data to modal
            var randomTargetId = generateGuid();
            $("#modalRemoveWishlistGolbal").find("input#hdfWishlistItemIdToDelete").val($(self).parent().find("input[type=hidden]").val());
            $("#modalRemoveWishlistGolbal").find("input#hdfWishlistItemRowId").val(randomTargetId);
            $("#modalRemoveWishlistGolbal").find("input#hdfWishlistConfirmType").val(WISHLIST_REMOVE_TYPE.AddToWishList);
            $(self).attr("id", randomTargetId);
            $('#modalRemoveWishlistGolbal').modal('show');
        }
    }
    return false;
};
function addToWishListSingle(item) {
    var self = item;
    if ($(".info-location >.btn-location >#lblRegion").length > 0) { //Check condition (must be logged in)
        //Adding to wishlist
        if ($(self).parent(".wrap-add-wishlist").hasClass("voted") === false) {
            //*****Update UI first*************//
            //Set Heart Icon to red
            $(self).parent().addClass("voted");
            //Number of Likes
            var likesElementValueString = $(self).parent().find("#NbOfAddedToWishList").text();
            var currentNbOfLike = parseInt(likesElementValueString.replace(",", "")) + 1;
            $(self).parent().find("#NbOfAddedToWishList").text(numberWithCommas(currentNbOfLike));
            //Change the text to ADDED TO WISHLIST
            $(self).parent().find("#lblAddToWishList").html("Added to Wishlist");
            //*****Send data to Server then*************//
            var model = new Object();
            model.ItemId = $(self).parent().find("#hdfItemId").val();
            model.UserId = "";
            model.NbOfLikes = "";
            $.post("/api/WishList/AddToWishList", model, function (data) { });
        }
        //Removing from wishlist
        else {
            //Set data to modal
            var randomTargetId = generateGuid();
            $("#modalRemoveWishlistGolbal").find("input#hdfWishlistItemIdToDelete").val($(self).parent().find("#hdfItemId").val());
            $("#modalRemoveWishlistGolbal").find("input#hdfWishlistItemRowId").val(randomTargetId);
            $("#modalRemoveWishlistGolbal").find("input#hdfWishlistConfirmType").val(WISHLIST_REMOVE_TYPE.AddToWishListSingle);
            $(self).parent().attr("id", randomTargetId);
            $('#modalRemoveWishlistGolbal').modal('show');
        }
    }
    return false;
};


function addToWishListWithoutDisplayLikes(item) {
    var self = item;
    if ($(".info-location >.btn-location >#lblRegion").length > 0) { //Check condition (must be logged in)
        if ($(self).hasClass("voted") === false) {
            //*****Update UI first*************//
            //Set Heart Icon to red
            $(self).addClass("voted");
            //*****Send data to Server then*************//
            var model = new Object();
            model.ItemId = $(self).find("#hdfItemId").val();
            model.UserId = "";
            model.NbOfLikes = "";
            $.post("/api/WishList/AddToWishList", model, function (data) { });
        }
        //Removing from wishlist
        else {
            //Set data to modal
            var randomTargetId = generateGuid();
            $("#modalRemoveWishlistGolbal").find("input#hdfWishlistItemIdToDelete").val($(self).find("#hdfItemId").val());
            $("#modalRemoveWishlistGolbal").find("input#hdfWishlistItemRowId").val(randomTargetId);
            $("#modalRemoveWishlistGolbal").find("input#hdfWishlistConfirmType").val(WISHLIST_REMOVE_TYPE.AddToWishListWithoutDisplayLikes);
            $(self).attr("id", randomTargetId);
            $('#modalRemoveWishlistGolbal').modal('show');
        }
    }
    return false;
};
$(document).on("click", "#confirm-delete-wishlist-ok", function () {
    var self = this;
    var wishlistRemoveType = $(self).parent().find("input#hdfWishlistConfirmType").val();
    var wishlistRowId = $(self).parent().find("input#hdfWishlistItemRowId").val();
    if (wishlistRemoveType == WISHLIST_REMOVE_TYPE.AddToWishListSingle) {
        //UnSet Heart Icon to red
        $("#" + wishlistRowId).removeClass("voted");
        //Number of Likes
        var likesElementValueString = $("#" + wishlistRowId).find("#NbOfAddedToWishList").text();
        var currentNbOfLike = parseInt(likesElementValueString.replace(",", "")) - 1;
        if (currentNbOfLike < 0) {
            currentNbOfLike = 0;
        }
        $("#" + wishlistRowId).find("#NbOfAddedToWishList").text(numberWithCommas(currentNbOfLike));
        //Change the text to ADD TO WISHLIST
        $("#" + wishlistRowId).find("#lblAddToWishList").html("Add to Wishlist");
    }
    else if (wishlistRemoveType == WISHLIST_REMOVE_TYPE.AddToWishList) {
        //*****Update UI first*************//
        //UnSet Heart Icon to red
        $("#" + wishlistRowId).removeClass("voted");
        //Number of Likes
        var likesElementValueString = $("#" + wishlistRowId).parent().find(".lblNbOfWishList").text();
        var currentNbOfLike = parseInt(likesElementValueString.replace(",", "")) - 1;
        if (currentNbOfLike < 0) {
            currentNbOfLike = 0;
        }
        if (currentNbOfLike < 2) {
            $("#" + wishlistRowId).parent().find(".lblNbOfWishList").parent().next().text("like");
        }
        else {
            $("#" + wishlistRowId).parent().find(".lblNbOfWishList").parent().next().text("likes");
        }
        $("#" + wishlistRowId).parent().find(".lblNbOfWishList").text(numberWithCommas(currentNbOfLike));
        //Change the text to ADD TO WISHLIST
        if ($("#" + wishlistRowId).find(".add-to-wishlist").html() !== undefined) {
            $("#" + wishlistRowId).find(".add-to-wishlist").html("ADD TO WISHLIST");
            $("#" + wishlistRowId).find(".add-to-wishlist").show();
            $("#" + wishlistRowId).find(".added-to-wishlist").hide();
        } else {
            $("#" + wishlistRowId).find(".added-to-wishlist").html("ADD TO WISHLIST");
            $("#" + wishlistRowId).find(".add-to-wishlist").hide();
            $("#" + wishlistRowId).find(".added-to-wishlist").show();
        }
    }
    else if (wishlistRemoveType == WISHLIST_REMOVE_TYPE.AddToWishListWithoutDisplayLikes) {
        //*****Update UI first*************//
        //UnSet Heart Icon to red
        $("#" + wishlistRowId).removeClass("voted");
    }
    //*****Send data to Server then*************//
    var wishlistId = $(self).parent().find("input#hdfWishlistItemIdToDelete").val();
    $.post("/api/WishList/RemoveWishList", { "itemId": wishlistId }, function (rs) { });
    //Close popup
    $("#modalRemoveWishlistGolbal").find(".btn-close-modal").click();
});

//***Set Active menu for page detail ****/
jQuery(function () {
    if ($("input[type=hidden]#top-menu-id").length > 0) {
        var currentTopMenuId = $("input[type=hidden]#top-menu-id").val();
        if (currentTopMenuId !== "") {
            currentTopMenuId = currentTopMenuId.replace("{", "");
            currentTopMenuId = currentTopMenuId.replace("}", "").toLowerCase();
        }
        var listMenuTopId = $("input[type=hidden][data-class=menuTopId]");
        for (var i = 0; i < listMenuTopId.length; i++) {
            if ($(listMenuTopId[i]).val() == currentTopMenuId) {
                $(listMenuTopId[i]).parent().addClass("active");
                break;
            }
        }
    }
});
//end code SA

(function ($) {

    function All() {
        var elementNav = $('.header'),
            elementLink = elementNav.find('a'),
            element = elementNav.find('a').parent();
        SetFixedNav();
        EventForDevice();

        //click button close location (prombt) => close
        elementNav.on('click', '.btn-close-location', function () {
            CloseMenu(elementNav, '.box-location');
            CloseOverlay();
        });

    }
    $.All = All;

    function NavOnDevice() {
        var elementNav = $('.header');

        //MOBILE:   focus in textbox search => show btn-reset
        //          focus out => hide btn-reset
        elementNav.on('focusin keyup', '.txt-search', function () {
            if (CheckDevice() && $(this).val().length >= 5) {
                elementNav.find('.btn-reset').removeClass('hide');
            }
        });
        elementNav.on('focusout', '.txt-search', function () {
            if (CheckDevice()) {
                // elementNav.find('.btn-reset').delay(5000).addClass('hide');
                setTimeout(function () {
                    elementNav.find('.btn-reset').addClass('hide');
                }, 200);
            }
        });

        // ON MOBILE: click button sign in => close all menu
        elementNav.on('click', '.btn-sign-in', function (event) {
            event.preventDefault();
            if (CheckDevice()) {
                CloseAllMenu();
            }
        });

        //click button navigation => show and hide menu
        elementNav.on('click', '.btn-nav', function (event) {
            event.preventDefault();
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                CloseMenu(elementNav, '.aspire-nav');
                CloseMenu(elementNav, '.box-location');
                $(".aspire-nav").children().removeClass(function (index, className) {
                    return (className.match(/selected\-depth\-\d+/g) || []).join(' ');
                });
                $(".aspire-nav li").removeClass("hover");
                $(".aspire-nav .sub-list").removeClass("active").attr('style', '');

                // Liem Le - remove active class on Destination
                //$('[data-dd-toggle="header-choose-city"]').removeClass('_active active');
                //$('[data-dd="header-choose-city"]').removeClass('_active active');
                //$('[data-dd-toggle="hero-choose-city"]').removeClass('_active active');
                //$('[data-dd="hero-choose-city"]').removeClass('_active active'); //Update selected city in header and hero destination dropdowns
            } else {
                $(this).addClass('active');
                OpenMenu(elementNav, '.aspire-nav');
            }
        });

        //click button search => show box-search and press button submit
        elementNav.on('click', '.btn-icon-search', function (event) {
            event.preventDefault();
            if (!$(this).hasClass('btn-close-search')) {
                if ($(this).hasClass('active')) {
                    //auto click submit form search
                    $('.box-search').find('.btn-transparent').trigger('click');
                } else {
                    $(this).addClass('active');
                    OpenMenu(elementNav, '.box-search');
                    $('.box-search').find('.txt-search').val('').trigger('focus');
                    $('.btn-nav').addClass('btn-close-search active');
                }
            }
        });

        //click button close search => hide box-search
        elementNav.on('click', '.btn-close-search', function (event) {
            CloseMenu(elementNav, '.box-search');
            CloseOverlay();
            CloseMenu(elementNav, '.aspire-nav');
            CloseMenu(elementNav, '.box-location');
            $(this).removeClass('btn-close-search active');
            $('.box-search').find('.txt-search').trigger('focusout');
            $('.btn-icon-search').removeClass('active');

        });

        //click button location => hide menu then show box location
        elementNav.on('click', '.btn-location', function (event) {
            event.preventDefault();
            OpenOverlay();
            CloseMenu(elementNav, '.aspire-nav');
            OpenMenu(elementNav, '.box-location');
        });

        //click button-back in box-location => hide box-location then show menu
        elementNav.on('click', '.btn-back', function (event) {
            event.preventDefault();
            CloseMenu(elementNav, '.box-location');
            OpenMenu(elementNav, '.aspire-nav');
        });

        //click button confirm in box-location => hide box-location then show menu
        elementNav.on('click', '.btn-confirm-location', function () {
            CloseMenu(elementNav, '.box-location');
            if (CheckDevice()) {
                OpenMenu(elementNav, '.aspire-nav');
            } else {
                CloseOverlay();
            }
        });

        $('.lt-overlay').on('click touchstart', function (event) {
            if ($('#cuisineSelect+.btn-group-selectbox').hasClass('open')) {
                return false;
            } 
            event.preventDefault();
            CloseOverlay();
            CloseMenu(elementNav, '.aspire-nav');
            CloseMenu(elementNav, '.box-location');
            $('.btn-nav').removeClass('active');
        });
    }
    $.NavOnDevice = NavOnDevice;

    function FindSelectedDepthNav() {
        var classes = $('.aspire-nav > .container').attr('class').split(' ');
        var fxclass = 0;
        for (var i = 0; i < classes.length; i++) {
            var matches = /^selected\-depth\-(.+)/.exec(classes[i]);
            if (matches != null) {
                fxclass = matches[1];
            }
        }
        return fxclass;
    }



    function EventForDevice() {
        var elementNav = $('.header'),
            elementLink = elementNav.find('a'),
            element = elementNav.find('a').parent(),
            clickedOpenBox = elementNav.find('.clicked-open');

        if (CheckDevice()) {
            element.off("mouseenter mouseleave");
            if (elementLink.next().hasClass('sub-list')) {
                var showNavbtn = elementLink.find(".icon-arrow-black, .icon-concierge");

                showNavbtn.each(function () {
                    var self = $(this);

                    if (self.parent().attr('href') == '#') {
                        self.parent().on('click', function (event) {
                            event.preventDefault();
                            var self = $(this);
                            if (self.parent().hasClass('hover')) {
                                self.parent().removeClass('hover');
                                CloseSubMenu(self.next().children('li.hover').children('a'));
                                CloseSubMenu(self);
                            } else {
                                self.parent().addClass('hover');
                                OpenSubMenu(self);
                            }

                            var fxclass = FindSelectedDepthNav();
                            var increase_depth = parseInt(fxclass) + 1;
                            var length_li = self.parent().find('> .sub-list > li').length;
                            if (fxclass) {
                                // height of Back button (44px) + title under Back button (58px) + height of navigation control on top + height of other list items
                                var depth_1 = self.closest('.menu-aspire').find('.sub-list.depth-1.active');
                                var height_list = depth_1.find('> li').first().height() + depth_1.find('> li:nth-child(2)').height() + 160 + (length_li - 2) * depth_1.find('> li').last().height();

                                $(".aspire-nav > .container").removeClass('selected-depth-' + fxclass);
                                $(".aspire-nav > .container").addClass('selected-depth-' + increase_depth);
                                // checking set css on class depth-2 and more
                                $('.sub-list.active').css({ 'overflow': 'unset' });
                                depth_1.css({ 'left': (increase_depth + '00%') });
                                if (depth_1.height() < height_list) {
                                    $('.sub-list.active.depth-' + fxclass).css('height', '');
                                    $('.sub-list.active.depth-' + increase_depth).css({ 'left': '0', 'background-color': '#011627', 'height': height_list });
                                } else {
                                    $('.sub-list.active').css({ 'overflow': '' });
                                    $('.sub-list.active.depth-' + increase_depth).css({ 'left': '0', 'background-color': '#011627', });
                                }
                            } else {
                                self.parents('.aspire-nav > .container').addClass('selected-depth-1');
                            }
                        })
                    } else {
                        self.on('click', function (event) {
                            event.preventDefault();
                            var self = $(this);
                            if (self.parent().parent().hasClass('hover')) {
                                self.parent().parent().removeClass('hover');
                                CloseSubMenu(self.parent().next().children('li.hover').children('a'));
                                CloseSubMenu(self.parent());
                            } else {
                                self.parent().parent().addClass('hover');
                                OpenSubMenu(self.parent());
                            }

                            var fxclass = FindSelectedDepthNav();
                            var increase_depth = parseInt(fxclass) + 1;
                            var length_li = self.parent().parent().find('> .sub-list > li').length;
                            if (fxclass) {
                                // height of Back button (44px) + title under Back button (58px) + height of navigation control on top + height of other list items
                                var depth_1 = self.closest('.menu-aspire').find('.sub-list.depth-1.active');
                                var height_list = depth_1.find('> li').first().height() + depth_1.find('> li:nth-child(2)').height() + 160 + (length_li - 2) * depth_1.find('> li').last().height();

                                $(".aspire-nav > .container").removeClass('selected-depth-' + fxclass);
                                $(".aspire-nav > .container").addClass('selected-depth-' + increase_depth);
                                // checking set css on class depth-2 and more
                                $('.sub-list.active').css({ 'overflow': 'unset' });
                                //depth_1.css({'left': (increase_depth + '00%')});
                                depth_1.css({ 'left': (increase_depth + '00%'), 'overflow-y': 'hidden', 'height': '100vh' });
                                if (depth_1.height() < height_list) {
                                    $('.sub-list.active.depth-' + fxclass).css('height', '');
                                    //$('.sub-list.active.depth-'+increase_depth).css({'left': '0', 'background-color': '#011627', 'height': height_list});
                                    $('.sub-list.active.depth-' + increase_depth).css({ 'left': '0', 'background-color': '#011627', 'height': height_list });
                                } else {
                                    $('.sub-list.active').css({ 'overflow': '' });
                                    //$('.sub-list.active.depth-'+increase_depth).css({'left': '0', 'background-color': '#011627',});
                                    $('.sub-list.active.depth-' + increase_depth).css({ 'left': '0', 'background-color': '#011627', 'height': '100vh', 'overflow-y': 'hidden' });
                                }
                            } else {
                                var depth_1 = self.closest('.menu-aspire').find('.sub-list.depth-1.active');
                                var height_list = depth_1.find('> li').first().height() + depth_1.find('> li:nth-child(2)').height() + 160 + (length_li - 2) * depth_1.find('> li').last().height();
                                self.parents('.aspire-nav > .container').addClass('selected-depth-1');
                                depth_1.css({ 'height': height_list, 'overflow': 'unset' });
                            }
                        });
                    }
                })

                var prevNavbtn = elementNav.find(".prev-nav-link");
                prevNavbtn.on('click', function (event) {
                    event.preventDefault();
                    var self = $(this);
                    var fxclass = FindSelectedDepthNav();
                    var decrease_depth = parseInt(fxclass) - 1;
                    var depth_1 = self.closest('.menu-aspire').find('.sub-list.depth-1.active');

                    if (decrease_depth > 0) {
                        $(".aspire-nav > .container").removeClass('selected-depth-' + fxclass);
                        $(".aspire-nav > .container").addClass('selected-depth-' + decrease_depth);
                    } else {
                        $(".aspire-nav > .container").removeClass('selected-depth-1');
                    }

                    // check height of previous list if large than default height, will hide overflow
                    var length_li = self.closest('.sub-list.active.depth-' + decrease_depth).find('> li').length;
                    var height_list = depth_1.find('> li').first().height() + depth_1.find('> li:nth-child(2)').height() + 160 + (length_li - 2) * depth_1.find('> li').last().height();

                    depth_1.css({ 'left': (decrease_depth + '00%') });
                    $('.sub-list.active').css({ 'overflow': '' });
                    $('.sub-list.depth-' + fxclass).removeAttr('style');
                    self.parent().parent().removeClass('active');
                    self.parent().parent().parent().removeClass('hover');

                    $('.sub-list.active').css({ 'overflow': 'unset' });
                    if (depth_1.height() < height_list) {
                        $('.sub-list.depth-' + decrease_depth).css('height', height_list);
                    } else {
                        $('.sub-list.active').css({ 'overflow': '' });
                    }
                });
            }

        } else {
            const isTablet = checkTablet();
            isTablet ? element.off("mouseenter mouseleave") : elementLink.off("click");
            element.on(isTablet ? 'click' : 'mouseenter', function (e) {
                e.stopPropagation();
                var _this = $(this);
                if ($(_this).hasClass('hover') && $(this).parent().hasClass('menu-aspire')) {
                    $(_this).removeClass('hover');
                    $(_this).children()[1].classList.remove('active');
                    window.location.href = $(_this).children()[0].href
                }
                else if (_this.children().hasClass('sub-list') && !_this.children().hasClass('clicked-open')) {
                    e.preventDefault();
                    OpenSubMenu(_this.children('a'));
                    $(".clicked-open").removeClass('active');

                    //keep menu after action mouseleave => mouseenter
                    if ((_this.parent().hasClass('depth-0'))) {
                        OpenSubMenu(_this.parent().parent().children('a'));
                    }
                }
                else if (_this.parent().hasClass('depth-0') && !_this.parent().hasClass('active')) {
                    keepMenu(_this.parent().parent().children('a'));
                }
            });

            // Open module-help and Close when click on button again.
            elementLink.on('click', function (e) { //vy.vy ticket JRCW-85
                //elementLink.on('click', '.contact-concierge', function (e) {
                //comment for Nav Destination selecting
                // e.stopPropagation(); 
                var _this = $(this);
                if (_this.siblings().hasClass('clicked-open') && !_this.siblings().hasClass('active')) {
                    $(".clicked-open").removeClass('active');
                    OpenSubMenu(_this);
                } else if (_this.siblings().hasClass('clicked-open') && _this.siblings().hasClass('active')) {
                    CloseSubMenu(_this);
                }
            });

            // Close module-help when focusout
            $('html').on('click', function () {
                clickedOpenBox.removeClass('active');
                clickedOpenBox.parent().removeClass('hover');
            });
            clickedOpenBox.on('click', function (e) {
                e.stopPropagation();
                var _this = $(this);
                if (!_this.hasClass('search')) {
                    CloseSubMenu(_this.siblings('a'));
                }
            });

            // close sub-list only when if it's not module-help
            element.on('mouseleave', function () {
                var _this = $(this);
                if (_this.children().hasClass('sub-list') && !_this.children().hasClass('clicked-open')) {
                    CloseSubMenu(_this.children('a'));
                    clickedOpenBox.parent().removeClass('hover');
                }
            });
        }
    }
    $.EventForDevice = EventForDevice;

    function SetFixedNav() {
        if (CheckDevice()) {
            RemoveFixed('.aspire-nav');
            // AddFixed('.top-nav');
            if ($(window).scrollTop() > $('.header').height()) {
                AddFixed('.aspire-nav');
                AddFixed('.wrap-btn-nav');
            } else {
                RemoveFixed('.aspire-nav');
                RemoveFixed('.wrap-btn-nav');
            }
        } else {
            RemoveFixed('.top-nav');
            if ($(window).scrollTop() > 80) {
                AddFixed('.aspire-nav');
            } else {
                RemoveFixed('.aspire-nav');
            }
        }
    }
    $.SetFixedNav = SetFixedNav;

    function moveBox() {
        if (CheckDevice()) {
            $('.header').prepend($('.box-search'));
        } else {
            $('.aspire-nav .container').prepend($('.box-search'));
        }
    }
    $.moveBox = moveBox;

    function AddFixed(element) {
        if (!$(element).hasClass('add-fixed')) {
            $(element).addClass('add-fixed');
        }
    }

    function RemoveFixed(element) {
        if ($(element).hasClass('add-fixed')) {
            $(element).removeClass('add-fixed');
        }
    }
    function keepMenu(element) {

        if (element && !element.next().hasClass('active')) {
            element.parent().addClass('hover');
            element.next().addClass('active');
        }
    }
    function OpenSubMenu(element) {
        if (!element.next().hasClass('active')) {
            element.parent().addClass('hover');
            var offsetLeft = element.next().offset().left;
            var offsetRight = $(window).width() - offsetLeft;
            if (offsetLeft < 270) {
                element.next().addClass('left');
            }
            if (offsetRight < 270) {
                element.next().addClass('right');
            }
            element.next().addClass('active');
        }
        $('.wrapper').css('min-height', 'auto');
    }
    $.OpenSubMenu = OpenSubMenu;
    function CloseSubMenu(element) {
        if (element.next().hasClass('active')) {
            element.parent().removeClass('hover');
            element.next().delay(500).removeClass('active');
        }
    }
    $.CloseSubMenu = CloseSubMenu;
    function OpenMenu(elementNav, element) {
        elementNav.find(element).addClass('active');
        $('body, html').addClass('unscroll');
    }
    function CloseMenu(elementNav, element) {
        if (elementNav.children(element).hasClass('active')) {
            elementNav.children(element).removeClass('active');
            $('body, html').removeClass('unscroll');
        }
    }
    function CloseAllMenu() {
        var elementNav = $('.header');
        CloseMenu(elementNav, '.aspire-nav');
        CloseMenu(elementNav, '.box-location');
        CloseMenu(elementNav, '.box-search');
        CloseOverlay();
        $('.sub-list').parent().removeClass('hover');
        $('.sub-list').removeClass('active');
        $('.btn-nav').removeClass('btn-close-search active');
        $('.btn-icon-search').removeClass('active');


        var fxclass = FindSelectedDepthNav();
        if (fxclass) {
            $('.aspire-nav > .container').removeClass('selected-depth-' + fxclass);
        }
    }
    $.CloseAllMenu = CloseAllMenu;
    function OpenOverlay() {
        if (!$('.lt-overlay').hasClass('active')) {
            $('.lt-overlay').addClass('active');
        }

    }
    function CloseOverlay() {
        if ($('.lt-overlay').hasClass('active')) {
            if ($('#cuisineSelect+.btn-group-selectbox').hasClass('open')) {
                return false;
            } else {
                $('.lt-overlay').removeClass('active');
            }
        }
    }
    function CheckDevice() {
        //if($(window).width() <= 767 && $('.btn-nav').is(':visible')) { //vy.vy ticket JRCW-85
        if ((($(window).width() <= 776) || (window.matchMedia("(orientation: portrait)").matches)) && $('.btn-nav').is(':visible')) {
            return true;
        } else {
            return false;
        }
    }
    function checkTablet() {
        var userAgent = navigator.userAgent.toLowerCase();
        return /(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(userAgent);
    }
    $.CheckDevice = CheckDevice;
}(jQuery))

function NAV() {
    jQuery.moveBox();
    jQuery.All();
    jQuery.NavOnDevice();
}
NAV();
$(window).scroll(function (event) {
    jQuery.SetFixedNav();
});

//FIX BUG in android 4.2 - 4.4: input keyboard show => .resize() run
var winWidth = $(window).innerWidth(), winHeight = $(window).innerHeight();
$(window).resize(function () {
    if ($(window).innerWidth() !== winWidth && $(window).innerHeight() !== winHeight) {
        jQuery.SetFixedNav();
        jQuery.moveBox();
        jQuery.CloseAllMenu();
        //jQuery.EventForDevice();
        winWidth = $(window).innerWidth();
        winHeight = $(window).innerHeight();
        if ($('#ui-datepicker-div').length > 0) {
            $('.datepickerOfBirth').datepicker("hide");
            $('.dateOfBirth').datepicker("hide");
            $('.reservationDate').datepicker("hide");
        }
    }
});

$("document").ready(function () {
    var url = window.location;
    // get url 
    var urlName = url.pathname.toLowerCase();
    var urlArray = urlName.split('/');
    if (urlArray[2] == '') {
        urlName = "/" + urlArray[1] + "/home";
    }
    //check menu same current addresss
    var element = $('.aspire-nav a').filter(function () {
        //get href link menu
        var href = this.getAttribute("data-slug");
        return (href != null && href.toLowerCase() === urlName);
    });
    var menuLength = element.length;
    if (menuLength > 0) {
        for (var menuIndex = 0; menuIndex < menuLength; menuIndex++) {
            var e = $(element[menuIndex]);
            if (e.hasClass("language-toggle")) {
                continue;
            }
            if (e.parents(".sub-list").length !== 0) {
                e.parents(".sub-list").each(function () {
                    $(this).parent("li").addClass("active");
                });
            }
            e.parent("li").addClass("active");
        }
    }

    $(".modal").each(function () {
        $(this).on('shown.bs.modal', function () {
            $('body').addClass('modal-open');
            $('html').addClass('unscroll');
        });

        $(this).on('hidden.bs.modal', function () {
            $('html').removeClass('unscroll');
        });
    });

    $('li.group-item.has-child i').click(function () {
        $(this).closest('.has-child').find('> .group-content .nav-menu').toggleClass('open');
    });

});
