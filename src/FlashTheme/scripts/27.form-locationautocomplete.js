﻿$(document).ready(function () {
    AutocompleteDiningLocation();
    DCRFormAutoComplete();
    AutoCompeleteLimoService();
});

function AutoCompeleteLimoService() {
    var listLimoService = [
{
    "value": "EmpireCLS Worldwide Chauffeured Services",
    "data": "EmpireCLS Worldwide Chauffeured Services"
}
    ]

    $("[data-spy='autoCompleteLimoService']").each(function () {
        var self = $(this);
        self.autocomplete({
            lookup: listLimoService,
            autoSelectFirst: true,
            minChars: 3,
            appendTo: self.parent(),
            autoSelectFirst: true,
            showNoSuggestionNotice: true,
            onSelect: function (suggestion) {
                var value = suggestion.value;
                if (self.hasClass("nameOfRestaurant")) {
                    $(".infoRestaurant").text(value);
                    $(".infoRestaurant").closest("p").show();
                } else if (self.hasClass("nameOfLocation")) {
                    $(".infoAddressRestaurant").text(value);
                    $(".infoAddressRestaurant").closest("p").show();
                } else if (self.hasClass("getPickupLocation")) {
                    $(".infoPickupLocation").text(value);
                    $(".infoPickupLocation").closest("p").show();
                } else if (self.hasClass("getDropoffLocation")) {
                    $(".infoDropoffLocation").text(value);
                    $(".infoDropoffLocation").closest("p").show();
                }
            },
            onSearchComplete: function (query, suggestions) {
                $(this).siblings(".autocomplete-suggestions").addClass('scrollbar-outer');
                $(this).siblings(".autocomplete-suggestions").scrollbar();
            }
        });
    });
}
function DCRFormAutoComplete() {
    //disable autocomplete if datasource is AK
    var dataSourceEl = $('#mastersource');
    if (dataSourceEl.length > 0 && dataSourceEl.val() !== 'ak') {
        //console.log("DataSource is AK, disable autocomplete");
        $('.locationForm').each(function (i, el) {
            var auto = $(el);
            var apiItemType;
            if (auto.hasClass("kb_dining") || auto.hasClass("ak_dining")) {
                apiItemType = "dining";
            }
            else if (auto.hasClass("kb_event")) {
                apiItemType = "event";
            }
            else if (auto.hasClass("kb_sight") || auto.hasClass("ak_sight")) {
                apiItemType = "sightseeing_activity";
            }
            else if (auto.hasClass("kb_accommodation") || auto.hasClass("ak_accommodation")) {
                apiItemType = "accommodation";
            }
            auto.autocomplete({
                serviceUrl: "/sm/DCRApi/LocationAutoComplete",
                params: { limit: 10, apiItemType: apiItemType },
                minChars: 3,
                appendTo: auto.parent(),
                autoSelectFirst: true,
                showNoSuggestionNotice: true,
                onSelect: function (suggestion) {
                    UpdateLocationData(suggestion.data);
                },
                onSearchComplete: function (query, suggestions) {
                    $(this).siblings(".autocomplete-suggestions").addClass('scrollbar-outer');
                    $(this).siblings(".autocomplete-suggestions").scrollbar();
                }
            });
        });
    }
    //auto compelete for dining dcr
}
function UpdateLocationData(updateValue) {
    //add to fix JRCW-373
    var splitCheckData = updateValue.split('| ');
    var summaryLocationOutPut = '';
    for (let i = 0; i < splitCheckData.length; i++) {
        summaryLocationOutPut += splitCheckData[i];
        if ((i < splitCheckData.length - 1) && (splitCheckData[i])) {
            summaryLocationOutPut += " | ";
        }
    }
    $('input[name=Location]').attr('data', summaryLocationOutPut);
    $(".infoAddressRestaurant").text(summaryLocationOutPut);
    $(".infoAddressRestaurant").closest("p").show();
}
function AutocompleteDiningLocation() {
    //disable autocomplete if datasource is AK
    var dataSourceEl = $('#mastersource');
    if (dataSourceEl.length > 0 && dataSourceEl.val() !== 'ak') {
        $("[data-spy='autoCompleteLocation']").each(function () {
            var self = $(this);

            dataUrl = "/DCRApi_AutoCompleteLocation";

            self.autocomplete({
                serviceUrl: dataUrl,
                minChars: 3,
                appendTo: self.parent(),
                autoSelectFirst: true,
                showNoSuggestionNotice: true,
                onSelect: function (suggestion) {
                    var value = suggestion.value;
                    if (self.hasClass("nameOfRestaurant")) {
                        $(".infoRestaurant").text(value);
                        $(".infoRestaurant").closest("p").show();
                    } else if (self.hasClass("nameOfLocation")) {
                        $(".infoAddressRestaurant").text(value);
                        $(".infoAddressRestaurant").closest("p").show();
                    } else if (self.hasClass("getPickupLocation")) {
                        $(".infoPickupLocation").text(value);
                        $(".infoPickupLocation").closest("p").show();
                    } else if (self.hasClass("getDropoffLocation")) {
                        $(".infoDropoffLocation").text(value);
                        $(".infoDropoffLocation").closest("p").show();
                    }
                },
                onSearchComplete: function (query, suggestions) {
                    $(this).siblings(".autocomplete-suggestions").addClass('scrollbar-outer');
                    $(this).siblings(".autocomplete-suggestions").scrollbar();
                }
            });
        });
    }
}