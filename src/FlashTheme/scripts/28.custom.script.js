﻿$("document").ready(function () {
    $(".chatbtn").click(function () {
        openLiveChat();
    });
});

var limitedForCheckIfItIsPasswordForgotStatus = 360;
function checkIfItIsPasswordForgotStatus() {
    if ($("#modal-forget-password .field-validation-error").text().length > 0) {
        $("#modal-forget-password").modal('show');
    } else {
        limitedForCheckIfItIsPasswordForgotStatus -= 10;
        if (limitedForCheckIfItIsPasswordForgotStatus < 0) {

        } else {
            setTimeout(function () { checkIfItIsPasswordForgotStatus() }, 500);
        }

    }
}
checkIfItIsPasswordForgotStatus();


function autoScrollToSelectedItem(selectIds) {
    initYelpCuisineFilter();
    // #299: auto scroll to selected item before - VyVo
    if (typeof selectIds != 'undefined' && selectIds != '') {
        var dropdowIdArr = selectIds.split(',');
        dropdowIdArr.forEach(function (selId) {
            var parentElm = $(selId).parent();
            var listUnstyled = parentElm.find('.wrap-multiselect .list-options');
            var container = listUnstyled.parent(); // #myDiv
            var scrollTo = container.find('li.active:first');
            var firstItem = listUnstyled.find('li:first');
            if (typeof scrollTo != 'undefined' && typeof firstItem != 'undefined' && scrollTo.length > 0 && firstItem.length > 0) {
                var scroll = $(parentElm).find('.scrollbar-outer');
                scroll.scrollTop(scrollTo[0].offsetTop - listUnstyled.find('li:first')[0].offsetTop);
            }

            //bind event for button click
            var btnShow = parentElm.find('div.btn-group.btn-group-selectbox button');
            if (typeof btnShow != 'undefined' && btnShow.length > 0) {
                var btnDropdown = btnShow[0];
                $(btnDropdown).off('click').on('click', function () {
                    autoScrollToSelectedItem(selId);
                });
            }
        });
    }

   
    // end #299

}

var Custom = function () {
    //JRCW-353 - custom function for running custom script after jquery is ready
    function onJqueryReady(customFunction) {        
        if (window.jQuery != undefined && typeof ($.fn.modal) == 'function' && $('#modal-signin').hasClass('in')) {            
            customFunction();
        } else {
            var checkContent = document.querySelector('.user-name .full-name');            
            var check = checkContent ? checkContent.textContent.length > 0 : false;
            if (!check) {
                setTimeout(function () { onJqueryReady(customFunction); }, 200);
            }
        }        
    }
    return {
        onJqueryReady: onJqueryReady
    }
}();
if (typeof (onReady) != "undefined") {
    Custom.onJqueryReady(onReady);
}

//fix scrollbar Form MyPreference -JRCW-334
var hiddenScrollbar = function (formId) {
    var form = $(formId);
    if (form && form.length > 0) {
        var scrollContent = $('.show-option');
        if (scrollContent && scrollContent.length > 0) {
            scrollContent.css('scrollbar-width', 'none'); /*Firefox 64*/
            scrollContent.css('-ms-overflow-style', 'none'); /*IE 11*/
        }
    }
}

function showLoading() {
    $("#ajaxLoading").remove();
    $("body").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");

}
function hideLoading() {
    $("#ajaxLoading").remove();
}

function initYelpCuisineFilter() {

    // JRCW-362 - start
    $('#cuisineSelect').next().on({
        "shown.bs.dropdown": function () {
            this.closable = false;
            //autoScrollToFirstSelectedItem("#cuiSelect");
        },
        "click": function () {
            this.closable = false;
        },
        "hide.bs.dropdown": function () {
            return this.closable;
        }

    });

    $('.wrap-multiselect .select-button-cancel').on('click', function (e) {
        $('#cuisineSelect').next().removeClass('open');
        $('#cuisineSelect + .btn-group-selectbox').css({ 'z-index': 'unset', 'background-color': 'unset' });

        var selected = [];
        $('#cuisineSelect option').each(function () {
            if ($(this).length > 0 && $(this)[0].selected && !$(this).attr('done') ) {
                selected.push($(this).val());
                $(this).removeAttr('done');
                $(this)[0].selected = false;
            }
            //re-select item mark done
            if ($(this).attr('done') && !$(this)[0].selected) {
                $(this)[0].selected = true;
            }
        });

        $('#cuisineSelect').next().find('.wrap-multiselect ul>li').each(function () {
            var self = $(this);
            var value = $(this).find('input[type=checkbox]').val();

            var index = $.inArray(value, selected);
            if (index < 0) {
                if (!$(this).attr('done')) {
                    self.removeClass('active');
                    $(this).removeAttr('done');
                    $(this).selected = false;
                }
                if ($(this).attr('done') && !$(this).selected) {
                    $(this).selected = true;
                }
            } 
        });
        $('.lt-overlay').removeClass('active');
        $('#cuisineSelect').multiselect('refresh');
    });

    $('.wrap-multiselect .select-button-done').on('click', function (e) {
        $('#cuisineSelect').next().removeClass('open');
        $('#cuisineSelect + .btn-group-selectbox').css({ 'z-index': 'unset', 'background-color': 'unset' });

        var selected = [];
        var unSelected = [];
        $('#cuisineSelect').next().find('.wrap-multiselect ul>li').each(function () {
            if ($(this).hasClass('active')) {
                var value = $(this).find('input[type=checkbox]').val();
                selected.push(value);
                $(this).attr('done', true);
            }
            //push item uncheck to remove
            if (!$(this).hasClass('active') && $(this).attr('done')) {
                var value = $(this).find('input[type=checkbox]').val();
                unSelected.push(value);
                $(this).removeAttr('done');
            }
        });

        $('#cuisineSelect option:selected').each(function () {
            var self = $(this);
            $(this).attr('done', true);
            console.log();
        });

        //remove item uncheck
        $('#cuisineSelect option').each(function () {
            var optValue = $(this).val();
            var index = $.inArray(optValue, unSelected);
            if (index >= 0) {
                $(this).removeAttr('done');
                $(this)[0].selected = false;
            }
        });
        $('.lt-overlay').removeClass('active');
    });

    // JRCW-362 - end
}

function preventCloseOverlayYelpLandingFilter() {
    if ($('#cuisineSelect+.btn-group-selectbox').hasClass('open')) {
        return false;
    } 
}

//prevent press esc to close modalVerifyCreditCard
//For Chrome, FF and IE11
window.onload = function () {
    $('#footerTxtCreditCardNumber').bind('change keyup keydown input', function (e) {
        preventBrowserDefaultBehaviorOnPressEscape(e);
    });
}

//helper function
function preventBrowserDefaultBehaviorOnPressEscape(event) {
    var pressKey = event.key;
    //console.log(pressKey);
    if (pressKey == "Escape" || pressKey == undefined || pressKey == "Esc") {
        //console.log('go to escape');
        event.preventDefault();
        event.stopImmediatePropagation();
    }
}



$(document).ready(function () {
    if ($('input[id="PrefResponseViaEmail"]').length > 0) {
        $('label[for="cbxEmail"]').attr("for", "PrefResponseViaEmail"); //temporary fix // JRCW-455
    }
    //$('#modalVerifyCreditCard').modal('hide');
    function forCustomCss() {
        //start
        function onJqueryReady() {
            if (window.jQuery != undefined && typeof ($.fn.modal) == 'function' && $('#modalVerifyCreditCard').hasClass('in')) {                
                if ($('input[id="PrefResponseViaEmail"]').length > 0) {
                    $('label[for="cbxEmail"]').attr("for", "PrefResponseViaEmail"); //temporary fix // JRCW-455
                }
            } else {
                setTimeout(function () { onJqueryReady(); }, 200);
            }
        }
        onJqueryReady();
        //end
    }
});