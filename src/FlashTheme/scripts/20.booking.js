
var Booking = function () {
    
    function spinner() {

        $(document).on('click touchstart', '.lt-spinner-prev', function(event) {
            event.preventDefault();
            var x = $(this).parent().find('.lt-spinner').val();
            if (x>0) {
                x=parseInt(x) - 1;
            };
            if(!isNaN(x)){
                $(this).parent().find('.lt-spinner').val(x);
            }
            var $this = $(this).siblings(".lt-spinner").attr("id");
            var valid = validator.element("#"+$this);
        });
        $(document).on('click touchstart', '.lt-spinner-next', function(event) {
            event.preventDefault();
            var x = $(this).parent().find('.lt-spinner').val();
            x = parseInt(x) + 1;
            if(!isNaN(x)){
                $(this).parent().find('.lt-spinner').val(x);
            }
            var $this = $(this).siblings(".lt-spinner").attr("id");
            var valid = validator.element("#"+$this);
        });
    }
    function autoResizeTextArea() {
        $('textarea').keyup(function (e) {
            console.log("press");
            var rows = $(this).val().split("\n");
            $(this).prop('rows', rows.length);
        });
    }
    function hidePhoto() {
        $('.box-add-photo').on('click touchstart', '.figure', function() {
            $(this).hide();
			$("#btnAttachPhoto").val('');
        });
    }	
	
    function validateBookingForm() {
        // create your custom rule
        jQuery.validator.addMethod("trueEMail", function(value, element) {
            return this.optional( element ) || ( /^[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,4}$/i.test( value ) && /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test( value ) );
        }, 'Please enter valid email address.');
		
		jQuery.validator.addMethod("one_required", function(){
			return $("#mainForm").find(".one_required:checked").length > 0;
		}, 'Please select at least one vehicle.');
		
		var validation = {
				rules:{},
				errorClass: "error has-error-icon",
                errorPlacement: function(error, element) {					
                     if ($(element).hasClass("one_required")) {							
							$("#contact-via-error").show();
                    } else {
                        error.insertAfter(element);
                    }
                }
			}
		
		validation.rules[selectorNames.mobileNumber] = {required: true, number: true};
		validation.rules[selectorNames.numberOfKids] = {required: true, number: true, min: 1};
		validation.rules[selectorNames.numberOfAdults] = {required: true, number: true, min: 1};
		validation.rules[selectorNames.emailAdress] = {email: true, trueEMail: true};
		
		validator = $("#mainForm").validate(validation);
		
        $("#mainForm input[type=text], #mainForm select").on('input focus change',function(e){
            if($.trim($(this).val()) == ''){
                $(this).val('');
            }
            var $this = $(this).attr("id");
            var valid = validator.element("#"+$this);
        });

        $("#mainForm input[type=checkbox]").on('change',function(e){
            var $this = $(this).attr("id");
            var errorId = $(this).data("error");
            var valid = validator.element("#"+$this);
            if(valid == true){
                $("#"+errorId).hide();
            }
        });
    }

    function bookEntertainment(){
        if($(".btn-toggle").length > 0){
            $(".btn-toggle").on("click", function(){
                $(this).closest(".item-group").find(".toggle-box").slideToggle('fast');
            });
        }

        // Handle header style for new style seating map API in entertainment-indiv
        if($(".page-book").length > 0){
            $(".page-book").siblings(".header").css({
                "background": "none",
                "height":"auto", 
                "text-align":"left", 
                "color":"inherit", 
                "font-weight":"normal", 
                "font-size":"inherit"
            });
        }
    }

    function printExpiryValue (){
        var month = $('.selectExpiryMonth select').find("option:selected").val();
        var year = $('.selectExpiryYear select').find("option:selected").val();
        $('.txtExpiryDate').val(month +"/"+ year);
        $('.box-expiry-date input.txtExpiryDate').removeClass('error lt-error');
        $('.box-expiry-date label.lt-error').css('display','none');
        $('.selectExpiryMonth .multiselect-selected-text').css("color","#555");
        $('.selectExpiryYear .multiselect-selected-text').css("color","#555");
    }
    function getValueCardExpiryDate(formClass, idOfExpiredDate){
        $('.selectExpiryMonth select').on('change', function(){
            monthVal = $(this).val();
            printExpiryValue();
            $("."+formClass).validate().element("#"+idOfExpiredDate);
        });
        $('.selectExpiryYear select').on('change', function(){
            yearVal = $(this).val();
            printExpiryValue();
            $("."+formClass).validate().element("#"+idOfExpiredDate);
        });

        //show month and year picker
        $(".txtExpiryDate").on("click", function(event){
            event.stopPropagation();
            $(".cardExpiryPicker").addClass('active');
        });
        $(window).click(function(){
            $(".cardExpiryPicker").removeClass('active');
        });
    }
    return {
        init: function () {
            App.init();
            bookEntertainment();
        },
        getValueCardExpiryDate: function (formClass, idOfExpiredDate) {
            getValueCardExpiryDate(formClass, idOfExpiredDate);
        }
    }
}();

jQuery(document).ready(function () {
	Booking.init();
});
