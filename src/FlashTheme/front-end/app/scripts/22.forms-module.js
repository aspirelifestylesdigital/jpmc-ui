//Demo data contry code
+function ($) {
    $('.auto-complete-select').each(function (index, el) {
        var $this = $(this);
        var data = $(this).data();
        if (data.autocomplete) {
            $this.select2({
                dropdownParent: $this.parent(),
                placeholder: "Please select",
                ajax: {
                    url: data.url,
                    headers: {
                        'Content-Type': 'application/json',
                        'Ocp-Apim-Subscription-Key': 'd66e6778e8264f4698f7b6edfd1ccf90'
                    },
                    dataType: 'json',
                    delay: 500,
                    data: function (params) {
                        return {
                            language: 'en',
                            term: params.term,
                            item_type: data.type,
                            limit: '100'
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        return {
                            results: data.data,
                        };
                    },
                    cache: true
                },
                data: dataCountryCode,
                escapeMarkup: function (markup) {
                    return markup;
                },
                minimumInputLength: 3,
                language: {
                    inputTooShort: function (args) {
                        return "Please enter at least 3 characters";
                    }
                },
                templateResult: formatResult,
                templateSelection: formatSelection,
            });
        } else if (!data.autocomplete) {
            $this.select2({
                data: data.url,
            });
        } else {
            console.log('Please enter true format select');
        }
    });

    function formatResult(data, container) {
        return data.disabled ? data.text : data.name;
    }

    function formatSelection(data, container) {
        return data.name;
    }

    $('.country-code').select2({
        data: dataCountryCode,
        minimumResultsForSearch: -1,
        dropdownParent: $(".phone-field"),
        templateSelection: function (data, container) {
            if (data.element.value != '') {
                return (data.code + ' ' + data.id);
            } else {
                return data.name;
            }
        },
        templateResult: function (data, container) {
            return (data.name + ' ' + data.id);
        }
    });

    var prefillPhoneCountryCode = $('#PrefillPhoneCountryCode').val();
    if (typeof (prefillPhoneCountryCode) !== 'undefined' && prefillPhoneCountryCode != null
        && prefillPhoneCountryCode.length > 0) {
        var ddlCountryCode = $('.country-code');
        if (typeof (ddlCountryCode) !== 'undefined' && ddlCountryCode != null) {
            ddlCountryCode.val(prefillPhoneCountryCode);
            ddlCountryCode.change();
        }
    }
}(jQuery);

jQuery(document).ready(function () { 
    var pathName = window.location.pathname;
    var txtEmailName = pathName.indexOf('other') > -1 || pathName.indexOf('dining') > -1 || pathName.indexOf('golf') > -1 ? 'EmailAddress' : 'Email';
    $('.fr-booking').validation({
        txtContactNumber: {
            name: 'MobileNumber'
        },
        txtEmail: {
            name: txtEmailName//'EmailAddress'
        }
    });

    //JRCW-55 start
     //if (pathName.indexOf('other-form') > -1) {
        var locationUrl = window.location.href;
        var categoryName = getParameterByName("name", locationUrl);
        var location = getParameterByName("location-prefill", locationUrl);
        var category = getParameterByName("type-prefill", locationUrl);   
        var topicDescription = getParameterByName("topic-description", locationUrl);
        //$("button[type='submit'].btn-red").first().on('click', function () {
        //    if (topicDescription) {
        //        //var previousData = $('textarea[name=SpecialRequirements]').val();
        //        //var newData = previousData + topicDescription
        //        //$('textarea[name=SpecialRequirements_Hidden]').val(topicDescription);
        //    }
        //});
        //JRCW-343
        function transferOfferTopicDescriptionToHiddenField() {
            if (topicDescription) {
                var specialRequirementsField = $('textarea[name=SpecialRequirements]').closest('.form-group').html();
                var specialRequirementsHidden = specialRequirementsField.replace("SpecialRequirements", "SpecialRequirements_Hidden");
                $('textarea[name=SpecialRequirements]').closest('.form-group').append("<div class='hiddenSpecialRequirements' style='display:none'>" + specialRequirementsHidden + "</div>");
                $('.hiddenSpecialRequirements').find('textarea[name=SpecialRequirements]').attr('name', "SpecialRequirements_Hidden");
                $('textarea[name=SpecialRequirements_Hidden]').val(topicDescription);
            }
        }
        transferOfferTopicDescriptionToHiddenField();    

        //console.log(topicDescription);
        

        if (category === 'Dining') {
            categoryName = getParameterByName("restaurantName", locationUrl);
        }        
        //var otherRequestForm = $('.other-request-form-module').find('textarea[name=SpecialRequirements]');
        //console.log(otherRequestForm);
        //var textEl = $('textarea[name=SpecialRequirements]');
        //other-request-form-module
        var textEl = $('.other-request-form-module').find('textarea[name=SpecialRequirements]');
        if (textEl.length > 0) {
            var valueFill = '';
            if (category != null) {
                valueFill = category;
            }
            if (categoryName != null) {
                valueFill += category != '' ? ' - ' + categoryName : categoryName;
            }
            if (location != null) {
                valueFill += valueFill != '' ? '\n' + location : location;
            }            
            textEl.val(valueFill);
        }
    //}
    //JRCW-55 end

    //Handel for dining form, yelp detail to dining form
    var reservationDate = getParameterByName("reservation-date", locationUrl);
    var reservationTime = getParameterByName("reservation-time", locationUrl);
    var reservationTime12Format = getParameterByName("reservation-time-12", locationUrl) == null ? "" : getParameterByName("reservation-time-12", locationUrl).toUpperCase();
    var amOrPm = reservationTime12Format.substr(reservationTime12Format.length - 2, 2);
    //console.log(amOrPm);
    var guestNo = getParameterByName("guest-no", locationUrl);
    //var textOther = $('#diningForm').find('textarea[name=SpecialRequirements]');
    //textOther.val(reservationDate + ' ' + reservationTime + ' ' + guestNo);
    //console.log(reservationTime);
    updateReservationDate(reservationDate);
    updateReservationTime(amOrPm, reservationTime, reservationTime12Format);
    updateNumberOfAdult(guestNo);
    
    // Handle transfer service fields
    // on Load

    // Event on Transfer Provider
    if ($('.getTransportType').val() == undefined || $('.getTransportType').val() == "Limo") {
        $('.boxTransferProvider').show();
    } else {
        $('.boxTransferProvider').hide();
    }

    // Event on Preferred Vehicle
    if ($('.getTransportType').val() == undefined || $('.getTransportType').val() != "Bus" || $('.getTransportType').val() != "Train") {
        $('.boxPreferredVehicle').show();
    } else {
        $('.boxPreferredVehicle').hide();
    }

    // Event on pickup location
    if ($('.getTransferService').val() == "Arrival") {
        $('.pickupMethod1').show();
        $('.pickupMethod2, .pickupMethod3').hide();
    }
    else if ($('.getTransferService').val() != "Arrival" && $('.getTransferProvider').val() == "limo.com") {
        $('.pickupMethod2').show();
        $('.pickupMethod1, .pickupMethod3').hide();
    } else {
        $('.pickupMethod3').show();
        $('.pickupMethod1, .pickupMethod2').hide();
    }

    // Event of dropoff location
    if ($('.getTransferService').val() == "Hourly") {
        $('.dropoffMethod1').show();
        $('.dropoffMethod2, .dropoffMethod3, .dropoffMethod4').hide();
    }
    else if ($('.getTransferService').val() == "Departure") {
        $('.dropoffMethod2').show();
        $('.dropoffMethod1, .dropoffMethod3, .dropoffMethod4').hide();
    } else if ($('.getTransferService').val() == "Arrival" || $('.getTransferService').val() == "Transfer" && $('.getTransferProvider').val() == "limo.com") {
        $('.dropoffMethod3').show();
        $('.dropoffMethod1, .dropoffMethod2, .dropoffMethod4').hide();
    } else {
        $('.dropoffMethod4').show();
        $('.dropoffMethod1, .dropoffMethod2, .dropoffMethod3').hide();
    }

    // on Change
    $(document).on('change', '.getTransportType', function () {
        // Event on Transfer Service
        if ($(this).val() == "Limo") {
            $('.boxTransferProvider').show();
        } else {
            $('.boxTransferProvider').hide();
            var options = [
              { label: 'Please select transfer service', title: 'Please select', value: '' },
              { label: 'Arrival', title: 'Arrival', value: 'Arrival' },
              { label: 'Departure', title: 'Departure', value: 'Departure' },
              { label: 'Transfer', title: 'Transfer', value: 'Transfer' },
              { label: 'Hourly', title: 'Hourly', value: 'Hourly' }
            ];
            $('.getTransferService').multiselect('dataprovider', options);
            $('.getTransferService').multiselect('rebuild');
        }

        // Event on Preferred Vehicle
        if ($(this).val() == "Bus" || $(this).val() == "Train") {
            $('.boxPreferredVehicle').hide();
        } else {
            $('.boxPreferredVehicle').show();
        }
    });
    $(document).on('change', '.getTransferProvider', function () {
        if ($(this).val() == "limo.com") {
            // Event on Transfer Service
            var options = [
              { label: 'Please select transfer service', title: 'Please select', value: '' },
              { label: 'From Airport', title: 'From Airport', value: 'Arrival' },
              { label: 'To Airport', title: 'To Airport', value: 'Departure' },
              { label: 'Point-to-point (one-way)', title: 'Point-to-point (one-way)', value: 'Transfer' },
              { label: 'Just drive (hourly)', title: 'Just drive (hourly)', value: 'Hourly' }
            ];
            $('.getTransferService').multiselect('dataprovider', options);
            $('.getTransferService').multiselect('rebuild');

            // Event on Preferred Vehicle
            $('.boxPreferredVehicle').hide();
        } else {
            // Event on Transfer Service
            var options = [
              { label: 'Please select transfer service', title: 'Please select', value: '' },
              { label: 'Arrival', title: 'Arrival', value: 'Arrival' },
              { label: 'Departure', title: 'Departure', value: 'Departure' },
              { label: 'Transfer', title: 'Transfer', value: 'Transfer' },
              { label: 'Hourly', title: 'Hourly', value: 'Hourly' }
            ];
            $('.getTransferService').multiselect('dataprovider', options);
            $('.getTransferService').multiselect('rebuild');

            // Event on Preferred Vehicle
            $('.boxPreferredVehicle').show();
        }
    });
    $(document).on('change', '.getTransferService', function () {
        $('.boxLocation').find('input.error, select.error').removeClass('error has-error-icon');
        $('.boxLocation').find('label.error').hide();

        // Event on pickup location
        if ($(this).val() == "Arrival") {
            $('.pickupMethod1').show();
            $('.pickupMethod2, .pickupMethod3').hide();
        }
        else if ($(this).val() != "Arrival" && $('.getTransferProvider').val() == "limo.com") {
            $('.pickupMethod2').show();
            $('.pickupMethod1, .pickupMethod3').hide();
        } else {
            $('.pickupMethod3').show();
            $('.pickupMethod1, .pickupMethod2').hide();
        }

        // Event of dropoff location
        if ($(this).val() == "Hourly") {
            $('.dropoffMethod1').show();
            $('.dropoffMethod2, .dropoffMethod3, .dropoffMethod4').hide();
        }
        else if ($(this).val() == "Departure") {
            $('.dropoffMethod2').show();
            $('.dropoffMethod1, .dropoffMethod3, .dropoffMethod4').hide();
        } else if ($(this).val() == "Arrival" || $(this).val() == "Transfer" && $('.getTransferProvider').val() == "limo.com") {
            $('.dropoffMethod3').show();
            $('.dropoffMethod1, .dropoffMethod2, .dropoffMethod4').hide();
        } else {
            $('.dropoffMethod4').show();
            $('.dropoffMethod1, .dropoffMethod2, .dropoffMethod3').hide();
        }
    });    
});

// Data prefill Dining Form
$(function () {
    var defaultLocationVal = $("input[name='Location']").val();
    var defaultRestaurantVal = $("input[name='RestaurantName']").val();
    var defaultPreferredAircraftVal = $("input[name='PreferredAircraft']").val();
    var defaultCruiseLineVal = $("input[name='PreferredCruiseLine']").val();
    var defaultPreferredCarVal = $("input[name='PreferredCarCompany']").val();
    var defaultAdults = $("input[name='NumberOfAdults']").val();
    var isInDiningForm = $('#diningForm').length > 0;

    if (typeof defaultPreferredCarVal != 'undefined' && defaultPreferredCarVal.length > 0) {
        $("input[name='PreferredCarCompany']").prop("readonly", "true");
    }

    if (typeof defaultLocationVal != 'undefined' && defaultLocationVal.length > 0) {
        $("input[name='Location']").prop("readonly", "true");
    }

    if (typeof defaultRestaurantVal != 'undefined' && defaultRestaurantVal.length > 0) {
        $("input[name='RestaurantName']").prop("readonly", "true");
        $("input[name='RecommendationFlag']").prop('checked', false);
    }

    if (typeof defaultPreferredAircraftVal != 'undefined' && defaultPreferredAircraftVal.length > 0) {
        $("input[name='PreferredAircraft']").prop("readonly", "true");
    }

    if (typeof defaultCruiseLineVal != 'undefined' && defaultCruiseLineVal.length > 0) {
        $("input[name='PreferredCruiseLine']").prop("readonly", "true");
    }

    if (typeof defaultAdults != 'undefined' && defaultAdults.length > 0 && isInDiningForm) {
        var locationUrl_param = window.location.href;
        var guestNo_param = getParameterByName("guest-no", locationUrl_param);
        if (!guestNo_param) {
            var defaultAdultNumber = 2;
            updateNumberOfAdult(defaultAdultNumber); //JRCW-352
            //set default data to 2:
            $('input[name=NumberOfAdults]').attr('value', defaultAdultNumber);
        }
    }
    
})


function updateBookThroughConciergeLink(internalReservationTime, internalReservationDate, internalGuestNo) {
    var rTimeLabel = "reservation-time";
    var rTime12Label = "reservation-time-12";
    var rDateLabel = "reservation-date";
    var rGuestLabel = "guest-no";
    var bl = "=";    
    //console.log(reservationDate + " / " + reservationTime);
    var encodeReservationTime = encodeURIComponent(Convert12to24Timeformat(internalReservationTime));
    var encodeReservationTime12 = encodeURIComponent(internalReservationTime);
    var encodeReservationDate = encodeURIComponent(internalReservationDate);

    var encodeReservationGuestNo = encodeURIComponent(internalGuestNo.split(' ')[0]);

    var locationUrl = $('.wrap-btn-book.btn-book-through-spa')[0].attributes.href.value;
    //console.log(locationUrl);
    var reservationTimeFromParam = getRealValueFromParameterByName(rTimeLabel, locationUrl);
    var reservationTime12FromParam = getRealValueFromParameterByName(rTime12Label, locationUrl);
    var reservationDateFromParam = getRealValueFromParameterByName(rDateLabel, locationUrl);

    var guestNoFromParam = getRealValueFromParameterByName(rGuestLabel, locationUrl);
    //console.log(guestNoFromParam);
    //console.log(reservationDateFromParam + " / " + reservationTimeFromParam);
    var newLocationUrl = locationUrl
        .replace(rTimeLabel + bl + reservationTimeFromParam, rTimeLabel + bl + encodeReservationTime)
        .replace(rTime12Label + bl + reservationTime12FromParam, rTime12Label + bl + encodeReservationTime12)
        .replace(rDateLabel + bl + reservationDateFromParam, rDateLabel + bl + encodeReservationDate)
        .replace(rGuestLabel + bl + guestNoFromParam, rGuestLabel + bl + encodeReservationGuestNo)
        ;
    //console.log(newLocationUrl);
    $('.wrap-btn-book.btn-book-through-spa')[0].setAttribute("href", newLocationUrl);
                    //console.log($('.wrap-btn-book.btn-book-through-spa')[0].attributes.href.value);
}

function getRealValueFromParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return (results[2]);
}
function Convert12to24Timeformat(str) {
    var time = str;
    var hours = Number(time.match(/^(\d+)/)[1]);
    var minutes = Number(time.match(/:(\d+)/)[1]);
    var AMPM = time.match(/\s(.*)$/)[1];
    if (AMPM.toUpperCase() == "PM" && hours < 12) hours = hours + 12;
    if (AMPM.toUpperCase() == "AM" && hours == 12) hours = hours - 12;
    var sHours = hours.toString();
    var sMinutes = minutes.toString();
    if (hours < 10) sHours = "0" + sHours;
    if (minutes < 10) sMinutes = "0" + sMinutes;
    var result = (sHours + ":" + sMinutes);
    //console.log(result);
    return result;
}


function updateReservationDate(internalReservationDate) {
    if (internalReservationDate !== null && internalReservationDate !== undefined) {
        $('.reservationDate').datepicker();
        $('.reservationDate').datepicker('setDate', new Date(internalReservationDate));
        $('.custom-dining-form input.reservationDate').datepicker('option', { dateFormat: "D, d MM, yy" });
        UI.changeInfoBox();
    } else {
        $('.custom-dining-form input.reservationDate').datepicker('setDate', new Date());
        $('.custom-dining-form input.reservationDate').datepicker('option', { dateFormat: "D, d MM, yy" });
        UI.changeInfoBox();
    }
}

function updateNumberOfAdult(internalGuestNo) {
    if (internalGuestNo !== null && internalGuestNo !== undefined) {
        $('.numberOfAdults').val(internalGuestNo);
        $('#numberOfAdults').val(internalGuestNo);
        UI.changeInfoBox();
    }
    
}
function updateReservationTime(internalAmOrPm, internalReservationTime, internalReservationTime12Format) {
    if (internalReservationTime != null && internalReservationTime != undefined) {

        processLevelOne();
        processLevelTwo();
        processLevelThree();
        processLevelFour();
        UI.changeInfoBox();

        function processLevelOne() {
            $('#ReservationTimeDivider').val(internalAmOrPm);
        }
        
        function processLevelTwo() {
            checkTimeAndReturnTextOrValue(internalReservationTime, "value");            
        }
        function checkTimeAndReturnTextOrValue(inputValue, type) {
            if ($('#timeDividerNewDinningForm').length > 0) {

                var timeSelectorContainer = $('#timeDividerNewDinningForm')[0].children;
                for (let i = 0; i < timeSelectorContainer.length; i++) {
                    var item = timeSelectorContainer[i];
                    var checkData = "";
                    if (type === "value") {
                        checkData = item.value;
                    } else if (type === "text") {
                        checkData = item.textContent;
                    }

                    var sl = "#timeDividerNewDinningForm option[value='" + item.value + "']";
                    if (checkData === inputValue) {
                        $(sl).prop("selected", true);
                    } else {
                        $(sl).prop("selected", false);
                    }
                }
            }
        }
        
        function processLevelThree() {
            var l3 = $('.box-group.time-picker.ReservationTime').find('.btn-group.btn-group-selectbox').find('.multiselect.dropdown-toggle.btn-selectbox.form-control');
            l3.attr("title", internalReservationTime12Format); //for example: 7:00 PM
            l3.find('span').text(internalReservationTime12Format); //for example: 7:00 PM            
            
        }
        
        function processLevelFour() {
            var l4 = $('.box-group.time-picker.ReservationTime').find('.btn-group.btn-group-selectbox').find('.wrap-multiselect').find('ul li');
            for (let i = 0; i < l4.length; i++) {
                var item = l4[i];
                if ($(item).find('input').val() === internalReservationTime) {
                    $(item).addClass("active");
                } else {
                    $(item).removeClass("active");
                }
            }
        }
    }
    
}


function updateReservationTimeOnDiningForm(new24FormatTimeValue, new12FormatTimeValue) {
    var reservationTime = new24FormatTimeValue; //for example: 7:00, 19:00
    var reservationTime12Format = new12FormatTimeValue.toUpperCase(); //for example: 7:00 AM, 19:00 PM
    var amOrPm = reservationTime12Format.substr(reservationTime12Format.length - 2, 2);
    updateReservationTime(amOrPm, reservationTime, reservationTime12Format);
}


//JRCW-381
function changeTimeOnDateChangeLimo() {    
    if (typeof pickupTimesByDefault !== "undefined" && pickupTimesByDefault) { //Use back-end to identify past time
        if (pickupTimesByDefault.length > 0) {
            main(pickupTimesByDefault);
        }
    } else { //use front-end to identify past time //
        //main(null);
    }
    function main(pickupTimesByDefault) {
        var isLimoFormWithPickUpDate = $('#fr-limoTransferForm').length > 0 && $('#pickupDate').length > 0;
        if (isLimoFormWithPickUpDate) {
            var selectedPickUpDate = new Date($('#pickupDate').val());
            var todayTimeStamp = new Date().setHours(0, 0, 0, 0);
            var today = new Date(todayTimeStamp);
            var selectedDateIsToday = selectedPickUpDate - today == 0;
            var allTimesLi = $('select[name="PickUpTime"]').next().find('.wrap-multiselect').find('.list-unstyled.list-options').find('li');
            displayTimes(selectedDateIsToday);

            function displayTimes(isToday) {
                
                var isActiveUnavailable = false;                
                for (let i = 1; i < allTimesLi.length; i++) {
                    var fe_item = $(allTimesLi[i]);
                    if (pickupTimesByDefault != null) {
                        var be_item = pickupTimesByDefault[i];
                        if (isToday) {
                            if (be_item.IsPast) {
                                fe_item.hide();
                                if (fe_item.hasClass("active")) {
                                    //console.log("this one is active", fe_item.text());
                                    fe_item.removeClass("active");   
                                    isActiveUnavailable = true;
                                }
                            } else {
                                fe_item.show();
                            }
                        } else {
                            fe_item.show();
                        }
                    } else {

                    }
                    
                }
                if (isToday) {
                    //set time to the nearest available time:
                    if (pickupTimesByDefault != null) {
                        if (isActiveUnavailable) {
                            var nearestAvailableTime = pickupTimesByDefault.filter(function (i) { return i.IsPast == false; })[1].Text;
                            //change value
                            $(allTimesLi).find('input[value="' + nearestAvailableTime + '"]').parent().parent().parent().addClass("active");
                            $('#pickupTime').val(nearestAvailableTime);
                            $('#pickupTime').next().find('.multiselect.dropdown-toggle.btn-selectbox.form-control').attr('title', nearestAvailableTime);
                            $('#pickupTime').next().find('.multiselect.dropdown-toggle.btn-selectbox.form-control').find('span').text(nearestAvailableTime);
                            
                            UI.changeInfoBox();

                        }
                        
                    }
                }
            }

        }
    }
}





//JRCW-488
$(document).ready(function () {
    if ($('form#diningForm').length > 0 && $('form#diningForm').find('.other-request-form-module').length > 0) {
        OtherRequestFormBeforeSubmit();
    }
    if ($('form#diningForm').length > 0 && $('form#diningForm').find('.other-request-form-module').length == 0) {
        DiningFormBeforeSubmit();
    }   

    LimoServiceFormBeforeSubmit();
    HotelBookingFormBeforeSubmit();
    TravelFlightFormBeforeSubmit();
    GolfCourseOldBeforeSubmit();
    if ($('form#fr-booking').length > 0 && $('form#fr-booking').attr("action").includes("SubmitGolfForm")) {
        GolfCourseNewBeforeSubmit();
    }
    if ($('form#fr-booking').length > 0 && $('form#fr-booking').attr("action").includes("SubmitTravelTourFrom")) {
        TravelTourBeforeSubmit();
    }
    CarRentalFormBeforeSubmit();
    CruiseFormBeforeSubmit();
    DiningWithMenuFormBeforeSubmit();
    if ($('form#fr-booking').length > 0 && $('form#fr-booking').attr("action").includes("SubmitEntertainmentFrom")) {
        EventsFormBeforeSubmit();
    }
    FeedbackFormBeforeSubmit();
    FlowerDeliveriesFormBeforeSubmit();

    if ($('form#fr-booking').length > 0 && $('form#fr-booking').attr("action").includes("SubmitLimoBooking")) {
        LimoServiceSelfServeFormBeforeSubmit();
    }
    
});


function DiningFormBeforeSubmit() {
    //JRCW-488
    $('form#diningForm').find('button[type="submit"]').clone().appendTo('.info-book');
    $('form#diningForm').find('button[type="submit"]').first().hide();
    $('form#diningForm').find('button[type="submit"]').next().removeAttr("type").attr("onClick", "submitDiningForm(event)");
}

function LimoServiceFormBeforeSubmit() {
    //JRCW-488
    $('form#fr-limoTransferForm').find('.btn.btn-red.booking-btn.showAjaxLoading').clone().appendTo('.info-book');
    $('form#fr-limoTransferForm').find('.btn.btn-red.booking-btn.showAjaxLoading').first().hide();
    $('form#fr-limoTransferForm').find('.btn.btn-red.booking-btn.showAjaxLoading').next().removeAttr("type").attr("onClick", "submitLimoServiceForm(event)");
}
function HotelBookingFormBeforeSubmit() {
    //JRCW-488
    $('form#fr-travelForm').find('.btn.btn-red.booking-btn.showAjaxLoading').clone().appendTo('.info-book');
    $('form#fr-travelForm').find('.btn.btn-red.booking-btn.showAjaxLoading').first().hide();
    $('form#fr-travelForm').find('.btn.btn-red.booking-btn.showAjaxLoading').next().removeAttr("type").attr("onClick", "submitHotelForm(event)");
}
function TravelFlightFormBeforeSubmit() {
    //JRCW-488
    $('form#fr-privateJetForm').find('.btn.btn-red.booking-btn.showAjaxLoading').clone().appendTo('.info-book');
    $('form#fr-privateJetForm').find('.btn.btn-red.booking-btn.showAjaxLoading').first().hide();
    $('form#fr-privateJetForm').find('.btn.btn-red.booking-btn.showAjaxLoading').next().removeAttr("type").attr("onClick", "submitTravelFlightForm(event)");
}
function OtherRequestFormBeforeSubmit() {
    //JRCW-488
    $('form#diningForm').find('#btnBook').clone().appendTo('.btn-book');
    $('form#diningForm').find('#btnBook').first().hide();
    $('form#diningForm').find('#btnBook').next().removeAttr("type").attr("onClick", "submitOtherRequestForm(event)");
}
function GolfCourseOldBeforeSubmit() {
    //JRCW-488
    $('form#golfForm').find('button[type="submit"]').clone().appendTo('.info-book');
    $('form#golfForm').find('button[type="submit"]').first().hide();
    $('form#golfForm').find('button[type="submit"]').next().removeAttr("type").attr("onClick", "submitGolfCourseOldForm(event)");
}
function GolfCourseNewBeforeSubmit() {
    //JRCW-488
    $('form#fr-booking').find('.btn.btn-red.booking-btn.showAjaxLoading').clone().appendTo('.info-book');
    $('form#fr-booking').find('.btn.btn-red.booking-btn.showAjaxLoading').first().hide();
    $('form#fr-booking').find('.btn.btn-red.booking-btn.showAjaxLoading').next().removeAttr("type").attr("onClick", "submitGolfCourseNewForm(event)");
}
function TravelTourBeforeSubmit() {
    //JRCW-488
    $('form#fr-booking').find('.btn.btn-red').clone().appendTo('.info-book');
    $('form#fr-booking').find('.btn.btn-red').first().hide();
    $('form#fr-booking').find('.btn.btn-red').last().removeAttr("type").attr("onClick", "submitTravelTourForm(event)"); //this one is select the last one, because of the template
}
function CarRentalFormBeforeSubmit() {
    //JRCW-488
    $('form#fr-transportationForm').find('.btn.btn-red.booking-btn.showAjaxLoading').clone().appendTo('.info-book');
    $('form#fr-transportationForm').find('.btn.btn-red.booking-btn.showAjaxLoading').first().hide();
    $('form#fr-transportationForm').find('.btn.btn-red.booking-btn.showAjaxLoading').next().removeAttr("type").attr("onClick", "submiCarRentalForm(event)");
}
function CruiseFormBeforeSubmit() {
    //JRCW-488
    $('form#fr-cruiseForm').find('.btn.btn-red.booking-btn.showAjaxLoading').clone().appendTo('.info-book');
    $('form#fr-cruiseForm').find('.btn.btn-red.booking-btn.showAjaxLoading').first().hide();
    $('form#fr-cruiseForm').find('.btn.btn-red.booking-btn.showAjaxLoading').next().removeAttr("type").attr("onClick", "submiCruiseForm(event)");
}
function DiningWithMenuFormBeforeSubmit() {
    //JRCW-488
    $('form#fr-newDiningForm').find('.btn.btn-red.booking-btn.showAjaxLoading').clone().appendTo('.info-book');
    $('form#fr-newDiningForm').find('.btn.btn-red.booking-btn.showAjaxLoading').first().hide();
    $('form#fr-newDiningForm').find('.btn.btn-red.booking-btn.showAjaxLoading').next().removeAttr("type").attr("onClick", "submitDiningWithMenuForm(event)");
}
function EventsFormBeforeSubmit() {
    //JRCW-488
    $('form#fr-booking').find('.btn.btn-red').clone().appendTo('.info-book');
    $('form#fr-booking').find('.btn.btn-red').first().hide();
    $('form#fr-booking').find('.btn.btn-red').last().removeAttr("type").attr("onClick", "submitEventsForm(event)");
}
function FeedbackFormBeforeSubmit() {
    //JRCW-488
    $('form#promoterScoreForm').find('.btn.btn-red.top-gap').clone().appendTo('form .row.col-sm-12.text-center');
    $('form#promoterScoreForm').find('.btn.btn-red.top-gap').first().hide();
    $('form#promoterScoreForm').find('.btn.btn-red.top-gap').next().removeAttr("type").attr("onClick", "submitFeedbackForm(event)");
}

function FlowerDeliveriesFormBeforeSubmit() {
    //JRCW-488
    $('form#fr-flowerForm').find('.btn.btn-red.booking-btn.showAjaxLoading').clone().appendTo('.info-book');
    $('form#fr-flowerForm').find('.btn.btn-red.booking-btn.showAjaxLoading').first().hide();
    $('form#fr-flowerForm').find('.btn.btn-red.booking-btn.showAjaxLoading').next().removeAttr("type").attr("onClick", "submitFlowerDeliveriesForm(event)");
}

function LimoServiceSelfServeFormBeforeSubmit() {
    //JRCW-488
    //$('form#fr-booking').find('#btnBooking').clone().appendTo('.info-book');
    //$('form#fr-booking').find('#btnBooking').first().hide();
    //$('form#fr-booking').find('.info-book').find('.btn.btn-red').last().removeAttr("type").attr("onClick", "submitLimoServiceSelfServeForm(event)");
}



function submitDiningForm(e) {
    e.preventDefault();
    var formId = $('form#diningForm');
    //check valid first
    if ($(formId).valid()) {
        $(".btn-submit-CreditCardNumber-footer").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
        App.verifyCC(function () { $('form#diningForm').find('button[type="submit"]:hidden').trigger('click'); });
    }

}
function submitLimoServiceForm(e) {
    e.preventDefault();
    var formId = $('form#fr-limoTransferForm');
    var submitButton = $('form#fr-limoTransferForm').find('.btn.btn-red.booking-btn.showAjaxLoading:hidden');
    //check valid first
    if ($(formId).valid()) {
        $(".btn-submit-CreditCardNumber-footer").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
        App.verifyCC(function () { submitButton.trigger('click'); });
    }
}
function submitHotelForm(e) {
    e.preventDefault();
    var formId = $('form#fr-travelForm');
    var submitButton = $('form#fr-travelForm').find('.btn.btn-red.booking-btn.showAjaxLoading:hidden');
    //check valid first
    if ($(formId).valid()) {
        $(".btn-submit-CreditCardNumber-footer").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
        App.verifyCC(function () { submitButton.trigger('click'); });
    }
}
function submitTravelFlightForm(e) {
    e.preventDefault();
    var formId = $('form#fr-privateJetForm');
    var submitButton = $('form#fr-privateJetForm').find('.btn.btn-red.booking-btn.showAjaxLoading:hidden');
    //check valid first
    if ($(formId).valid()) {
        $(".btn-submit-CreditCardNumber-footer").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
        App.verifyCC(function () { submitButton.trigger('click'); });
    }
}
function submitOtherRequestForm(e) {
    e.preventDefault();
    var formId = $('form#diningForm');
    var submitButton = $('form#diningForm').find('#btnBook:hidden');
    //check valid first
    if ($(formId).valid()) {
        $(".btn-submit-CreditCardNumber-footer").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
        App.verifyCC(function () { submitButton.trigger('click'); });
    }
}
function submitGolfCourseOldForm(e) {
    e.preventDefault();
    var formId = $('form#golfForm');
    var submitButton = $('form#golfForm').find('.btn.btn-red:hidden');
    //check valid first
    if ($(formId).valid()) {
        $(".btn-submit-CreditCardNumber-footer").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
        App.verifyCC(function () { submitButton.trigger('click'); });
    }
}

function submitGolfCourseNewForm(e) {
    e.preventDefault();
    var formId = $('form#fr-booking');
    var submitButton = $('form#fr-booking').find('.btn.btn-red.booking-btn.showAjaxLoading:hidden');
    //check valid first
    if ($(formId).valid()) {
        $(".btn-submit-CreditCardNumber-footer").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
        App.verifyCC(function () { submitButton.trigger('click'); });
    }
}

function submitTravelTourForm(e) { 
    e.preventDefault();
    var formId = $('form#fr-booking');
    var submitButton = $('form#fr-booking').find('.btn.btn-red:hidden');
    //check valid first
    if ($(formId).valid()) { //The data in this form has some problem, some select box do not have any data, so it can not pass .valid part. test until now
        $(".btn-submit-CreditCardNumber-footer").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
        App.verifyCC(function () { submitButton.trigger('click'); });
    }
}

function submiCarRentalForm(e) {
    e.preventDefault();
    var formId = $('form#fr-transportationForm');
    var submitButton = $('form#fr-transportationForm').find('.btn.btn-red.booking-btn.showAjaxLoading:hidden');
    //check valid first
    if ($(formId).valid()) {
        $(".btn-submit-CreditCardNumber-footer").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
        App.verifyCC(function () { submitButton.trigger('click'); });
    }
}
function submiCruiseForm(e) {
    e.preventDefault();
    var formId = $('form#fr-cruiseForm');
    var submitButton = $('form#fr-cruiseForm').find('.btn.btn-red.booking-btn.showAjaxLoading:hidden');
    //check valid first
    if ($(formId).valid()) {
        $(".btn-submit-CreditCardNumber-footer").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
        App.verifyCC(function () { submitButton.trigger('click'); });
    }
}
function submitDiningWithMenuForm(e) {
    e.preventDefault();
    var formId = $('form#fr-newDiningForm');
    var submitButton = $('form#fr-newDiningForm').find('.btn.btn-red.booking-btn.showAjaxLoading:hidden');
    //check valid first
    if ($(formId).valid()) {
        $(".btn-submit-CreditCardNumber-footer").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
        App.verifyCC(function () { submitButton.trigger('click'); });
    }
}

function submitEventsForm(e) {
    e.preventDefault();
    var formId = $('form#fr-booking');
    var submitButton = $('form#fr-booking').find('.btn.btn-red:hidden');
    //check valid first
    if ($(formId).valid()) {
        $(".btn-submit-CreditCardNumber-footer").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
        App.verifyCC(function () { submitButton.trigger('click'); });
    }
}

function submitFeedbackForm(e) {
    e.preventDefault();
    var formId = $('form#promoterScoreForm');
    var submitButton = $('form#promoterScoreForm').find('.btn.btn-red.top-gap:hidden');
    //check valid first
    if ($(formId).valid()) {
        $(".btn-submit-CreditCardNumber-footer").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
        App.verifyCC(function () { submitButton.trigger('click'); });
    }
}

function submitFlowerDeliveriesForm(e) {
    e.preventDefault();
    var formId = $('form#fr-flowerForm');
    var submitButton = $('form#fr-flowerForm').find('.btn.btn-red.booking-btn.showAjaxLoading:hidden');
    //check valid first
    if ($(formId).valid()) {
        $(".btn-submit-CreditCardNumber-footer").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
        App.verifyCC(function () { submitButton.trigger('click'); });
    }
}

function submitLimoServiceSelfServeForm(e) {
    e.preventDefault();
    var formId = $('form#fr-booking');
    var submitButton = $('form#fr-booking').find('.btn.btn-red:hidden');
    //check valid first
    if ($(formId).valid()) {
        $(".btn-submit-CreditCardNumber-footer").append("<div id='ajaxLoading' class='loading'><div class='loading-image'></div></div>");
        App.verifyCC(function () { submitButton.trigger('click'); });
    }
}
//form can not test TravelTourForm, EventsForm: same situation
//form limo service self serve can not search - can not test

//transfer is the same with limo fr-limoTransferForm, done
//travel accomodation - same with hotel , done
//travel acc old - blank page