
var LifestyleIndiv = function () {

    function boxBookSticky(){
        var positionContent = $(".content-indiv").offset().top;
        $(window).scroll(function(){
            if($(window).scrollTop() > positionContent){
                $(".box-btn-book").addClass("sticky");
            } else {
                $(".box-btn-book").removeClass("sticky");
            }
        });
    }

    function autoScrollItinerary(){
        $(window).on("load resize", function(){
            var itineraryContent = $(".block-article-style.intinerary .tab-content");
            var leftHeight = itineraryContent.find(".tab-inner-left").outerHeight();
            var articleHeight = leftHeight - itineraryContent.find(".tab-inner-right .itinerary-title").height() - 30;
            itineraryContent.find(".tab-inner-right .itinerary-article").addClass('scrollbar-outer');
            if($(window).width() > 991){
                itineraryContent.find(".tab-inner-right .itinerary-article").scrollbar({
                    onUpdate: function(){
                        itineraryContent.find(".tab-inner-right .itinerary-article").css("max-height", articleHeight);
                    }
                });
            } else{
                itineraryContent.find(".tab-inner-right .itinerary-article").scrollbar();
            }
        });
    }
    return {
        init: function () {
            App.init();
            boxBookSticky();
            autoScrollItinerary();
            
            $(document).ready(function(){
                // Handle button book
                if($(".box-btn-book .item").length == 1) {
                    $(".box-btn-book .item").css("width","100%");
                }
                
                // Handle BlockScheduleDetails
                if($(".block-schedule-details input[type='radio']:checked").length > 0){
                    var packageSchedule = $(".block-schedule-details input[type='radio']:checked");
                    packageSchedule.closest(".item").find(".item-content").addClass("active");
                }
                $(document).on("click", ".block-schedule-details .select-btn", function(){
                    $(".block-schedule-details .item-content").removeClass("active");
                    $(this).closest(".item").find(".item-content").addClass("active");
                });

                //Handle BlockScheduleSummary
                if($(".block-schedule-summary input[type='radio']:checked").length > 0){
                    var packageSchedule = $(".block-schedule-summary input[type='radio']:checked");
                    packageSchedule.closest(".item").addClass("selected");
                }
                $(document).on("click", ".block-schedule-summary .select-btn label", function(){
                    $(".block-schedule-summary .item").removeClass("selected");
                    $(this).closest(".item").addClass("selected");
                });

                // Handle header style for new style seating map API in entertainment-indiv
                if($(".page-entertainment-indiv").length > 0){
                    $(".page-entertainment-indiv").siblings(".header").css({
                        "background": "none",
                        "height":"auto", 
                        "text-align":"left", 
                        "color":"inherit", 
                        "font-weight":"normal", 
                        "font-size":"inherit"
                    });
                }
            });
        } 
    }
}();