
var CityGuideIndiv = function () {
    function initSlick() {
        $(document).ready(function(){

            $('.slides-recommends').slick({
                dots: true,
                arrows: false,
                infinite: false,
                speed: 300,
                slidesToShow: 3,
                slidesToScroll: 3,
                slidesPerRow : 1,
                rows:1
            });

            $(window).on("resize load", function(){
                var winWidth = $(window).width();
                if(winWidth < 768){
                    $('.slides-recommends').slick("unslick");
                    $('.slides-recommends').slick({
                        dots: true,
                        arrows: false,
                        infinite: false,
                        speed: 300,
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        rows: 2
                    });
                } else if((winWidth >= 768) && (winWidth < 1024)){
                    $('.slides-recommends').slick("unslick");
                    $('.slides-recommends').slick({
                        dots: true,
                        arrows: false,
                        infinite: false,
                        speed: 300,
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        rows: 2,
                        slidesPerRow : 1
                    });
                } else{
                    $('.slides-recommends').slick("unslick");
                    $('.slides-recommends').slick({
                        dots: true,
                        arrows: false,
                        infinite: false,
                        speed: 300,
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        rows: 1
                    });
                }
            });
        });
    }
    function boxBookSticky(){
        var positionContent = $(".content-indiv").offset().top;
        $(window).scroll(function(){
            if($(window).scrollTop() > positionContent){
                $(".box-btn-book").addClass("sticky");
            } else {
                $(".box-btn-book").removeClass("sticky");
            }
        });
    }
    return {
        init: function () {
            App.init();
            initSlick();
            boxBookSticky();
        }
    }
}();