
var TravelPackages = function () {
    function initSlick() {
        $(document).ready(function(){
            if($(".slides-recommends .slide").length > 4){
                $('.slides-recommends').slick({
                    dots: true,
                    arrows: false,
                    infinite: false,
                    speed: 300,
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    slidesPerRow : 1,
                    rows:3,
                    responsive: [
                        {
                            breakpoint: 980,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                rows: 4,
                                slidesPerRow : 1
                            }
                        },
                        {
                            breakpoint: 767,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                slidesPerRow : 1,
                                rows: 4
                            }
                        }
                    ]
                });
            } else{
                $('.slides-recommends').slick({
                    dots: true,
                    arrows: false,
                    infinite: false,
                    speed: 300,
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    slidesPerRow : 1,
                    rows:2,
                    responsive: [
                        {
                            breakpoint: 980,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                rows: 4,
                                slidesPerRow : 1
                            }
                        },
                        {
                            breakpoint: 767,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                slidesPerRow : 1,
                                rows: 4
                            }
                        }
                    ]
                });
            }
        });
    }
    function moduleFilter() {
        $('.list-checkbox li').on("click", function() {
            if($(this).hasClass('on')) {
                $(this).removeClass('on');
            } else {
                $(this).addClass('on');
            };
        });
    }
    return {
        init: function () {
            App.init();
            moduleFilter();
        }
    }
}();