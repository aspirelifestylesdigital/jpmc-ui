(function ($) {
	$.fn.capslide = function (options) {
		var opts = $.extend({}, $.fn.capslide.defaults, options);
		return this.each(function () {
			$this = $(this);
			var o = $.meta ? $.extend({}, opts, $this.data()) : opts;
			if (!o.showcaption) $this.find('.ic_caption').css('display', 'none');
			else $this.find('.ic_text').css('display', 'none');
			var _img = $this.find('img:first');
			var w = _img.css('width');
			var h = _img.css('height');
			$('.ic_caption', $this).css({ 'color': o.caption_color, 'bottom': '0px', 'width': w });
			$this.css({ 'width': w, 'height': h });

			$('.ic_more a').on('click', function() {
				console.log('something')
			})

			$this.on('click',function (e) {

				if ($(this).find('.ic_text').css("display") == 'none') {
					$('.ic_text', $(this)).slideDown(500);
					// add plus icon here
					$('.capslide-more', $(this)).css('display','none');
					$('.capslide-less', $(this)).css('display','inline-block');
				}
				else {
					$('.ic_text', $(this)).slideUp(500);
					// add minus icon here
					$('.capslide-less', $(this)).css('display','none');
					$('.capslide-more', $(this)).css('display','inline-block');
				}
				const $more = $("<div class='more'><a style='color:#000'>more</a></div>");
				var divh = $(this).find(".ic_summary").parent().height();
				var ele = $(this).find(".ic_summary");
				if (ele.find(".more").length <= 0) {
					ele.append($more);
				}
				while (ele.height() > divh) {
					displayTextContent(ele);
				}
				function displayTextContent(ele) {
					$(".ic_summary .more").remove();
					// Check if the paragraph's height is taller than the container's height. If it is:
					var newText = ele.text().replace(/(\w+)\W*$/, '...');
					ele.text(newText);
					ele.append($more);
				}
				$(".ic_summary .more").remove();

				var destinationLabelHeight = $(this).find('.ic_destination_wrapper').height();
				var labelDes = $(this).find('.ic_destination');
				//slice label destination but not add 3 dots if long text
				while (labelDes.height() > destinationLabelHeight) {
					var newTitle = labelDes.text().replace(/,\W*\s(\S)*$/, '');
					labelDes.text(newTitle);
				}

				
				
				// if ($.trim($(this).find('.ic_summary').text()) === "") $(this).find(".ic_text .ic_more").remove();
				if ($.trim($(this).find('.ic_summary').text()) === "") $(this).find(".ic_text .ic_more").css({'margin-top': 0, 'bottom': '5px', 'line-height': 1});

				e.stopPropagation();
			});
			var divhtitle = $(this).find(".ic_title").parent().height();
			while ($(this).find(".ic_title").height() > divhtitle) {
				var elementTitle = $(this).find(".ic_title");
				var newElementTitle = elementTitle.text().replace(/\W*\s(\S)*$/, '...');

				elementTitle.text(newElementTitle); // add an ellipsis at the last shown space
			}
			// displayTitles();
			displayTileInnerText();
			alignGrayBar();

		});
	};

	/*Liem Buu forward ticket 1066*/
	function displayTitles() {
		var _module = [];
		var _container = $("div[class*='tile_module']").find('.container').width();
		var index = 0;
		var a = 0;
		var _itemInline = 0;
		_module.push($("div[class*='tile_module']"));
		if (_container == undefined) return false;
		while (index < _module.length) {
			var _item = _module[index].find('.view-type-tilesview');
			var _itemWidth = _module[index].find('.view-type-tilesview').width();
			_itemInline = parseInt(_container / _itemWidth);
			while (a < _item.length) {
				var newItem = _item.slice(0 + a, _itemInline + a);
				for (var itemIndex = 0; itemIndex < newItem.length; itemIndex++) {
					var e = $(newItem[itemIndex]);
					var height = e.find(".ic_title").height();
					for (var n = 0; n < newItem.length; n++) {
						var _e = $(newItem[n]);
						var _height = _e.find(".ic_title").height();
						if (height != _height) {
							//var _min = e.find(".ic_title").parent().height();
							var min = height < _height ? height : _height;
							//min = min < _min ? min : _min;
							while (e.find(".ic_title").height() > min) {
								// console.log(e.find(".ic_title").height(), min, e.find(".ic_title").parent().height())
								var elementTitle = e.find(".ic_title");
								var newElementTitle = elementTitle.text().replace(/\W*\s(\S)*$/, '...');
								elementTitle.text(newElementTitle); // add an ellipsis at the last shown space
							}
						}
					}
				}
				a += _itemInline;
			}
			index++;
		}
	}
	function displayTileInnerText() {
		var _innerItems = $(".content.ic_content.ic_caption").find('.inner');
		if (_innerItems.length > 0) {
			for (var itemIndex = 0; itemIndex < _innerItems.length; itemIndex++) {
				var item = $(_innerItems[itemIndex]);
				var titleHeight = item.find('.ic_height_title').height();
				var title = item.find('.ic_title');

				// slice title and add 3 dots at last if long title
				while (title.outerHeight() > titleHeight) {
					var newTitle = title.text().replace(/\W*\s(\S)*$/, '...');
					title.text(newTitle);
				}
			}
		} else return;
	}

	function alignGrayBar() {
		var arrayTiles = $(".content.ic_content.ic_caption"); // number of tiles
		var containerWidth = $('.container').width(); //width of container
		var indexToCount = 0;
		if (arrayTiles.length > 0) {
			var itemWidth = $(arrayTiles[indexToCount]).width();
			var inlineTiles = parseInt(containerWidth / itemWidth); //caculate number of tiles per line

			
			while (indexToCount < arrayTiles.length) {
				var tempItems = arrayTiles.slice(0 + indexToCount, inlineTiles + indexToCount); // get tiles per line each group
				var maxHeight = $(tempItems[0]).height();
				var summaryHeight = 0;

				// get heightest ic_text item
				for (var itemIndex = 0; itemIndex < tempItems.length; itemIndex++) {
					var item = $(tempItems[itemIndex]).find('.ic_text');
					item.css({
						'display':'block',
						'visibility':'hidden'
					});
					if (item.outerHeight() > maxHeight) {
						maxHeight = item.outerHeight();
						summaryHeight = $(item).find('.ic_summary').innerHeight();
					}
					maxHeight = item.outerHeight() > maxHeight ? item.outerHeight() : maxHeight;
					console.log(maxHeight, summaryHeight);

					item.css({
						'display':'none',
						'visibility':'visible'
					});
				}

				// set height of tiles equal to max-height
				for (var index = 0; index < tempItems.length; index++) {
					$(tempItems[index]).find('.ic_text').css('height', maxHeight);
					// $(tempItems[index]).find('.ic_summary').css('height', summaryHeight);

				}
				indexToCount += inlineTiles; // count to next line

			}
		} else return;
	}

	$.fn.capslide.defaults = {
		caption_color: 'white',
		caption_bgcolor: 'black',
		overlay_bgcolor: 'blue',
		border: '1px solid #fff',
		showcaption: true
	};
})(jQuery);