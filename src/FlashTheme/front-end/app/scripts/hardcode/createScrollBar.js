$(document).ready(function(){
	if ($('[data-spy="scrollBar"]').length > 0) {
        $('[data-spy="scrollBar"]').each(function() {
            $(this).addClass('scrollbar-outer');
            $(this).scrollbar();
        });
    }
});
