var moduleHeader = new function() {
    var inist = {
        options     = {},

        header      = '.header',

        listNAV     = '.aspire-nav',

        btnClosePrombtLocation = '.btn-close-location',

        version     = '2.0',
        author      = 'loctran.ict@gmail.com',

        init: function (option) {
            this.extendDefaultsOption(option);
            this.eventsHandlers();
            return this;
        },

        extendDefaultsOption: function (option) {
            this.options = $.extend(true, this.options, option);            
        },

        eventsHandlers: function () {
            var self = this;

            //click button close location (prombt) => close
            $(document).on('click', self.btnClosePrombtLocation, $.proxy(...));

            self.header.on('click', '.selector', function(event) {
                event.preventDefault();
                /* Act on the event */
            });

        },

        eventsListeners: function () {
        }

    };

    return inist;
}