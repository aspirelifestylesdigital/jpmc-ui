(function ($) {
    $.fn.capslide = function (options) {
        var opts = $.extend({}, $.fn.capslide.defaults, options);
        return this.each(function () {
            $this = $(this);
            var o = $.meta ? $.extend({}, opts, $this.data()) : opts;
            if (!o.showcaption) $this.find('.ic_caption').css('display', 'none');
            else $this.find('.ic_text').css('display', 'none');
            var _img = $this.find('img:first');
            var w = _img.css('width');
            var h = _img.css('height');
            var textCount = 157;
            // $('.ic_caption',$this).css({'color':o.caption_color,'background-color':o.caption_bgcolor,'bottom':'0px','width':w});
            $('.ic_caption', $this).css({ 'color': o.caption_color, 'bottom': '0px', 'width': w });
            // $('.overlay',$this).css('background-color',o.overlay_bgcolor);
            // $this.css({'width':w , 'height':h, 'border':o.border});
            $this.css({ 'width': w, 'height': h });

            $this.unbind().click(function (e) {
                if ($(this).find('.ic_text').css("display") == 'none') {
                    $('.ic_text', $(this)).slideDown(500);
                    // add plus icon here
                    $('.capslide-more', $(this)).css('display', 'none');
                    $('.capslide-less', $(this)).css('display', 'inline-block');
                }
                else {
                    $('.ic_text', $(this)).slideUp(500);
                    // add minus icon here
                    $('.capslide-less', $(this)).css('display', 'none');
                    $('.capslide-more', $(this)).css('display', 'inline-block');
                }
                const $more = $("<div class='more'><a style='color:#000'>more</a></div>");
                var divh = $(this).find(".ic_summary").parent().height();
                var ele = $(this).find(".ic_summary");
                if (ele.find(".more").length <= 0) {
                    ele.append($more);
                }
                while (ele.height() > divh) {
                    displayTextContent(ele);
                }
                function displayTextContent(ele) {
                    $(".ic_summary .more").remove();
                    // Check if the paragraph's height is taller than the container's height. If it is:
                    var newText = ele.text().replace(/(\w+)\W*$/, '...');
                    ele.text(newText);
                    ele.append($more);
                }
                $(".ic_summary .more").remove();
                if ($.trim($(this).find('.ic_summary').text()) === "") $(this).find(".ic_text .ic_more").css({ 'margin-top': 0, 'bottom': '5px', 'line-height': 1 });
                e.stopPropagation();
            });

            var divhtitle = $(this).find(".ic_title").parent().height();
            while ($(this).find(".ic_title").height() > divhtitle) {
                var elementTitle = $(this).find(".ic_title");
                var newElementTitle = elementTitle.text().replace(/\W*\s(\S)*$/, '...');
                elementTitle.text(newElementTitle); // add an ellipsis at the last shown space
            }
            displayTitles();
        });
    };


    /*Liem Buu forward ticket 1066*/
    function displayTitles() {
        var _module = [];
        _module.push($("div[class*='tile_module']"));
        var _container = $("div[class*='tile_module']").find('.container').width();
        if (_container == undefined || _module.length === 0) return false;
        var a = 0;
        var _itemInline = 0;
        var _item = _module[0].find('.view-type-tilesview');
        var _itemWidth = _module[0].find('.view-type-tilesview').width();
        _itemInline = parseInt(_container / _itemWidth);
        while (a < _item.length) {
            var newItem = _item.slice(0 + a, _itemInline + a);
            for (var itemIndex = 0; itemIndex < newItem.length; itemIndex++) {
                var e = $(newItem[itemIndex]);
                var height = e.find(".ic_title").height();
                for (var n = 0; n < newItem.length; n++) {
                    var _e = $(newItem[n]);
                    var _height = _e.find(".ic_title").height();
                    if (height != _height) {
                        var min = height < _height ? height : _height;
                        if (min > 0) {
                            while (e.find(".ic_title").height() > min) {
                                var elementTitle = e.find(".ic_title");
                                var newElementTitle = elementTitle.text().replace(/\W*\s(\S)*$/, '...');
                                elementTitle.text(newElementTitle); // add an ellipsis at the last shown space
                            }
                        }
                    }
                }
            }
            a += _itemInline;
        }
    }

    $.fn.capslide.defaults = {
        caption_color: 'white',
        caption_bgcolor: 'black',
        overlay_bgcolor: 'blue',
        border: '1px solid #fff',
        showcaption: true
    };

})(jQuery);