$(function () {
    var sessionTimeout = 2;
    var displaySessionTimeout = sessionTimeout - 1;

    var displayTimeoutTimer = window.setTimeout(DisplaySessionTimeoutWarning, displaySessionTimeout * 60000);

    function DisplaySessionTimeoutWarning() {
        var sessionTimeoutTimer = window.setTimeout(SessionTimedOut, 60000);
        
        $("#modal-timeout").modal("show");
        
        $("#btnSessionContinue").click(function () {
            window.clearTimeout(sessionTimeoutTimer);
            displayTimeoutTimer = window.setTimeout(DisplaySessionTimeoutWarning, displaySessionTimeout * 60000);
        });
    }

    function SessionTimedOut() {
        window.location.href = "session-timeout.html";
    }
});
