var SignUp = function () {
    $(window).bind("pageshow", function (event) {
        if (event.originalEvent.persisted) {
            window.location.reload()
        }
    });

    function printExpiryValue() {
        var month = $('.selectExpiryMonth select').find("option:selected").val();
        var year = $('.selectExpiryYear select').find("option:selected").val();
        $('.txtExpiryDate').val(month + "/" + year);
        $('.box-expiry-date input.txtExpiryDate').removeClass('error lt-error');
        $('.box-expiry-date label.lt-error').css('display', 'none');
        $('.selectExpiryMonth .multiselect-selected-text').css("color", "#011627");
        $('.selectExpiryYear .multiselect-selected-text').css("color", "#011627");
    }

    function getValueCardExpiryDate(formClass, idOfExpiredDate) {
        $('.selectExpiryMonth select').on('change', function () {
            monthVal = $(this).val();
            printExpiryValue();
            $("." + formClass).validate().element("#" + idOfExpiredDate);
        });
        $('.selectExpiryYear select').on('change', function () {
            yearVal = $(this).val();
            printExpiryValue();
            $("." + formClass).validate().element("#" + idOfExpiredDate);
        });

        //show month and year picker
        $(".txtExpiryDate").on("click", function (event) {
            event.stopPropagation();
            $(".cardExpiryPicker").addClass('active');
        });
        $(window).click(function () {
            $(".cardExpiryPicker").removeClass('active');
        });
    }

    function showDialogWarning() {
        $('.btn-check-availability').click(function () {
            $('html,body').addClass('signup-modal-open');
            $('.signup-modal').modal('show');
        })


        // $(".modal").each(function () {
        //     $(this).on('shown.bs.modal', function () {
        //         $('body').removeClass('modal-open');
        //         // $('body').addClass('signup-modal-open');
        //         $('html').removeClass('unscroll');
        //     });
        // });
    }
    return {
        init: function (formClass, idOfExpiredDate) {
            App.init();
            getValueCardExpiryDate(formClass, idOfExpiredDate);
            showDialogWarning();
        }
    }
}();