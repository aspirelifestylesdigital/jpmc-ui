+function ($) {
    var Validation = function (element, option) {
        $element = $(element);
        this.options = $.extend(true, Validation.DEFAULTS, option);
        this.getRuleMess = function () {
            var rule = {}, message = {}, key;
            for (key in this.options) {
                var name = this.options[key].name;
                message[name] = this.options[key].message;
                rule[name] = this.options[key].rule;
            }
            return [rule, message];
        }
        this.init(element);
    }

    var mgsCurrentPassword = typeof placeHolderCurrentPassword == 'undefined' ? 'This field is required.' : placeHolderCurrentPassword;
    var mgsPassword = typeof placeHolderPassword == 'undefined' ? 'Have you entered the correct password?' : placeHolderPassword;
    var mgsConfirmPassword = typeof placeHolderConfirmPassword == 'undefined' ? 'Please ensure that your passwords match' : placeHolderConfirmPassword;

    var mgsRequired = typeof placeHolderRequired == 'undefined' ? "This field is required." : placeHolderRequired;
    var mgsInvalid = typeof placeHolderInvalid == 'undefined' ? "Value is invalid." : placeHolderInvalid;
    var mgsFirstName = typeof placeHolderFirstName == 'undefined' ? "Tell us your first name" : placeHolderFirstName;
    var mgsMobileNum = typeof placeHolderMobileNum == 'undefined' ? "Please enter only numbers." : placeHolderMobileNum;
    var mgsEmail = typeof placeHolderEmail == 'undefined' ? "Have you entered a valid email address?" : placeHolderEmail;
    var mgsCourseName = typeof placeHolderCourseName == 'undefined' ? "Have you entered the course name?" : placeHolderCourseName;
    var mgsCardExpiryDate = typeof placeHolderCardExpiryDate == 'undefined' ? "Have you entered the correct date?" : placeHolderCardExpiryDate;

    var mgsSalutation = typeof placeHolderSalutation == 'undefined' ? 'How should we address you?' : placeHolderSalutation;
    var mgsLastName = typeof placeHolderLastName == 'undefined' ? 'Tell us your last name' : placeHolderLastName;
    var mgsContactNumber = typeof placeHolderContactNumber == 'undefined' ? 'Please enter only numbers' : placeHolderContactNumber;
    var mgsBinNumber = typeof placeHolderBinNumber == 'undefined' ? 'Please enter the first 6 digits of your credit card' : placeHolderBinNumber;
    var mgsErrMinDish = typeof placeHoldermgsErrMinDish == 'undefined' ? 'Please select at least 1 dish' : placeHoldermgsErrMinDish;

    var mgsRequiredCheck = typeof placeHolderRequiredCheck == 'undefined' ? 'Please read and acknowledge this check-box to continue' : placeHolderRequiredCheck;
    var mgsNameOfCard = typeof placeHolderNameOfCard == 'undefined' ? "Tell us the name on your credit card" : placeHolderNameOfCard;
    var mgsValidCardNumber = typeof placeHolderValidCardNumber == 'undefined' ? "Have you entered a valid credit card number?" : placeHolderValidCardNumber;
    var msgDuplicated = typeof placeHolderDuplicated == 'undefined' ? 'Alternate Tee Time must be different from Preferred Tee Time' : placeHolderDuplicated;

    var mgsChallengeQuestion = typeof placeHolderChallengeQuestion == 'undefined' ? 'Please select question' : placeHolderChallengeQuestion;
    var mgsChallengeAnswer = typeof placeHolderChallengeAnswer == 'undefined' ? 'Please enter answer' : placeHolderChallengeAnswer;
	var mgsChallengeAnswerTooShort = typeof placeHolderChallengeAnswerTooShort == 'undefined' ? 'The answer is not long enough.' : placeHolderChallengeAnswerTooShort;
	var mgsChallengeAnswerTooLong = typeof placeHolderChallengeAnswerTooLong == 'undefined' ? 'The answer must be at most 100 characters in length.' : placeHolderChallengeAnswerTooLong;
    var mgsChallengeAnswerTooWeak = typeof placeHolderChallengeAnswerTooWeak == 'undefined' ? 'The answer must not be part of the question.' : placeHolderChallengeAnswerTooWeak;
    var mgsChallengeAnswerDuplicateQuestion = typeof placeHolderChallengeAnswerDuplicateQuestion == 'undefined' ? 'The answer must be different from the question.' : placeHolderChallengeAnswerDuplicateQuestion;

    //var mgsRequiredMaxLength = typeof placeHolderRequiredMaxLength == 'undefined' ? 'Please enter no more than 39 characters' : placeHolderRequiredMaxLength;

    Validation.VER = '1.0';
    Validation.AUTHOR = 'LocTran';
    Validation.DEFAULTS = {
        //txtPassword: {
        //    name: 'NewPassword',
        //    rule: {
        //        required: function (element) {
        //            return isFieldRequired(element);
        //        }
        //    },
        //    message: mgsPassword
        //},
        txtConfirmPassword: {
            name: 'ConfirmPassword',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsConfirmPassword
        },
        txtSignUpPassword: {
            name: 'txtSignUpPassword',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                },
                checkPassword: true
            },
            message: mgsPassword
        },
        ChallengeQuestion: {
            name: 'ChallengeQuestion',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        ForgotChallengeAnswer: {
            name: 'ForgotChallengeAnswer',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                },
            },
            message: mgsRequired
        },
        selectChallengeQuestion: {
            name: 'ChallengeQuestion',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        txtChallengeAnswer: {
            name: 'txtChallengeAnswer',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                },
                minlength: 4,
                maxlength: 100,
                checkChallengeAnswerSameQuestion: true,
                checkChallengeAnswerToWeak: true
            },
            message: {
                required: mgsRequired,
                minlength: mgsChallengeAnswerTooShort,
                maxlength: mgsChallengeAnswerTooLong,
                checkChallengeAnswerSameQuestion: mgsChallengeAnswerDuplicateQuestion,
                checkChallengeAnswerToWeak: mgsChallengeAnswerTooWeak
            }
        },
        GolfClub: {
            name: 'GolfClub',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        CourseType: {
            name: 'CourseType',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        dishWarning: {
            name: 'dishWarning',
            rule: {
                number: true,
                isrequired: true
            },
            message: mgsErrMinDish,
        },
        DateOfPlay: {
            name: 'DateOfPlay',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        AlternativeTeeTime: {
            name: 'AlternativeTeeTime',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        RestaurantName: {
            name: 'RestaurantName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        Location: {
            name: 'Location',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        Occasion: {
            name: 'Occasion',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        StartingLocation: {
            name: 'StartingLocation',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        RecipientName: {
            name: 'RecipientName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        DeliveryDate: {
            name: 'DeliveryDate',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        DeliveryAddress: {
            name: 'DeliveryAddress',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        NameOfEvent: {
            name: 'NameOfEvent',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        PerformaceId: {
            name: 'PerformaceId',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        Date: {
            name: 'Date',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        Time: {
            name: 'Time',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        CardNumber: {
            name: 'CardNumber',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        SecurityCode: {
            name: 'SecurityCode',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        ShippingAddressFirstName: {
            name: 'ShippingAddress.FirstName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        ShippingAddressLastName: {
            name: 'ShippingAddress.LastName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        ShippingAddressPhoneNumber: {
            name: 'ShippingAddress.PhoneNumber',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        ShippingAddressAddress: {
            name: 'ShippingAddress.Address',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        ShippingAddressCity: {
            name: 'ShippingAddress.City',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        ShippingAddressCountry: {
            name: 'ShippingAddress.Country',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        ShippingAddressState: {
            name: 'ShippingAddress.State',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        ShippingAddressZipCode: {
            name: 'ShippingAddress.ZipCode',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        ShippingMethod: {
            name: 'ShippingMethod',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },

        BillingAddressFirstName: {
            name: 'BillingAddress.FirstName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        BillingAddressLastName: {
            name: 'BillingAddress.LastName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        BillingAddressPhoneNumber: {
            name: 'BillingAddress.PhoneNumber',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        BillingAddressAddress: {
            name: 'BillingAddress.Address',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        BillingAddressCity: {
            name: 'BillingAddress.City',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        BillingAddressCountry: {
            name: 'BillingAddress.Country',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        BillingAddressState: {
            name: 'BillingAddress.State',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        BillingAddressZipCode: {
            name: 'BillingAddress.ZipCode',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        CheckInDate: {
            name: 'CheckInDate',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        CheckOutDate: {
            name: 'CheckOutDate',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        VehicleType: {
            name: 'VehicleType',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        PickupLocation: {
            name: 'PickupLocation',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        PickupTime: {
            name: 'PickupTime',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        AutoCompDropOffAirport: {
            name: 'AutoCompDropOffAirport',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        PaxFirstName: {
            name: 'PaxFirstName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        PaxLastName: {
            name: 'PaxLastName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        PaxPhoneNumber: {
            name: 'PaxPhoneNumber',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                },
                checkPhoneNumber: true
            },
            message: mgsMobileNum
        },
        PaxEmail: {
            name: 'PaxEmail',
            rule: {
                email: true,
                trueEMail: true
            },
            message: mgsEmail
        },
        PaymentFirstName: {
            name: 'PaymentFirstName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        PaymentLastName: {
            name: 'PaymentLastName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        DdlPaymentState: {
            name: 'DdlPaymentState',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        PaymentCity: {
            name: 'PaymentCity',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        ZipCode: {
            name: 'ZipCode',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        PaymentAddress: {
            name: 'PaymentAddress',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        FromLocation: {
            name: 'FromLocation',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        ToLocation: {
            name: 'ToLocation',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        PlayDate: {
            name: 'PlayDate',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        PreferredRegion: {
            name: 'PreferredRegion',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        PreferredTeeTime: {
            name: 'PreferredTeeTime',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        PickUpDate: {
            name: 'PickUpDate',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        ReturnDate: {
            name: 'ReturnDate',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        PickUpLocation: {
            name: 'PickUpLocation',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        DropOffLocation: {
            name: 'DropOffLocation',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        PreferredVehicleType: {
            name: 'PreferredVehicleType',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        DriverName: {
            name: 'DriverName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        DriverAge: {
            name: 'DriverAge',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        LimoService: {
            name: 'LimoService',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        PickUpTime: {
            name: 'PickUpTime',
            rule: {
                required: true
            },
            message: mgsRequired
        },
        DropOffTime: {
            name: 'DropOffTime',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        AutoCompPickupAirport: {
            name: 'AutoCompPickupAirport',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        AirlineHandle: {
            name: 'AirlineHandle',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        FlightHandle: {
            name: 'FlightHandle',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        FlightTerminal: {
            name: 'FlightTerminal',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        TourCode: {
            name: 'TourCode',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        TourAvailableDay: {
            name: 'AvailableDay',
            rule: {
                required: true
            },
            message: mgsRequired
        },
        TourAvailableMonthYear: {
            name: 'AvailableMonthYear',
            rule: {
                required: true
            },
            message: ''
        },
        AdultPassengersNumber: {
            name: 'Passengers[0].Number',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        PaymentCountry: {
            name: 'PaymentCountry',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        Travelers0FirstName: {
            name: 'Travelers[0].FirstName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        Travelers1FirstName: {
            name: 'Travelers[1].FirstName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        Travelers2FirstName: {
            name: 'Travelers[2].FirstName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        Travelers3FirstName: {
            name: 'Travelers[3].FirstName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        Travelers4FirstName: {
            name: 'Travelers[4].FirstName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        Travelers5FirstName: {
            name: 'Travelers[5].FirstName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        Travelers6FirstName: {
            name: 'Travelers[6].FirstName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        Travelers7FirstName: {
            name: 'Travelers[7].FirstName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        Travelers8FirstName: {
            name: 'Travelers[8].FirstName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },

        Travelers0LastName: {
            name: 'Travelers[0].LastName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        Travelers1LastName: {
            name: 'Travelers[1].LastName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        Travelers2LastName: {
            name: 'Travelers[2].LastName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        Travelers3LastName: {
            name: 'Travelers[3].LastName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        Travelers4LastName: {
            name: 'Travelers[4].LastName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        Travelers5LastName: {
            name: 'Travelers[5].LastName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        Travelers6LastName: {
            name: 'Travelers[6].LastName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        Travelers7LastName: {
            name: 'Travelers[7].LastName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        Travelers8LastName: {
            name: 'Travelers[8].LastName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },

        Password: {
            name: 'Password',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },

        BookingQuestions0Answer: {
            name: 'BookingQuestions[0].Answer',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        BookingQuestions1Answer: {
            name: 'BookingQuestions[1].Answer',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        BookingQuestions2Answer: {
            name: 'BookingQuestions[2].Answer',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        BookingQuestions3Answer: {
            name: 'BookingQuestions[3].Answer',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        BookingQuestions4Answer: {
            name: 'BookingQuestions[4].Answer',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        BookingQuestions5Answer: {
            name: 'BookingQuestions[5].Answer',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        BookingQuestions6Answer: {
            name: 'BookingQuestions[6].Answer',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        BookingQuestions7Answer: {
            name: 'BookingQuestions[7].Answer',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        BookingQuestions8Answer: {
            name: 'BookingQuestions[8].Answer',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        BookingQuestions9Answer: {
            name: 'BookingQuestions[9].Answer',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        // BinNumber: {
            // name: 'BinNumber',
            // rule: {
                // required: function (element) {
                    // return isFieldRequired(element);
                // }
            // },
            // message: mgsBinNumber
        // },
        StartDate: {
            name: 'StartDate',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },

        CuisineType: {
            name: 'CuisineType',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        ReservationDate: {
            name: 'ReservationDate',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsRequired
        },
        ReservationName: {
            name: 'ReservationName',
            rule: {
                required: true
            },
            message: mgsFirstName
        },
        MobileNumber: {
            name: 'MobileNumber',
            rule: {
                checkPhoneNumber: true
            },
            message: mgsMobileNum
        },
        EmailAddress: {
            name: 'EmailAddress',
            rule: {
                email: true,
                trueEMail: true
            },
            message: mgsEmail
        },

        numberOfKids: {
            name: 'NumberOfKids',
            rule: {
                number: true,
                min: 0,
                max: 999
            },
            message: mgsInvalid
        },
        numberOfAdults: {
            name: 'NumberOfAdults',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                },
                number: true,
                min: 1,
                max: 999
            },
            message: mgsInvalid
        },
        numberOfRooms: {
            name: 'NumberOfRooms',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                },
                number: true,
                min: 1,
                max: 999
            },
            message: mgsInvalid
        },
        numberOfPassengers: {
            name: 'NumberOfPassengers',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                },
                number: true,
                min: 1,
                max: 50
            },
            message: mgsInvalid
        },
        bouquetQuantity: {
            name: 'BouquetQuantity',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                },
                number: true,
                min: 1,
                max: 999
            },
            message: mgsInvalid
        },
        BouquetSize: {
            name: 'BouquetSize',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                },
                number: true,
                min: 1,
                max: 999
            },
            message: mgsInvalid
        },
        numberOfPlayers: {
            name: 'NumberOfPlayers',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                },
                number: true,
                min: 1,
                max: 999
            },
            message: mgsInvalid
        },
        numberOfInfants: {
            name: 'NumberOfInfants',
            rule: {
                number: true,
                min: 0,
                max: 999
            },
            message: mgsInvalid
        },
        numberOfCabins: {
            name: 'NumberOfCabins',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                },
                number: true,
                min: 1,
                max: 999
            },
            message: mgsInvalid
        },
        selectSalutation: {
            name: 'selectSalutation',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsSalutation
        },
        txtFirstName: {
            name: 'txtFirstName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                },
                maxlength: 25
            },
            message: mgsFirstName
        },
        txtLastName: {
            name: 'txtLastName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                },
                maxlength: 25
            },
            message: mgsLastName
        },
        txtBinNumber: {
            name: 'BinNumber',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                },
                maxlength: 20
            },
            message: mgsBinNumber
        },
        selectCreditName: {
            name: 'selectCreditName',
            rule: {
                maxlength: 40
            },
            message: mgsNameOfCard
        },
        txtEmail: {
            name: 'txtEmail',
            rule: {
                required: function (element) {
                    if ($(".getContactedEmail").prop("checked") == false) {
                        return false;
                    } else {
                        return true;
                    }
                },
                email: true,
                trueEMail: true
            },
            message: mgsEmail
        },
        txtPassengerEmail: {
            name: 'PaxEmail',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                },
                email: true,
                trueEMail: true
            },
            message: mgsEmail
        },
        txtAddress: {
            name: 'txtAddress',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: "Have you entered the address?"
        },
        stateName: {
            name: 'stateName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: "Have you entered the state?"
        },
        postalCode: {
            name: 'postalCode',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: "Have you entered the postal code?"
        },
        datepickerOfBirth: {
            name: 'datepickerOfBirth',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: "When were you born?"
        },
        txtContactNumber: {
            name: 'txtContactNumber',
            rule: {
                required: function (element) {
                    if ($(".getContactedMobile").prop("checked") == false) {
                        return false;
                    } else {
                        return true;
                    }
                },
                checkPhoneNumber: true
            },
            message: mgsMobileNum
        },
        txtOnlyNumber: {
            name: 'txtOnlyNumber',
            rule: {
                number: true,
            },
            message: mgsMobileNum
        },
        txtCreditCard: {
            name: 'txtCreditCard',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                },
                digits: true,
                minlength: 6,
                maxlength: 6
            },
            message: mgsValidCardNumber
        },
        txtExpiryDate: {
            name: 'txtExpiryDate',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsCardExpiryDate
        },
        txtExpiryMonth: {
            name: 'txtExpiryMonth',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
                // digits: true,
                // range: [1, 12],
                // checkMonth: true  
            },
            message: mgsCardExpiryDate
        },
        txtExpiryYear: {
            name: 'txtExpiryYear',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
                // digits: true,
                // maxlength: 2,
                // checkYear: true   
            },
            message: mgsCardExpiryDate
        },
        txtCurrentPassword: {
            name: 'txtCurrentPassword',
            rule: {
                required: function (element) {
                    if ($(".lt-check-password").val()) {
                        return true;
                    } else {
                        return false;
                    }
                }
            },
            message: mgsCurrentPassword
        },
        txtPassword: {
            name: 'txtPassword',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                },
                checkPassword: true
            },
            message: mgsPassword
        },
        txtConfirmPassword: {
            name: 'txtConfirmPassword',
            rule: {
                required: true,
                equalTo: "#txtPassword"
            },
            message: mgsConfirmPassword
        },
        cbxTerm1: {
            name: 'cbxTerm1',
            rule: {
                required: true
            },
            message: mgsRequiredCheck
        },
        cbxTerm2: {
            name: 'cbxTerm2',
            rule: {
                required: true
            },
            message: mgsRequiredCheck
        },
        categoryName: {
            name: 'categoryName',
            rule: {
                required: true
            }
        },
        driverAge: {
            name: 'driverAge',
            rule: {
                required: true,
                number: true,
                min: 18,
                max: 999
            },
            message: mgsInvalid
        },
        contactUsEmail: {
            name: 'contactUsEmail',
            rule: {
                required: true,
                email: true,
                trueEMail: true
            },
            message: mgsEmail
        },
        contactUsPhone: {
            name: 'contactUsPhone',
            rule: {
                checkPhoneNumber: true
            },
            message: "How can we reach you?"
        },
        reservationTour: {
            name: 'reservationTour',
            rule: {
                required: function (element) {
                    if ($('.getCruise').prop("checked") == false)
                        return true;
                    else
                        return false;
                }
            },
            message: {
                required: mgsRequired
            }
        },
        reservationCruise: {
            name: 'reservationCruise',
            rule: {
                required: function (element) {
                    if ($('.getTour').prop("checked") == false)
                        return true;
                    else
                        return false;
                }
            },
            message: {
                required: mgsRequired
            }
        },
        nameOfRestaurant: {
            name: 'nameOfRestaurant',
            rule: {
                required: function (element) {
                    if ($("[data-spy='cbxShowHide']").prop("checked") == false)
                        return true;
                    else
                        return false;
                }
            },
            message: {
                required: mgsRequired
            }
        },
        alterFirstValue: {
            name: 'alterFirstValue',
            rule: {
                required: function (element) {
                    if ($('.alterFirstValue').prop("checked") == true) {
                        return true;
                    }
                    else
                        return false;
                }
            },
            message: {
                required: mgsRequired
            }
        },
        alterSecondValue: {
            name: 'alterSecondValue',
            rule: {
                required: function (element) {
                    if ($('.alterSecondValue').prop("checked") == true) {
                        return true;
                    }
                    else
                        return false;
                }
            },
            message: {
                required: mgsRequired
            }
        },
        IsRecommendHotel: {
            name: 'HotelName',
            rule: {
                required: function (element) {
                    if ($('.alterFirstValue').prop("checked") == true) {
                        return true;
                    }
                    else
                        return false;
                }
            },
            message: {
                required: mgsRequired
            }
        },
        IsRecommendRegion: {
            name: 'Region',
            rule: {
                required: function (element) {
                    if ($('.alterSecondValue').prop("checked") == true) {
                        return true;
                    }
                    else
                        return false;
                }
            },
            message: {
                required: mgsRequired
            }
        },
        contactedEmail: {
            name: 'contactedEmail',
            rule: {
                required: function (element) {
                    if ($('.getContactedMobile').prop("checked") == false)
                        return true;
                    else
                        return false;
                }
            },
            message: {
                required: mgsRequired
            }
        },
        contactedMobile: {
            name: 'contactedMobile',
            rule: {
                required: function (element) {
                    if ($('.getContactedEmail').prop("checked") == false)
                        return true;
                    else
                        return false;
                }
            },
            message: {
                required: mgsRequired
            }
        },
        PrefResponseViaEmail: {
            name: 'PrefResponseViaEmail',
            rule: {
                required: function (element) {
                    if ($('.getContactedMobile').prop("checked") == false)
                        return true;
                    else
                        return false;
                }
            },
            message: {
                required: mgsRequired
            }
        },
        PrefResponseViaMobile: {
            name: 'PrefResponseViaMobile',
            rule: {
                required: function (element) {
                    if ($('.getContactedEmail').prop("checked") == false)
                        return true;
                    else
                        return false;
                }
            },
            message: {
                required: mgsRequired
            }
        },
        CardExpiredDate: {
            name: "CardExpiredDate",
            rule: {
                required: true
            },
            message: mgsCardExpiryDate
        },
        mobileNumberVal: {
            name: 'mobileNumberVal',
            rule: {
                checkPhoneNumber: true
            },
            message: mgsMobileNum
        },
        emailAddressVal: {
            name: 'emailAddressVal',
            rule: {
                email: true,
                trueEMail: true
            },
            message: mgsEmail
        },
        nameOfGolfCourse: {
            name: 'CourseName',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                }
            },
            message: mgsCourseName
        },
        accountNum: {
            name: 'accountNum',
            rule: {
                required: function (element) {
                    return isFieldRequired(element);
                },
                number: true,
                minlength: 4
            }
        },
        Term1: {
            name: 'Term1',
            rule: {
                required: true
            },
            message: mgsRequiredCheck
        },
        Term2: {
            name: 'Term2',
            rule: {
                required: true
            },
            message: mgsRequiredCheck
        },
        duplicatedAlternativeTime: {
            name: 'duplicatedAlternativeTime',
            rule: {
                // required: function (element) {
                    // if ($(".reservationTime").val() && $(".reservationTime").val() == $(".alternativeTime").val()) {
                        // return true;
                    // } else {
                        // return false;
                    // }
                // },
				required: true,
                duplicated: true
            },
			message: {
                required: mgsRequired,
				duplicated: msgDuplicated
            }
            //message: msgDuplicated
        },
        //txtdiningOthers: {
        //    name: 'OtherDiningPreference',
        //    rule: {
        //        onlyText: false,
        //        maxlength: 39
        //    },
        //    message: mgsRequiredMaxLength
        //},
        //txtrtransferMess: {
        //    name: 'OtherRentalPreference',
        //    rule: {
        //        onlyText: false,
        //        maxlength: 39
        //    },
        //    message: mgsRequiredMaxLength
        //},
        //txtfavouriteGolf: {
        //    name: 'favouriteGolf',
        //    rule: {
        //        onlyText: false,
        //        maxlength: 39
        //    },
        //    message: mgsRequiredMaxLength
        //},
    };


    Validation.prototype.init = function (ele) {
        // var _errorClass = "error has-error-icon";
        var _errorClass = "error lt-error";
        // if ($(ele).hasClass('signUpForm') || $(ele).hasClass('fr-profile') || $(ele).hasClass('apr-form')) {
        //     _errorClass = "error lt-error";
        // }
        //validation
        this.addNewMethod();
        var ruleMess = this.getRuleMess();
        var validation = {
            rules: ruleMess[0],
            messages: ruleMess[1],
            errorClass: _errorClass,
            errorPlacement: function (error, element) {
                // error.insertAfter(element);
                if (element.is(":checkbox")) {
                    $(ele).hasClass('signUpForm') ? error.appendTo(element.parent().next()) : error.appendTo(element.parent());
                } else if (element.is(":radio")) {
                    error.appendTo(element.parent().parent().parent());
                } else if (element.hasClass('txt-expiry-date')) {
                    error.appendTo(element.parent().parent());
                } else {
                    error.appendTo(element.parent());
                }
            }
        };

        var validator = $(ele).validate(validation);
        //real time check validaton
        this.callBack(ele);

        //call birthday picker
        if ($('.datepickerOfBirth').length > 0) {
            this.callPickBirthday($('.datepickerOfBirth'));
        };
        //call book date picker
        if ($('.reservationDate').length > 0) {
            this.callPickDate($('.reservationDate'));
        };
        //check Card Expiry Date
        if ($('.txt-expiry-date').length > 0) {
            $element
                .on('keyup', '.txt-expiry-year', function () {
                    var id = $('.txt-expiry-month').attr('id');
                    $element.validate().element('#' + id);
                })
                .on('keyup', '.txt-expiry-month', function () {
                    var id = $('.txt-expiry-year').attr('id');
                    $element.validate().element('#' + id);
                });
        };

        //call aleart password error
        if ($('.box-list-spec').length > 0) {
            $('.box-list-spec').logcheckpass('.lt-check-password');
        }
    }

    Validation.prototype.addNewMethod = function () {
        var getDate = new Date(),
            currentDate = new Date();
        jQuery.validator.addMethod("isrequired", function (value, element) {
            var sum = 0;
            $('.MainCourse').each(function () {
                sum += Number($(this).val());
            })
            if (Number($('#MaxDishNumber').val()) > 0 && sum == 0) {
                return false
            }
            else {
                return true
            }
        }, mgsErrMinDish);
        jQuery.validator.addMethod("duplicated", function (value, element) {
            if ($(".reservationTime").val() == $(".alternativeTime").val()
                && ($(".reservationTime").val() && $(".alternativeTime").val())) {
                return false
            }
            return true
        }, 'Input Error.');
        jQuery.validator.addMethod("trueEMail", function (value, element) {
            return this.optional(element) || (/^([a-z0-9]|.+)([-._][a-z0-9]+|.)*@([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,4}$/i.test(value) && /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test(value));
        }, 'Input Error.');
        jQuery.validator.addMethod("onlyText", function (value, element) {
            return this.optional(element) || (/^(?:[a-zA-Z\s]|[\u4E00-\u9FFF]|[\u0E00-\u0E7F]|[\u00c0-\u1ef9])*$/i.test(value));
        }, 'Input Error.');
        jQuery.validator.addMethod("checkPhoneNumber", function (value, element) {
            return this.optional(element) || (/^([+]{0,1})([0-9\s|*]{5,20})$/.test(value));
        }, 'Input Error.');
        jQuery.validator.addMethod("checkPassword", function (value, element) {
             var regExp = /^(?=.*[0-9])(?=.*[!@#\$%\^\&*])(?=.*[a-z])(?=.*[A-Z])([a-zA-Z0-9!@#\$%\^\&*\)\(\s\[\]\{\}\?\/\\+=.,;`~:'"<>|_-]{10,})$/;
            $firstName = ($('#txtFirstName').val() || " ").toLowerCase();
            $lastName = ($('#txtLastName').val() || " ").toLowerCase();
            $password = ($('#txtPassword').val() || " ").toLowerCase();
            $email = ($('#txtEmail').val() || " ").toLowerCase();
            $email = $email.split("@")[0];
            return this.optional(element) || (($password.indexOf($email) == -1) && ($password.indexOf($firstName) == -1) && ($password.indexOf($lastName) == -1)) && regExp.test(value)
        }, 'Input Error.');
        jQuery.validator.addMethod("checkMonth", function (value, element) {
            var compare = (currentDate.setMonth(value - 1) >= getDate);
            currentDate.setDate(getDate.getDate());
            return this.optional(element) || compare;
        }, 'Input Error.');
        jQuery.validator.addMethod("checkYear", function (value, element) {
            var inputYear = parseInt('20'.concat(value));
            return this.optional(element) || (currentDate.setYear(inputYear) >= getDate);
        }, 'Input Error.');
        jQuery.validator.addMethod("checkExpiredate", function (value, element) {
            var inputMonth = parseInt(value.split('/')[0]);
            var inputYear = parseInt('20'.concat(value.split('/')[1]));
            return this.optional(element) || (currentDate.setFullYear(inputYear, inputMonth - 1) >= getDate);
        }, 'Input Error.');
		jQuery.validator.addMethod("checkChallengeAnswerSameQuestion", function (value, element) {
            var challengeQuestion = ($('#ChallengeQuestion').val().replace(/\s/g, '').toLowerCase() || '');
            var answer = ($('#txtChallengeAnswer').val() || '').replace(/\s/g, '').toLowerCase();
            if (answer === challengeQuestion) {
                return false;
            }
            return true;
        }, 'Input Error.');
        jQuery.validator.addMethod("checkChallengeAnswerToWeak", function (value, element) {
            var challengeQuestionArray = ($('#ChallengeQuestion').val().replace(/\?/g, '').toLowerCase() || '').split(' ');
            var answerArray = ($('#txtChallengeAnswer').val() || '').toLowerCase().split(' ');
            var answerLength = answerArray.length;
            for (var i = 0; i < answerLength; i++) {
                if ($.inArray(answerArray[i], challengeQuestionArray) >= 0) {
                    return false;
                }
            }
            return true;
        }, 'Input Error.');
    }

    Validation.prototype.callBack = function (element) {
        $(element).on('focus change', 'select, input, textarea', function () {
            // Trim value of fields before validating
            if ($(this).val() != null && typeof ($(this).val()) !== "undefined") {
                if (typeof ($(this).val()) === 'string') {
                    var strTrim = $(this).val().trim();
                    $(this).val(strTrim);
                }
            }

            // If element is not select tour and cruises checkbox, It will validate immediately
            if (!$(this).hasClass("getTour") && !$(this).hasClass("getCruise")) {
                var self = $(this).attr("id");
                if (self) {
                    var valid = $(element).validate().element('#' + self);
                }
            }

            //validate email field and mobile number field
            $(this).on("focusout change", function () {
                if ($(this).hasClass("mobileNumber")) {
                    $(element).validate().element('#mobileNumberVal');
                }
                else if ($(this).hasClass("emailAddress")) {
                    $(element).validate().element('#emailAddressVal');
                }
            });
        });
    }

    Validation.prototype.callPickBirthday = function (element) {
        var $self = $(element);
        var dateToday = new Date();
        var yrRange = dateToday.getFullYear() - 100 + ":" + (dateToday.getFullYear());
        $(element).datepicker({
            showOtherMonths: true,
            selectOtherMonths: false,
            dateFormat: "M d, yy",
            setDate: new Date(),
            changeYear: true,
            changeMonth: true,
            yearRange: yrRange,
            beforeShow: function (input, inst) {
                $self.trigger('blur');
                $('#ui-datepicker-div').addClass("profile-datepicker");
                if ($(document).width() <= 992) {
                    $('.lt-overlay').addClass('active');
                }
                if ($(window).width() <= 767 && $('.btn-nav').is(':visible')) {
                    setTimeout(function () {
                        $('#ui-datepicker-div').css('top', $('.txtEmail').offset().top + 'px');
                    }, 1);
                }
            },
            onSelect: function (date) {
                //exp: $('.sign-up').validate().element($('.birthday'));
                $element.validate().element(element);
                if ($(document).width() <= 992) {
                    $('.lt-overlay').removeClass('active');
                }
                $(".datepickerOfBirth").closest(".box-item").find(".error.lt-error").css("display", "none");
            },
            onClose: function (selectedDate) {
                // Display value of birthday following new rule
                // var realValue = selectedDate;
                // $self.siblings("input").val(realValue);
                // var length = realValue.length - 3;
                // var stars = "";
                // for (var i = 0; i < length; i++) {
                //     stars += "*";
                // }
                // var patt = new RegExp(".{" + length + "}$", "g");
                // var fakeValue = realValue.replace(patt, stars);
                // $self.val(fakeValue);
            }
        });
        $('.lt-overlay').on('click touchstart', function () {
            $(element).datepicker("hide");
            $(this).removeClass('active');
        });
    }

    Validation.prototype.callPickDate = function (element) {
        var $self = $(element);
        var offset = Number($('#valueTimeZone').val() || 0);
        var scrollAmount = $(document).scrollTop();
        var date = new Date();
        var nextDate = $('.datepicker-NextDate').val();
        var bookGolf = $('.datepicker-bookGolf').val();
        var shortDateFormat = {
            dayNamesShort: $(element).datepicker('option', 'dayNamesShort'),
            dayNames: $(element).datepicker('option', 'dayNames'),
            monthNamesShort: $(element).datepicker('option', 'monthNamesShort'),
            monthNames: $(element).datepicker('option', 'monthNames'),
            dateFormat: $(element).datepicker('option', 'dateFormat')
        }
        $(element).datepicker({
            regional: getLanguageCookie(),
            showOtherMonths: true,
            selectOtherMonths: false,
            dateFormat: "DD, MM d, yy",
            setDate: new Date(),
            changeMonth: true,
            changeYear: true,
            minDate: getMinDate(offset, bookGolf, nextDate),
            altField: "#reservationDateSecond",
            beforeShow: function (input, inst) {
                $self.trigger('blur');
                $('#ui-datepicker-div').addClass("booking-datepicker");
                if ($('.ui-datepicker').closest("body").find(".page-my-reminders").length > 0) {
                    $(this).closest("body").find('#ui-datepicker-div').addClass("bigger-ui-datepicker");
                }
                if ($(document).width() <= 992) {
                    $('.lt-overlay').addClass('active');
                    stopBodyScrolling(true);
                }
                if ($(window).width() <= 767 && $('.btn-nav').is(':visible')) {
                    setTimeout(function () {
                        $('#ui-datepicker-div').css('top', $('.reservationDate').offset().top + 'px');
                    }, 1);
                }
                return { minDate: getMinDate(offset, bookGolf, nextDate) };
            },
            onSelect: function (selectedDate) {
                var getDateValue = $(this).datepicker("getDate");
                $element.validate().element($(this));
                $(".infoDate").closest("p").show();
                $('.infoDate').text($.datepicker.formatDate('D, M d yy', getDateValue, shortDateFormat));
                $(".infoPickupDate").closest("p").show();
                $('.infoPickupDate').text($.datepicker.formatDate('D, M d yy', getDateValue, shortDateFormat));
                if ($(document).width() <= 992) {
                    $('.lt-overlay').removeClass('active');
                    stopBodyScrolling(false);
                }
                if (bookGolf != undefined) {
                    var getDateValueTimeZone = new Date(new Date(new Date().getTime() + offset * 3600 * 1000).toUTCString().replace(/ GMT$/, ""));
                    if (offset == 0 ){
                        getDateValueTimeZone = new Date();
                    }
                    if (getDateValueTimeZone.getDate() == getDateValue.getDate()
                        && getDateValueTimeZone.getMonth() == getDateValue.getMonth()
                        && getDateValueTimeZone.getFullYear() == getDateValue.getFullYear()) {
                        //set getDateValueTimeZone to test Time Zone 
                        // getDateValueTimeZone.setHours(getDateValueTimeZone.getHours() - 2)
                        var currentHours = getDateValueTimeZone.getHours() * 2;
                        var curentMinutes = Math.floor(getDateValueTimeZone.getMinutes() / 30);
                        if (getDateValueTimeZone.getHours() < 8) {
                            $(".reservationTimeGroup").find('li').each(function (index, item) {
                                if (index <= (currentHours + curentMinutes + 15)) {
                                    $(item).prop('hidden', 'true')
                                }
                            });
                            $(".alternativeTimeGroup").find('li').each(function (index, item) {
                                if (index <= (currentHours + curentMinutes + 15)) {
                                    $(item).prop('hidden', 'true');
                                }
                            });
                        }
                        else {
                            $(".reservationTimeGroup").find('li').each(function (index, item) {
                                $(item).prop('hidden', 'true')
                            });
                            $(".alternativeTimeGroup").find('li').each(function (index, item) {
                                $(item).prop('hidden', 'true');
                            })
                            ResetDateTimeInput();
                        }
                    }
                    if (getDateValueTimeZone.getDate() > getDateValue.getDate()
                        && getDateValueTimeZone.getMonth() == getDateValue.getMonth()
                        && getDateValueTimeZone.getFullYear() == getDateValue.getFullYear()) {
                        $(".reservationTimeGroup").find('li').each(function (index, item) {
                            $(item).prop('hidden', 'true')
                        });
                        $(".alternativeTimeGroup").find('li').each(function (index, item) {
                            $(item).prop('hidden', 'true');
                        })
                        ResetDateTimeInput();
                    }
                    if (getDateValueTimeZone.getDate() < getDateValue.getDate()) {
                        $(".reservationTimeGroup").find('li').each(function (index, item) {
                            $(item).removeAttr('hidden')
                        });
                        $(".alternativeTimeGroup").find('li').each(function (index, item) {
                            $(item).removeAttr('hidden');
                        })
                        if (getDateValueTimeZone.getHours() >= 17 && getDateValue.getDate() - getDateValueTimeZone.getDate() == 1) {
                            currentHours = ((getDateValueTimeZone.getHours() - 12) - 5) * 2;
                            curentMinutes = Math.floor(getDateValueTimeZone.getMinutes() / 30) + 1;
                            $(".reservationTimeGroup").find('li').each(function (index, item) {
                                if (index <= (currentHours + curentMinutes)) {
                                    $(item).prop('hidden', 'true')
                                }
                            });
                            $(".alternativeTimeGroup").find('li').each(function (index, item) {
                                if (index <= (currentHours + curentMinutes)) {
                                    $(item).prop('hidden', 'true');
                                }
                            });
                            ResetDateTimeInput();
                        }
                    }
                }
            },
            onClose: function (selectedDate) {
                if (element.length == 2) {
                    if (selectedDate != "") {
                        var getDateValue = $(this).datepicker("getDate");
                        if ($(this).hasClass('check-in-date')) {
                            $(".check-out-date").closest("p").show();
                            $(".check-in-time").closest("p").show();
                            $('.check-out-date').datepicker("option", "minDate", selectedDate);
                            $('.check-in-time').text($.datepicker.formatDate('D, M d yy', getDateValue, shortDateFormat));
                        }
                        if ($(this).hasClass('check-out-date')) {
                            $(".check-in-date").closest("p").show();
                            $(".check-out-time").closest("p").show();
                            $('.check-in-date').datepicker("option", "maxDate", selectedDate);
                            $('.check-out-time').text($.datepicker.formatDate('D, M d yy', getDateValue, shortDateFormat));
                        }
                    }
                }
            }
        });
        $('.lt-overlay').on('click touchstart', function () {
            $(element).datepicker("hide");
            $(this).removeClass('active');
            stopBodyScrolling(false);
        });
    }
	function ResetDateTimeInput() {
        //$(".reservationTimeGroup").find('li').each(function (index, item) {
        //    $(item).prop('hidden', 'true')
        //});
        //$(".alternativeTimeGroup").find('li').each(function (index, item) {
        //    $(item).prop('hidden', 'true');
        //})
        // for each select field on the page
        $("select").each(function (e, index) {
            if (index.id == 'reservationTime' || index.id == 'alternativeTime') {
                $(this).val($("#" + $(this).attr("id") + " option:first").val());
            }
            $(".infoReservation").parent().hide();
            $(".infoAlternative").parent().hide();
            $("select").each(function (index) {
                var emptyOption = $(this).find('option[value=""]');
                if (typeof (emptyOption) !== 'undefined' && emptyOption.length > 0) {
                    $(this).val("");
                } else {
                    var placeHolder = $(this).attr("placeHolder");
                    if (typeof (placeHolder) === 'undefined' || placeHolder == null) {
                        placeHolder = "Please Select";
                    }
                    $(this).prepend("<option value='' selected='selected'>" + placeHolder + "</option>");
                    $(this).multiselect('rebuild');
                }
                $(this).multiselect('refresh');
            });
        });
    }

    function stopBodyScrolling(bool) { // finding how to scroll center when open datepicker, checking on jquery-ui.js
        if (bool === true) {
            $('body').addClass('unscrollDatePicker');
            // if devices are iPhone|iPad|iPod

            if (/iPhone|iPad|iPod|/i.test(navigator.userAgent)) {
                var scrollAmount = $(document).scrollTop();
                $('body').css({ position: 'fixed', bottom: scrollAmount + 'px', left: '0', right: '0', top: '0', height: '100vh !important' });
                //$('body').css({ position: 'absolute', bottom: 0, left:'0',right:'0' });
                $('body').attr("data-scroll", scrollAmount)
            }

        } else {
            $('body').removeClass('unscrollDatePicker');
            // if devices are iPhone|iPad|iPod
            var scrollAmount = $('body').attr("data-scroll");
            $('body').css({ position: '', bottom: '', left: '', right: '', top: '', height: '' });
            $(document).scrollTop(scrollAmount);

        }
    }

    function getLanguageCookie() {
        var value = "; " + document.cookie;
        var parts = value.split("; " + "LanguageCookies" + "=");
        if (parts == undefined)
            return "en";
        if (parts.length == 2)
            return parts.pop().split(";").shift();
    }
    function getMinDate(offset, bookGolf, nextDate) {
        var setNextDate = new Date(new Date(new Date().getTime() + (offset + 24) * 3600 * 1000).toUTCString().replace(/ GMT$/, ""));
        var date = new Date(new Date(new Date().getTime() + offset * 3600 * 1000).toUTCString().replace(/ GMT$/, ""));
        if (offset == 0) {
            date = new Date();
            setNextDate = +1;
        }
        if (date.getHours() >= 8 && bookGolf != undefined || nextDate != undefined) {
            return setNextDate;
        }
        return date;
    }
    function Plugin(option) {
        return this.each(function () {
            var $this = $(this);
            var options = typeof option == 'object' && option;
            var data = new Validation(this, options);
        })
    }
    $.fn.validation = Plugin;
}(jQuery);

//function check password
+function ($) {
    var LogCheckPass = function (element, target) {
        this.$element = $(element);
        this.$target = $(target);
        this.$target.on('keyup', $.proxy(this.init, this));
        $('#txtEmail').on('keyup', $.proxy(this.init, this));
        $('#txtFirstName').on('keyup', $.proxy(this.init, this));
        $('#txtLastName').on('keyup', $.proxy(this.init, this));
        $("#signup-check-password").on('click', $.proxy(this.setTypeInput, this));
        // this.regExSpecChar = /[\)\(\s\[\]\{\}\?\/\\+=.,;`~:'"<>|_-]/;
        this.regExSpecChar = /[!@#\$%\^\&*]/;
        this.regExNum = /[0-9]/;
        this.regExUpper = /[A-Z]/;
        this.regExLower = /[a-z]/;
        //liem buu

        this.setTypeInput();
        this.init();
    }

    LogCheckPass.prototype.init = function () {
        this.checkChar();
        this.checkNum();
        this.checkSpecChar();
        this.checkUpperChar();
        this.checkLowerChar();
        this.checkCompareUserNameChar();
        this.checkCompareNameChar();
        if (this.checkChar() && this.checkNum() && this.checkSpecChar() && this.checkUpperChar() && this.checkLowerChar() && this.checkCompareUserNameChar() && this.checkCompareNameChar()) {
            this.$element.find(".list_spec").hide();
            this.$element.find(".alert_spec").show();
        } else {
            this.$element.find(".alert_spec").hide();
            this.$element.find(".list_spec").show();
        }
    }
    LogCheckPass.prototype.setTypeInput = function () {
        //this.$target.val("");
        if ($("#signup-check-password").is(':checked')) {
            this.$target.attr('type', 'text');
			$('#txtCurrentPassword').attr('type', 'text');
        } else {
            this.$target.attr('type', 'password');
			$('#txtCurrentPassword').attr('type', 'password');
        }

    }
    LogCheckPass.prototype.checkChar = function () {
        if (this.$target.val().length >= 10) {
            this.$element.find(".char").addClass('complete');
            return true;
        }
        else {
            this.$element.find(".char").removeClass('complete');
            return false;
        };
    }
    LogCheckPass.prototype.checkSpecChar = function () {
        if (this.regExSpecChar.test(this.$target.val())) {
            this.$element.find(".specChar").addClass('complete');
            return true;
        }
        else {
            this.$element.find(".specChar").removeClass('complete');
            return false;
        };

    }
    LogCheckPass.prototype.checkNum = function () {
        if (this.regExNum.test(this.$target.val())) {
            this.$element.find(".number").addClass('complete');
            return true;
        }
        else {
            this.$element.find(".number").removeClass('complete');
            return false;
        };

    }
    LogCheckPass.prototype.checkUpperChar = function () {
        if (this.regExUpper.test(this.$target.val())) {
            this.$element.find(".upperCase").addClass('complete');
            return true;
        }
        else {
            this.$element.find(".upperCase").removeClass('complete');
            return false;
        };
    }
    LogCheckPass.prototype.checkLowerChar = function () {
        if (this.regExLower.test(this.$target.val())) {
            this.$element.find(".lowerCase").addClass('complete');
            return true;
        }
        else {
            this.$element.find(".lowerCase").removeClass('complete');
            return false;
        };
    }
    LogCheckPass.prototype.checkCompareUserNameChar = function () {
        $target = (this.$target.val() || "").toLowerCase();
        $email = ($('#txtEmail').val() || "_").toLowerCase().split("@")[0];
        if ($target.indexOf($email) == -1 && $target != "") {
            this.$element.find(".partChar").addClass('complete');
            return true;
        }
        else {
            this.$element.find(".partChar").removeClass('complete');
            return false;
        };
    }

    LogCheckPass.prototype.checkCompareNameChar = function () {
        $firstName = ($('#txtFirstName').val() || "_").toLowerCase();
        $lastName = ($('#txtLastName').val() || "_").toLowerCase();
        $target = this.$target.val().toLowerCase();
        if ($target.indexOf($firstName) == -1 && $target.indexOf($lastName) == -1 && $target != "") {
            this.$element.find(".partNameChar").addClass('complete');
            return true;
        }
        else {
            this.$element.find(".partNameChar").removeClass('complete');
            return false;
        };
    }


    function Plugin(target) {
        return this.each(function () {
            var data = new LogCheckPass(this, target);
        });
    }
    $.fn.logcheckpass = Plugin;
}(jQuery);
