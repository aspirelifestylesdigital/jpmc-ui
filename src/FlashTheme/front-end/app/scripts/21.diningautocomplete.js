$(document).ready(function () {
    AutocompleteDining();
});
function AutocompleteDining() {
    var listLocation = [
{
    "value": "Canada",
    "data": "null,null,Canada"
},
{
    "value": "France",
    "data": "null,null,France"
},
{
    "value": "Italy",
    "data": "null,null,Italy"
},
{
    "value": "United Kingdom",
    "data": "null,null,United Kingdom"
},
{
    "value": "United States",
    "data": "null,null,United States"
},
{
    "value": "London, United Kingdom",
    "data": "London,null,United Kingdom"
},
{
    "value": "Paris, France",
    "data": "Paris,null,France"
},
{
    "value": "Rome, Italy",
    "data": "Rome,null,Italy"
},
{
    "value": "BC, Canada",
    "data": "null,BC,Canada"
},
{
    "value": "CA, United States",
    "data": "null,CA,United States"
},
{
    "value": "DC, United States",
    "data": "null,DC,United States"
},
{
    "value": "FL, United States",
    "data": "null,FL,United States"
},
{
    "value": "GA, United States",
    "data": "null,GA,United States"
},
{
    "value": "HI, United States",
    "data": "null,HI,United States"
},
{
    "value": "IL, United States",
    "data": "null,IL,United States"
},
{
    "value": "LA, United States",
    "data": "null,LA,United States"
},
{
    "value": "MA, United States",
    "data": "null,MA,United States"
},
{
    "value": "MB, Canada",
    "data": "null,MB,Canada"
},
{
    "value": "MD, United States",
    "data": "null,MD,United States"
},
{
    "value": "NH, United States",
    "data": "null,NH,United States"
},
{
    "value": "NJ, United States",
    "data": "null,NJ,United States"
},
{
    "value": "NV, United States",
    "data": "null,NV,United States"
},
{
    "value": "NY, United States",
    "data": "null,NY,United States"
},
{
    "value": "OH, United States",
    "data": "null,OH,United States"
},
{
    "value": "ON, Canada",
    "data": "null,ON,Canada"
},
{
    "value": "PA, United States",
    "data": "null,PA,United States"
},
{
    "value": "PE, Canada",
    "data": "null,PE,Canada"
},
{
    "value": "QC, Canada",
    "data": "null,QC,Canada"
},
{
    "value": "TX, United States",
    "data": "null,TX,United States"
},
{
    "value": "VT, United States",
    "data": "null,VT,United States"
},
{
    "value": "WA, United States",
    "data": "null,WA,United States"
},
{
    "value": "WI, United States",
    "data": "null,WI,United States"
},
{
    "value": "Akron, OH",
    "data": "Akron,OH,United States"
},
{
    "value": "Alpharetta, GA",
    "data": "Alpharetta,GA,United States"
},
{
    "value": "Atlanta, GA",
    "data": "Atlanta,GA,United States"
},
{
    "value": "Aventura, FL",
    "data": "Aventura,FL,United States"
},
{
    "value": "Baltimore, MD",
    "data": "Baltimore,MD,United States"
},
{
    "value": "Bedminster, NJ",
    "data": "Bedminster,NJ,United States"
},
{
    "value": "Boston, MA",
    "data": "Boston,MA,United States"
},
{
    "value": "Burlington, MA",
    "data": "Burlington,MA,United States"
},
{
    "value": "Cambridge, MA",
    "data": "Cambridge,MA,United States"
},
{
    "value": "Chicago, IL",
    "data": "Chicago,IL,United States"
},
{
    "value": "Cleveland, OH",
    "data": "Cleveland,OH,United States"
},
{
    "value": "Columbus, GA",
    "data": "Columbus,GA,United States"
},
{
    "value": "Coral Gables, FL",
    "data": "Coral Gables,FL,United States"
},
{
    "value": "Dallas, TX",
    "data": "Dallas,TX,United States"
},
{
    "value": "Decatur, GA",
    "data": "Decatur,GA,United States"
},
{
    "value": "Fort Worth, TX",
    "data": "Fort Worth,TX,United States"
},
{
    "value": "Genesee Depot, WI",
    "data": "Genesee Depot,WI,United States"
},
{
    "value": "Healdsburg, CA",
    "data": "Healdsburg,CA,United States"
},
{
    "value": "Honolulu, HI",
    "data": "Honolulu,HI,United States"
},
{
    "value": "j, MA",
    "data": "j,MA,United States"
},
{
    "value": "Kailua-Kona, HI",
    "data": "Kailua-Kona,HI,United States"
},
{
    "value": "Kamuela, HI",
    "data": "Kamuela,HI,United States"
},
{
    "value": "Kihei, HI",
    "data": "Kihei,HI,United States"
},
{
    "value": "Kohala Coast, HI",
    "data": "Kohala Coast,HI,United States"
},
{
    "value": "Koloa, HI",
    "data": "Koloa,HI,United States"
},
{
    "value": "Laguna Beach, CA",
    "data": "Laguna Beach,CA,United States"
},
{
    "value": "Lahaina, HI",
    "data": "Lahaina,HI,United States"
},
{
    "value": "Lake Bluff, IL",
    "data": "Lake Bluff,IL,United States"
},
{
    "value": "Las Vegas, NV",
    "data": "Las Vegas,NV,United States"
},
{
    "value": "Los Angeles, CA",
    "data": "Los Angeles,CA,United States"
},
{
    "value": "Makawao, HI",
    "data": "Makawao,HI,United States"
},
{
    "value": "Manhattan Beach, CA",
    "data": "Manhattan Beach,CA,United States"
},
{
    "value": "Marina del Rey, CA",
    "data": "Marina del Rey,CA,United States"
},
{
    "value": "Miami Beach, FL",
    "data": "Miami Beach,FL,United States"
},
{
    "value": "Miami, FL",
    "data": "Miami,FL,United States"
},
{
    "value": "Milwaukee, WI",
    "data": "Milwaukee,WI,United States"
},
{
    "value": "Montreal, ON",
    "data": "Montreal,ON,Canada"
},
{
    "value": "Montreal, PE",
    "data": "Montreal,PE,Canada"
},
{
    "value": "Montreal, QC",
    "data": "Montreal,QC,Canada"
},
{
    "value": "Napa, CA",
    "data": "Napa,CA,United States"
},
{
    "value": "New Orleans, LA",
    "data": "New Orleans,LA,United States"
},
{
    "value": "New York, NY",
    "data": "New York,NY,United States"
},
{
    "value": "Newport Beach, CA",
    "data": "Newport Beach,CA,United States"
},
{
    "value": "North Shore, HI",
    "data": "North Shore,HI,United States"
},
{
    "value": "Oakland, CA",
    "data": "Oakland,CA,United States"
},
{
    "value": "ON H2Z 2B2, QC",
    "data": "ON H2Z 2B2,QC,Canada"
},
{
    "value": "Orlando, FL",
    "data": "Orlando,FL,United States"
},
{
    "value": "Pacific Palisades, CA",
    "data": "Pacific Palisades,CA,United States"
},
{
    "value": "Paia, HI",
    "data": "Paia,HI,United States"
},
{
    "value": "Philadelphia, PA",
    "data": "Philadelphia,PA,United States"
},
{
    "value": "Plano, TX",
    "data": "Plano,TX,United States"
},
{
    "value": "Playa Del Rey, CA",
    "data": "Playa Del Rey,CA,United States"
},
{
    "value": "Portsmouth, NH",
    "data": "Portsmouth,NH,United States"
},
{
    "value": "Quebec, QC",
    "data": "Quebec,QC,Canada"
},
{
    "value": "Rutherford, CA",
    "data": "Rutherford,CA,United States"
},
{
    "value": "San Francisco, CA",
    "data": "San Francisco,CA,United States"
},
{
    "value": "Santa Monica, CA",
    "data": "Santa Monica,CA,United States"
},
{
    "value": "Seattle, VT",
    "data": "Seattle,VT,United States"
},
{
    "value": "Seattle, WA",
    "data": "Seattle,WA,United States"
},
{
    "value": "Smyrna, GA",
    "data": "Smyrna,GA,United States"
},
{
    "value": "Somerville, CA",
    "data": "Somerville,CA,United States"
},
{
    "value": "Somerville, MA",
    "data": "Somerville,MA,United States"
},
{
    "value": "Sonoma, CA",
    "data": "Sonoma,CA,United States"
},
{
    "value": "St Helena, CA",
    "data": "St Helena,CA,United States"
},
{
    "value": "St. Helena, CA",
    "data": "St. Helena,CA,United States"
},
{
    "value": "Strongsville, OH",
    "data": "Strongsville,OH,United States"
},
{
    "value": "Tofino, BC",
    "data": "Tofino,BC,Canada"
},
{
    "value": "Toronto, ON",
    "data": "Toronto,ON,Canada"
},
{
    "value": "Vancouver, BC",
    "data": "Vancouver,BC,Canada"
},
{
    "value": "Vancouver, MB",
    "data": "Vancouver,MB,Canada"
},
{
    "value": "Waikoloa, HI",
    "data": "Waikoloa,HI,United States"
},
{
    "value": "Wailea, HI",
    "data": "Wailea,HI,United States"
},
{
    "value": "Washington, DC",
    "data": "Washington,DC,United States"
},
{
    "value": "West Hollywood, CA",
    "data": "West Hollywood,CA,United States"
},
{
    "value": "West Toronto, ON",
    "data": "West Toronto,ON,Canada"
},
{
    "value": "West Vancouver, BC",
    "data": "West Vancouver,BC,Canada"
},
{
    "value": "Winter Garden, FL",
    "data": "Winter Garden,FL,United States"
},
{
    "value": "Winter Park, FL",
    "data": "Winter Park,FL,United States"
},
{
    "value": "Woodinville, WA",
    "data": "Woodinville,WA,United States"
},
{
    "value": "Yountville, CA",
    "data": "Yountville,CA,United States"
}
    ]
    if ($("[data-spy='autoComplete'].ia_dining").length > 0) {
        $("[data-spy='autoComplete'].ia_dining").each(function () {
            var self = $(this);
            self.autocomplete({
                lookup: listLocation,
                autoSelectFirst: true,
                minChars: 3,
                appendTo: self.parent(),
                onSelect: function (suggestion) {
                    $("#filter_location").text(suggestion.data);
                    var value = suggestion.value;
                    if (self.hasClass("nameOfRestaurant")) {
                        $(".infoRestaurant").text(value);
                        $(".infoRestaurant").closest("p").show();
                    } else if (self.hasClass("nameOfLocation")) {
                        $(".infoAddressRestaurant").text(value);
                        $(".infoAddressRestaurant").closest("p").show();
                    } else if (self.hasClass("getPickupLocation")) {
                        $(".infoPickupLocation").text(value);
                        $(".infoPickupLocation").closest("p").show();
                    } else if (self.hasClass("getDropoffLocation")) {
                        $(".infoDropoffLocation").text(value);
                        $(".infoDropoffLocation").closest("p").show();
                    }
                },
                onSearchComplete: function (query, suggestions) {
                    $(this).siblings(".autocomplete-suggestions").addClass('scrollbar-outer');
                    $(this).siblings(".autocomplete-suggestions").scrollbar();
                }
            });
        });
    }
}

