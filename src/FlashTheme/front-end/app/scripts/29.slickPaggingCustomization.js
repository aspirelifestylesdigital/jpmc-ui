//"use strict";
//var _slickContentClassNameConst = '.multiple-items-responsive_';
////function reInitSlickWhenAjaxLoad(moduleId) {
////	//init slick when load page
////	var $tileSlickCarousel = $(_slickContentClassNameConst + item_Id);
////	if ($tileSlickCarousel.length > 0) {
////		if ($tileSlickCarousel.find('.slick-slide').length == 0) {
////			var $inputSlickCarousel = $('#' + item_Id + ' input.tiles_module_pagination_carousel');
////			if ($inputSlickCarousel.length > 0 && $inputSlickCarousel.val() === _carouselConst) {
////				var itemDisplay = parseInt($inputSlickCarousel.attr('data-items-on-page'));
////				var itemsTotal = parseInt($inputSlickCarousel.attr('data-total-items'));
////				var rowsDisplay = 1;
////				if (itemDisplay > 4) {
////					rowsDisplay = calcRowsDisplayFollowItemsDisplay(itemDisplay, itemsTotal);
////					itemDisplay = 4;
////				}
////				initSlick(itemDisplay, itemDisplay, rowsDisplay, item_Id);
////				//trigger event
////				handleEventSlick(item_Id, 1, rowsDisplay, $inputSlickCarousel);
////			}
////		}
////	}
////}
//function initSlick(slidesToShowDefault, slidesToScrolllDefault, rowsDisplay, moduleItemId) {
//	$(_slickContentClassNameConst + moduleItemId).slick({
//		dots: true,
//		infinite: false,
//		speed: 300,
//		slidesPerRow: slidesToShowDefault,
//		rows: rowsDisplay,
//		responsive: [
//            {
//            	breakpoint: 980, // tablet breakpoint
//            	settings: {
//            		//slidesToShow: 3,
//            		//slidesToScroll: 3
//            		slidesPerRow: 3,
//            		rows: 1,
//            	}
//            },
//            {
//            	breakpoint: 480, // mobile breakpoint
//            	settings: {
//            		//slidesToShow: 1,
//            		//slidesToScroll: 1
//            		slidesPerRow: 2,
//            		rows: 1,
//            	}
//            }
//		]
//	});
//	//hide dot indicators
//	hideCarouselPaging();
//	//set status for navigation
//	setArrowNavigationStats(true, moduleItemId, rowsDisplay, 1, 1);
//}

//function handleEventSlick(moduleId, currentPage, rowDisplay, $inputSlickModule) {
//	$(_slickContentClassNameConst + moduleId).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
//		var itemTotals = parseInt($inputSlickModule.attr('data-total-items'));
//		var pageNumber = parseInt($(_slickContentClassNameConst + moduleId + ' .slick-dots li.slick-active button').text());

//		var totalCurrentItem = pageNumber * rowDisplay * 4;

//		var direction = '';
//		//var currentPage = 1;
//		if (currentSlide === 0 && nextSlide === slick.$slides.length - 1 && totalCurrentItem > itemTotals) {
//			// its going from the first slide to the last slide (backwards)
//			direction = 'prev';
//			currentPage = 1;
//			pageNumber = 1;
//		} else if ((nextSlide > currentSlide || (currentSlide === (slick.$slides.length - 1) && nextSlide === 0)) && totalCurrentItem < itemTotals) {
//			// its either going normally forwards or going from the last slide to the first
//			direction = 'next';
//			pageNumber += 1;
//		} else {
//			direction = 'prev';
//			pageNumber -= 1;
//		}
//		currentPage = pageNumber;

//		var queryString = $inputSlickModule.attr('data-query-string');

//		//update curent item
//		var totalItemLoaded = $(_slickContentClassNameConst + moduleId).find('.view-type-tilesview').length;


//		if (direction === 'next' && totalItemLoaded < itemTotals) {
//			var urlGet = _urlGetPageModuleContentConst + queryString;
//			var params = buildParamsWhenPagging(moduleId, pageNumber + 1);
//			ajaxGetDataForCarousel(urlGet, params, function (error, responseData) {
//				if (error) {
//					return;
//				}
//				if (responseData.data != "") {
//					var slideToAdd = '';
//					//if (rowDisplay === 1) {
//						slideToAdd = $(responseData.data).find(".data-container").eq(1).html();
//					//} else {
//						slideToAdd = rebuildSlideInCaseMoreRowDisplay(rowDisplay, $(responseData.data).find(".data-container").eq(1));

//					//}

//					//add slide to slick
//					if (slideToAdd !== '') {
//						$('#' + moduleId + ' .multiple-items-responsive').slick('slickAdd', slideToAdd, null, false);
//						setArrowNavigationStats(false, moduleId, rowDisplay, nextSlide, currentPage);
//					}
//					//Hide dot indicators
//					hideCarouselPaging();
//				}
//			});
//		}

//		setArrowNavigationStats(false, moduleId, rowDisplay, nextSlide, currentPage);
//	});
//}

//function setArrowNavigationStats(isInit, moduleItemId, noOfRowDisplay, nextSlide, currentPage) {
//	var $inputSlick = $('input.tiles_module_pagination_carousel[data-item-id="' + moduleItemId + '"]');
//	if ($inputSlick.length > 0) {
//		var totalItem = parseInt($inputSlick.attr('data-total-items'));
//		var itemDisplay = parseInt($inputSlick.attr('data-items-on-page'));

//		var rowCalculate = 0;

//		if (itemDisplay % 2 == 0) {
//			rowCalculate = Math.floor((itemDisplay / 4));
//		}
//		else {
//			rowCalculate = Math.floor((itemDisplay / 4)) + 1;
//		}

//		var finalDisplay = 4 * rowCalculate;
//		if (isInit) {
//			//alway hide previous arrow when init
//			$(_slickContentClassNameConst + moduleItemId + ' button.slick-prev').hide();
//			if (itemDisplay >= totalItem) {
//				$(_slickContentClassNameConst + moduleItemId + ' button.slick-next').hide();
//			}

//		} else {
//			if (nextSlide === 0) {
//				//go to the 1st slide
//				$(_slickContentClassNameConst + moduleItemId + ' button.slick-prev').hide();
//				$(_slickContentClassNameConst + moduleItemId + ' button.slick-next').show();
//			} else if (((nextSlide * noOfRowDisplay) + itemDisplay >= totalItem) || ((currentPage * finalDisplay) >= totalItem)) {
//				//go to the last slide
//				$(_slickContentClassNameConst + moduleItemId + ' button.slick-next').hide();
//				$(_slickContentClassNameConst + moduleItemId + ' button.slick-prev').show();
//			} else {
//				$(_slickContentClassNameConst + moduleItemId + ' button.slick-prev').show();
//				$(_slickContentClassNameConst + moduleItemId + ' button.slick-next').show();

//			}
//		}
//	}
//}

//function rebuildSlideInCaseMoreRowDisplay(rowsDisplay, data) {
//	var result = '';
//	var dataReturnCount = data.children().length;
//	//note data return some case does not = displayItem
//	//set missing item is empty
//	var col1Start = '<div class="slick-slide"><div>';
//	var col1End = '</div></div>';

//	for (var i = 0; i < dataReturnCount; i++) {
//		$(data.children()[i]).css({
//			'display': 'inline-block'
//		});
//		col1Start += data.children()[i].outerHTML;
//	}
//	col1Start += col1End;
//	result = col1Start;

//	return result;
//}

//function calcRowsDisplayFollowItemsDisplay(itemsDisplay, totalItems) {
//	var rowDisplay = 1;
//	var itemBasedToCalc = itemsDisplay <= totalItems ? itemsDisplay : totalItems;
//	if (parseInt(itemBasedToCalc) % 4 === 0) {
//		rowDisplay = parseInt(itemBasedToCalc / 4);
//	} else {
//		rowDisplay = parseInt(itemBasedToCalc / 4) + 1;
//	}

//	return rowDisplay;
//}

//function isModuleSlickCarousel(moduleId) {
//	return $(_slickContentClassNameConst + moduleId).length !== 0;
//}

//function addAttributeForSlickTile(slickModule, rowDisplay, itemsDisplay, moduleId) {
//	var attrItemsDisplay = slickModule.attr('data-itemsDisplay');
//	if (typeof attrItemsDisplay === typeof undefined || attrItemsDisplay === false) {
//		slickModule.attr('data-itemsDisplay', itemsDisplay);
//	}
//	var attrRowDisplay = slickModule.attr('data-rowDisplay');
//	if (typeof attrRowDisplay === typeof undefined || attrRowDisplay === false) {
//		slickModule.attr('data-rowDisplay', rowDisplay);
//	}
//	//var attrModuleCarouselId = slickModule.attr('data-moduleCarouselId');
//	//if (typeof attrModuleCarouselId === typeof undefined || attrModuleCarouselId === false) {
//	//    slickModule.attr('data-moduleCarouselId', 'tiles_module_pagination_carousel_' + moduleId);
//	//}
//}

//var SlickPaggingCustomization = function () {
//	return {
//		init: function ($modulePaginationCarousel, _carouselConst, _slickContentClassNameConst, _urlGetPageModuleContentConst) {
//			//renderModalVerifyCreditCard(ccNumberLabel, ccNumberPlaceHolder, ccNumberSubmitButton);
//			if ($modulePaginationCarousel.length > 0) {
//				$.each($modulePaginationCarousel, function (idx, module) {
//					var $slickModule = $(module);
//					if ($slickModule.val() === _carouselConst) {
//						var itemsDisplay = parseInt($slickModule.attr('data-items-on-page'));
//						var itemsTotal = parseInt($slickModule.attr('data-total-items'));
//						//var itemTotals = parseInt(slickModule.attr('data-total-items'));
//						var currentPage = 1;
//						var rowDisplay = 1;
//						if (itemsDisplay > 4) {
//							rowDisplay = calcRowsDisplayFollowItemsDisplay(itemsDisplay, itemsTotal);
//							itemsDisplay = 4;
//						}
//						//add attribute to store var
//						var itemId = $slickModule.attr('data-item-id');
//						//addAttributeForSlickTile(slickModule, rowDisplay, itemsDisplay, itemId);
//						//init slick
//						initSlick(itemsDisplay, itemsDisplay, rowDisplay, itemId);
//						//handle event
//						handleEventSlick(itemId, currentPage, rowDisplay, $slickModule);
//					}
//				});
//			}
//		},

//		reInitSlickWhenAjaxLoad: function (_slickContentClassNameConst, item_Id) {
//			//init slick when load page
//			var $tileSlickCarousel = $(_slickContentClassNameConst + item_Id);
//			if ($tileSlickCarousel.length > 0) {
//				if ($tileSlickCarousel.find('.slick-slide').length == 0) {
//					var $inputSlickCarousel = $('#' + item_Id + ' input.tiles_module_pagination_carousel');
//					if ($inputSlickCarousel.length > 0 && $inputSlickCarousel.val() === _carouselConst) {
//						var itemDisplay = parseInt($inputSlickCarousel.attr('data-items-on-page'));
//						var itemsTotal = parseInt($inputSlickCarousel.attr('data-total-items'));
//						var rowsDisplay = 1;
//						if (itemDisplay > 4) {
//							rowsDisplay = calcRowsDisplayFollowItemsDisplay(itemDisplay, itemsTotal);
//							itemDisplay = 4;
//						}
//						initSlick(itemDisplay, itemDisplay, rowsDisplay, item_Id);
//						//trigger event
//						handleEventSlick(item_Id, 1, rowsDisplay, $inputSlickCarousel);
//					}
//				}
//			}
//		},

//		reInitSlickWhenDestinationLoad: function (itemId) {

//		}
//	}
//}();