	//Function to import js script
	function require(script) {
             $.ajax({
                 url: script,
                 dataType: "script",
                 async: false,
                 success: function () {
                     // all good...
                 },
                 error: function () {
                     throw new Error("Could not load script " + script);
                 }
             });
         };
    require("https://maps.googleapis.com/maps/api/js?key=AIzaSyDQxAEtNnEzPoharGD9IDvrWSsha_70zEI&amp;libraries=places&amp;language=en");
    //****************************Create Google Maps***************************************************************//
		 var markers = [];
         var data = [];
         //Implement on icon click (NOT on popup)
         $(".map-click").on("click", function () {
             var self = this;
             var data_id = $(self).attr("data-id");
             //Remove active classes on other branches
             $(".google-maps-popup >.info-maps >.scrollbar-outer").find(".item").removeClass("active");
             //$.each($(".google-maps-popup >.info-maps >.scrollbar-outer").find(".item"), function () {
                // var self1 = this;
                // var latLong = $(self1).find("#latitude").val() +"-"+ $(self1).find("#longtitude").val()+"-"+$(self1).find("#restauranttitle").val();
                // if (latLong === data_id) {
                     //Set active class to selected branch
                //     $(self1).addClass("active");
                // }
             //});
             var latitude = data_id.split("-")[0];
             var longtitude = data_id.split("-")[1];
			 var itemid = data_id.split("-")[2];
             var hotel_name_branch = $(self).attr("data-name");
             $("#current_lattitude").val(latitude);
             $("#current_longtitude").val(longtitude);
			 $("#current_itemid").val(itemid);
             $("#current_branch_name").val(hotel_name_branch);
             //Set Modola Title
             $(".modal-top >.modal-title").html(hotel_name_branch);
         });
         //Call function to create map when modal showed (CANNOT call this function when $(".map-click") because of issue GOOGLE MAP & MODAL)
         $('#modalMaps').on('shown.bs.modal', function () {
             //if (markers.length == 0) { //Map not created yet
                 $("#map_canvas").empty();
                 CreateMap();
             //}
             var selected_lat = $("#current_lattitude").val();
             var selected_long = $("#current_longtitude").val();
			 var selected_id= $("#current_itemid").val();
             var indexes = $.map(data, function (obj, index) {
                 if (obj.Latitude === selected_lat && obj.Longitude === selected_long && obj.itemid === selected_id) {
                     return index;
                 }
             });
             google.maps.event.trigger(markers[indexes[0]], 'click');
         });
		 //Implement on branch name click (on popup)
         $(".map-sub-click").click(function () {
             var self = this;
             //Remove active classes on other branches
             $(self).parents(".scrollbar-outer").find(".item").removeClass("active");
             //Set active class to selected branch
             $(self).find(".item").addClass("active");
             //Set data to build map
             var latitude = $(self).find("#latitude").val();
             var longtitude = $(self).find("#longtitude").val();
             var itemid = $(self).find("#itemid").val();
             var hotel_name_branch = $(self).attr("data-name");
             //Set Modola Title
             $(".modal-top >.modal-title").html(hotel_name_branch);
             //if (markers.length == 0) { //Map not created yet
                 $("#map_canvas").empty();
                 CreateMap();
             //}
             var indexes = $.map(data, function (obj, index) {
                 if (obj.Latitude === latitude && obj.Longitude === longtitude && obj.itemid === itemid) {
                     return index;
                 }
             });
             google.maps.event.trigger(markers[indexes[0]], 'click');
         });
		 //Function to get list branches
         function getData() {
			 data = [];
             $.each($(".google-maps-popup >.info-maps >.scrollbar-outer").find(".item"), function () {
                 var self = this;
                 var item = new Object();
                 item.branch_name = $(self).parents(".map-sub-click").attr("data-name");
                 item.Latitude = $(self).find("#latitude").val();
                 item.Longitude = $(self).find("#longtitude").val();
				 item.itemid =  $(self).find("#itemid").val();
                 data.push(item);
             });
         }
		 //Function to create map
         function CreateMap() {
                //Get data
                getData();
                //Building the map
                var myOptions = {
                    scrollwheel: true,
					zoom: 16,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
                var infoWindow = new google.maps.InfoWindow();
                var closeClickListenerFn = markerCloseClickFunction(i, latLng);
                google.maps.event.addListener(infoWindow, 'closeclick', closeClickListenerFn);
                google.maps.event.addListener(infoWindow, 'domready', function() {
                   // Reference to the DIV which receives the contents of the infowindow using jQuery
                   var iwOuter = $('.gm-style-iw');

                   /* The DIV we want to change is above and below the .gm-style-iw DIV. */
                   iwOuter.prev().css('display', 'none');
                   iwOuter.next().css('display', 'none');
                });
                markers = [];
				for (var i = 0; i < data.length; ++i) {
                    var latLng = new google.maps.LatLng(data[i].Latitude, data[i].Longitude);
                    var marker = new google.maps.Marker({
                        position: latLng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        //icon: "/Images/icon-google-map.png",
                        map: map
                    });

                    var clickListenerFn = markerClickFunction(i, latLng);
                    google.maps.event.addListener(marker, 'click', clickListenerFn);
                    markers.push(marker);
                }
             // Auto Zoom to fit all markers
                /*var latlngbounds = new google.maps.LatLngBounds();
                if (data.length === 1) {
                    latlngbounds.extend(new google.maps.LatLng(data[0].Latitude, data[0].Longitude));
                    map.fitBounds(latlngbounds);
                    var listener = google.maps.event.addListener(map, "idle", function () {
                        if (map.getZoom() > 14) {
                            map.setZoom(14);
                        }
                        google.maps.event.removeListener(listener);
                    });
                }
                else {
                    for (var j = 0; j < data.length; j++) {
                        latlngbounds.extend(new google.maps.LatLng(data[j].Latitude, data[j].Longitude));
                    }
                    map.fitBounds(latlngbounds);

                }*/
                function markerClickFunction(i, latlng) {
                    return function () {
                        map.panTo(latlng);
						//Set active to branch name
						//Remove active classes on other branches
						var data_id = data[i].Latitude + "-" + data[i].Longitude + "-" + data[i].itemid;
						 $(".google-maps-popup >.info-maps >.scrollbar-outer").find(".item").removeClass("active");
						 $.each($(".google-maps-popup >.info-maps >.scrollbar-outer").find(".item"), function () {
							 var self1 = this;
							 var latLong = $(self1).find("#latitude").val() + "-" + $(self1).find("#longtitude").val() + "-" + $(self1).find("#itemid").val();
							 if (latLong === data_id) {
								 //Set active class to selected branch
								 $(self1).addClass("active");
							 }
						 });
                        //Change Icon to all branches
                        for (var j = 0; j < markers.length; j++) {
                            markers[j].setIcon("/aspire_theme/images/icon-map-small.png");
                        }
                        //Set active icon
                        markers[i].setIcon("/aspire_theme/images/icon-map-large.png");
                        var content = '<p>' + data[i].branch_name + '</p>';
                        infoWindow.setContent(content);
                        infoWindow.setPosition(latlng);
                        infoWindow.open(map);
                    };
                }
                function markerCloseClickFunction(i, latlng) {
                    return function () {
                    };
                }
         }