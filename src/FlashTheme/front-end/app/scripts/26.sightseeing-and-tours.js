$(document).ready(function () {
    DCRFilterAutoComplete();
    
})



function DCRFilterAutoComplete() {
    //auto compelete for dining, event, tour dcr
    $('.locationFilter').each(function (i, el) {
        var auto = $(el);
        var apiItemType;
        if (auto.hasClass("kb_dining")) {
            apiItemType = "dining";
        }
        else if (auto.hasClass("kb_event")) {
            apiItemType = "event";
        }
        else {
            apiItemType = "sightseeing_activity";
        }
        var form = $(this).closest('form');
        var searchBox = null;
        if (isNotNull(form)) {
            searchBox = form.find('input[name=filter_location]');
        }
        auto.autocomplete({
            serviceUrl: "/sm/DCRApi/LocationAutoComplete",
            params: { limit: 10, apiItemType: apiItemType },
            minChars: 3,
            appendTo: auto.parent(),
            autoSelectFirst: true,
            showNoSuggestionNotice: true,
            onSelect: function (suggestion) {
                if (isNotNull(searchBox)) {
                    searchBox.attr('data', suggestion.data);
                }
            },
            onSearchComplete: function (query, suggestions) {
                $(this).siblings(".autocomplete-suggestions").addClass('scrollbar-outer');
                $(this).siblings(".autocomplete-suggestions").scrollbar();
            },
            onValueChange: function () {
                var newLocation = $(this).val();
                if (isNotNull(searchBox)) {
                    searchBox.attr('data', newLocation);
                }
            }
        });
    });
}
