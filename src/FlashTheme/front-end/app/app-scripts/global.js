'use strict';
jQuery.extend(jQuery.validator.messages, {
    required: 'Vui lòng điền thông tin.',
    remote: 'Please fix this field.',
    email: 'Vui lòng nhập chính xác email.',
    url: 'Please enter a valid URL.',
    date: 'Please enter a valid date.',
    dateISO: 'Please enter a valid date (ISO).',
    number: 'Vui lòng chỉ nhập số (0->9)',
    digits: 'Please enter only digits.',
    creditcard: 'Please enter a valid credit card number.',
    equalTo: 'Mật khẩu mới phải giống nhau',
    accept: 'Please enter a value with a valid extension.',
    maxlength: jQuery.validator.format(
        'Please enter no more than {0} characters.'
    ),
    minlength: jQuery.validator.format('Vui lòng nhập ít nhất {0} ký tự.'),
    rangelength: jQuery.validator.format(
        'Please enter a value between {0} and {1} characters long.'
    ),
    range: jQuery.validator.format('Please enter a value between {0} and {1}.'),
    max: jQuery.validator.format(
        'Please enter a value less than or equal to {0}.'
    ),
    min: jQuery.validator.format(
        'Please enter a value greater than or equal to {0}.'
    )
});
var isDevice = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
        navigator.userAgent
    ),
    isAndroid = /Android/i.test(navigator.userAgent),
    isIos = /iPhone|iPad|iPod/i.test(navigator.userAgent),
    isIE11 = !!(
        navigator.userAgent.match(/Trident/) &&
        navigator.userAgent.match(/rv[ :]11/)
    ),
    isMobile = $(window).width() < 768;
const global = {
    detectDevices: function() {
        var a = isDevice === true ? ' device' : ' pc',
            b = isAndroid === true ? ' android' : ' not-android',
            c = isIos === true ? ' ios' : ' not-ios',
            d = isMobile ? ' mobile' : ' desktop',
            e = isIE11 ? ' ie11' : ' ',
            htmlClass = a + b + c + d + e;
        $('html').addClass(htmlClass);
        if (isDevice) $('body').css('cursor', 'pointer');
    },
    overflowBody: function() {
        $('html, body').css({
            height: '100%',
            overflow: 'hidden'
        });
    },
    clearOverflowBody: function() {
        $('html, body').css({
            height: 'auto',
            overflow: 'visible'
        });
    },
    scrollTo: function(elem) {
        var offset = 0;
        if ($(elem).length > 0) {
            offset = $(elem).offset().top;
        }
        $('html, body').animate(
            {
                scrollTop: offset
            },
            500
        );
    },
    init: function() {},
    load: function() {},
    resize: function() {}
};
