module.exports = {
	options: {
		//banner: '/*! Updated on <%= grunt.template.today("mm/dd/yyyy") %> */\n',
		mangle: false
	},
	dist: {
		files: {
			'js/google.map.helper.js': [
				'scripts/google.map.helper.js'
			],
			'js/validation.js': [
				'scripts/hardcode/validation.js'
			],
			'js/app.js': [
				'scripts/hardcode/app.js'
			],
			'js/pages/booking.js': [
				'scripts/hardcode/booking.js'
			],
			'js/pages/culinary.js': [
				'scripts/hardcode/culinary.js'
			],
			'js/pages/gourment.js': [
				'scripts/hardcode/gourment.js'
			],
			'js/pages/index.js': [
				'scripts/hardcode/index.js'
			],
			'js/pages/indexloggedin.js': [
				'scripts/hardcode/indexloggedin.js'
			],
			'js/pages/profile.js': [
				'scripts/hardcode/profile.js'
			],
			'js/pages/thankyou.js': [
				'scripts/hardcode/thankyou.js'
			],
			'js/pages/faqs.js': [
				'scripts/hardcode/faqs.js'
			],
			'js/pages/feedback.js': [
				'scripts/hardcode/feedback.js'
			],
			'js/pages/customerservice.js': [
				'scripts/hardcode/customerservice.js'
			],
			'js/pages/terms.js': [
				'scripts/hardcode/terms.js'
			],
			'js/pages/privacy.js': [
				'scripts/hardcode/privacy.js'
			],
			'js/pages/searchresults.js': [
				'scripts/hardcode/searchresults.js'
			],
			'js/pages/experiences.js': [
				'scripts/hardcode/experiences.js'
			],
			'js/pages/experience_package.js': [
				'scripts/hardcode/experience_package_individual.js'
			],
			'js/pages/signup.js': [
				'scripts/hardcode/signup.js'
			],
			'js/pages/travel.js': [
				'scripts/hardcode/travel.js'
			],
			'js/pages/travel-packages.js': [
				'scripts/hardcode/travel-packages.js'
			],
			'js/pages/city-guide.js': [
				'scripts/hardcode/city-guide.js'
			],
			'js/pages/travel-packages-indiv.js': [
				'scripts/hardcode/travel-packages-indiv.js'
			],
			'js/pages/hotel-subcat.js': [
				'scripts/hardcode/hotel-subcat.js'
			],
			'js/pages/hotel-without-new-pagination.js': [
				'scripts/hardcode/hotel-without-new-pagination.js'
			],
			'js/pages/city-guide-indiv.js': [
				'scripts/hardcode/city-guide-indiv.js'
			],
			'js/pages/lifestyle.js': [
				'scripts/hardcode/lifestyle.js'
			],
			'js/pages/golf.js': [
				'scripts/hardcode/golf.js'
			],
			'js/pages/nightlife.js': [
				'scripts/hardcode/nightlife.js'
			],
			'js/pages/entertainment.js': [
				'scripts/hardcode/entertainment.js'
			],
			'js/pages/lifestyle-indiv.js': [
				'scripts/hardcode/lifestyle-indiv.js'
			],
			'js/pages/ui.js': [
				'scripts/hardcode/ui.js'
			],
			'js/pages/listCityCountryFormat.js': [
				'scripts/hardcode/listCityCountryFormat.js'
			],
			'js/pages/listCityStateCountryFormat.js': [
				'scripts/hardcode/listCityStateCountryFormat.js'
			],
			'js/pages/listCountryFormat.js': [
				'scripts/hardcode/listCountryFormat.js'
			],
			'js/pages/listStateCountryFormat.js': [
				'scripts/hardcode/listStateCountryFormat.js'
			],
			'js/pages/listContactNumber.js': [
				'scripts/hardcode/listContactNumber.js'
			],
			'js/libs/jquery-1.12.1.min.js': [
				'scripts/libs/jquery-1.12.1.min.js'
			],
			'js/libs/jquery-ui.min.js': [
				'scripts/libs/jquery-ui.js'
			],
			'js/libs/jquery.dataTables.min.js': [
				'scripts/libs/jquery.dataTables.js'
			],
			'js/libs/bootstrap.min.js': [
				'scripts/libs/bootstrap.min.js'
			],
			'js/libs/jquery.validate.min.js': [
				'scripts/libs/jquery.validate.js'
			],
			'js/libs/slick.min.js': [
				'scripts/libs/slick.js'
			],
			'js/libs/jquery.scrollbar.js': [
				'scripts/libs/jquery.scrollbar.js'
			],
			'js/libs/lightgallery-all.js': [
				'scripts/libs/lightgallery-all.js'
			],
			'js/libs/bootstrap-multiselect.js': [
				'scripts/libs/bootstrap-multiselect.js'
			],
			'js/libs/pqselect.js': [
				'scripts/libs/pqselect.js'
			],
			'js/libs/jquery.dotdotdot.min.js': [
				'scripts/libs/jquery.dotdotdot.js'
			],
			'js/libs/sol.js': [
				'scripts/libs/sol.js'
			],
			'js/libs/jquery.autocomplete.min.js': [
				'scripts/libs/jquery.autocomplete.js'
			],
			'js/libs/seatingCharts/seatingCharts-excanvas.min.js': [
				'scripts/libs/seatingCharts/seatingCharts-excanvas.js'
			],
			'js/libs/seatingCharts/seatingCharts-hammer.min.js': [
				'scripts/libs/seatingCharts/seatingCharts-hammer.js'
			],
			'js/libs/seatingCharts/seatingCharts-Overlay.min.js': [
				'scripts/libs/seatingCharts/seatingCharts-Overlay.js'
			],
			'js/libs/seatingCharts/seatingCharts-tuMap.min.js': [
				'scripts/libs/seatingCharts/seatingCharts-tuMap.js'
			],
			'js/libs/seatingCharts/seatingCharts-tuMap.min.js': [
				'scripts/libs/seatingCharts/seatingCharts-tuMap.js'
			],
			'js/libs/seatingCharts/SeatMaps.js': [
				'scripts/libs/seatingCharts/SeatMaps.js'
			],
			'js/pages/travel-packages-categories.js': [
				'scripts/libs/travel-packages-categories.js'
			],
			'js/zopim/zopimchat.js': [
				'scripts/zopim/zopimchat.js'
			],
			'js/libs/jquery.ui.touch-punch.min.js': [
				'scripts/libs/jquery.ui.touch-punch.js'
			],
			'js/libs/jquery.simplePagination.js': [
				'scripts/libs/jquery.simplePagination.js'
			],
			'js/libs/select2.full.min.js': [
				'scripts/libs/select2.full.min.js'
			],
			'js/libs/smartcrop.js': [
				'scripts/libs/smartcrop.js'
			]
		}
	}
};