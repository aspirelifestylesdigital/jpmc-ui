module.exports = {
  development: {
    options: {
      outputSourceFiles: true
    },
    files: {
      'css/style.vy.css': 'less/style.less',
      // 'css/jquery-ui.css': 'less/jquery-ui/_jquery-ui.css',
      // 'css/jquery.dataTables.css': 'less/jquery-ui/_jquery.dataTables.css',
      // 'css/seatingCharts-style.css': 'less/seatingCharts/_seatingCharts-style.css',
      // 'css/seatingCharts-tuMap.css': 'less/seatingCharts/_seatingCharts-tuMap.css'
    }
  },
  production: {
    options: {
      outputSourceFiles: true,
      separator: '\n',
      compress: true
    },
    files: {
      // 'css/style.min.css': 'less/style.less',
      // 'css/jquery-ui.min.css': 'less/jquery-ui/_jquery-ui.css',
      // 'css/jquery.dataTables.min.css': 'less/jquery-ui/_jquery.dataTables.css',
      // 'css/seatingCharts-style.css': 'less/seatingCharts/_seatingCharts-style.css',
      // 'css/seatingCharts-tuMap.css': 'less/seatingCharts/_seatingCharts-tuMap.css'
    }
  }
};
