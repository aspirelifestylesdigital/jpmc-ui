module.exports = {
	files: ['Gruntfile.js', 'js/hardcode/*.js'],
	options: {
		globals: {
			jQuery: true,
			console: true,
			modules: true
		}
	}
};